$(document).ready(function(){
	$("#divinfoads").hide();
	$("#cepAd").mask("99999-999");
	
	
	$("#infoads").click(function(){
		if($("#infoads").is(":checked")){
			$("#divinfoads").show('slow');
		}else{
			$("#divinfoads").hide('slow');
			$(".infoads").val(null);
		}
	});
	
	$("#tpprocesso").change(function(){
		
		if ($(this).val() != "0") {
			var tpproc = $(this).val(); 
			$.ajax({
				url : "/gdocprocessos/processo/listaregras.json",
				data : {
					id : tpproc
				},
				type : 'get',
				dataType : 'json',
				success : function(regras) {
					$('#regras').empty();
					for ( var i = 0; i < regras.length; i++) {
						$('#regras').append('<td></td><td>'+ regras[i].nmRegra + '</td>');
					}
				}
			});
		}
	});
	
	$(".number").bind("keyup blur focus", function(e) {
	 	   e.preventDefault();
	        var expre = /[A-Za-z\.\�\�\@\`\?\^\~\'\"\!\?\#\$\%\?\�\_\+\=\.\,\-\:\;\<\>\|\�\�\�\]\[\{\}\\ \)\(\*\&\/\\]/g;
	 
	        // REMOVE OS CARACTERES DA EXPRESSAO ACIMA
	        if ($(this).val().match(expre))
	            $(this).val($(this).val().replace(expre,''));
	    });
	
});