$(document).ready(function(){
	$("#vlDoc").priceFormat({
		prefix : "",
		centsSeparator : ",",
		thousandsSeparator : "",
		centsLimit : 2
	});
	
	$("#linhavlr").hide();
	$(".resumo").hide();
	
	$("#usoatividade").change(function(){
		var iduso = $("#usoatividade").val();
		if(iduso != 0){
			$.ajax({
				url : "/gdocprocessos/guia/ufir.json",
				data : {
					id : iduso
					},
					type : 'get',
					dataType : 'json',
					success : function(resposta) {
						$("#ufir").html("<strong>"+resposta[0]+" / "+resposta[1].replace(".", ",")+"<strong>");
						$("#vlDoc").val(resposta[2].replace(".", ","));
						$("#linhavlr").show('slow');
					}
			});
		}
	});
	
	$("#numparcelas").blur(function(){
		var vldoc = $("#vlDoc");
		var numparcelas = $("#numparcelas");
		if(numparcelas.val() == ""){
			numparcelas.val(1);
		}
		$("#vltotal").html("R$ "+vldoc.val());
		
		$.ajax({
			url : "/gdocprocessos/guia/calculoparcela.json",
			data : {
				parcelas : numparcelas.val(),
				valor : vldoc.val()
				},
				type : 'get',
				dataType : 'json',
				success : function(resposta) {
					$("#parc").html(numparcelas.val()+" x de R$ "+resposta.replace(".",","));
				}
		});
		$(".resumo").show('slow');
	});
});