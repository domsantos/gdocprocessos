$(document).ready(function(){
	$("#datainicial").mask("99/99/9999");
	$("#datafinal").mask("99/99/9999");
});


function gera(url){
	if($("#datainicial").val() == ""){
		$("#erro").html("<div class='alert'><p>Informe a Data Inicial</p></div>");
		return false;
	}

	if($("#datafinal").val() == ""){
		$("#erro").html("<div class='alert'><p>Informe a Data Final</p></div>");
		return false;
	}
	var parametros = "?datainicial="+$("#datainicial").val()+"&datafinal="+$("#datafinal").val();
	var pagina = url+parametros;
 	window.open(pagina,'Window','width=200,height=200');
}
