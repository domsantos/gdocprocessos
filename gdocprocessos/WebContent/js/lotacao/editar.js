$(document).ready(function(){
	$("#entidades").change( function() {
		var entiSelec = $(this).val();
		if ($(this).val() != "0") {
			$.ajax({
					url : "/gdocprocessos/lotacao/listasetores.json",
					data : {
						id : entiSelec
							},
					type : 'get',
					dataType : 'json',
					success : function(setores) {
									$('#setores').empty();
									$('#setores').append('<option value="0">SELECIONE..</option>');
										for ( var i = 0; i < setores.length; i++) {
											$("#setores").append('<option value="'+setores[i].cdSetor+ '">'+ setores[i].nmSetor+ '</option>');
										}
							}
			});
		}
	});
});