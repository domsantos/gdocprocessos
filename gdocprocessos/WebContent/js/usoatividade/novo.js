$(document).ready(function() {
		$("#vlr").priceFormat({
			prefix : "",
			centsSeparator : ",",
			thousandsSeparator : "",
			centsLimit : 2
		});
		
		$(".number").bind("keyup blur focus", function(e) {
		 	   e.preventDefault();
		        var expre = /[A-Za-z\.\�\�\@\`\?\^\~\'\"\!\?\#\$\%\?\�\_\+\=\.\,\-\:\;\<\>\|\�\�\�\]\[\{\}\\ \)\(\*\&\/\\]/g;
		 
		        // REMOVE OS CARACTERES DA EXPRESSAO ACIMA
		        if ($(this).val().match(expre))
		            $(this).val($(this).val().replace(expre,''));
		    });
	});