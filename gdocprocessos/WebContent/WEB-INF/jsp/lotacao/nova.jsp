<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>NOVA LOTA��O</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css" />
<script type="text/javascript"
	src="<c:url value="/js/jquery-1.6.1.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/lotacao/nova.js"/>"></script>
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>NOVA LOTA��O</legend>
				<div class="row">
					<div class="panelform span4 offset4">
						<c:if test="${not empty errors}">
							<div class="alert">
								<c:forEach var="error" items="${errors}">
									<strong>${error.category} </strong> ${error.message}<br />
								</c:forEach>
							</div>
						</c:if>
						<form method="post" action="<c:url value="/lotacao/cadastrar"/> ">
							<table>
								<tr>
									<td>Usu�rio:</td>
									<td><select name="lotacao.idUsuario.idUsuario">
											<option>SELECIONE..</option>
											<c:forEach var="usuario" items="${listaUser}">
												<option value="${usuario.idUsuario}">${usuario.nmUsuario}</option>
											</c:forEach>
									</select></td>
								</tr>
								<tr>
									<td>Entidade:</td>
									<td><select id="entidades" name="entidade">
											<option value="0">SELECIONE..</option>
											<c:forEach var="entidade" items="${listaEntidade}">
												<option value="${entidade.idEntidade}"
													<c:if test="${entidadeSessao eq entidade.idEntidade}">selected</c:if>>${entidade.dsEntidade}</option>
											</c:forEach>
									</select></td>
								</tr>
								<tr>
									<td>Setor:</td>
									<td><select id="setores" name="lotacao.cdSetor.cdSetor">
											<option value="0">SELECIONE..</option>
											<c:forEach var="setor" items="${listSetor}">
												<option value="${setor.cdSetor}">${setor.nmSetor}</option>
											</c:forEach>
									</select></td>
								</tr>
								<tr>
									<td><input class="btn" type="submit" value="Cadastrar" /></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
				<a href="<c:url value="/lotacao" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>