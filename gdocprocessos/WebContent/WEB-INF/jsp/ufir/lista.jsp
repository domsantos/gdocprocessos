<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Lista UFIR</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Ufir</legend>
				<a class="btn" href="<c:url value="/ufir/novo" />"><span>Novo
										UFIR</span></a>
				<div class="row">
					<div class="well span4 offset4">
						<table class="table">
							<thead>
								<tr>
									<td>Ano</td>
									<td >Valor da Ufir</td>
									<td></td>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="tblUfir" items="${tblUfirList}">
									<tr>
										<td>${tblUfir.nrAno}</td>
										<td>${tblUfir.vlUfir}</td>
										<td><a href="<c:url value="/ufir/editar/${tblUfir.idUfir}" />">Editar</a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<jsp:include page="/WEB-INF/jsp/layout/pags.jsp" ></jsp:include>
					</div>
					
				</div>
				<a href="<c:url value="/menuadmin" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>