<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Novo Tramite</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
<script type="text/javascript"
	src="<c:url value="/js/jquery-1.6.1.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/tramite/novotramite.js"/>"></script>
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Novo Tr�mite</legend>
				<div class="row">
					<div class="panelform span4 offset4">
						<form method="post" action="<c:url value="/tramite/tramitar"/>">
							<div id="processo">
								<table>
									<tr>
										<td>Processo:</td>
										<td><strong>${tblProcessos.nrProcesso}/${tblProcessos.nrAno}</strong><input
											type="hidden" name="tblTramites.idProcesso.idProcesso"
											value="${tblProcessos.idProcesso}" /></td>
									</tr>
								</table>
								<c:if test="${not empty errors}">
									<div class="alert">
										<c:forEach var="error" items="${errors}">
											<strong>${error.category} </strong> ${error.message}<br />
										</c:forEach>
									</div>
								</c:if>
							</div>
							<ul class="listrota">
								<li><input id="btporrota" type="radio" value="1"
									name="tpTramite" />Tramitar Por Rota
									<ul>
										<li>
											<div id="divporrota">
												<c:if test="${empty tblRotas}">
													<table>
														<tr>
															<td colspan="4"><strong>Nenhuma Rota cadastrada.</strong></td>
														</tr>
													</table>
												</c:if>
												<c:if test="${not empty tblRotas}">
													<table>
														<tr>
															<td>Seguir Rota:</td>
															<td><strong>${tblRotas.nmRota}</strong></td>
														</tr>
														<tr>
															<td>Pr�ximo Setor:</td>
															<td><strong>${tblRotas.cdSetor.nmSetor}</strong> <input type="hidden"
																value="${tblRotas.cdRota}" name="tblRotas.cdRota" /></td>
														</tr>
														<tr>
															<td>Documentos Exigidos:</td>
															<td></td>
														</tr>
														<c:if test="${not empty listadocs}">
															<c:forEach var="docs" items="${listadocs}">
																<tr>	
																	<td></td><td>${docs.nmDocumentoexigido}</td>	
																</tr>
															</c:forEach>
														</c:if>
														<c:if test="${empty listadocs}">
															<tr>	
																<td></td><td>Nenhum Documento exigido.</td>	
															</tr>
														</c:if>
													</table>
												</c:if>
											</div>
										</li>
									</ul></li>
								<li><input id="btsemrota" type="radio" value="2"
									name="tpTramite" />Tramitar Sem Rota
									<ul>
										<li>
											<div id="divsemrota">
												<table>
													<tr>
														<td>Entidade:</td>
														<td><select id="entidades"
															name="tblTramites.idEntidade.idEntidade">
																<option value="0">SELECIONE..</option>
																<c:forEach var="entidade" items="${entidadeList}">
																	<option value="${entidade.idEntidade}">${entidade.dsEntidade}</option>
																</c:forEach>
														</select></td>
													</tr>
													<tr>
														<td>Setor:</td>
														<td><select id="setores"
															name="tblTramites.cdSetor.cdSetor">
																<option value="0">SELECIONE..</option>
														</select></td>
													</tr>
												</table>
											</div>
										</li>
									</ul></li>
							</ul>
							<div>
								<table>
									<tr>
										<td>Despacho*:</td>
										<td><textarea rows="4" cols="40"
												name="tblTramites.dsDispacho"></textarea></td>
									</tr>
									<tr>
										<td><input type="submit" value="Tramitar"  class="btn" /></td>
									</tr>
								</table>
							</div>
						</form>
						<a href="<c:url value="/processo/processosetor" />" class="btn btn-large pull-right">Voltar</a>
					</div>
				</div>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>