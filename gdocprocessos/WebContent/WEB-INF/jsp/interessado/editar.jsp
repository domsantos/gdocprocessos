<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EDITAR INTERESSADO</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
		<script type="text/javascript"
	src="<c:url  value="/js/jquery-1.6.1.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url  value="/js/jquery.maskedinput-1.3.min.js"/>"></script>
	<script type="text/javascript"
	src="<c:url  value="/js/interessado/novo.js"/>"></script>
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Interessado: ${tblInteressados.nmInteressado} </legend>
				<div class="row">
			<div class="panelform span10 offset1">
					<c:if test="${not empty errors}">
						<div class="alert">
							<c:forEach var="error" items="${errors}">
								<strong>${error.category} </strong> ${error.message}<br />
							</c:forEach>
						</div>
					</c:if>
					<form method="post"
						action="<c:url value="/interessado/salvaredicao" />">
						<table>
							<tr>
								<td>Nome*:</td>
								<td colspan="3"><input type="text" name="tblInteressados.nmInteressado" class="span4"
									value="${tblInteressados.nmInteressado}" maxlength="80" size="80"/></td>
							</tr>
							<tr>
								<td>CPF/CNPJ*:</td>
								<td colspan="3"><input type="text" name="tblInteressados.nrCpfCnpj" id="cpfcnpj" class="span2"
									value="${tblInteressados.nrCpfCnpj}" maxlength="14" size="14" /></td>
							</tr>
							<tr>
								<td>Enderešo*:</td>
								<td colspan="3"><input type="text" name="tblInteressados.dsEndereco"
									value="${tblInteressados.dsEndereco}" maxlength="120" size="120" class="span4"/></td>
<!-- 							</tr> -->
<!-- 							<tr> -->
								<td>Numero:</td>
								<td><input type="text"  class="number" name="tblInteressados.nrEndereco" class="span2"
									value="${tblInteressados.nrEndereco}" maxlength="20" size="20" /></td>
							</tr>
							<tr>
								<td>Complemento:</td>
								<td colspan="3"><input type="text" name="tblInteressados.dsComplemento" class="span4"
									value="${tblInteressados.dsComplemento}" maxlength="100" size="100" /></td>
<!-- 							</tr> -->
<!-- 							<tr> -->
								<td>Bairro:</td>
								<td><input type="text" name="tblInteressados.dsBairro"
									value="${tblInteressados.dsBairro}" maxlength="50" size="50" /></td>
							</tr>
							<tr>
								<td>CEP:</td>
								<td><input type="text" id="cep" name="tblInteressados.nrCep" class="span2"
									value="${tblInteressados.nrCep}" /></td>
<!-- 							</tr> -->
<!-- 							<tr> -->
								<td>Cidade:</td>
								<td><input type="text" name="tblInteressados.nmCidade"
									value="${tblInteressados.nmCidade}" maxlength="50" size="50" class="span2"/></td>
<!-- 							</tr> -->
<!-- 							<tr> -->
								<td>UF:</td>
								<td><input type="text" name="tblInteressados.dsUf" class="span1"
									value="${tblInteressados.dsUf}" maxlength="2" size="2"/></td>
							</tr>
<!-- 							<tr> -->
<!-- 								<td>MatrÝcula:</td> -->
<!-- 								<td><input type="text" name="tblInteressados.nrMatricula" -->
<%-- 									value="${tblInteressados.nrMatricula}" /></td> --%>
<!-- 							</tr> -->
<!-- 							<tr> -->
<!-- 								<td>Senha:</td> -->
<!-- 								<td><input type="text" name="tblInteressados.vlSenha" -->
<%-- 									value="${tblInteressados.vlSenha}" /></td> --%>
<!-- 							</tr> -->
<!-- 							<tr> -->
<!-- 								<td>Email:</td> -->
<!-- 								<td><input type="text" name="tblInteressados.dsEmail" -->
<%-- 									value="${tblInteressados.dsEmail}" /></td> --%>
<!-- 							</tr> -->
							<tr>
								<td><input type="hidden"
									name="tblInteressados.idInteressado"
									value="${tblInteressados.idInteressado}" /> <input class="btn"
									type="submit" value="Salvar" /></td>
							</tr>
						</table>
					</form>
				</div>
				</div>
				<a href="<c:url value="/interessado/selecionainteressado" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>