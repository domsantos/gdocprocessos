<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>NOVO INTERESSADO</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
	<script type="text/javascript"
	src="<c:url  value="/js/jquery-1.6.1.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url  value="/js/jquery.maskedinput-1.3.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url  value="/js/interessado/novo.js"/>"></script>
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
		<fieldset>
			<legend>Interessado</legend>
			<div id="loader" class="row">
				<div class="pull-text-center span4 offset4 ">
					<h3>Aguarde!</h3>
					<img alt="loading" src="<c:url value="/img/loader.gif" />">
					<br />
				</div>
			</div>
			<div class="row">
			<div class="panelform span10 offset1">
			<c:if test="${not empty errors}">
							<div class="alert">
								<c:forEach var="error" items="${errors}">
									<strong>${error.category} </strong> ${error.message}<br />
								</c:forEach>
							</div>
						</c:if>
			<form method="post" action="<c:url value="/interessado/cadastrar" />" >
				<table class="">
					<tr>
						<td>Nome*:</td>
						<td colspan="3"><input type="text" name="tblInteressados.nmInteressado" maxlength="80" size="80" class="span4" /></td>
					</tr>
					<tr>
						<td>CPF/CNPJ*:</td>
						<td colspan="3"><input type="text" id="cpfcnpj" name="tblInteressados.nrCpfCnpj" maxlength="14" size="14" class="span2" /></td>
					</tr>
					<tr>
						<td>Enderešo*:</td>
						<td colspan="3"><input type="text" name="tblInteressados.dsEndereco" maxlength="120" size="120" class="span4" /> </td>
<!-- 					</tr> -->
<!-- 					<tr> -->
						<td>Numero:</td>
						<td><input type="text" class="number" name="tblInteressados.nrEndereco" maxlength="20" size="20" class="span2" /></td>
					</tr>
					<tr>
						<td>Complemento:</td>
						<td colspan="3"><input type="text" name="tblInteressados.dsComplemento" maxlength="100" size="100" class="span4" /></td>
<!-- 					</tr> -->
<!-- 					<tr> -->
						<td>Bairro:</td>
						<td><input type="text" name="tblInteressados.dsBairro" maxlength="50" size="50"/></td>
					</tr>
					<tr >
						<td>CEP:</td>
						<td><input id="cep" type="text" name="tblInteressados.nrCep" class="span2" /></td>
<!-- 					</tr> -->
<!-- 					<tr> -->
						<td>Cidade:</td>
						<td><input type="text" name="tblInteressados.nmCidade" maxlength="50" size="50" class="span2" /></td>
<!-- 					</tr> -->
<!-- 					<tr> -->
						<td>UF:</td>
						<td><input type="text" name="tblInteressados.dsUf"  maxlength="2" size="2" class="span1" /></td>
					</tr>
<!-- 					<tr> -->
<!-- 						<td>MatrÝcula:</td> -->
<!-- 						<td><input type="text" name="tblInteressados.nrMatricula" /></td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td>Senha:</td> -->
<!-- 						<td><input type="text" name="tblInteressados.vlSenha" /></td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td>Email:</td> -->
<!-- 						<td><input type="text" name="tblInteressados.dsEmail" /></td> -->
<!-- 					</tr> -->
					<tr>
						<td colspan="2"><input id="sub" class="btn" type="submit" value="Cadastrar" /></td>
					</tr>
				</table>
			</form>
			
			</div>
			</div>
			<a href="<c:url value="/interessado/selecionainteressado" />" class="btn btn-large pull-right">Voltar</a>
		</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>