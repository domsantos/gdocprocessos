<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ taglib uri="http://functions" prefix="myfunc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SELECIONA INTERESSADO</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
<script type="text/javascript"
	src="<c:url value="/js/jquery-1.6.1.min.js"/>"></script>
	<script type="text/javascript">
	$(document).ready(function(){
	});
	</script>
</head>
<body>
	<div class="conteudo">
	<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
		<fieldset>
			<legend>Interessado</legend>
			<div class="row">
			<div class="span6">
			<c:if test="${not empty errors}">
							<div class="alert">
								<c:forEach var="error" items="${errors}">
									${error.message}<br />
								</c:forEach>
							</div>
						</c:if>
				<form method="post" action="<c:url value="/interessado/pesquisar" />">
					<table class="">
						<tr>
							<td>Nome:</td>
							<td><input type="text" name="tblInteressados.nmInteressado" /></td>
						</tr>
						<tr>
							<td>ou</td>
						</tr>
						<tr>
							<td>CPF/CNPJ:</td>
							<td><input type="text" id="cpfcnpj" name="tblInteressados.nrCpfCnpj" /></td>
						</tr>
						<tr>
							<td><input class="btn" type="submit" value="Procurar" /></td>
							
						</tr>
					</table>
				</form>
			</div>
			<div class="span6">
				<div class="centerlink">
					<a class="" href="<c:url value="/interessado/novo" />">NOVO INTERESSADO</a>
				</div>
				<div class="centerlink">
					<a class="" href="<c:url value="/interessado/processoanonimo" />">PROCESSO AN�NIMO</a>
				</div>
			</div>
			</div>
		</fieldset>
	
	<fieldset>
		<legend>Resultado</legend>
		<c:if test="${empty listaInteressados}">
			<h3>N�o h� registros a mostrar.</h3>
		</c:if>
		<c:if test="${not empty listaInteressados}">
			<div class="well">
				<table class="table">
					<thead>
						<tr >
							<td>NOME</td>
							<td>CPF / CNPJ</td>
							<td>ENDERE�O</td>
							<td colspan="2"></td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="tblInteressados" items="${listaInteressados}">
							<tr>
								<td>${tblInteressados.nmInteressado}</td>
								<td>${myfunc:maskCPFCNPJ(tblInteressados.nrCpfCnpj)}</td>
								<td>${tblInteressados.dsEndereco}</td>
								<td><a href="<c:url value="/interessado/editar/${tblInteressados.idInteressado}" />">EDITAR</a></td>
								<td><a href="<c:url value="/processo/novoprocesso/${tblInteressados.idInteressado}" />">ABRIR PROCESSO</a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</c:if>
		<a href="<c:url value="/home" />" class="btn btn-large pull-right">Voltar</a>
	</fieldset>
	</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>