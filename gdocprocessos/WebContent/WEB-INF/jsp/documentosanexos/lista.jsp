<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DOCUMENTOS ANEXADOS</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
		<fieldset>
		<legend>Documentos Anexados</legend>
			<div class="row">
				<div class="well span4 offset4 pull-text-center">
					<p><a href="<c:url value="/documentosanexos/${idProcesso}/novoanexo" />" class="btn">Anexar Documento</a>
					<a href="<c:url value="/processo/${idProcesso}/cartaoprocesso" />" class="btn" target="_blank">Emitir Cart�o do Processo</a></p>
				 </div>
			</div>
			<div class="row">
					<div class="well span8 offset2">
						<c:if test="${not empty errors}">
							<div class="alert">
								<c:forEach var="error" items="${errors}">
									<strong>${error.category} </strong> ${error.message}<br />
								</c:forEach>
							</div>
						</c:if>
						<table class="table">
						<thead> 
							<tr> 
								<td>Documento</td>
								<td>Data do Anexo</td>
								<td></td>
							</tr>
						</thead>
						<tbody>
							<c:if test="${empty docAnexoList}">
								<tr>
									<td colspan="2"><strong>Nenhum Documento Anexado</strong></td>
								</tr>
							</c:if>
							<c:forEach items="${docAnexoList}" var="docAnexo">
								<tr>
									<td>${docAnexo.nmDocumentoanexo}</td>
									<td><fmt:formatDate pattern="dd/MM/yyyy" value="${docAnexo.dtAnexo}" /></td>
									<td>
										<a href="<c:url value="/documentosanexos/${docAnexo.cdCodAnexo}/editar" />">Editar Anexo</a> | 
										<a target="_blank" href="<c:url value="/documentosanexos/${docAnexo.cdCodAnexo}/baixaranexo" />">Baixar Arquivo</a>
									
									</td>
								</tr>
							</c:forEach>
						</tbody>
						</table>					
					</div>
		
			</div>
				</fieldset>
			<a href="<c:url value="/processo/${idProcesso}/resumo" />" class="btn btn-large pull-right">Voltar</a>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>