<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>GUIA DETALHE</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>GUIA ${tblValorDocumento.nrGuia}</legend>
				<div class="well span6 offset3">
					<c:if test="${link}">
						<div class="pull-text-center">
							<h3>
								<a target="_blank" href="<c:url value="/guia/${tblValorDocumento.cdValorDocumento}/reimprimirguias" />">Imprimir Parcelas em Aberto</a>
							</h3>
						</div>
					</c:if>
					<table class="table">
						<thead>
							<tr>
								<td>Parcela</td>
								<td>Valor da Parcela</td>
								<td>Data do Vencimento</td>
								<td>Situa��o</td>
							</tr>	
						</thead>
						<tbody>
							<c:forEach var="tblDam" items="${tblDamList}">
								<tr>
									<td>${tblDam.nrParcela}</td>
									<td>${tblDam.vlDam}</td>
									<td><fmt:formatDate pattern="dd/MM/yyyy" value="${tblDam.dtVencimento}" /></td>
									<c:if test="${tblDam.stSituacao eq sim}">
										<td>PAGO</td>
									</c:if>
									<c:if test="${tblDam.stSituacao eq nao}">
										<td>EM ABERTO</td>
									</c:if>									
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>