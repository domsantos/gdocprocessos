<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://functions" prefix="myfunc"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>JUNTAR PROCESSO</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>JUNTAR PROCESSO AO
					${processo.nrProcesso}/${processo.nrAno}</legend>
				<div class="row">
					<div class="well span6 offset3">
						<p>
							PROCESSO: <b>${processo.nrProcesso}/${processo.nrAno}</b>
						</p>
						<p>
							TIPO DO PROCESSO: <b>${processo.cdTipoprocesso.nmTipoprocesso}</b>
						</p>
						<p>
							INTERESSADO: <b>${myfunc:maskCPFCNPJ(processo.idInteressado.nrCpfCnpj)}
								- ${processo.idInteressado.nmInteressado}</b>
						</p>
					</div>
				</div>
				<div class="row">
					<div class="panelform span6 offset3">
						<c:if test="${not empty errors}">
							<div class="alert">
								<c:forEach var="error" items="${errors}">
									<strong>${error.category} </strong> ${error.message}<br />
								</c:forEach>
							</div>
						</c:if>
						<form method="post" action="<c:url value="/processojuntado/juntar" />">
							<table>
								<tr>
									<td>N�mero do Processo*:</td>
									<td><input type="text" name="processo.nrProcesso" /></td>
								</tr>
								<tr>
									<td>Ano do Processo*:</td>
									<td><input type="text" name="processo.nrAno" /></td>
								</tr>
								<tr>
									<td>
									<input type="hidden" value="${processo.idProcesso}" name="idproc"/>
									<input type="submit" value="Juntar Processo" class="btn" /></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
				<a href="<c:url value="/processo/${processo.idProcesso}/resumo" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>