<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>REGRAS</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
</head>
<body>
	<div class="conteudo">
	<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Regras do Tipo de Processo: ${tipoprocesso.nmTipoprocesso}</legend>
				<div class="row">
					<div class="well span2 offset5 pull-text-center">
						<a href="<c:url value="/regra/${tipoprocesso.cdTipoprocesso}/novo"/>" class="btn btn-large">Nova Regra</a>
					</div>
				</div>
				<div class="row">
				<div class="well span6 offset3">
					<table class="table">
						<thead>
							<tr>
								<td>C�d. Regra</td>
								<td colspan="2">Regra</td>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="tblRegras" items="${tblRegrasList}">
								<tr>
									<td>${tblRegras.cdRegra}</td>
									<td>${tblRegras.nmRegra}</td>
									<td><a href="<c:url value="/regra/editar/${tblRegras.cdRegra}" />">Editar</a>
									|
										<a href="<c:url value="/regra/excluir/${tblRegras.cdRegra}" />">Excluir</a></td>
								</tr>
							</c:forEach>							
						</tbody>
					</table>
					<jsp:include page="/WEB-INF/jsp/layout/pags.jsp" ></jsp:include>
				</div>
				</div>
				<a href="<c:url value="/tipoprocesso" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>