<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>NOVA SENHA</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Nova Senha</legend>
				<div class="row">
					<div class="panelform span4 offset4">
						<form method="post"
							action="<c:url value="/usuario/salvarnovasenha"/>">
							<c:if test="${not empty errors}">
								<div class="alert">
									<c:forEach var="error" items="${errors}">
										<strong>${error.category} </strong> ${error.message}<br />
									</c:forEach>
								</div>
							</c:if>
							<table>
								<tr>
									<td>Senha Anterior:</td>
									<td><input type="password" name="usuario.vlSenha" maxlength="40"></td>
								</tr>
								<tr>
									<td>Nova Senha:</td>
									<td><input type="password" name="novaSenha" maxlength="40" ></td>
								</tr>
								<tr>
									<td>Confirme a Nova Senha:</td>
									<td><input type="password" name="confNovaSenha" maxlength="40"></td>
								</tr>
								<tr>
									<td><input type="hidden" name="usuario.idUsuario"
										value="${tblUsuario.idUsuario}" /> <input type="submit" class="btn"
										value="Redefinir" /></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
				<a href="<c:url value="/home" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>