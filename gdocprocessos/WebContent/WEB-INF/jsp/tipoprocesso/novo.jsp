<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Novo Tipo de Processo</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Novo Tipo de Processo</legend>
				<div class="row">
				<div class="panelform span4 offset4">
					<form method="post"
						action="<c:url value="/tipoprocesso/cadastrar"/>">
						<table>
							<tr>
								<td>Nome*:</td>
								<td><input name="tblTipoprocesso.nmTipoprocesso"
									type="text" maxlength="40" /></td>
							</tr>
							<tr>
								<td>Descri��o:</td>
								<td><input name="tblTipoprocesso.dsTipoprocesso"
									type="text" maxlength="120" /></td>
							</tr>
							<tr>
								<td>Confirma��o Presencial*:</td>
								<td><select name="tblTipoprocesso.stConfirmacaoPresencia">
										<option>SELECIONE..</option>
										<option value="S">SIM</option>
										<option value="N">N�O</option>
								</select></td>
							</tr>
							<tr>
								<td>Abrir pela Internet*:</td>
								<td><select name="tblTipoprocesso.stAbrirInternet">
										<option>SELECIONE..</option>
										<option value="S">SIM</option>
										<option value="N">N�O</option>
								</select></td>
							</tr>
							<tr>
								<td><input type="submit" value="Cadastrar" class="btn" /></td>
							</tr>
						</table>
					</form>
				</div>
				</div>
				<a href="<c:url value="/tipoprocesso" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>