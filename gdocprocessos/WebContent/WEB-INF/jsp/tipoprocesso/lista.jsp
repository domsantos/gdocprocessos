<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>LISTA TIPO PROCESSO</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Tipo de Processo</legend>
				<a class="btn" href="<c:url value="/tipoprocesso/novo" />"><span>Novo Tipo de Processo</span></a>
				<div class="row">
				<div class="well span8 offset2">
					
					<table class="table">
						<thead>
							<tr>
								<td>Cod. Tipo Processo</td>
								<td colspan="2">Tipo Processo</td>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="tblTipoprocesso" items="${tblTipoprocessoList}">
								<tr>
									<td>${tblTipoprocesso.cdTipoprocesso}</td>
									<td>${tblTipoprocesso.nmTipoprocesso}</td>
									<td><a
										href="<c:url value="/tipoprocesso/editar/${tblTipoprocesso.cdTipoprocesso}" />">Editar</a>
										|
									<a
										href="<c:url value="/regra/${tblTipoprocesso.cdTipoprocesso}" />">Listar Regras</a>
										|
									<a
										href="<c:url value="/rota/${tblTipoprocesso.cdTipoprocesso}" />">Listar Rotas</a></td>	
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<jsp:include page="/WEB-INF/jsp/layout/pags.jsp" ></jsp:include>
				</div>
				</div>
				<a href="<c:url value="/menuadmin" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>

</body>
</html>