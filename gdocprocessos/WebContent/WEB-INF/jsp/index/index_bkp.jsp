<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="<c:url  value="/js/jquery-1.6.1.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url  value="/js/jquery.maskedinput-1.3.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url  value="/js/index/index.js"/>"></script>
<title>GED PROCESSOS- Login</title>
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
</head>
<body>
	<div class="conteudo">
	<div class="head">
		<div class="container">
			<div class="row">
				<div class="span6">
					<div class="panelbrasao">
					<a class="" href="#"> <img id="brasao"  title="P.M.B"
						src="<c:url value="/img/gdocprocessoslogo.jpg" />">
					</a>
					</div>
				</div>
				<div class="span6">
					<div class="panellogin">
						<c:if test="${not empty errors}">
							<div class="alert">
								<c:forEach var="error" items="${errors}">
									<strong>${error.category} </strong> ${error.message}<br />
								</c:forEach>
							</div>
						</c:if>
						<form method="post" action="<c:url value="/index/entrar" />">
							<table class="input">
								<tr>
									<td>Matr�cula:</td>
									<td><input id="nrmatricula" type="text"
										name="usuario.nrMatricula" /></td>
								</tr>
								<tr>
									<td>Senha:</td>
									<td><input id="vlsenha" type="password"
										name="usuario.vlSenha" /></td>
								</tr>
								<tr>
									<td><button class="btn btn-large" id="btentrar"
											type="submit" value="Entrar">Entrar</button></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
	
	<script type="text/javascript">
		var aviso = 'Aviso Importante\n\n';
		aviso += 'Sr (as). Usu�rios (as),\n\n';
		aviso += 'A partir do dia 05/09/2014 (sexta-feira), estaremos implantando ';
		aviso += 'uma nova vers�o do Sistema de Gest�o de Processos. ';
		aviso += 'Esta atualiza��o ser� totalmente compat�vel com o Banco de Dados atual, ';
		aviso += 'ou seja, nenhum dado ser� perdido como  por exemplo: ';
		aviso += 'Processo, Encaminhamento, Interessado e etc.\n\n';
		aviso += 'O objetivo � oferecer um servi�o melhor e com novas funcionalidades. ';
		aviso += 'Temos a certeza de podermos contar com a compreens�o e ';
		aviso += 'coopera��o de todos (as) durante o per�odo de atualiza��o sistema.';
		
		alert(aviso);
	</script>
</body>
</html>