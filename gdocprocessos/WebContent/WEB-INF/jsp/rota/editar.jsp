<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Editar Rota</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css" />
<script type="text/javascript"
	src="<c:url value="/js/jquery-1.6.1.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/rota/editar.js"/>"></script>
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Editar Rota ${tblRotas.nmRota}</legend>
				<div class="row">
				<div class="panelform span5 offset3">
					<c:if test="${not empty errors}">
						<div class="alert">
							<c:forEach var="error" items="${errors}">
								<strong>${error.category} </strong> ${error.message}<br />
							</c:forEach>
						</div>
					</c:if>
					<form method="post" action="<c:url value="/rota/salvaredicao" />">
						<table>
							<tr>
								<td>Nome*:</td>
								<td><input name="tblRotas.nmRota" size="40" maxlength="40" type="text"
									value="${tblRotas.nmRota}" /><input type="hidden"
									name="tblRotas.cdRota" value="${tblRotas.cdRota}" /></td>
							</tr>
							<tr>
								<td>Tipo do Processo*:</td>
								<td><select id="tpprocessos"
									name="tblRotas.cdTipoprocesso.cdTipoprocesso">
										<option value="">SELECIONE..</option>
										<c:forEach var="tblTipoprocesso"
											items="${tblTipoprocessoList}">
											<option value="${tblTipoprocesso.cdTipoprocesso}"
												<c:if test="${tblTipoprocesso.cdTipoprocesso eq tipoprocesso.cdTipoprocesso}">selected</c:if>>${tblTipoprocesso.nmTipoprocesso}</option>
										</c:forEach>
								</select></td>
							</tr>
							<tr>
								<td>Setor*:</td>
								<td><select id="setores" name="tblRotas.cdSetor.cdSetor">
										<option value="">SELECIONE..</option>
										<c:forEach var="tblSetor" items="${tblSetorList}">
											<option value="${tblSetor.cdSetor}"
												<c:if test="${tblSetor.cdSetor eq setor.cdSetor}" >selected</c:if>>${tblSetor.nmSetor}</option>
										</c:forEach>
								</select></td>
							</tr>
							<tr>
								<td>Prioridade*:</td>
								<td><select id="prioridade" name="tblRotas.cdPrioridade">
										<option value="0">SELECIONE..</option>
										<option value="${prioridadealta}"
											<c:if test="${prioridadealta eq tblRotas.cdPrioridade}">selected</c:if>>ALTA</option>
										<option value="${prioridadenormal}"
											<c:if test="${prioridadenormal eq tblRotas.cdPrioridade}">selected</c:if>>NORMAL</option>
										<option value="${prioridadebaixa}"
											<c:if test="${prioridadebaixa eq tblRotas.cdPrioridade}">selected</c:if>>BAIXA</option>
								</select></td>
							</tr>
							<tr>
								<td>Rota Paralela*:</td>
								<td><select id="prioridade" name="tblRotas.stRotaParalela">
										<option value="0">SELECIONE..</option>
										<option value="${paralelasim}"
											<c:if test="${paralelasim eq tblRotas.stRotaParalela}">selected</c:if>>SIM</option>
										<option value="${paralelanao}"
											<c:if test="${paralelanao eq tblRotas.stRotaParalela}">selected</c:if>>N&Atilde;O</option>
								</select></td>
							</tr>
							<tr>
								<td>Ordem da Rota*:</td>
								<td><input name="tblRotas.nrOrdemrota"
									value="${tblRotas.nrOrdemrota}" type="text"  class="number"/></td>
							</tr>
							<tr>
								<td>Prazo da Rota (em dias):</td>
								<td><input name="tblRotas.nrPrazoRota" type="text" value="${tblRotas.nrPrazoRota}" class="number" /></td>
							</tr>
							<tr><td><a href="<c:url value="/rota/${tblRotas.cdRota}/regras" />">Documentos da Rota</a></td></tr>
							<tr>
								<td><input type="submit" value="Cadastrar Rota" class="btn" /></td>
							</tr>
						</table>
					</form>
				</div>
				</div>
				<a href="<c:url value="/rota/${tblRotas.cdRota}" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>