<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EDITAR LOCAL DE ARQUIVO</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">

</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Editar ${reg.dsEnderecoarquivamento}</legend>
				<div class="row">
					<div class="panelform span6 offset3">
						<c:if test="${not empty errors}">
							<div class="alert">
								<c:forEach var="error" items="${errors}">
									<strong>${error.category} </strong> ${error.message}<br />
								</c:forEach>
							</div>
						</c:if>
						<form action="<c:url value="/localarquivo/salvaredicao"  />" method="post">
							<table>
								<tr>
									<td>N�vel ACIMA do Local*:</td>
									<td><select name="local.idNivelsuperior" class="span3">
											<option value="">SELECIONE..</option>
											<option value="${localRaiz}" <c:if test="${localRaiz eq reg.idNivelsuperior}">selected</c:if>>RAIZ</option>
											<c:forEach var="local" items="${localList}">
												<option value="${local.id}" <c:if test="${local.id eq reg.idNivelsuperior}">selected</c:if>>${local.ordem} - ${local.desc}</option>
											</c:forEach>
									</select></td>
								</tr>
								<tr>
									<td>Nome do Local*:</td>
									<td><input type="text" name="local.dsEnderecoarquivamento" maxlength="50" class="span4" value="${reg.dsEnderecoarquivamento}" /></td>
								</tr>
								<tr>
									<td>
										<input type="hidden" name="local.idEnderecoarquivamento" value="${reg.idEnderecoarquivamento}" />
										<input class="btn" type="submit" value="Salvar" />
									</td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</fieldset>
			<a href="<c:url value="/localarquivo" />" class="btn btn-large pull-right">Voltar</a>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>