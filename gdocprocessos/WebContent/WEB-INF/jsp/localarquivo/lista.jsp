<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>LOCAIS DE ARQUIVAMENTO</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">

</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Locais de Arquivamento</legend>
				<c:if test="${not empty errors}">
					<div class="alert">
						<c:forEach var="error" items="${errors}">
							<strong>${error.category} </strong> ${error.message}<br />
						</c:forEach>
					</div>
				</c:if>
				
					<a class="btn" href="<c:url value="/localarquivo/novo" />"><span>Novo
							Local de Arquivamento</span></a>
				
				<div class="row">
					<div class="well span6 offset3">
						<table class="table">
							<thead>
								<tr>
									<td colspan="2">Locais</td>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="local" items="${localList}">
									<tr>
										<td><b>${local.ordem}</b> - ${local.desc}</td>
										<td><a
											href="<c:url value="/localarquivo/editar/${local.id}" />">Editar</a>
											| <a
											href="<c:url value="/localarquivo/excluir/${local.id}" />">Excluir</a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<a href="<c:url value="/menuadmin" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>