<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
<script type="text/javascript"
	src="<c:url value="/js/jquery-1.6.1.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/priceformat/priceformat.1.7.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/priceformat/priceformat.1.7.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/usoatividade/novo.js"/>"></script>
<title>NOVO USO ATIVIDADE</title>
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Novo Uso Atividade</legend>
				<div class="row">
					<div class="panelform span4 offset4">
						<c:if test="${not empty errors}">
							<div class="alert">
								<c:forEach var="error" items="${errors}">
									<strong>${error.category} </strong> ${error.message}<br />
								</c:forEach>
							</div>
						</c:if>
						<form method="post"
							action="<c:url value="/usoatividade/cadastrar"/>">
							<table>
								<tr>
									<td>Descri��o*:</td>
									<td><input name="tblUsoatividade.dsUsoatividade"
										maxlength="100" type="text" /></td>
								</tr>
								<tr>
									<td>Valor:*</td>
									<td><input id="vlr" name="valorufir" type="text" /></td>
								</tr>
								<tr>
									<td>Tributo*:</td>
									<td><select name="tblUsoatividade.idTributo.idTributo">
											<option value="0">SELECIONE..</option>
											<c:forEach var="tblTributo" items="${tblTributoList}">
												<option value="${tblTributo.idTributo}">${tblTributo.nmTributo}</option>
											</c:forEach>
									</select></td>
								</tr>
								<tr>
									<td>Ufir*:</td>
									<td><select name="tblUsoatividade.idUfir.idUfir">
											<option value="0">SELECIONE..</option>
											<c:forEach var="tblUfir" items="${tblUfirList}">
												<option value="${tblUfir.idUfir}">${tblUfir.nrAno} / ${tblUfir.vlUfir}</option>
											</c:forEach>
									</select></td>
								</tr>
								<tr>
									<td>C�d. de Desmembramento:*</td>
									<td><input name="tblUsoatividade.cdDesmembramento"
										maxlength="2" type="text" class="number" /></td>
								</tr>
								<tr>
									<td>N�m. do Doc. Cont�bil:*</td>
									<td><input name="tblUsoatividade.nrDocumentoContabil"
										maxlength="20" type="text" class="number" /></td>
								</tr>
								<tr>
									<td>Exerc�cio:</td>
									<td><input name="tblUsoatividade.nrAnoExercicio"
										maxlength="4" type="text" class="number" /></td>
								</tr>
								<tr>
									<td>Tributo Exclusivo:</td>
									<td><select name="tblUsoatividade.stTributoExclusivo">
											<option value="">SELECIONE..</option>
											<option value="${sim}">SIM</option>
											<option value="${nao}">N�O</option>
									</select></td>
								</tr>
								<tr>
									<td><input type="submit" value="Cadastrar" class="btn" /></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
				<a href="<c:url value="/usoatividade" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>