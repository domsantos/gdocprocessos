<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
<script type="text/javascript"
	src="<c:url value="/js/jquery-1.6.1.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/priceformat/priceformat.1.7.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/priceformat/priceformat.1.7.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/usoatividade/novo.js"/>"></script>
<title>EDITAR USO ATIVIDADE</title>
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Editar Uso Atividade</legend>
				<div class="row">
					<div class="panelform span4 offset4">
						<c:if test="${not empty errors}">
							<div class="alert">
								<c:forEach var="error" items="${errors}">
									<strong>${error.category} </strong> ${error.message}<br />
								</c:forEach>
							</div>
						</c:if>
						<form method="post" action="<c:url value="/usoatividade/salvaredicao"/>">
							<table>
								<tr>
									<td>Descri��o*:</td>
									<td><input name="tblUsoatividade.dsUsoatividade" value="${tblUsoatividade.dsUsoatividade}"
										maxlength="100" type="text" /><input name="tblUsoatividade.cdUsoatividade"
										type="hidden"  value="${tblUsoatividade.cdUsoatividade}"/></td>
								</tr>
								<tr>
									<td>Valor:*</td>
									<td><input id="vlr" name="valorufir" type="text" value="<fmt:formatNumber minFractionDigits="2" maxFractionDigits="2"  value=" ${tblUsoatividade.nrQtdufir}"/>"/></td>
								</tr>
								<tr>
									<td>Tributo*:</td>
									<td><select name="tblUsoatividade.idTributo.idTributo">
											<option value="0">SELECIONE..</option>
											<c:forEach var="tblTributo" items="${tblTributoList}">
												<option value="${tblTributo.idTributo}" <c:if test="${tblUsoatividade.idTributo.idTributo eq tblTributo.idTributo }">selected</c:if>  >${tblTributo.nmTributo}</option>
											</c:forEach>
									</select></td>
								</tr>
								<tr>
									<td>Ufir*:</td>
									<td><select name="tblUsoatividade.idUfir.idUfir">
											<option value="0">SELECIONE..</option>
											<c:forEach var="tblUfir" items="${tblUfirList}">
												<option value="${tblUfir.idUfir}" <c:if test="${tblUsoatividade.idUfir.idUfir eq tblUfir.idUfir }">selected</c:if> >${tblUfir.nrAno} / ${tblUfir.vlUfir}</option>
											</c:forEach>
									</select></td>
								</tr>
								
								<tr>
									<td>C�d. de Desmembramento:*</td>
									<td><input name="tblUsoatividade.cdDesmembramento"
										maxlength="5" type="text"  value="${tblUsoatividade.cdDesmembramento}" class="number"/></td>
								</tr>
								<tr>
									<td>N�m. do Doc. Cont�bil:*</td>
									<td><input name="tblUsoatividade.nrDocumentoContabil" maxlength="20" type="text" value="${tblUsoatividade.nrDocumentoContabil}" class="number" /></td>
								</tr>
								<tr>
									<td>Exerc�cio:</td>
									<td><input name="tblUsoatividade.nrAnoExercicio" value="${tblUsoatividade.nrAnoExercicio}" 
										maxlength="4" type="text" class="number"/></td>
								</tr>
								<tr>
									<td>Tributo Exclusivo:</td>
									<td><select name="tblUsoatividade.stTributoExclusivo">
											<option value="">SELECIONE..</option>
											<option value="${sim}" <c:if test="${tblUsoatividade.stTributoExclusivo eq sim }">selected</c:if> >SIM</option>
											<option value="${nao}" <c:if test="${tblUsoatividade.stTributoExclusivo eq nao }">selected</c:if> >N�O</option>
									</select></td>
								</tr>
								<tr>
									<td><input type="submit" value="Cadastrar" class="btn" /></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
				<a href="<c:url value="/usoatividade" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>