<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>LISTA</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Uso Atividade</legend>
				<a class="btn" href="<c:url value="/usoatividade/novo" />"><span>Novo
										Uso/Atividade</span></a>
				<div class="row">
					<div class="well span8 offset2">
						<table class="table">
							<thead>
								<tr>
									<td>C�d. do Uso</td>
									<td>Descri��o</td>
									<td>Tributo</td>
									<td >Valor</td>
									<td></td>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="tblUsoatividade" items="${tblUsoatividadeList}">
									<tr>
										<td>${tblUsoatividade.cdUsoatividade}</td>
										<td>${tblUsoatividade.dsUsoatividade}</td>
										<td>${tblUsoatividade.idTributo.nmTributo}</td>
										<td><fmt:formatNumber minFractionDigits="2" maxFractionDigits="2"  value=" ${tblUsoatividade.nrQtdufir}"/></td>
										<td><a href="<c:url value="/usoatividade/editar/${tblUsoatividade.cdUsoatividade}" />">EDITAR</a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<jsp:include page="/WEB-INF/jsp/layout/pags.jsp"></jsp:include>
					</div>
				</div>
				<a href="<c:url value="/menuadmin" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>