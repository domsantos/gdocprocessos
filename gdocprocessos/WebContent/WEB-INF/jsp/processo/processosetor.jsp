<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PROCESSOS NO SETOR</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
	
	<style>
		#dialog-form {
		    visibility: hidden;
		    position: absolute;
		    left: 0px;
		    top: 0px;
		    width:100%;
		    height:100%;
		    text-align:center;
		    z-index: 1000;
		 }
	</style>
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Processos no(a)
					${sessaoUsuario.lotacaoUser.cdSetor.nmSetor}</legend>
				<div class="row">
					<div class="panelform span6 offset3">
						<form action="<c:url value="/processo/pesquisaprocessosetor" />"
							method="post">
							<p>
								Processo: <input type="text" name="numprocesso" class="span2"/> Ano: <input class="span1"
									type="text" name="nrano" /> <input class="btn" type="submit"
									value="PESQUISAR" />
							</p>
						</form>
					</div>
				</div>
				<div class="well">
					<table class="table">
						<thead>
							<tr>
<!-- 								<td>Cod. Processo</td> -->
								<td>Processo</td>
								<td>Tipo Processo</td>
								<td colspan="2">Interessado</td>
							</tr>
						</thead>
						<tbody>
							<c:if test="${empty tblProcessosList}">
								<tr>
									<td colspan="4"><h2>Nenhum Processo encontrado</h2></td>
								</tr>
							</c:if>
							<c:forEach var="tblProcessos" items="${tblProcessosList}">
								<tr>
<%-- 									<td>${tblProcessos.idProcesso}</td> --%>
									<td>${tblProcessos.nrProcesso}/${tblProcessos.nrAno}</td>
									<td>${tblProcessos.cdTipoprocesso.nmTipoprocesso}</td>
									<td>${tblProcessos.idInteressado.nmInteressado}</td>
									<td><a
										href="<c:url value="/processo/${tblProcessos.idProcesso}/detalhe" />">
											<img title="Detalhar" alt="Detalhar" src="<c:url value="/img/detalhar.jpg"/>" >
									</a> <a
										href="<c:url value="/tramite/novo/${tblProcessos.idProcesso}" />">
											<img title="Tramitar" alt="Tramitar" src="<c:url value="/img/tramitar.jpg"/>">
									</a> <a
										href="<c:url value="/processo/${tblProcessos.idProcesso}/resumo" />">
											<img title="Resumo" alt="Resumo" src="<c:url value="/img/resumo.jpg"/>">
									</a> <a
										href="<c:url value="/processo/${tblProcessos.idProcesso}/encerrarprocesso" />">
											<img title="Encerrar" alt="Encerrar" src="<c:url value="/img/encerrar.jpg"/>">
									</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<jsp:include page="/WEB-INF/jsp/layout/pags.jsp"></jsp:include>
				</div>

			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>
