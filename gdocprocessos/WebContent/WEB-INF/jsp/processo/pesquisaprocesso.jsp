<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PESQUISAR PROCESSO</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
<script type="text/javascript"
	src="<c:url  value="/js/jquery-1.6.1.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url  value="/js/processo/pesquisaprocesso.js"/>"></script>
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Pesquisar Processo</legend>
				<div class="row">
					<div class="panelform span6 offset3">
						<form action="<c:url value="/processo/pesquisar" />" method="post">
							<c:if test="${not empty errors}">
								<div class="alert">
									<c:forEach var="error" items="${errors}">
										<strong>${error.category} </strong> ${error.message}<br />
									</c:forEach>
								</div>
							</c:if>
							<p>
								Processo: <input type="text" name="tblProcessos.nrProcesso" class="span2" />
								Ano: <input name="tblProcessos.nrAno" type="text" class="span1"/>
							</p>
							<p>
								<input id="avancado" type="checkbox" name="maiscampos" /> Mais
								Campos? ( Ao selecionar 'Mais Campos' Voc� n�o � obrigado a
								inserir o n�mero/ano do processo)
							</p>
							<br />
							<div id="pesqavancada">
								<fieldset>
									<legend>Outros Campos</legend>
									<p>
										Interessado: <input type="text"
											name="tblProcessos.idInteressado.nmInteressado" />
									</p>
									<p>
										Tipo Processo: <select id="tpprocs"
											name="tblProcessos.cdTipoprocesso.cdTipoprocesso">
											<option value="0">SELECIONE..</option>
											<c:forEach var="tblTipoprocesso"
												items="${tblTipoprocessoList}">
												<option value="${entidade.idEntidade}">${entidade.dsEntidade}</option>
											</c:forEach>
										</select>
									</p>
									<p>
										Entidade de Origem: <select id="entidades"
											name="tblProcessos.idEntidade.idEntidade">
											<option value="0">SELECIONE..</option>
											<c:forEach var="entidade" items="${entidadeList}">
												<option value="${entidade.idEntidade}">${entidade.dsEntidade}</option>
											</c:forEach>
										</select>
									</p>
									<p>
										Situa��o do Processo: <select name="tblProcessos.stProcesso">
											<option value="X">SELECIONE..</option>
											<option value="A">ABERTO</option>
											<option value="E">ENCERRADO</option>
											<option value="F">FECHADO</option>
											<option value="C">CANCELADO</option>
										</select>
									</p>
								</fieldset>
							</div>
							<p>
								<input type="submit" value="Pesquisar Processo" class="btn" />
							</p>
						</form>
					</div>
				</div>
				<fieldset>
					<legend>Resultado da Pesquisa</legend>
					<c:if test="${empty tblProcessosList}">
						<h2>Nenhum Processo encontrado</h2>
					</c:if>
					<c:if test="${not empty tblProcessosList}">
						<div class="row">
							<div class="well span8 offset2">
								<table class="table">
									<thead>
										<tr>
											<td>Num. Processo</td>
											<td>Interessado</td>
											<td colspan="2">Tipo do Processo</td>
										</tr>
									</thead>
									<tbody>
										<c:if test="${empty tblProcessosList}">
											<tr>
												<td colspan="4">Nenhum Processo a exibir</td>
											</tr>
										</c:if>
										<c:forEach var="tblProcessos" items="${tblProcessosList}">
											<tr>
												<td>${tblProcessos.nrProcesso}/${tblProcessos.nrAno}</td>
												<td>${tblProcessos.idInteressado.nmInteressado}</td>
												<td>${tblProcessos.cdTipoprocesso.nmTipoprocesso}</td>
												<td><a
													href="<c:url value="/processo/${tblProcessos.idProcesso}/detalhe" />">
													<img title="Detalhar" alt="Detalhar" src="<c:url value="/img/detalhar.jpg"/>" >
												</a></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</c:if>
				</fieldset>
				<a href="<c:url value="/home" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>