<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Novo Processo</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css" />
<script type="text/javascript"
	src="<c:url  value="/js/jquery-1.6.1.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url  value="/js/processo/novoprocesso.js"/>"></script>
<script type="text/javascript"
	src="<c:url  value="/js/jquery.maskedinput-1.3.min.js"/>"></script>
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Processo</legend>
				<div class="row">
				<div class="panelform span8 offset2">
					<c:if test="${not empty errors}">
						<div class="alert">
							<c:forEach var="error" items="${errors}">
								<strong>${error.category} </strong> ${error.message}<br />
							</c:forEach>
						</div>
					</c:if>
					<form method="post"
						action="<c:url value="/processo/cadastrarprocesso"/>">
						<table>
							<tr>
								<td>Interessado:</td>
								<td>${tblInteressados.nmInteressado}</td>
							</tr>
							<tr>
								<td>Tipo do Processo*:</td>
								<td><select id="tpprocesso"
									name="tblProcessos.cdTipoprocesso.cdTipoprocesso">
										<option value="0">SELECIONE..</option>
										<c:forEach var="tblTipoprocesso"
											items="${tblTipoprocessoList}">
											<option value="${tblTipoprocesso.cdTipoprocesso}">${tblTipoprocesso.nmTipoprocesso}</option>
										</c:forEach>
								</select></td>
							</tr>
							<tr>
								<td>Regras:</td>
								<td></td>
							</tr>
							<tr id="regras">
								<td></td><td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>Descri��o do Processo*:</td>
								<td><textarea rows="5" cols="60"
										name="tblProcessos.dsProcesso"></textarea></td>
							</tr>
							<tr>
								<td><input type="checkbox" id="infoads" /> Adicionar mais
									informa��es?</td>
							</tr>
						</table>
						<div id="divinfoads">
							<fieldset>
								<legend>Informa��es Adicionais</legend>
								<table>
									<tr>
										<td>Endere�o:</td>
										<td colspan="3"><input type="text" name="tblProcessos.dsEndereco" class="span4"
											class="infoads" maxlength="100"/></td>
									</tr>
									<tr>
										<td>N�mero:</td>
										<td><input type="text" name="tblProcessos.nrEndereco" class="span2"
											class="infoads number" maxlength="20"/></td>
									</tr>
									<tr>
										<td>Complemento:</td>
										<td colspan="3"><input type="text" name="tblProcessos.dsComplemento" class="span4"
											class="infoads" maxlength="100"/></td>
									</tr>
									<tr>
										<td>CEP:</td>
										<td><input id="cepAd" type="text" name="tblProcessos.nrCep" class="span2"
											class="infoads" /></td>
									</tr>
									<tr>
										<td>Bairro:</td>
										<td><input type="text" name="tblProcessos.dsBairro"
											class="infoads" maxlength="50" /></td>
									
										<td>Cidade:</td>
										<td><input type="text" name="tblProcessos.nmCidade"
											class="infoads" maxlength="50"/></td>
									</tr>
									<tr>
										<td>UF:</td>
										<td><input type="text" name="tblProcessos.dsUf" class="span1"
											class="infoads" maxlength="2" /></td>
									</tr>
								</table>
							</fieldset>
						</div>
						<input type="hidden"
							name="tblProcessos.idInteressado.idInteressado"
							value="${tblInteressados.idInteressado}" /> <input class="btn"
							type="submit" value="Abrir Processo" />
					</form>
					
				</div>
				</div>
				<a href="<c:url value="/interessado/selecionainteressado" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>	
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>