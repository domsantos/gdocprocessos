<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ENCERRAR PROCESSO</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
</head>
<body>
	<div class="conteudo">
	<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>ENCERRAR PROCESSO ${tblProcessos.nrProcesso}/${tblProcessos.nrAno} </legend>
				<div class="row">
					<div class="panelform span4 offset4">
						<form action="<c:url value="/processo/salvarenceprocesso"/>" method="post">
							<table>
								<tr>
									<td>
										Processo: <strong>${tblProcessos.nrProcesso}/${tblProcessos.nrAno}</strong> 
										<input type="hidden" name="tblProcessos.idProcesso" value="${tblProcessos.idProcesso}" />
									</td>
								</tr>
								<tr>
									<td>Data de Abertura:</td>
									<td><fmt:formatDate pattern="dd/MM/yyyy" value="${tblProcessos.dtAberturaProcesso}" /></td> 
								</tr>
								<tr>
									<td>Descri��o do Encerramento*:</td>
									<td><textarea name="tblProcessos.dsEncerramento"></textarea></td>
								</tr>
								<tr>
									<td><input type="submit" value="Encerrar Processo" class="btn" /></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
				<a href="<c:url value="/processo/processosetor" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>