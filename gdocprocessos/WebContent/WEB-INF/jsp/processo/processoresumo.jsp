<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://functions" prefix="myfunc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>RESUMO PROCESSO</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Processo: <strong>${processo.nrProcesso}/${processo.nrAno}</strong></legend>
				<div class="row">
					<div class="panelform span6 offset3">
						<p>Processo: <strong>${processo.nrProcesso}/${processo.nrAno}</strong></p>
						<p>Tipo do Processo: <strong>${tipoprocesso.nmTipoprocesso}</strong></p>
						<p>Interessado: <strong>${myfunc:maskCPFCNPJ(interessado.nrCpfCnpj)} - ${interessado.nmInteressado}</strong></p>
					</div>
				</div>
				<fieldset>
					<legend>Op��es</legend>
					<div class="row">
						<div class="span4 offset4">
							<div class="well well-large well-links">
								<p>
									<a href="<c:url value="/tramite/novo/${processo.idProcesso}" />" >Tramitar</a>
								</p>
								<p>
									<a href="<c:url value="/processo/${processo.idProcesso}/capaProcesso" />" target="_blank">Emitir Capa do Processo</a>
								</p>
								<p>
									<a href="<c:url value="/guia/novaguia/${processo.idProcesso}" />" >Gerar uma Guia de
										Pagamento</a>
								</p>
								<p>
									<a href="<c:url value="/processo/${processo.idProcesso}/cartaoprocesso" />" target="_blank">Emitir Cart�o do Processo</a>
								</p>
								<p>
									<a href="<c:url value="/guia/${processo.idProcesso}/lista" />">Listar DAMs Geradas</a>
								</p>
								<p>
									<a href="<c:url value="/documentosanexos/${processo.idProcesso}/documentos"/>">Documentos Anexados</a>
								</p>
								<p>
									<a href="<c:url value="/processojuntado/juntarprocesso/${processo.idProcesso}"/>">Juntar Processo</a>
								</p>
							</div>
						</div>
					</div>
				</fieldset>
			</fieldset>
			<a href="<c:url value="/processo/processosetor" />" class="btn btn-large pull-right">Voltar</a>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>