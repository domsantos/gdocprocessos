	$(document).ready(function() {
			$(" input[type='radio']").click(function() {
					if ($(this).val() == 1) {
						$("#divporrota").show('slow');
						$("#divsemrota").hide('slow');
					} else {
						$("#divsemrota").show('slow');
						$("#divporrota").hide('slow');
					}
			});
			$('#entidades') .change(function() {
				$('#btsemrota').attr('checked', true);
				$("#divporrota").hide('slow');
				var entiSelec = $(this).val();
				if ($(this).val() != "0") {
					$.ajax({
						url : "/gdocprocessos/tramite/listasetores.json",
						data : {
							id : entiSelec
						},
						type : 'get',
						dataType : 'json',
						success : function(setores) {
							$('#setores').empty();
							$('#setores').append('<option value="0">SELECIONE..</option>');
							for ( var i = 0; i < setores.length; i++) {
								$('#setores').append('<option value="' + setores[i].cdSetor + '">' + setores[i].nmSetor + '</option>');
							}
						}
					});
				}
			});
	});