$(document).ready(function(){
		$("#pesqavancada").hide();
		
		$("#avancado").change(function(){
			if($("#avancado").is(":checked")){
				$.ajax({
					url : "/gdocprocessos/processo/listatiposprocesso.json",
					data : {
						},
					type : 'get',
					dataType : 'json',
					success : function(tipos) {
						$('#tpprocs').empty();
						$('#tpprocs').append('<option value="0">SELECIONE..</option>');
						for ( var i = 0; i < tipos.length; i++) {
							$('#tpprocs').append('<option value="' + tipos[i].cdTipoprocesso + '">' + tipos[i].nmTipoprocesso+ '</option>');
						}
					}
				});
				
				$.ajax({
					url : "/gdocprocessos/processo/listaentidades.json",
					data : {
						},
					type : 'get',
					dataType : 'json',
					success : function(entidades) {
						$('#entidades').empty();
						$('#entidades').append('<option value="0">SELECIONE..</option>');
						for ( var i = 0; i < entidades.length; i++) {
							$('#entidades').append('<option value="' + entidades[i].idEntidade + '">' + entidades[i].dsEntidade+ '</option>');
						}
					}
				});
				
				$("#pesqavancada").show('slow');	
				
				}else{
					$("#pesqavancada").hide('slow');	
				}
		});
	});