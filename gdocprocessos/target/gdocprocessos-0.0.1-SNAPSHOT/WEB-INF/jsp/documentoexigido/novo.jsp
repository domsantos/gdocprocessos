<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Novo Documento</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Novo Documento</legend>
				<div class="row">
					<div class="panelform span4 offset4">
						<c:if test="${not empty errors}">
							<div class="alert">
								<c:forEach var="error" items="${errors}">
									<strong>${error.category} </strong> ${error.message}<br />
								</c:forEach>
							</div>
						</c:if>
						<form action="<c:url value="/documentoexigido/cadastrar" />" method="post">
							<table>
								<tr>
									<td>Documento*:</td>
									<td><input type="text"
										name="tblDocumentosexigidos.nmDocumentoexigido" maxlength="40"
										size="40" /></td>
								</tr>
								<tr>
									<td>Descri��o:</td>
									<td><input type="text"
										name="tblDocumentosexigidos.dsDocumentoexigido" maxlength="100"
										size="100" /></td>
								</tr>
								<tr>
									<td colspan="2"> <input type="submit" value="Cadastrar" class="btn" /></td>
								</tr>
							</table>
						</form>
					</div>
				</div>				
				<a href="<c:url value="/docsexigidos" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>