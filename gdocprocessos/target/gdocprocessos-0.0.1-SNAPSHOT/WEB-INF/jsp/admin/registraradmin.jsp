<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>REGISTRAR ADMINISTRADOR</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
<script type="text/javascript"
	src="<c:url  value="/js/jquery-1.6.1.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url  value="/js/jquery.maskedinput-1.3.min.js"/>"></script>
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>REGISTRAR ADMINISTRADOR</legend>
				<div class="row">
					<div class="panelform span6 offset3">
						<c:if test="${not empty errors}">
							<div class="alert">
								<c:forEach var="error" items="${errors}">
									<strong>${error.category} </strong> ${error.message}<br />
								</c:forEach>
							</div>
						</c:if>
						<form action="<c:url value="/admin/registrar" />" method="post">
							<table>
								<tr>
									<td>Usu�rio:</td>
									<td><select name="usuario.idUsuario" class="span3">
											<option value="0">SELECIONE..</option>
											<c:forEach var="tblUsuario" items="${tblUsuarioList}">
												<option value="${tblUsuario.idUsuario}">${tblUsuario.nmUsuario}</option>
											</c:forEach>
									</select></td>
								</tr>
								<tr>
									<td>N�vel de Administra��o:</td>
									<td><select name="statusAdmin" class="span3">
											<option value="">SELECIONE..</option>
											<option value="${adminEntidade}">ADMIN. DE ENTIDADE</option>
											<option value="${adminMaster}">MASTER ADMIN.</option>
									</select></td>
								</tr>
								<tr>
									<td colspan="2"><input type="submit" value="Cadastrar"
										class="btn" /></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</fieldset>
			<a href="<c:url value="/menuadmin" />" class="btn btn-large pull-right">Voltar</a>
		</div>
		
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>