<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Editar Ufir</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
	
<script type="text/javascript"
	src="<c:url value="/js/jquery-1.6.1.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/priceformat/priceformat.1.7.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/priceformat/priceformat.1.7.min.js"/>"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#vlr").priceFormat({
			prefix : "",
			centsSeparator : ",",
			thousandsSeparator : "",
			centsLimit : 5
		});

		$("input[name='tblUfir.nrAno']").bind("keyup blur focus", function(e) {
	    	   e.preventDefault();
	           var expre = /[A-Za-z\.\�\�\@\`\?\^\~\'\"\!\?\#\$\%\?\�\_\+\=\.\,\-\:\;\<\>\|\�\�\�\]\[\{\}\\ \)\(\*\&\/\\]/g;
	    
	           // REMOVE OS CARACTERES DA EXPRESSAO ACIMA
	           if ($(this).val().match(expre))
	               $(this).val($(this).val().replace(expre,''));
	       });
	});
</script>	
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
		<fieldset>
			<legend>Editar UFIR</legend>
			<div class="row">
				<div class="panelform span4 offset4">
					<c:if test="${not empty errors}">
							<div class="alert">
								<c:forEach var="error" items="${errors}">
									<strong>${error.category} </strong> ${error.message}<br />
								</c:forEach>
							</div>
						</c:if>
					<form action="<c:url value="/ufir/salvar" />" method="post">
						<table>
							<tr>
								<td>Ano da Ufir*:</td>
								<td>
									<input type="text" name="tblUfir.nrAno" value="${tblUfir.nrAno}"/>
									<input type="hidden" name="tblUfir.idUfir" value="${tblUfir.idUfir}" />
								</td>
							</tr>
							<tr>
								<td>Valor da Ufir*:</td>
								<td><input id="vlr" type="text" name="valorufir" value="${tblUfir.vlUfir}" /></td>
							</tr>
							<tr>
								<td colspan="2"><input type="submit" value="Salvar" class="btn"/></td>
							</tr>
						</table>
					</form>
				</div>
			</div>
			<a href="<c:url value="/ufir" />" class="btn btn-large pull-right">Voltar</a>
		</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>