<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DOCUMENTOS</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>DOCUMENTOS</legend>
				<div class="row">
					<div class="well span4 offset4">
						<h2 class="pull-text-center" >${numrota.nmRota}</h2>
						<input type="hidden" id="numrota" value="${numrota.cdRota}" />
					</div>
				</div>
				<div class="row">
					<div class="well span6 offset3">
						<table class="table">
							<thead>
								<tr>
									<td colspan="2">Documento</td>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="dto" items="${lista}">
									<tr>
										<td>${dto.nome}</td>
										<c:if test="${dto.situacao eq nao}" >
											<td>
												<a href="<c:url value="/rota/adicionadocumento/${numrota.cdRota}/${dto.id}" />">Adicionar Documento</a>
											</td>
										</c:if>
										<c:if test="${dto.situacao eq sim}" >
											<td>
												<a href="<c:url value="/rota/removedocumento/${numrota.cdRota}/${dto.id}" />">Remover Documento</a>
											</td>
										</c:if>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<a href="<c:url value="/rota/editar/${numrota.cdRota}" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>