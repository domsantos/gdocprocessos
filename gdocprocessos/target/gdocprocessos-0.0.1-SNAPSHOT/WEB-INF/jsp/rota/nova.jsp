<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Nova Rota</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css" />
<script type="text/javascript"
	src="<c:url value="/js/jquery-1.6.1.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/rota/nova.js"/>"></script>
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Nova Rota</legend>
				<div class="row">
				<div class="panelform span4 offset4">
					<c:if test="${not empty errors}">
						<div class="alert">
							<c:forEach var="error" items="${errors}">
								<strong>${error.category} </strong> ${error.message}<br />
							</c:forEach>

						</div>
					</c:if>
					<form method="post" action="<c:url value="/rota/cadastrarrota" />">
						<table>
							<tr>
								<td>Nome*:</td>
								<td><input name="tblRotas.nmRota" size="40" maxlength="40"  type="text" /></td>
							</tr>
							<tr>
								<td>Tipo do Processo*:</td>
								<td>${tipoProcesso.nmTipoprocesso} <input type="hidden" name="tblRotas.cdTipoprocesso.cdTipoprocesso" value="${tipoProcesso.cdTipoprocesso}" />
								
								</td>
							</tr>
							<tr>
								<td>Setor*:</td>
								<td><select id="setores" name="tblRotas.cdSetor.cdSetor">
										<option value="">SELECIONE..</option>
										<c:forEach var="tblSetor" items="${tblSetorList}">
											<option value="${tblSetor.cdSetor}">${tblSetor.nmSetor}</option>
										</c:forEach>
								</select></td>
							</tr>
							<tr>
								<td>Prioridade*:</td>
								<td><select id="prioridade" name="tblRotas.cdPrioridade">
										<option value="0">SELECIONE..</option>
										<option value="${prioridadealta}">ALTA</option>
										<option value="${prioridadenormal}">NORMAL</option>
										<option value="${prioridadebaixa}">BAIXA</option>
								</select></td>
							</tr>
							<tr>
								<td>Rota Paralela*:</td>
								<td><select id="prioridade" name="tblRotas.stRotaParalela">
										<option value="0">SELECIONE..</option>
										<option value="${paralelasim}">SIM</option>
										<option value="${paralelanao}">N&Atilde;O</option>
								</select></td>
							</tr>
							<tr>
								<td>Ordem da Rota*:</td>
								<td><input name="tblRotas.nrOrdemrota" type="text" class="number" value="${nrOrdem}" /></td>
							</tr>
							<tr>
								<td>Prazo da Rota (em dias):</td>
								<td><input name="tblRotas.nrPrazoRota" type="text" class="number" /></td>
							</tr>
							<tr>
								<td><input type="submit" value="Cadastrar Rota" class="btn" /></td>
							</tr>
						</table>
					</form>
				</div>
				</div>
				<a href="<c:url value="/rota/${tipoProcesso.cdTipoprocesso}" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>