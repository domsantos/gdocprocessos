<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="pagination pagination-centered pagination-small">
		<ul class="">
			<c:forEach var="pagina" items="${paginationList}">
				<c:if test="${pagina eq first}">
					<li 
						<c:if test="${pagina eq pagSelec}">class='active'</c:if> 
					>
						<a href='<c:url value="${link}${pagina}" />' >In�cio</a>
					</li>
				</c:if>
				<c:if test="${!(pagina  eq first)}">
					<c:if test="${!(pagina eq last)}">
						<c:if test="${pagina eq '...' }">
							<li>
								<a href='#' >${pagina}</a>
							</li>
						</c:if>
						<c:if test="${pagina != '...' }">
							<li 
								<c:if test="${pagina eq pagSelec}">class='active'</c:if> 
							>
								<a href="<c:url value="${link}${pagina}" />" >${pagina}</a>
							</li>
						</c:if>
					</c:if>
				</c:if>			
				<c:if test="${pagina eq last}">
					<li 
						<c:if test="${pagina eq pagSelec}">class='active'</c:if> 
					>
						<a href='<c:url value="${link}${pagina}" />' >Fim</a>
					</li>
				</c:if>
			</c:forEach>
		</ul>
</div>