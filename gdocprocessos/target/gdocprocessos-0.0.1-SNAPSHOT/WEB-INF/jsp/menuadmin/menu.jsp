<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>MENU DO ADMINISTRADOR</title>
<script type="text/javascript"
	src="<c:url  value="/js/jquery-1.10.2.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url  value="/js/bootstrap.js"/>"></script>
	
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css" />

</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<br />
		<div class="container">
			<div class="navbar">
				<div class="navbar-inner">
					<ul class="nav">

						<li><a href="<c:url value="/docsexigidos" />">Documentos Exigidos</a></li>
						<li><a href="<c:url value="/tipoprocesso" />">Tipos de Processo</a></li>
						
						<!--  Financeito -->
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"> Financeiro <b class="caret"></b>
								<ul class="dropdown-menu">
									<li><a href="<c:url value="/tributo" />">Tributos</a></li>
									<li><a href="<c:url value="/ufir" />">Ufir</a></li>
									<li><a href="<c:url value="/usoatividade" />">Uso Atividade</a></li>
								</ul>
						</a></li>
						
						<!--  Seguran�a -->
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"> Seguran�a <b class="caret"></b>
								<ul class="dropdown-menu">
									<li><a href="<c:url value="/entidade" />">Entidades</a></li>
									<li><a href="<c:url value="/usuario" />">Usuarios</a></li>
									<li><a href="<c:url value="/lotacao" />">Lota��es</a></li>
									<li><a href="<c:url value="/perfil" />">Perfils</a></li>
									<li><a href="<c:url value="/admin/novo" />">Cadastrar Administrador</a></li>
									<li><a href="<c:url value="/admin/registraradmin" />">Registra Administrador</a></li>
									<li><a href="<c:url value="/localarquivo" />">Local de Arquivamento de Processos</a></li>
								</ul>
						</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>