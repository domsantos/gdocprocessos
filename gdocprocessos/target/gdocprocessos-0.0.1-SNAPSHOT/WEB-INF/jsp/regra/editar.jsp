<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Editar Regra</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Editar Regra ${tblRegras.nmRegra}</legend>
				<div class="row">
					<div class="panelform span4 offset4">
						<form action="<c:url value="/regra/salvar" />" method="post">
							<table>
							<tr>
									<td>Tipo do Processo:</td>
									<td>${regra.cdTipoprocesso.nmTipoprocesso} <input type="hidden" name="tblRegras.cdTipoprocesso.cdTipoprocesso" value="${regra.cdTipoprocesso.cdTipoprocesso}" /></td>
								</tr>
								<tr>
									<td>Nome da Regra:</td>
									<td><input type="text" name="tblRegras.nmRegra" value="${regra.nmRegra}" maxlength="40" /> <input type="hidden" name="tblRegras.cdRegra" value="${regra.cdRegra}"/></td>
								</tr>
								<tr>
									<td>Descri��o*:</td>
									<td><textarea rows="5" cols="60" name="tblRegras.dsRegra" >${regra.dsRegra}</textarea></td>
								</tr>
								<tr>
									<td colspan="2"><input type="submit" value="Salvar"
										class="btn" /></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
				<a href="<c:url value="/regra/${regra.cdTipoprocesso.cdTipoprocesso}" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>