<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Editar Entidade</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">

			<fieldset>
				<legend>Editar Entidade</legend>
				<div class="row">
				<div class="panelform span4 offset4">
					<form action="<c:url value="/entidade/salvaredicao"  />"
						method="post">
						<c:forEach var="error" items="${errors}">
    			${error.category} - ${error.message}<br />
						</c:forEach>
						<table>
							<tr>
								<td><label>Nome:</label></td>
								<td><input name="entidade.dsEntidade" type="text"
									value="${entidade.dsEntidade}" maxlength="50" size="40" /><input
									type="hidden" name="entidade.idEntidade"
									value="${entidade.idEntidade}" /></td>

							</tr>
							<tr>
								<td><label>C�digo da Entidade:</label></td>
								<td><input name="entidade.cdEntidade" type="text" maxlength="2" value="${entidade.cdEntidade}"/></td>
							</tr>
							<tr>
								<td><label>Nome Entidade (Capa do Processo)*:</label></td>
								<td><input name="entidade.dsEntidadeExtenso" type="text" maxlength="70" value="${entidade.dsEntidadeExtenso}"/></td>
							</tr>
							<tr>
								<td><input type="submit" value="Salvar" class="btn" /></td>
							</tr>
						</table>
					</form>
				</div>
				</div>
				<a href="<c:url value="/entidade" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>