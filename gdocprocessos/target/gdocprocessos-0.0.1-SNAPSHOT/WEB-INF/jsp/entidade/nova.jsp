<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>NOVA ENTIDADE here</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Nova Entidade</legend>
				<div class="row">
				<div class="panelform span4 offset4">
					<c:if test="${not empty errors}">
						<div class="alert">
							<c:forEach var="error" items="${errors}">
								<strong>${error.category} </strong> ${error.message}<br />
							</c:forEach>
						</div>
					</c:if>	
					<form action="<c:url value="/entidade/cadastrar"  />" method="post">
						<table>
							<tr>
								<td><label>Nome:</label></td>
								<td><input name="entidade.dsEntidade" type="text" maxlength="50"/></td>
							</tr>
							<tr>
								<td><label>C�digo da Entidade:</label></td>
								<td><input name="entidade.cdEntidade" type="text" maxlength="2"/></td>
							</tr>
							<tr>
								<td><label>Nome Entidade (Capa do Processo)*:</label></td>
								<td><input name="entidade.dsEntidadeExtenso" type="text" maxlength="70"/></td>
							</tr>
							<tr>
								<td><input type="submit" value="Cadastrar" class="btn" /></td>
							</tr>
						</table>
					</form>
				</div>
				</div>
				<a href="<c:url value="/entidade" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>	
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>