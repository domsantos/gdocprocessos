<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://functions" prefix="myfunc"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EDITAR PERFIL</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css" />
</head>

<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
					<legend>Perfil ${p.dsPerfil}</legend>
					<div class="row">
					<form action="<c:url value="/perfil/salvar"  />" method="post">
						<div class="panelform span4 offset4">
							<c:if test="${not empty errors}">
								<div class="alert">
									<c:forEach var="error" items="${errors}">
										<strong>${error.category} </strong> ${error.message}<br />
									</c:forEach>
								</div>
							</c:if>
							<table>
								<tr>
									<td>Descri��o:</td>
									<td><input type="text" name="perfil.dsPerfil" value="${p.dsPerfil}" maxlength="30" />
										<input type="hidden" name="perfil.cdPerfil"
										value="${p.cdPerfil}" /></td>
								</tr>
							</table>
						</div>
						<div class="panelform span10 offset1">
							<fieldset>
								<legend>APLICA��ES</legend>
									<c:forEach var="tblAplicacoes" items="${tblAplicacoesList}"
										varStatus="s">
												<div class="span3 float-left">
											<input type="checkbox" name="lista[${s.index}]" value="${tblAplicacoes.cdAplicacao}"
												<c:if test="${myfunc:contem(p.tblAplicacoesList ,tblAplicacoes)}">  
               										checked="checked"  
        										</c:if> 
        									/>
												${tblAplicacoes.dsAplicacao}
												</div>
									</c:forEach>
							</fieldset>
							<br />
							<p class="pull-text-center"><input class="btn" type="submit" value="Salvar"></p>
						</div>
					</form>
					</div>
					<a href="<c:url value="/perfil" />" class="btn btn-large pull-right">Voltar</a>
				</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>