<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PROCESSOS EM ATRASO</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
<script type="text/javascript"
	src="<c:url  value="/js/jquery-1.6.1.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url  value="/js/relatorios/processosatrasados.js"/>"></script>
<script type="text/javascript"
	src="<c:url  value="/js/jquery.maskedinput-1.3.min.js"/>"></script>
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Processos em Atraso</legend>
				<div class="row">
					<div class="panelform span6 offset3">
						<div id="erro">
						</div>
						<p>Per�odo do Relat�rio:</p>
						<p>
							Data Inicial: <input type="text" id="datainicial" />
						</p>
						<p>
							Data Final: <input type="text" id="datafinal" />
						</p>
						<p>
							<input id="link" type="button" 
								onclick="javascript:gera('<c:url value="/relatorios/processosematraso"/>');"
								class="btn" value="Gerar Relat�rio"/>
						</p>
					</div>
				</div>
				<a href="<c:url value="/relatorios/menurelatorio"/>"  class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>