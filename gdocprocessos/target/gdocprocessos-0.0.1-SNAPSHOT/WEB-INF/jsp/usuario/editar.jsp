<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
<script type="text/javascript"
	src="<c:url  value="/js/jquery-1.6.1.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url  value="/js/jquery.maskedinput-1.3.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url  value="/js/usuario/novo.js"/>"></script>
<title>EDITAR USUARIO</title>
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp" ></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Editar Usu�rio ${tblUsuario.nmUsuario}</legend>
				<div class="row">
					<div class="panelform span6 offset3">
						<c:if test="${not empty errors}">
							<div class="alert">
								<c:forEach var="error" items="${errors}">
									<strong>${error.category} </strong> ${error.message}<br />
								</c:forEach>
							</div>
						</c:if>
						<form action="<c:url value="/usuario/salvarusuario" />"
							method="post">
							<table>
								<tr>
									<td>Matr�cula:</td>
									<td><input id="nrMatricula" type="text"
										name="usuario.nrMatricula" value="${tblUsuario.nrMatricula}" /></td>
								</tr>
								<tr>
									<td>Nome:</td>
									<td><input type="text" name="usuario.nmUsuario"
										value="${tblUsuario.nmUsuario}" maxlength="50"/></td>
								</tr>
								<tr>
									<td>Perfil:</td>
									<td><select name="usuario.cdPerfil.cdPerfil" class="span4">
											<option>SELECIONE..</option>
											<c:forEach var="tblPerfil" items="${tblPerfilList}">
												<option value="${tblPerfil.cdPerfil}"
													<c:if test="${tblUsuario.cdPerfil.cdPerfil eq tblPerfil.cdPerfil}">selected</c:if>>${tblPerfil.dsPerfil}</option>
											</c:forEach>
									</select></td>
								</tr>
								<tr>
									<td><input type="hidden" name="usuario.idUsuario"
										value="${tblUsuario.idUsuario}" /> <input type="submit" class="btn"
										value="Cadastrar" /></td>
								</tr>
							</table>

						</form>
					</div>
				</div>
				<a href="<c:url value="/usuario" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>