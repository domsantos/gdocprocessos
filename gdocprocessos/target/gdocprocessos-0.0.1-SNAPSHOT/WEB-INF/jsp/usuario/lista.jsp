<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://functions" prefix="myfunc"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>LISTA USUARIOS</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Usu�rios</legend>
				<a class="btn" href="<c:url value="/usuario/novo" />"><span>Novo Usu�rio</span></a>
				<div class="row">
					<div class="well span6 offset3">
					<table class="table">
						<thead>
							<tr>
								<td>C�d.</td>
								<td>Matr�cula</td>
								<td colspan="2">Usu�rio</td>
							</tr>
						</thead>
					
						<c:forEach var="tblUsuario" items="${tblUsuarioList}">
							<tr>
								<td>${tblUsuario.idUsuario}</td>
								<td>
									${myfunc:maskMatricula(tblUsuario.nrMatricula)}
								</td>
								<td>${tblUsuario.nmUsuario}</td>
								<td><a
									href="<c:url  value="/usuario/editar/${tblUsuario.idUsuario}" />">Editar</a></td>
							</tr>
						</c:forEach>
					</table>
					<jsp:include page="/WEB-INF/jsp/layout/pags.jsp" ></jsp:include>	
					</div>
				</div>
				<a href="<c:url value="/menuadmin" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>