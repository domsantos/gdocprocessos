<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Editar Setor</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>SETOR</legend>
				<div class="row">
					<div class="panelform span3 offset4">
						<form action="<c:url value="/setor/salvaedicao"  />" method="post">
							<c:if test="${not empty errors}">
							<div class="alert">
								<c:forEach var="error" items="${errors}">
									<strong>${error.category} </strong> ${error.message}<br />
								</c:forEach>
							</div>
						</c:if>	
							<table>
								<tr>
									<td>Entidade:</td>
								</tr>
								<tr>
									<td>${tblSetor.idEntidade.dsEntidade} <input type="hidden" value="${tblSetor.idEntidade}" name="tblSetor.idEntidade" />  </td>
								</tr>
								<tr>
									<td>Nome do Setor*:</td>
								</tr>
								<tr>
									<td><input type="hidden" name="tblSetor.cdSetor"
										value="${tblSetor.cdSetor}" /> <input name="tblSetor.nmSetor" type="text" maxlength="50"
										value="${tblSetor.nmSetor}" /></td>
								</tr>
								<tr>
									<td>Descri��o:</td>
								</tr>
								<tr>
									<td><input name="tblSetor.dsSetor" type="text"
										value="${tblSetor.dsSetor}" maxlength="120" /></td>
								</tr>
																<tr>
									<td>
										<input type="submit" value="Salvar"  class="btn" />
									</td>
								</tr>
							</table>
						</form>
					</div>
				</div>
				<a href="<c:url value="/setor/${entidade.idEntidade}" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>