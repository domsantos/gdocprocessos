<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>LISTA</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">

			<fieldset>
				<legend>Setor</legend>
				<div class="row">
					<div class="well span8 offset2">
						<a class="btn" href="<c:url value="/setor/novo/${entidade.idEntidade}" />"><span>Novo Setor</span></a>
					
						<table class="table">
							<thead>
								<tr>
									<td>C�d. Setor</td>
									<td>Setor</td>
									<td colspan="2">Descri��o</td>
									
								</tr>
							</thead>
							<c:forEach var="tblSetor" items="${tblSetorList}">
								<tr>
									<td>${tblSetor.cdSetor}</td>
									<td>${tblSetor.nmSetor}</td>
									<td>${tblSetor.dsSetor}</td>
									<td><a
										href="<c:url  value="/setor/editar/${tblSetor.cdSetor}" />">Editar</a></td>
								</tr>
							</c:forEach>
						</table>
						<jsp:include page="/WEB-INF/jsp/layout/pags.jsp" ></jsp:include>
					</div>
				</div>
				<a href="<c:url value="/entidade" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>