<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PROCESSOS A RECEBER</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
	<script type="text/javascript"
	src="<c:url  value="/js/jquery-1.10.2.min.js"/>"></script>
	<script type="text/javascript"
	src="<c:url  value="/js/bootstrap.js"/>"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			
		});

		function receber(url){
			var aux = url.split("@");
			var mensagem = "Deseja Receber o Processo "+aux[1]+"?";
			$("#msg").html(mensagem);
			$("#btestorno").attr('href', aux[0]);
			$("#myModal").modal('show');						
		}
	</script>
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend> Receber Tr�mite</legend>
				<div class="row">
					<div class="well span10 offset1">
					<c:if test="${not empty mensagem}">
							<div class="alert alert-success">
								<strong>${mensagem}</strong>
							</div>
						</c:if>
						<table class="table">
							<thead>
								<tr>
									<td>Processo</td>
									<td>Interessado</td>
									<td colspan="2">Tipo Processo</td>
								</tr>
							</thead>
							<tbody>
								<c:if test="${empty tblProcessosList}">
									<tr>
										<td colspan="4"><h2>Nenhum Processo a Receber</h2></td>
									</tr>
								</c:if>
								<c:forEach var="tblProcessos" items="${tblProcessosList}">
									<tr>
										<td>${tblProcessos.nrProcesso}/${tblProcessos.nrAno}</td>
										<td>${tblProcessos.idInteressado.nmInteressado}</td>
										<td>${tblProcessos.cdTipoprocesso.nmTipoprocesso}</td>
										<td><a class="btn"
											href='<c:url value="/processo/${tblProcessos.idProcesso}/detalhe" />'>Detalhar
												Processo</a> | 
												<button id="open" onclick="javascript:receber('<c:url value="/tramite/receberprocesso/${tblProcessos.idProcesso}"/>@${tblProcessos.nrProcesso}/${tblProcessos.nrAno}')" class="btn" data-toggle="modal">Receber Processo</button>
												
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<jsp:include page="/WEB-INF/jsp/layout/pags.jsp"></jsp:include>
					</div>
					
					<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
				<h3 id="myModalLabel">Receber Processo</h3>
			</div>
			<div class="modal-body">
				<p id="msg"></p>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Voltar</button>
				<a id="btestorno" href="<c:url value="/tramite/receberprocesso/"/>" class="btn btn-large" >Receber</a>
			</div>
		</div>
		
				</div>
			</fieldset>
			<a href="<c:url value="/home" />" class="btn btn-large pull-right">Voltar</a>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>