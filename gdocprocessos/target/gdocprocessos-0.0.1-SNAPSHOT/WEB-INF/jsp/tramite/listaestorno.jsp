<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>LISTA PROCESSOS A ESTORNAR</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
	<script type="text/javascript"
	src="<c:url  value="/js/jquery-1.10.2.min.js"/>"></script>
	<script type="text/javascript"
	src="<c:url  value="/js/bootstrap.js"/>"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			
		});

		function estornar(url){
			var aux = url.split("@");
			var mensagem = "Deseja Estornar o �ltimo Tr�mite do processo "+aux[1]+"?";
			$("#msg").html(mensagem);
			$("#btestorno").attr('href', aux[0]);
			$("#myModal").modal('show');				
		}
	</script>
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Processos a Estornar</legend>
				<div class="row">
					<div class="well span6 offset3">
						<c:if test="${not empty mensagem}">
							<div class="alert alert-success">
								<strong>${mensagem}</strong>
							</div>
						</c:if>
						<table class="table">
						<thead>
							<tr>
								<td>Processo</td>
								<td></td>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="processo" items="${tblProcessosList}">
								<tr>
									<td>${processo.nrProcesso}/${processo.nrAno}</td>
									<td>
<%-- 										<a href="<c:url value="/tramite/estornartramite/${processo.idProcesso}" />">Estornar �ltimo Tr�mite</a> --%>
										<button id="open" onclick="javascript:estornar('<c:url value="/tramite/estornartramite/${processo.idProcesso}" />@${processo.nrProcesso}/${processo.nrAno}')" class="btn" data-toggle="modal">Estornar �ltimo Tr�mite</button>
									</td>
								</tr>
							</c:forEach>
							</tbody>
						</table>
						<jsp:include page="/WEB-INF/jsp/layout/pags.jsp"></jsp:include>
					</div>
				</div>
					<a href="<c:url value="/home" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
		
		<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
				<h3 id="myModalLabel">Estornar Tr�mite</h3>
			</div>
			<div class="modal-body">
				<p id="msg"></p>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Voltar</button>
				<a id="btestorno" href="<c:url value="/tramite/estornartramite/" />" class="btn btn-large" >Estornar</a>
			</div>
		</div>

	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>