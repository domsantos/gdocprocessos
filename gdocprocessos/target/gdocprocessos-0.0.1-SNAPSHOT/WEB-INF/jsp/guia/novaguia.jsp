<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>NOVA GUIA</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
	<script type="text/javascript"
	src="<c:url value="/js/jquery-1.6.1.min.js"/>"></script>
	<script type="text/javascript"
	src="<c:url value="/js/guia/novaguia.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/priceformat/priceformat.1.7.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/priceformat/priceformat.1.7.min.js"/>"></script>
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Nova DAM</legend>
				<div class="row">
					<div class="panelform span6 offset3">
						<h2>${processo.nrProcesso}/${processo.nrAno}</h2>
						<c:if test="${not empty errors}">
							<div class="alert">
								<c:forEach var="error" items="${errors}">
									<strong>${error.category} </strong> ${error.message}<br />
								</c:forEach>
							</div>
						</c:if>
						<form method="post" action="<c:url value="/guia/gravarguia"/>" >
							<table>
								<tr>
									<td>Uso Atividade*:</td>
									<td><select id="usoatividade" name="usoatividade" class="span4">
											<option value="0">SELECIONE..</option>
											<c:forEach var="tblUsoatividade"
												items="${tblUsoatividadeList}">
												<option value="${tblUsoatividade.cdUsoatividade}">${tblUsoatividade.dsUsoatividade}</option>
											</c:forEach>
									</select></td>
								</tr>
								<tr id="linhavlr" >
									<td>Valor da UFIR:</td>
									<td id="ufir"></td>
								</tr>
								<tr>
									<td>Valor da Guia*:</td>
									<td><input id="vlDoc" type="text"
										name="valorDocumento" /></td>
								</tr>
								<tr>
									<td>Dias para o vencimento*:</td>
									<td><input id="diasVendimento"type="text" name="diasVencimento" value="${diasVencimento}" class="span1" /></td>
								</tr>
								<tr>
									<td>N�m. de Parcelas*:</td>
									<td><input id="numparcelas"type="text" name="tblValorDocumento.nrParcelas" class="span1" /></td>
								</tr>
								<tr class="resumo">
									<td>Resumo:</td>
								</tr>
								<tr class="resumo">
									<td>Valor Total:</td>
									<td id="vltotal"></td>
								</tr>
								<tr class="resumo">
									<td>Parcela:</td>
									<td id="parc"></td>
								</tr>
								<tr>
									<td>Descri��o*:</td>
									<td><textarea id="" name="tblValorDocumento.dsPagamento"></textarea></td>
								</tr>
								<tr>
									<td colspan="2"><input type="submit" value="Gerar DAM" class="btn" /><input
										type="hidden" value="${processo.idProcesso}"
										name="tblValorDocumento.idProcesso.idProcesso" /></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
				<a href="<c:url value="/processo/${processo.idProcesso}/resumo" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>