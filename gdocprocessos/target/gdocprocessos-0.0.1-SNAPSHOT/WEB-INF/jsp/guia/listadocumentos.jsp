<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>GUIAS DO PROCESSO</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Guias do Processo ${processo.nrProcesso}/${processo.nrAno}</legend>
				<div class="row">
				<div class="well span6 offset3">
					<table class="table">
						<thead>
							<tr>
								<td>Guia</td>
								<td>N�m. de Parcelas</td>
								<td>Valor Total</td>
								<td></td>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="tblValorDocumento" items="${tblValorDocumentoList}">
								<tr>
									<td>${tblValorDocumento.nrGuia}</td>
									<td>${tblValorDocumento.nrParcelas}</td>
									<td><fmt:formatNumber minFractionDigits="2" maxFractionDigits="2"  value=" ${tblValorDocumento.vlDocumento}"/></td>
									<td>
										<a href="<c:url value="/guia/${tblValorDocumento.cdValorDocumento}/listarparcelas" />">Detalhar</a> 
									</td>
								</tr>							
							</c:forEach>
						</tbody>
					</table>
				</div>
				</div>
				<a href="<c:url value="/processo/${processo.idProcesso}/resumo" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>