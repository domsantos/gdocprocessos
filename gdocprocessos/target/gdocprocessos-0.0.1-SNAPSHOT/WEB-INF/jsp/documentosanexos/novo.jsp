<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>NOVO ANEXO</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
		<fieldset>
			<legend>Novo Anexo</legend>
			<div class="row">
				<div class="panelform span6 offset3">
				<c:if test="${not empty errors}">
							<div class="alert">
								<c:forEach var="error" items="${errors}">
									<strong>${error.category} </strong> ${error.message}<br />
								</c:forEach>
							</div>
						</c:if>
					<form method="post" action="<c:url value="/documentosanexos/cadastrarnovoanexo" />" enctype="multipart/form-data">

						<p>Processo: ${tblProcessos.nrProcesso}/${tblProcessos.nrAno}
						<p>Documento*: <input type="text" class="span4" maxlength="60" name="tblDocumentosanexos.nmDocumentoanexo"/></p>
						<p>Esp�cie: <select id="especie" name="especie">
										<option value="0">SELECIONE..</option>
										<c:forEach var="especie" items="${especieList}" >
											<option value="${especie.idEspecie}">${especie.dsEspecie}</option>
										</c:forEach>
								</select></p>
								
						<p>Imagem: <input type="file" name="arquivo" > </p>
						<p><input type="submit" value="Anexar" class="btn"/> <input type="hidden" name="tblDocumentosanexos.idProcesso.idProcesso" value="${tblProcessos.idProcesso}" /></p>
						
					</form>
				</div>
			</div>
		</fieldset>
		<a href="<c:url value="/documentosanexos/${tblProcessos.idProcesso}/documentos" />" class="btn btn-large pull-right">Voltar</a>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>