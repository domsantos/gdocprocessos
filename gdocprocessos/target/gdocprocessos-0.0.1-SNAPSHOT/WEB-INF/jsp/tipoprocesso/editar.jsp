<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tipo de Processo</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
</head>
<body>
	<div class="conteudo">
	<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Tipo de Processo ${tblTipoprocesso.nmTipoprocesso}</legend>
				<div class="row">
				<div class="panelform span4 offset4">
				<c:if test="${not empty errors}">
							<div class="alert">
								<c:forEach var="error" items="${errors}">
									<strong>${error.category} </strong> ${error.message}<br />
								</c:forEach>
							</div>
						</c:if>
				<form method="post"
					action="<c:url value="/tipoprocesso/salvaredicao"/>">
					<table>
						<tr>
							<td>Nome*:</td>
							<td><input name="tblTipoprocesso.nmTipoprocesso" type="text" maxlength="40"
								value="${tblTipoprocesso.nmTipoprocesso}" /></td>
						</tr>
						<tr>
							<td>Descri��o:</td>
							<td><input name="tblTipoprocesso.dsTipoprocesso" type="text" maxlength="120"
								value="${tblTipoprocesso.dsTipoprocesso}" /></td>
						</tr>
						<tr>
							<td>Confirma��o Presencial*:</td>
							<td><select name="tblTipoprocesso.stConfirmacaoPresencia">
									<option>SELECIONE..</option>
									<option value="S"
										<c:if test="${tblTipoprocesso.stConfirmacaoPresencia eq tpativo }">selected</c:if>>SIM</option>
									<option value="N"
										<c:if test="${tblTipoprocesso.stConfirmacaoPresencia eq tpdesativo }">selected</c:if>>N�O</option>
							</select></td>
						</tr>
						<tr>
							<td>Abrir pela Internet*:</td>
							<td><select name="tblTipoprocesso.stAbrirInternet">
									<option>SELECIONE..</option>
									<option value="S"
										<c:if test="${tblTipoprocesso.stAbrirInternet eq tpativo }">selected</c:if>>SIM</option>
									<option value="N"
										<c:if test="${tblTipoprocesso.stAbrirInternet eq tpdesativo }">selected</c:if>>N�O</option>
							</select></td>
						</tr>
						<tr>
							<td><input type="hidden"
								name="tblTipoprocesso.cdTipoprocesso"
								value="${tblTipoprocesso.cdTipoprocesso}" /><input class="btn"
								type="submit" value="Salvar" /></td>
						</tr>
					</table>
				</form>
				</div>
				</div>
				<a href="<c:url value="/tipoprocesso" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>