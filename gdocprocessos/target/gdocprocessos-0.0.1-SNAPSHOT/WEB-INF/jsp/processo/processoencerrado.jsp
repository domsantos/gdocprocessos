<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PROCESSOS ENCERRADOS</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
</head>
<body>
	<div class="conteudo">
	<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Processos Encerrados</legend>
				<div class="row">
				<div class="well span6 offset3">
					<table class="table">
						<thead>
							<tr>
								<td>Processo</td>
								<td></td>
							</tr>
						</thead>
						<tbody>
							<c:if test="${empty tblProcessosList}">
								<tr>
									<td colspan="4"><h2>Nenhum Processo encontrado</h2></td>
								</tr>
							</c:if>
							<c:forEach var="tblProcessos" items="${tblProcessosList}">
								<tr>
									<td>${tblProcessos.nrProcesso}/${tblProcessos.nrAno}</td>
									<td><a href="<c:url value="/processo/arquivarprocesso/${tblProcessos.idProcesso}" />">Arquivar Processo</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<jsp:include page="/WEB-INF/jsp/layout/pags.jsp"></jsp:include>
				</div>
				</div>
					<a href="<c:url value="/home" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>