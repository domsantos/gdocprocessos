<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PESQUISAR PROCESSO</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
<script type="text/javascript"
	src="<c:url  value="/js/jquery-1.6.1.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url  value="/js/processo/pesquisaprocesso.js"/>"></script>
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Protocolar Documento</legend>
				<div class="row">
					<div class="panelform span6 offset3">
						<form action="<c:url value="/processo/salvarprotocolo" />" method="post">
							<c:if test="${not empty errors}">
								<div class="alert">
									<c:forEach var="error" items="${errors}">
										<strong>${error.category} </strong> ${error.message}<br />
									</c:forEach>
								</div>
							</c:if>
							<p>
							<fieldset>
								<label>Descri��o do Protocolo:</label>
								<input type="hidden" name="tblProcessos.cdTipoprocesso.cdTipoprocesso" value="999999"/> 
								<input type="hidden" name="tblProcessos.idInteressado.idInteressado" value="11"/>
								<textarea rows="5" cols="60" name="tblProcessos.dsProcesso" class="span5"></textarea>
								<p>
									<input class="btn" type="submit" value="Abrir Processo" />
								</p>  
							</fieldset>
						</form>
					</div>
				</div>
				<a href="<c:url value="/home" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>