<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DETALHE PROCESSO</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Detalhe</legend>
				<div class="row">
					<div class="span6 offset3 panelform">
						<p>Processo: <strong>${tblProcessos.nrProcesso}/${tblProcessos.nrAno} - ${tblProcessos.cdTipoprocesso.nmTipoprocesso}</strong> </p>
						<p>Interessado: <strong>${tblProcessos.idInteressado.nmInteressado}</strong></p>
						<p>Situa��o: <c:if test="${tblProcessos.stProcesso eq aberto}"><strong>ABERTO</strong></c:if>
									<c:if test="${tblProcessos.stProcesso eq fechado}"><strong>ARQUIVADO</strong> </c:if>
									<c:if test="${tblProcessos.stProcesso eq cancelado}"><strong>CANCELADO</strong></c:if>
									<c:if test="${tblProcessos.stProcesso eq encerrado}"><strong>ENCERRADO</strong></c:if>
						</p>
						<c:if test="${tblProcessos.stProcesso eq aberto}">
							<p>Em Andamento: <strong>${tempoProcesso}</strong> dias.</p>
						</c:if>
						<c:if test="${tblProcessos.stProcesso != aberto}">
							<p>Dura��o: <strong>${tempoProcesso}</strong> dias.</p>
						</c:if>
						<p>Descri��o do Processo: <strong>${tblProcessos.dsProcesso}</strong> </p>
						<p>Data de Abertura: <strong><fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${tblProcessos.dtAberturaProcesso}" /></strong> </p>
						<c:if test="${tblProcessos.stProcessoweb != aberto}">
							<p>Data de Encerramento: <strong><fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${tblProcessos.dtEncerramento}" /></strong> </p>
							<p>Despacho de Encerramento: <strong>${tblProcessos.dsEncerramento}</strong> </p>
						</c:if>
						<c:if test="${tblProcessos.stProcessoweb eq fechado}">
							<p>Data do Arquivo: <strong><fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${localArquivo.dtArquivamento}" /></strong> </p>
							<p>Descri��o do Arquivo: <strong>${localArquivo.dsArquivamento}</strong> </p>
							<p>Local do Arquivo: <strong><c:forEach var="local" items="${listalocalArquivo}">${local.dsEnderecoarquivamento} - </c:forEach></strong> </p>
						</c:if>
						<c:if test="${not empty processospai}">
							<p>Processo Juntado a Processos: 
								<c:forEach var="processo" items="${processospai}" >
									<a href="<c:url value="/processo/${processo.idProcessoPai.idProcesso}/detalhe" />">${processo.idProcessoPai.nrProcesso}/${processo.idProcessoPai.nrAno}</a> -
								</c:forEach>
								</p>
						</c:if>
						<c:if test="${not empty processosfilho}">
							<p>Processos Juntados a este Processo: 
								<c:forEach var="processo" items="${processosfilho}">
									<a href="<c:url value="/processo/${processo.idProcessoFilho.idProcesso}/detalhe" />">${processo.idProcessoFilho.nrProcesso}/${processo.idProcessoFilho.nrAno}</a> - 
								</c:forEach>
							</p>
						</c:if>
					</div>
				</div>
				<div class="row">
					<div class="span10 offset1 well">
						<table class="table">
							<thead>
								<tr>
									<td>Data do Tramite</td>
									<td>Para o Setor/Entidade</td>
									<td>Tramitado Por</td>
									<td>Despacho</td>
									<td>Data do Recebimento</td>
									<td>Recebido Por</td>
									
								</tr>
							</thead>
							<tbody>
								<c:forEach var="tblTramites" items="${tblTramitesList}">
									<tr>
										<td><fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${tblTramites.dtTramite}" /></td>
										<td>${tblTramites.cdSetor.nmSetor}/${tblTramites.cdSetor.idEntidade.dsEntidade}</td>
										<td>${tblTramites.idUsuarioTramite.nmUsuario}</td>
										<td><strong>${tblTramites.dsDispacho}</strong></td>
										<td>
										<fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${tblTramites.dtRecebimento}" />
										</td>
										<td>${tblTramites.idUsuarioRecebe.nmUsuario}</td>
										
									</tr>	
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<c:if test="${not empty tblRotasList}"> 
				<div class="row">
					<div class="span6 offset3 well">
						<table class="table">
							<thead>
								<tr>
									<td>Rota</td>
									<td>Setor</td>
									<td>Status</td>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="rotas" items="${tblRotasList}">
									<tr>
										<td>${rotas.rota.nmRota}</td>
										<td>${rotas.rota.cdSetor.nmSetor}</td>
										<td>
											<c:if test="${rotas.status eq PASSOU}">
												<img src="<c:url value="/img/accept.png" />">
											</c:if>
											<c:if test="${rotas.status eq NAOPASSOU}">
												<img src="<c:url value="/img/warning.png" />">
											</c:if>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				</c:if>
				<a href="<c:url value="/processo/processosetor" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>