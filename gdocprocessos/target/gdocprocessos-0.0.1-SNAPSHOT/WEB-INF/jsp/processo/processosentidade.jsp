<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PROCESSOS</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Processos no(a)
					${sessaoUsuario.lotacaoUser.cdSetor.idEntidade.dsEntidade}</legend>
				<div class="row">
					<div class="panelform span6 offset3">
						<form
							action="<c:url value="/processo/pesquisaprocessosentidade" />"
							method="post">
							<p>
								Processo: <input type="text" name="numprocesso" class="span2" /> Ano: <input type="text" name="nrano" class="span1" />
								<input class="btn" type="submit" value="Pesquisar" />
							</p>
						</form>
					</div>
				</div>
				<div class="well">
					<table class="table">
						<thead>
							<tr>
								<td>N�m. Processo</td>
								<td>Interessado</td>
								<td>Tipo do Processo</td>
								<td colspan="2">Data de Abertura</td>
							</tr>
						</thead>
						<tbody>
							<c:if test="${empty tblProcessosList}">
								<tr>
									<td colspan="4"><h2>Nenhum Processo encontrado.</h2></td>
								</tr>
							</c:if>
							<c:forEach var="tblProcessos" items="${tblProcessosList}">
								<tr>
									<td>${tblProcessos.nrProcesso}/${tblProcessos.nrAno}</td>
									<td>${tblProcessos.idInteressado.nmInteressado}</td>
									<td>${tblProcessos.cdTipoprocesso.nmTipoprocesso}</td>
									<td><fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${tblProcessos.dtAberturaProcesso}" /></td>
									<td><a href="<c:url value="/processo/${tblProcessos.idProcesso}/detalhe" />">
										<img title="Detalhar" alt="Detalhar" src="<c:url value="/img/detalhar.jpg"/>" >
									</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<jsp:include page="/WEB-INF/jsp/layout/pags.jsp" ></jsp:include>
				</div>
				<a href="<c:url value="/home" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
</body>
</html>