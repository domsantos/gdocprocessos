<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ARQUIVAR PROCESSO</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Arquivar Processo ${processo.nrProcesso}/${processo.nrAno}</legend>
				<div class="row">
					<div class="panelform span6 offset3">
						<c:if test="${not empty errors}" >
							<div class="alert">
								<c:forEach var="error" items="${errors}">
									<strong>${error.category} </strong> ${error.message}<br />
								</c:forEach>
							</div>
						</c:if>
						<form action="<c:url value="/processo/arquivar"/>" method="post">
						<table>
							<tr>
								<td>Local a ser Arquivado*:</td>
								<td><select name="local.idEnderecoarquivamento" class="span3">
										<option value="0">SELECIONE..</option>
										<c:forEach var="local" items="${localList}">
											<option value="${local.id}">${local.ordem} -
												${local.desc}</option>
										</c:forEach>
								</select></td>
							</tr>
							<tr>
								<td>Descri��o*:</td><td><input type="text" name="dsarq" maxlength="50" /></td>
							</tr>
							<tr>
								<td>
								<input type="hidden" value="${processo.idProcesso}" name="local.idProcesso" />
								<input class="btn" type="submit" value="Cadastrar" /></td>
							</tr>
						</table>
						</form>
					</div>
				</div>
				<a href="<c:url value="/processo/processoencerrado" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>