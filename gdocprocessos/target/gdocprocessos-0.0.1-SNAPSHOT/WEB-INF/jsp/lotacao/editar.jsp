<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EDITAR LOTACAO</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>Editar Lota��o</legend>
				<div class="row">
					<div class="panelform span4 offset3">
						<c:if test="${not empty errors}">
							<div class="alert">
								<c:forEach var="error" items="${errors}">
									<strong>${error.category} </strong> ${error.message}<br />
								</c:forEach>
							</div>
						</c:if>
						<form method="post"
							action="<c:url value="/lotacao/salvaredicao" />">
							<table>
								<tr>
									<td>Usu�rio:</td>
									<td><select name="lotacao.idUsuario.idUsuario">
											<option>SELECIONE..</option>
											<c:forEach var="usuario" items="${listaUser}">
												<option value="${usuario.idUsuario}"
													<c:if test="${usuario.idUsuario eq lot.idUsuario.idUsuario}" >selected</c:if>>${usuario.nmUsuario}</option>
											</c:forEach>
									</select></td>
								</tr>
								<tr>
									<td>Setor:</td>
									<td><select name="lotacao.cdSetor.cdSetor">
											<option>SELECIONE..</option>
											<c:forEach var="setor" items="${listaSetor}">
												<option value="${setor.cdSetor}"
													<c:if test="${setor.cdSetor eq lot.cdSetor.cdSetor}" >selected</c:if>>${setor.nmSetor}</option>
											</c:forEach>
									</select></td>
								</tr>
							</table>
							<p>
								<c:if test="${lot.stLotacao eq desativada}">
									<a href="<c:url value="/lotacao/ativar/${lot.cdLotacao}" />">Ativar
										Lota��o</a>
								</c:if>
								<c:if test="${lot.stLotacao eq ativada}">
									<a href="<c:url value="/lotacao/desativar/${lot.cdLotacao}" />">Desativar
										Lota��o</a>
								</c:if>
							</p>
							<p>
								<input class="btn" type="submit" value="Cadastrar" /> <input type="hidden" name="lotacao.cdLotacao" value="${lot.cdLotacao}" />
							</p>
						</form>
					</div>
				</div>
				<a href="<c:url value="/lotacao" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
</body>
</html>