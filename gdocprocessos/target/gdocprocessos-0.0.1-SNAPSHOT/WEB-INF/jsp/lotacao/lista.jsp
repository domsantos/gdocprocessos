<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>LOTA��O</title>
<link href="<c:url value="/css/styles.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/estilo.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"
	type="text/css">
<link href="<c:url value="/css/bootstrap-responsive.css" />"
	rel="stylesheet" type="text/css">
</head>
<body>
	<div class="conteudo">
		<jsp:include page="/WEB-INF/jsp/layout/menu.jsp"></jsp:include>
		<div class="container">
			<fieldset>
				<legend>LOTA��O</legend>
				<a class="btn" href="<c:url value="/lotacao/nova" />"><span>Nova Lota��o</span></a>
				<div class="row">
					<div class="panelform span6">
						<form action="<c:url value="/lotacao/pesquisalotacao" />" >
							<p>Usu�rio: <input type="text" name="nomeUsuario" /> <input class="btn" type="submit" value="Pesquisar Lota��o"/></p>
						</form>
					</div>
				 </div>
				<div class="well">
					<c:if test="${not empty errors}">
						<div class="alert">
							<c:forEach var="error" items="${errors}">
								<strong>${error.category} </strong> ${error.message}<br />
							</c:forEach>
						</div>
					</c:if>
					
					<table class="table">
						<thead>
							<tr>
								<td>C�d. Lota��o</td>
								<td>Usu�rio</td>
								<td colspan="2">Setor</td>
								
							</tr>
						</thead>
						<c:forEach var="tblLotacao" items="${tblLotacaoList}">
							<tr>
								<td>${tblLotacao.cdLotacao}</td>
								<td>${tblLotacao.idUsuario.nmUsuario}</td>
								<td>${tblLotacao.cdSetor.dsSetor}</td>
								<td><a
									href="<c:url value="/lotacao/editar/${tblLotacao.cdLotacao}"/>">Editar</a> | 
									<c:if test="${tblLotacao.stLotacao eq desativada}">
										<a href="<c:url value="/lotacao/ativar/${tblLotacao.cdLotacao}" />">Ativar Lota��o</a>
									</c:if> <c:if test="${tblLotacao.stLotacao eq ativada}">
										<a href="<c:url value="/lotacao/desativar/${tblLotacao.cdLotacao}" />">Desativar Lota��o</a>
									</c:if>
								</td>	
							</tr>
						</c:forEach>
					</table>
					<jsp:include page="/WEB-INF/jsp/layout/pags.jsp" ></jsp:include>
				</div>
				<a href="<c:url value="/menuadmin" />" class="btn btn-large pull-right">Voltar</a>
			</fieldset>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/layout/foot.jsp"></jsp:include>
</body>
</html>