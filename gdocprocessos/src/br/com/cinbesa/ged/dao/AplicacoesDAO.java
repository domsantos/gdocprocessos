package br.com.cinbesa.ged.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.TblAplicacoes;

@Component
public class AplicacoesDAO extends GenericDao<TblAplicacoes, Integer>{
	private static final long serialVersionUID = 1L;
	
	public AplicacoesDAO(EntityManager em) {
		super(em);
	}
	
	public List<TblAplicacoes> listatodos(){
		return super.findAll("TblAplicacoes.findAll");
	}
	
	public TblAplicacoes find(Integer id){
		return super.find(TblAplicacoes.class, id);
	}
}
