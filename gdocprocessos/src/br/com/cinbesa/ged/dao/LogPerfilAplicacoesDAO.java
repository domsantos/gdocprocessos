package br.com.cinbesa.ged.dao;

import javax.persistence.EntityManager;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.TblLogPerfilAplicacoes;

@Component
public class LogPerfilAplicacoesDAO extends GenericDao<TblLogPerfilAplicacoes, Integer>{
	
	private static final long serialVersionUID = 1L;

	public LogPerfilAplicacoesDAO(EntityManager em) {
		super(em);
	}
	
	public void registrar(TblLogPerfilAplicacoes log){
		em.persist(log);
	}
	
	
}
