package br.com.cinbesa.ged.dao;

import javax.persistence.EntityManager;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.TblLogPerfil;

@Component
public class LogPerfilDAO extends GenericDao<TblLogPerfil, Integer>{

	private static final long serialVersionUID = 1L;

	public LogPerfilDAO(EntityManager em) {
		super(em);
	}
	
	public void registrar(TblLogPerfil log){
		em.persist(log);
	}
	
}
