package br.com.cinbesa.ged.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.Entidade;
import br.com.cinbesa.ged.entidade.TblLotacao;
import br.com.cinbesa.ged.entidade.TblSetor;
import br.com.cinbesa.ged.entidade.TblUsuario;
import br.com.cinbesa.ged.util.ListaApp;


@Component
public class LotacaoDAO extends GenericDao<TblLotacao, Integer>{
	
	private static final long serialVersionUID = 1L;
	
	
	public LotacaoDAO(EntityManager em) {
		super(em);
	}
	
	public TblLotacao salvar(TblLotacao lotacao){
		return em.merge(lotacao);
	}
	public Long countListaTodos(Entidade entidade){
		Query q = em.createQuery("SELECT count(l) FROM TblLotacao l " +
				"where l.idUsuario " +
				"IN(SELECT u FROM TblLotacao lo " +
				"join lo.idUsuario u " +
				"join lo.cdSetor s " +
				"where lo.stLotacao = :stlotacao AND s.idEntidade = :entidade)");
		q.setParameter("entidade", entidade);
		q.setParameter("stlotacao", ListaApp.LOTACAOATIVADA);
		try{
			return (Long) q.getSingleResult();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			return null;
		}
	}
	
	public List<TblLotacao> listatodos(int startAt, int maxPerPage, Entidade entidade){
		Query q = em.createQuery("SELECT l FROM TblLotacao l " +
				"where l.idUsuario " +
				"IN(SELECT u FROM TblLotacao lo " +
				"join lo.idUsuario u " +
				"join lo.cdSetor s " +
				"where lo.stLotacao = :stlotacao AND s.idEntidade = :entidade) " +
				"order by l.stLotacao ");
		q.setFirstResult(startAt);
		q.setMaxResults(maxPerPage);
		q.setParameter("entidade", entidade);
		q.setParameter("stlotacao", ListaApp.LOTACAOATIVADA);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			return null;
		}
	}
	
	
	public TblLotacao verificaLotacaoUsuario(TblUsuario usuario, char situacao) {
		Query q = em.createQuery("SELECT t FROM TblLotacao t WHERE t.stLotacao = :stLotacao AND t.idUsuario = :idUsuario");
		q.setParameter("stLotacao", situacao);
		q.setParameter("idUsuario", usuario);
		try{
			return (TblLotacao) q.getSingleResult();
		}catch(NoResultException e){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		
	}	
	
	public TblLotacao verificaLotacao(TblUsuario usuario, TblSetor setor) {
		Query q = em.createQuery("SELECT t FROM TblLotacao t WHERE t.cdSetor = :cdSetor AND t.idUsuario = :idUsuario");
		q.setParameter("cdSetor", setor);
		q.setParameter("idUsuario", usuario);
		
		try{
			return (TblLotacao) q.getSingleResult();
		}catch(NoResultException e){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblLotacao> pesquisaLotacaoPorNomeUsuario(String nomeUsuario){
		Query q = em.createQuery("SELECT l FROM TblLotacao l JOIN l.idUsuario u WHERE u.nmUsuario like :nomeUsuario");
		q.setParameter("nomeUsuario", "%"+nomeUsuario+"%");
		try{
			return q.getResultList();
		}catch(NoResultException nres){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
