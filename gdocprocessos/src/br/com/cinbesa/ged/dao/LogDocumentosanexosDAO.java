package br.com.cinbesa.ged.dao;

import javax.persistence.EntityManager;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.TblLogDocumentosanexos;

@Component
public class LogDocumentosanexosDAO extends GenericDao<TblLogDocumentosanexos, Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LogDocumentosanexosDAO(EntityManager em) {
		super(em);
	}
	
	public void registrar(TblLogDocumentosanexos tblLogDocumentosanexos){
		em.persist(tblLogDocumentosanexos);
	}
	
}
