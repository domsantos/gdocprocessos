package br.com.cinbesa.ged.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.dto.LocalArquivoDTO;
import br.com.cinbesa.ged.entidade.Entidade;
import br.com.cinbesa.ged.entidade.TblEnderecoarquivamento;

@Component
public class LocalArquivoDAO extends GenericDao<TblEnderecoarquivamento, Integer>{

	private static final long serialVersionUID = 1L;
	
	public LocalArquivoDAO(EntityManager em) {
		super(em);
	}
	
	public List<LocalArquivoDTO> listatodos(Entidade entidade){
		Query q = em.createQuery("SELECT new br.com.cinbesa.ged.dto.LocalArquivoDTO(e.idEnderecoarquivamento, e.idNivelsuperior, e.dsEnderecoarquivamento) FROM TblEnderecoarquivamento e where e.idEntidade = :entidade");
		q.setParameter("entidade", entidade);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void salvar(TblEnderecoarquivamento local){
		em.persist(local);
	}
	
	public List<TblEnderecoarquivamento> listaLocaisFilhos(TblEnderecoarquivamento pai){
		Query q = em.createQuery("SELECT e FROM TblEnderecoarquivamento e where e.idNivelsuperior = :pai");
		q.setParameter("pai", pai.getIdEnderecoarquivamento());
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void excluir(TblEnderecoarquivamento local){
		em.remove(local);
	}
}
