package br.com.cinbesa.ged.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.TblInteressados;

@Component
public class InteressadoDAO extends GenericDao<TblInteressados, Integer>{

	private static final long serialVersionUID = 1L;

	public InteressadoDAO(EntityManager em) {
		super(em);
	}
	
	public List<TblInteressados> listaPorCPFCNPJ(String cpfcnpj){
		Query q = em.createNamedQuery("TblInteressados.findByNrCpfCnpj");
		q.setParameter("nrCpfCnpj", cpfcnpj);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public TblInteressados getInteressadoAnonimo(String cpf){
		Query q = em.createQuery("SELECT t FROM TblInteressados t WHERE t.nrCpfCnpj = :nrCpfCnpj");
		q.setParameter("nrCpfCnpj", cpf);
		try{
			return (TblInteressados) q.getSingleResult();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblInteressados> listaPorNome(TblInteressados interessado){
		Query q = em.createNamedQuery("TblInteressados.findByNmInteressado");
		q.setParameter("nmInteressado", "%"+interessado.getNmInteressado()+"%");
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public TblInteressados salvar(TblInteressados tblInteressados){
		return em.merge(tblInteressados);
	}
	
}
