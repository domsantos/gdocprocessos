package br.com.cinbesa.ged.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.Entidade;
import br.com.cinbesa.ged.entidade.TblRotas;
import br.com.cinbesa.ged.entidade.TblTipoprocesso;


@Component
public class RotaDAO extends GenericDao<TblRotas, Integer>{

	private static final long serialVersionUID = 1L;

	public RotaDAO(EntityManager em) {
		super(em);
	}
	
	public void salvarRota(TblRotas rota){
		em.persist(rota);
	}
	
	public Long count(Entidade entidade){
		Query q = em.createQuery("SELECT count(r) FROM TblRotas r " +
				"join r.cdSetor s " +
				"where s.idEntidade = :entidade " +
				"ORDER BY r.cdTipoprocesso, r.nrOrdemrota");
		q.setParameter("entidade", entidade);
		try{
			return (Long) q.getSingleResult();
		}catch(NoResultException nores){
			return null;			
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Integer getMaxOrdem(TblTipoprocesso tipoProcesso){
		Query q = em.createQuery("SELECT max(r.nrOrdemrota) FROM TblRotas r " +
				"where r.cdTipoprocesso = :tipoProcesso ");
		
		q.setParameter("tipoProcesso", tipoProcesso);
		try{
			short n = (Short) q.getSingleResult();
			return  (int) n;
		}catch(NoResultException nores){
			return null;			
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public Long count(Entidade entidade, TblTipoprocesso tipoProcesso){
		Query q = em.createQuery("SELECT count(r) FROM TblRotas r " +
				"join r.cdSetor s " +
				"where s.idEntidade = :entidade " +
				"and r.cdTipoprocesso = :tipoProcesso "
				);
		q.setParameter("entidade", entidade);
		q.setParameter("tipoProcesso", tipoProcesso);
		
		try{
			return (Long) q.getSingleResult();
		}catch(NoResultException nores){
			return null;			
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblRotas> listaRotasOrdenado(int startAt, int maxPerPage, Entidade entidade){
		Query q = em.createQuery("SELECT r FROM TblRotas r " +
				"join r.cdSetor s " +
				"where s.idEntidade = :entidade " +
				"ORDER BY r.cdTipoprocesso, r.nrOrdemrota");
		q.setFirstResult(startAt);
		q.setMaxResults(maxPerPage);
		q.setParameter("entidade", entidade);
		return q.getResultList();
	}
	
	public List<TblRotas> listaRotasPorTipoProcessoOrdenado(int startAt, int maxPerPage, Entidade entidade, TblTipoprocesso tipoProcesso){
		Query q = em.createQuery("SELECT r FROM TblRotas r " +
				"join r.cdSetor s " +
				"where s.idEntidade = :entidade " +
				"and r.cdTipoprocesso = :tipoProcesso " +
				"ORDER BY r.cdTipoprocesso, r.nrOrdemrota");
		q.setFirstResult(startAt);
		q.setMaxResults(maxPerPage);
		q.setParameter("entidade", entidade);
		q.setParameter("tipoProcesso", tipoProcesso);
		return q.getResultList();
	}
	
	public TblRotas getRotaPorTPProcOrdem(TblTipoprocesso tipoproc, short ordemrota){
		Query q = em.createQuery("SELECT r FROM TblRotas r WHERE r.cdTipoprocesso = :cdTipoprocesso AND r.nrOrdemrota = :nrOrdemrota");
		q.setParameter("cdTipoprocesso", tipoproc);
		q.setParameter("nrOrdemrota", ordemrota);
		try{
			return (TblRotas) q.getSingleResult();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblRotas> listaPorTipoProcesso(TblTipoprocesso tipoprocesso){
		Query q = em.createQuery("SELECT r FROM TblRotas r WHERE r.cdTipoprocesso =:cdTipoprocesso ORDER BY r.nrOrdemrota");
		q.setParameter("cdTipoprocesso", tipoprocesso);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch (Exception e) {
				e.printStackTrace();
				return null;
		}
	}
}
