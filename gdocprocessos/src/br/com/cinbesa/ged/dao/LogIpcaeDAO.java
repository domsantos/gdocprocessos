package br.com.cinbesa.ged.dao;

import javax.persistence.EntityManager;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.TblIpcae;
import br.com.cinbesa.ged.entidade.TblLogIpcae;

@Component
public class LogIpcaeDAO extends GenericDao<TblIpcae, Integer>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LogIpcaeDAO(EntityManager em) {
		super(em);
	}
	
	public void registrar(TblLogIpcae logIpcae){
		em.persist(logIpcae);
	}
	
}
