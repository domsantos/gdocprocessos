package br.com.cinbesa.ged.dao;

import javax.persistence.EntityManager;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.TblLogUfir;


@Component
public class LogUfirDAO extends GenericDao<TblLogUfir, Integer>{

	private static final long serialVersionUID = 1L;

	public LogUfirDAO(EntityManager em) {
		super(em);
	}
	
	public void registrar(TblLogUfir log){
		em.persist(log);
	}
}
