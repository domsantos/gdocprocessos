package br.com.cinbesa.ged.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;


import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.TblProcessos;
import br.com.cinbesa.ged.entidade.TblSetor;
import br.com.cinbesa.ged.entidade.TblTramites;

@Component
public class TramiteDAO extends GenericDao<TblTramites, Long>{

	private static final long serialVersionUID = 1L;

	public TramiteDAO(EntityManager em) {
		super(em);
	}
	
	public TblTramites salvarTramite(TblTramites tramite){
		return em.merge(tramite);
	}
	
	public List<TblTramites> listaTramitesPorProcesso(TblProcessos processo){
		Query q = em.createQuery("SELECT t FROM TblTramites t WHERE t.idProcesso = :idProcesso ORDER BY t.dtTramite ASC");
		q.setParameter("idProcesso", processo);
		List<TblTramites> resp = null;
		try{
			resp = q.getResultList();
		}catch(NoResultException nores){
			resp = null;
		}catch(Exception e){
			e.printStackTrace();
			resp = null;
		}finally{
			return resp;
		}
	}
	
	public TblTramites getUltimoTramite(TblProcessos processo, char sitProc, TblSetor setor){
		Query q = em.createQuery("SELECT t FROM TblTramites t WHERE t.idProcesso = :idProcesso AND t.cdSetor = :cdSetor AND t.stTramite = :stTramite");
		q.setParameter("idProcesso", processo);
		q.setParameter("cdSetor", setor);
		q.setParameter("stTramite", sitProc);
		TblTramites resp = null;
		try{
			resp = (TblTramites) q.getSingleResult();
		}catch(NoResultException nores){
			resp = null;
		}catch(Exception e){
			e.printStackTrace();
			resp = null;
		}finally{
			return resp;
		}
	}
	
	public Long countlistaProcessosAEstornar(char sitTramiteEspera, TblSetor setor){
		Query q = em.createQuery("SELECT count(p) FROM TblTramites t " +
				"JOIN t.idProcesso p " +
				"WHERE t.idTramite = (" +
						"SELECT max(t2.idTramite) FROM TblTramites t2 " +
						"WHERE t2.idProcesso = t.idProcesso " +
						"AND t2.cdSetor = :setor " +
						"AND t2.idTramite != (" +
								"SELECT max(t3.idTramite) FROM TblTramites t3 " +
								"WHERE t3.stTramite = :situacao " +
								"AND t3.idProcesso = t.idProcesso)) ");
		q.setParameter("setor", setor);
		q.setParameter("situacao", sitTramiteEspera);
		
		try{
			return (Long) q.getSingleResult();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblProcessos> listaProcessosAEstornar(char sitTramiteEspera, TblSetor setor,  int startAt, int maxPerPage){
		Query q = em.createQuery("SELECT p FROM TblTramites t " +
				"JOIN t.idProcesso p " +
				"WHERE t.idTramite = (" +
						"SELECT max(t2.idTramite) FROM TblTramites t2 " +
						"WHERE t2.idProcesso = t.idProcesso " +
						"AND t2.cdSetor = :setor " +
						"AND t2.idTramite != (" +
								"SELECT max(t3.idTramite) FROM TblTramites t3 " +
								"WHERE t3.stTramite = :situacao " +
								"AND t3.idProcesso = t.idProcesso)) " +
				"ORDER BY t.dtTramite DESC");
		q.setParameter("setor", setor);
		q.setParameter("situacao", sitTramiteEspera);
		q.setFirstResult(startAt);
		q.setMaxResults(maxPerPage);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public void deletarTramiteEspera(char situacaoEspera, TblProcessos proc){
		Query q = em.createQuery("DELETE FROM TblTramites t " +
				"WHERE t.stTramite = :situacao " +
				"AND t.idProcesso = :processo");
		q.setParameter("situacao", situacaoEspera);
		q.setParameter("processo", proc);
		q.executeUpdate();
	}
	
	public TblTramites estornarTramite(char situacaoTramitado, TblProcessos proc, TblSetor setor){
		Query q = em.createQuery("SELECT t FROM TblTramites t " +
				"WHERE t.idTramite = (" +
					"SELECT max(t2.idTramite) FROM TblTramites t2 " +
					"WHERE t2.stTramite = :sit " +
					"AND t2.idProcesso = :proc " +
					"AND t2.cdSetor = :setor)");
		
		q.setParameter("setor", setor);
		q.setParameter("sit", situacaoTramitado);
		q.setParameter("proc", proc);
		
		try{
			return (TblTramites) q.getSingleResult();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
}
