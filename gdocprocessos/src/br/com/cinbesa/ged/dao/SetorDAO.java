package br.com.cinbesa.ged.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.Entidade;
import br.com.cinbesa.ged.entidade.TblSetor;

@Component
public class SetorDAO extends GenericDao<TblSetor, Integer> {
	
	private static final long serialVersionUID = 1L;
	
	public SetorDAO(EntityManager em) {
		super(em);
	}
	
	public Long count(Entidade entidade){
		Query q = em.createQuery("SELECT count(s) FROM TblSetor s " +
				"where s.idEntidade = :entidade");
		q.setParameter("entidade", entidade);
		try{
			return (Long) q.getSingleResult();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblSetor> listatodos(){
		return super.findAll("TblSetor.findAll");
	}
	
	public List<TblSetor> listatodos(int startAt, int maxPerPage, Entidade entidade){
		Query q = em.createQuery("Select s FROM TblSetor s " +
				"where s.idEntidade = :entidade");
		q.setParameter("entidade", entidade);
		q.setFirstResult(startAt);
		q.setMaxResults(maxPerPage);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblSetor> listaPorEntidade(Entidade entidade){
		Query q = em.createQuery("SELECT s FROM TblSetor s WHERE s.idEntidade = :idEntidade");
		q.setParameter("idEntidade", entidade);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public TblSetor salvar(TblSetor setor){
		return em.merge(setor);
	}
}
