package br.com.cinbesa.ged.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.TblProcessoArquivado;
import br.com.cinbesa.ged.entidade.TblProcessoArquivadoPK;
import br.com.cinbesa.ged.entidade.TblProcessos;

@Component
public class ProcArquivadoDAO extends GenericDao<TblProcessoArquivado, TblProcessoArquivadoPK>{

	public ProcArquivadoDAO(EntityManager em) {
		super(em);
	}
	
	
	public void cadastrar(TblProcessoArquivado procarq){
		em.persist(procarq);
	}
	
	public TblProcessoArquivado findPorProcesso(TblProcessos proc){
		Query q = em.createQuery("SELECT pa FROM TblProcessoArquivado pa " +
				"WHERE pa.tblProcessoArquivadoPK.idProcesso = :idproc");
		q.setParameter("idproc", proc.getIdProcesso());
		try{
			return (TblProcessoArquivado) q.getSingleResult();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
}
