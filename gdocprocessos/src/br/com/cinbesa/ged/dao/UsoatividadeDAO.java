package br.com.cinbesa.ged.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.Entidade;
import br.com.cinbesa.ged.entidade.TblUsoatividade;

@Component
public class UsoatividadeDAO extends GenericDao<TblUsoatividade, Integer>{

	private static final long serialVersionUID = 1L;

	public UsoatividadeDAO(EntityManager em) {
		super(em);
	}
	
	public TblUsoatividade salvarUsoAtividade(TblUsoatividade tblUsoatividade){
		return em.merge(tblUsoatividade);
	}
	
	public TblUsoatividade atualizarUsoAtividade(TblUsoatividade tblUsoatividade){
		return em.merge(tblUsoatividade);
	}
	
	public Long count(Entidade entidade){
		Query q = em.createQuery("SELECT count(r) FROM TblUsoatividade r " +
				"where r.idEntidade = :entidade");
		q.setParameter("entidade", entidade);
		try{
			return (Long) q.getSingleResult();
		}catch(NoResultException nores){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblUsoatividade> listartodos(int startAt, int maxPerPage, Entidade entidade){
		Query q = em.createQuery("SELECT r FROM TblUsoatividade r " +
				"where r.idEntidade = :entidade");
		q.setParameter("entidade", entidade);
		q.setFirstResult(startAt);
		q.setMaxResults(maxPerPage);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblUsoatividade> listarPorEntidade(Entidade entidade){
		Query q = em.createQuery("SELECT r FROM TblUsoatividade r where r.idEntidade = :entidade");
		q.setParameter("entidade", entidade);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
