package br.com.cinbesa.ged.dao;

import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.dto.DocAnexoDTO;
import br.com.cinbesa.ged.entidade.TblDocumentosanexos;
import br.com.cinbesa.ged.entidade.TblDocumentosexigidos;
import br.com.cinbesa.ged.entidade.TblProcessos;

@Component
public class DocsAnexosDAO extends GenericDao<TblDocumentosanexos, Integer>{

	private static final long serialVersionUID = 1L;

	public DocsAnexosDAO(EntityManager em) {
		super(em);
	}
	
	public List<DocAnexoDTO> listaPorProcesso(TblProcessos proc){
		Query q = em.createQuery("SELECT new br.com.cinbesa.ged.dto.DocAnexoDTO(d.cdDocumentoanexo, d.dtAnexo, d.nmDocumentoanexo) FROM TblDocumentosanexos d where d.idProcesso = :proc");
		q.setParameter("proc", proc);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public TblDocumentosanexos novo(TblDocumentosanexos tblDocumentosanexos){
		return em.merge(tblDocumentosanexos);
	}
	
	public TblDocumentosanexos salvarEdicao(TblDocumentosanexos tblDocumentosanexos){
		return em.merge(tblDocumentosanexos);
	}
}
