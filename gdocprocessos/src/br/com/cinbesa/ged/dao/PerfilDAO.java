package br.com.cinbesa.ged.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.Entidade;
import br.com.cinbesa.ged.entidade.TblPerfil;


@Component
public class PerfilDAO extends GenericDao<TblPerfil, Integer>{
	private static final long serialVersionUID = 1L;
	
	public PerfilDAO(EntityManager em) {
		super(em);
	}
	
	public Long count(){
		Query q = em.createQuery("SELECT count(p) from TblPerfil p");
		return (Long) q.getSingleResult();
	}
	
	public List<TblPerfil> listatodos(Entidade entidade){
		Query query = em.createQuery("SELECT p FROM TblPerfil p where p.idEntidade = :idEntidade");
		query.setParameter("idEntidade", entidade);
		try{
			return query.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblPerfil> listatodos(int startAt, int maxPerPage, Entidade entidade){
		Query query = em.createQuery("SELECT p FROM TblPerfil p where p.idEntidade = :idEntidade");
		query.setFirstResult(startAt);
		query.setMaxResults(maxPerPage);
		query.setParameter("idEntidade", entidade);
		try{
			return query.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblPerfil> lista(Entidade entidade){
		Query query = em.createQuery("SELECT p FROM TblPerfil p where p.idEntidade = :idEntidade");
		query.setParameter("idEntidade", entidade);
		try{
			return query.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public TblPerfil findPorDescricao(TblPerfil perfil){
		Parametros<String> param = new Parametros<String>("dsPerfil", perfil.getDsPerfil());
		return super.findSingleResult("TblPerfil.findByDsPerfil", param);
	}
	
	public TblPerfil salvar(TblPerfil perfil){
		return em.merge(perfil);
	}
	
}
