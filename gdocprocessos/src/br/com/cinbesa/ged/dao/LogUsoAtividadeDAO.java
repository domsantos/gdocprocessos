package br.com.cinbesa.ged.dao;

import javax.persistence.EntityManager;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.TblLogUsoatividade;

@Component
public class LogUsoAtividadeDAO extends GenericDao<TblLogUsoatividade, Integer>{

	private static final long serialVersionUID = 1L;

	public LogUsoAtividadeDAO(EntityManager em) {
		super(em);
	}
	
	public void registrar(TblLogUsoatividade log){
		em.persist(log);
	}
	
}
