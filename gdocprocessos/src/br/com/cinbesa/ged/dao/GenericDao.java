package br.com.cinbesa.ged.dao;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import br.com.caelum.vraptor.ioc.Component;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;

import javax.persistence.NoResultException;
import javax.persistence.Query;



public class GenericDao<T, E extends Serializable> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    EntityManager em;
    


    public GenericDao(EntityManager em) {
    	this.em = em;
    }

    public T find(Class<T> c, E chave) {
        T find = em.find(c, chave);
        return find;
    }

    @SuppressWarnings("unchecked")
    public List<T> findAll(String namedQuery, Parametros<?>... parametros) {
        Query q = em.createNamedQuery(namedQuery);

        if (parametros != null) {
            for (Parametros<?> item : parametros) {
                if (item != null) {
                    q.setParameter(item.getNome(), item.getValor());
                }
            }
        }

        List resultList = q.getResultList();

        return resultList;
    }


    
    @SuppressWarnings("unchecked")
	public T findSingleResult(String namedQuery, Parametros<?>... parametros) {
        Query q = em.createNamedQuery(namedQuery);

        if (parametros != null) {
            for (Parametros<?> item : parametros) {
                if (item != null) {
                    q.setParameter(item.getNome(), item.getValor());
                }
            }
        }
        T result = null;
        try{
        	result = (T) q.getSingleResult();
        }catch(NoResultException e){
        	return null;
        }catch(Exception e){
        	e.printStackTrace();
        }
        	return result;
        

    }


    public void update(T clazz) {
        em.merge(clazz);
    }
    
    public void closeConnetion() {
        if (em.isOpen()) {
            em.close();
        }
    }


    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

}
