package br.com.cinbesa.ged.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.TblDam;
import br.com.cinbesa.ged.entidade.TblValorDocumento;

@Component
public class DamDAO extends GenericDao<TblDam, Integer>{

	private static final long serialVersionUID = 1L;

	public DamDAO(EntityManager em) {
		super(em);
	}
	
	public TblDam cadastrarDAM(TblDam dam){
		return em.merge(dam);
	}

	public TblDam salvarDAM(TblDam dam){
		return em.merge(dam);
	}
	
	public List<TblDam> listaPorValorDocumento(TblValorDocumento valorDocumento){
		Query q = em.createQuery("SELECT d FROM TblDam d WHERE d.cdValorDocumento = :documento");
		q.setParameter("documento", valorDocumento);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}
	}
	
	public List<TblDam> listaParcelasPorVlDocSituacao(TblValorDocumento valorDocumento, char situacao){
		Query q = em.createQuery("SELECT d FROM TblDam d WHERE d.cdValorDocumento = :documento AND d.stSituacao = :situacao");
		q.setParameter("documento", valorDocumento);
		q.setParameter("situacao", situacao);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}
	}
	
	
}
