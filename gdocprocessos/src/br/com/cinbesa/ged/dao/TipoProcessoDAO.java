package br.com.cinbesa.ged.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.Entidade;
import br.com.cinbesa.ged.entidade.TblTipoprocesso;

@Component
public class TipoProcessoDAO extends GenericDao<TblTipoprocesso, Integer>{
	
	private static final long serialVersionUID = 1L;
	
	public TipoProcessoDAO(EntityManager em) {
		super(em);
	}
	
	public Long count(Entidade entidade){
		Query q = em.createQuery("SELECT count(tp) from TblTipoprocesso tp " +
				"where tp.idEntidade = :entidade");
		q.setParameter("entidade", entidade);
		try{
			return (Long) q.getSingleResult();
		}catch(NoResultException ne){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public List<TblTipoprocesso> listaTipos(Entidade entidade){
		Query q = em.createQuery("SELECT tp FROM TblTipoprocesso tp " +
				"where tp.idEntidade = :entidade");
		q.setParameter("entidade", entidade);
		try{
			return q.getResultList();
		}catch(NoResultException ne){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblTipoprocesso> listaTipos(int startAt, int maxPerPage, Entidade entidade){
		Query q = em.createQuery("SELECT tp FROM TblTipoprocesso tp " +
				"where tp.idEntidade = :entidade");
		q.setFirstResult(startAt);
		q.setMaxResults(maxPerPage);
		q.setParameter("entidade", entidade);
		try{
			return q.getResultList();
		}catch(NoResultException ne){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public TblTipoprocesso salvar(TblTipoprocesso tblTipoprocesso){
		return em.merge(tblTipoprocesso);
	}
	
	public List<TblTipoprocesso> listaPorEntidade(Entidade entidade){
		Query q = em.createQuery("SELECT t FROM TblTipoprocesso t WHERE t.idEntidade = :idEntidade");
		q.setParameter("idEntidade", entidade);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
}
