package br.com.cinbesa.ged.dao;

import javax.persistence.EntityManager;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.TblLogTipoprocesso;

@Component
public class LogTipoprocessoDAO extends GenericDao<TblLogTipoprocesso, Integer>{

	
	private static final long serialVersionUID = 1L;

	
	public LogTipoprocessoDAO(EntityManager em) {
		super(em);
	}
	
	
	public void registrar(TblLogTipoprocesso log){
		em.persist(log);
	}
	
	
}
