package br.com.cinbesa.ged.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.TblDocumentosexigidos;


@Component
public class DocsexigidosDAO extends GenericDao<TblDocumentosexigidos, Integer>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DocsexigidosDAO(EntityManager em) {
		super(em);
	}
	
	public void cadastrar(TblDocumentosexigidos tblDocumentosexigidos){
		em.persist(tblDocumentosexigidos);
	}
	
	public Long count(){
		Query q = em.createQuery("SELECT count(d) FROM TblDocumentosexigidos d ");
		try{
			return (Long) q.getSingleResult();
		}catch(NoResultException nores){
			return null;			
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblDocumentosexigidos> lista(int startAt, int maxPerPage){
		Query q = em.createQuery("SELECT d FROM TblDocumentosexigidos d");
		q.setFirstResult(startAt);
		q.setMaxResults(maxPerPage);
		return q.getResultList();
	}
	public List<TblDocumentosexigidos> lista(){
		Query q = em.createQuery("SELECT d FROM TblDocumentosexigidos d");
		return q.getResultList();
	}
}
