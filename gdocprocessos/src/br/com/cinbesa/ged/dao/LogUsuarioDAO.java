package br.com.cinbesa.ged.dao;

import javax.persistence.EntityManager;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.TblLogUsuario;

@Component
public class LogUsuarioDAO extends GenericDao<TblLogUsuario, Integer>{

	public LogUsuarioDAO(EntityManager em) {
		super(em);
	}
	
	public void registrar(TblLogUsuario log){
		em.persist(log);
	}
	
}
