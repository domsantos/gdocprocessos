package br.com.cinbesa.ged.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;

import br.com.cinbesa.ged.entidade.TblRegras;
import br.com.cinbesa.ged.entidade.TblTipoprocesso;

@Component
public class RegraDAO extends GenericDao<TblRegras, Integer>{
	
	
	private static final long serialVersionUID = 1L;

	public RegraDAO(EntityManager em) {
			super(em);
	}
	
	
	public Long count(){
		Query q = em.createQuery("SELECT count(r) FROM TblRegras r");
		try{
			return (Long) q.getSingleResult();
		}catch(NoResultException nores){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Long count(TblTipoprocesso tpproc){
		Query q = em.createQuery("SELECT count(r) FROM TblRegras r where r.cdTipoprocesso = :id");
		q.setParameter("id", tpproc);
		try{
			return (Long) q.getSingleResult();
		}catch(NoResultException nores){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblRegras> listartodos(int startAt, int maxPerPage, TblTipoprocesso tpproc){
		Query q = em.createQuery("SELECT r FROM TblRegras r where r.cdTipoprocesso = :cdTipoprocesso");
		q.setParameter("cdTipoprocesso", tpproc);
		q.setFirstResult(startAt);
		q.setMaxResults(maxPerPage);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void cadastrar(TblRegras tblRegras){
		em.persist(tblRegras);
	}
	
	public List<TblRegras> listartodos(TblTipoprocesso tipoprocesso){
		Query q = em.createQuery("SELECT r FROM TblRegras r WHERE r.cdTipoprocesso = :cdTipoprocesso");
		q.setParameter("cdTipoprocesso", tipoprocesso);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void delete(TblRegras regras){
		em.remove(regras);
	}
}
