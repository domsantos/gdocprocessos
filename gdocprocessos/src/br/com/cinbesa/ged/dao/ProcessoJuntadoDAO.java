package br.com.cinbesa.ged.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.TblProcessos;
import br.com.cinbesa.ged.entidade.TblProcessosJuntados;



@Component
public class ProcessoJuntadoDAO extends GenericDao<TblProcessosJuntados, Long>{

	
	public ProcessoJuntadoDAO(EntityManager em) {
		super(em);
	}
	
	public void cadastrar(TblProcessosJuntados procs){
		em.persist(procs);
	}
	
	public List<TblProcessosJuntados> listaProcessosFilhos(TblProcessos proc){
		Query q = em.createQuery("SELECT pj FROM TblProcessosJuntados pj " +
				"WHERE pj.idProcessoPai = :proc");
		q.setParameter("proc", proc);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblProcessosJuntados> listaProcessosPais(TblProcessos proc){
		Query q = em.createQuery("SELECT pj FROM TblProcessosJuntados pj " +
				"WHERE pj.idProcessoFilho = :proc");
		q.setParameter("proc", proc);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
}
