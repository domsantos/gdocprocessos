package br.com.cinbesa.ged.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.TblTributo;

@Component
public class TributoDAO  extends GenericDao<TblTributo, Integer>{

	private static final long serialVersionUID = 1L;

	public TributoDAO(EntityManager em) {
		super(em);
	}
	
	
	public TblTributo salvar(TblTributo tributo){
		return em.merge(tributo);
	}
	
	
	public Long count(){
		Query q = em.createQuery("SELECT count(t) FROM TblTributo t");
		return (Long) q.getSingleResult();
	}
	
	public List<TblTributo> listaTodos(int startAt, int maxperpage){
		Query q = em.createQuery("SELECT t FROM TblTributo t");
		q.setFirstResult(startAt);
		q.setMaxResults(maxperpage);
		try{
			return q.getResultList();
		}catch (NoResultException e) {
			return null;
		}catch (Exception e) {
				e.printStackTrace();
				return null;
		}
	}
	
	public List<TblTributo> listaTodos(){
		Query q = em.createQuery("SELECT t FROM TblTributo t");
		try{
			return q.getResultList();
		}catch (NoResultException e) {
			return null;
		}catch (Exception e) {
				e.printStackTrace();
				return null;
		}
	}
}
