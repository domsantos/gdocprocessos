package br.com.cinbesa.ged.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.Especies;

@Component
public class EspecieDAO extends GenericDao<Especies, Integer>{

private static final long serialVersionUID = 1L;
	
	public EspecieDAO(EntityManager em) {
		super(em);
	}

	public List<Especies> listatodos(){
		Query q = em.createQuery("SELECT e FROM Especies e");
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
