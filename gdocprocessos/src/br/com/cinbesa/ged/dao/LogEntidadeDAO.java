package br.com.cinbesa.ged.dao;

import javax.persistence.EntityManager;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.TblLogEntidade;

@Component
public class LogEntidadeDAO extends GenericDao<TblLogEntidade, Integer> {

	private static final long serialVersionUID = 1L;

	public LogEntidadeDAO(EntityManager em) {
		super(em);
	}

	public void registrar(TblLogEntidade tblLogEntidade){
		em.persist(tblLogEntidade);
	}
}
