package br.com.cinbesa.ged.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.TblUfir;

@Component
public class UfirDAO extends GenericDao<TblUfir, Integer>{

	private static final long serialVersionUID = 1L;

	public UfirDAO(EntityManager em) {
		super(em);
	}

	public TblUfir salvar(TblUfir ufir){
		return em.merge(ufir);
	}
	
	public Long count(){
		Query q = em.createQuery("SELECT count(r) FROM TblUfir r");
		try{
			return (Long) q.getSingleResult();
		}catch(NoResultException nores){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblUfir> listartodos(int startAt, int maxPerPage){
		Query q = em.createQuery("SELECT r FROM TblUfir r ORDER BY r.nrAno ASC");
		q.setFirstResult(startAt);
		q.setMaxResults(maxPerPage);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblUfir> listartodos(){
		Query q = em.createQuery("SELECT r FROM TblUfir r ORDER BY r.nrAno ASC");
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public TblUfir ufirPorAno(short ano){
		Query q = em.createQuery("SELECT r FROM TblUfir r where r.nrAno = :nrAno");
		q.setParameter("nrAno", ano);
		try{
			return (TblUfir)q.getSingleResult();
		}catch(NoResultException nores){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
