package br.com.cinbesa.ged.dao;

import javax.persistence.EntityManager;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.TblLogInteressados;
@Component
public class LogInteressadoDAO extends GenericDao<TblLogInteressados, Integer>{

	private static final long serialVersionUID = 1L;

	public LogInteressadoDAO(EntityManager em) {
		super(em);
	}
	
	public void registrar(TblLogInteressados log){
		em.persist(log);
	}
	
}
