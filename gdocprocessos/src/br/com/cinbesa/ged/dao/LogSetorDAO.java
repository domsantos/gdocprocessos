package br.com.cinbesa.ged.dao;

import javax.persistence.EntityManager;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.TblLogSetor;


@Component
public class LogSetorDAO extends GenericDao<TblLogSetor, Integer>{

	public LogSetorDAO(EntityManager em) {
		super(em);
	}
	
	public void registro(TblLogSetor logSetor){
		em.persist(logSetor);
	}
	
	
}
