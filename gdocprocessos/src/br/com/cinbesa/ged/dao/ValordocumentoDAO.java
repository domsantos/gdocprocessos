package br.com.cinbesa.ged.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.TblProcessos;
import br.com.cinbesa.ged.entidade.TblValorDocumento;

@Component
public class ValordocumentoDAO extends GenericDao<TblValorDocumento, Integer>{

	private static final long serialVersionUID = 1L;

	public ValordocumentoDAO(EntityManager em) {
		super(em);
	}

	public TblValorDocumento cadastrar(TblValorDocumento valorDocumento){
		return em.merge(valorDocumento);
	}
	
	public List<TblValorDocumento> listaDocumentos(TblProcessos processo){
		Query q = em.createQuery("SELECT v FROM TblValorDocumento v WHERE v.idProcesso = :processo ORDER BY v.dtLancamento DESC");
		q.setParameter("processo", processo);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
}
