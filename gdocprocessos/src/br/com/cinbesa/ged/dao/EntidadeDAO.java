package br.com.cinbesa.ged.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.Entidade;


@Component
public class EntidadeDAO extends GenericDao<Entidade, Integer>{

	private static final long serialVersionUID = 1L;
	
	public EntidadeDAO(EntityManager em) {
		super(em);
	}

	public Long count(){
		Query q = em.createQuery("SELECT count(e) FROM Entidade e");
		try{
			return (Long) q.getSingleResult();
		}catch(NoResultException mnores){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public List<Entidade> listatodos(){
		return super.findAll("Entidade.findAll");
	}
	
	public List<Entidade> listatodos(int startAt, int maxPerPage){
		Query q = em.createQuery("SELECT e FROM Entidade e");
		q.setFirstResult(startAt);
		q.setMaxResults(maxPerPage);
		try{
			return q.getResultList();
		}catch(NoResultException mnores){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Entidade salvar(Entidade entidade){
		return em.merge(entidade);
	}
	
	public Entidade atualizar(Entidade entidade){
		return em.merge(entidade);
	}
	
	public List<Entidade> listaPorTipoProcesso(Integer idTpProc){
		Query q = em.createQuery("SELECT e FROM TblTipoprocesso tp JOIN tp.idEntidade e WHERE tp.cdTipoprocesso = :cdTipoprocesso");
		q.setParameter("cdTipoprocesso", idTpProc);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	

}
