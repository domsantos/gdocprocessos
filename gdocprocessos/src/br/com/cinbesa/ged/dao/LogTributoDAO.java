package br.com.cinbesa.ged.dao;

import javax.persistence.EntityManager;

import br.com.caelum.vraptor.ioc.Component;

import br.com.cinbesa.ged.entidade.TblLogTributo;

@Component
public class LogTributoDAO extends GenericDao<TblLogTributo, Integer>{

	public LogTributoDAO(EntityManager em) {
		super(em);
		
	}

	public void registrar(TblLogTributo log){
		em.persist(log);
	}
	
	
}
