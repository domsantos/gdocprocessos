package br.com.cinbesa.ged.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.dto.RelatorioProcessoAtrasoDTO;
import br.com.cinbesa.ged.dto.RelatorioProcessoVsSetorDTO;
import br.com.cinbesa.ged.entidade.Entidade;

@Component
public class RelatoriosDAO {
	
	private EntityManager em;
	
	public RelatoriosDAO(EntityManager em){
		this.em = em;
	}
	
	public List<RelatorioProcessoAtrasoDTO> listaProcessosAtrasados(Date dataIncial, 
			Date dataFinal, Entidade entidade, char stProcesso){
		Query q = em.createQuery("SELECT new br.com.cinbesa.ged.dto.RelatorioProcessoAtrasoDTO( " +
				"p.idProcesso, " +
				"p.nrProcesso, " +
				"p.nrAno, " +
				"t.dtTramite, " +
				"s.nmSetor, " +
				"(DAY(GETDATE() - p.dtAberturaProcesso) - (SELECT SUM(r.nrPrazoRota) FROM TblRotas r WHERE r.cdTipoprocesso = p.cdTipoprocesso))" +
				") " +
				"FROM TblTramites t "+
				"JOIN t.idProcesso p "+
				"JOIN t.cdSetor s "+
				"WHERE p.cdTipoprocesso in (SELECT tp2.cdTipoprocesso FROM TblTipoprocesso tp2 "+
											"WHERE tp2.idEntidade = :entidade AND tp2.cdTipoprocesso in (SELECT cdTipoprocesso "+
													"FROM TblRotas r " +
													"WHERE r.cdTipoprocesso = tp2.cdTipoprocesso)) "+
				"AND t.idTramite = (SELECT MAX(tra.idTramite) FROM TblTramites tra " +
									"WHERE tra.idProcesso = p.idProcesso) "+
				"AND p.dtAberturaProcesso BETWEEN :datainicial AND :datafinal "+
				"AND p.stProcesso = :situacao "+
				"AND (SELECT DAY(GETDATE() - pr.dtAberturaProcesso) " +
						"FROM TblProcessos pr " +
						"WHERE pr.idProcesso = p.idProcesso) " +
						"> " +
						"(SELECT SUM(r.nrPrazoRota) " +
						"FROM TblRotas r WHERE " +
						"r.cdTipoprocesso = p.cdTipoprocesso) " +
				"ORDER BY s.nmSetor");
		q.setParameter("entidade", entidade);
		q.setParameter("datainicial", dataIncial);
		q.setParameter("datafinal", dataFinal);
		q.setParameter("situacao", stProcesso);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public List<RelatorioProcessoVsSetorDTO> listaProcessosVsSetor(Date dataIncial, 
			Date dataFinal, 
			Entidade entidade, 
			char stProcessoAtivo, 
			char stTramiteFechado, 
			char stTramiteArquivado){
		Query q = em.createQuery("SELECT new br.com.cinbesa.ged.dto.RelatorioProcessoVsSetorDTO(p.nrProcesso, p.nrAno, p.dtAberturaProcesso, s.nmSetor, tp.nmTipoprocesso) "+
									"FROM TblTramites t " +
									"JOIN t.idProcesso p " +
									"JOIN p.cdTipoprocesso tp " +
									"JOIN t.cdSetor s " +
									"WHERE p.stProcesso = :stProcessoAtivo " +
										"AND s.idEntidade = :entidade " +
										"AND t.idTramite = (SELECT MAX(tr.idTramite) FROM TblTramites tr WHERE tr.idProcesso = p.idProcesso AND (tr.stTramite != :stTramiteFechado OR tr.stTramite != :stTramiteArquivado)) " +
										"AND p.dtAberturaProcesso BETWEEN :datainicial and :datafinal " +
									"ORDER BY s.nmSetor, tp.nmTipoprocesso");
		q.setParameter("stProcessoAtivo", stProcessoAtivo);
		q.setParameter("entidade", entidade);
		q.setParameter("stTramiteFechado", stTramiteFechado);
		q.setParameter("stTramiteArquivado", stTramiteArquivado);
		q.setParameter("datainicial",dataIncial);
		q.setParameter("datafinal", dataFinal);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
}
