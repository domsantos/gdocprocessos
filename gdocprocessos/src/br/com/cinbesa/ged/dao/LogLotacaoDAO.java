package br.com.cinbesa.ged.dao;

import javax.persistence.EntityManager;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.TblLogLotacao;
@Component
public class LogLotacaoDAO extends GenericDao<TblLogLotacao, Integer>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LogLotacaoDAO(EntityManager em) {
		super(em);
	}

	public void registrar(TblLogLotacao logLotacao){
		em.persist(logLotacao);
	}

}
