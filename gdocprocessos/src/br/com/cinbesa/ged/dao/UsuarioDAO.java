package br.com.cinbesa.ged.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.Entidade;
import br.com.cinbesa.ged.entidade.TblUsuario;

@Component
public class UsuarioDAO extends GenericDao<TblUsuario, Integer> {
	private static final long serialVersionUID = 1L;
	
	public UsuarioDAO(EntityManager em) {
		super(em);
	}
	
	public TblUsuario salvar(TblUsuario usuario){
		return em.merge(usuario);
	}
	
	public TblUsuario login(TblUsuario usuario){
		Query q = em.createNamedQuery("TblUsuario.findByNrMatricula");
		q.setParameter("nrMatricula", usuario.getNrMatricula());
		try{
			return (TblUsuario) q.getSingleResult();
		}catch (NoResultException nores) {
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public TblUsuario buscaPorMatricula(String nrMat){
		Query q = em.createNamedQuery("TblUsuario.findByNrMatricula");
		q.setParameter("nrMatricula", nrMat);
		try{
			return (TblUsuario) q.getSingleResult();
		}catch (NoResultException nores) {
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblUsuario> listartodos( Entidade entidade, char stLotacao){
		Query q = em.createQuery("select u from TblLotacao l " +
				"join l.idUsuario u " +
				"join l.cdSetor s " +
				"join s.idEntidade e " +
				"where e.idEntidade = :entidade AND " +
				"l.stLotacao = :status");
		q.setParameter("entidade", entidade.getIdEntidade());
		q.setParameter("status", stLotacao);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblUsuario> listartodos(int startAt,int maxPerPag, Entidade entidade, char stLotacao){
		Query q = em.createQuery("select u from TblLotacao l " +
				"join l.idUsuario u " +
				"join l.cdSetor s " +
				"join s.idEntidade e " +
				"where e.idEntidade = :entidade AND " +
				"l.stLotacao = :status");
		q.setFirstResult(startAt);
		q.setMaxResults(maxPerPag);
		q.setParameter("entidade", entidade.getIdEntidade());
		q.setParameter("status", stLotacao);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Long count(Entidade entidade, char stLotacao){
		Query q = em.createQuery("SELECT count(u) from TblLotacao l " +
				"join l.idUsuario u " +
				"join l.cdSetor s " +
				"join s.idEntidade e " +
				"where e.idEntidade = :entidade AND " +
				"l.stLotacao = :status"
				);
		q.setParameter("entidade", entidade.getIdEntidade());
		q.setParameter("status", stLotacao);
		try{
			return (Long) q.getSingleResult();
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblUsuario> listarPorEntidadeLotacaoAdmin(Entidade entidade, char statusLotacaoAtivado, char statusAdmin){
		Query query = em.createQuery("select u from TblLotacao l " +
				"join l.idUsuario u " +
				"join l.cdSetor s " +
				"join s.idEntidade e " +
				"where e.idEntidade = :entidade AND " +
				"l.stLotacao = :status AND " +
				"u.cdNivelAdministrador = :statusAdmin");
		query.setParameter("entidade", entidade.getIdEntidade());
		query.setParameter("status", statusLotacaoAtivado);
		query.setParameter("statusAdmin", statusAdmin);
		try{
			return query.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
