package br.com.cinbesa.ged.dao;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import sun.security.util.BigInt;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.entidade.Entidade;
import br.com.cinbesa.ged.entidade.TblProcessos;
import br.com.cinbesa.ged.entidade.TblSetor;
import br.com.cinbesa.ged.util.ListaApp;

@Component
public class ProcessoDAO extends GenericDao<TblProcessos, Long>{

	private static final long serialVersionUID = 1L;

	public ProcessoDAO(EntityManager em) {
		super(em);
	}
	
	public Long countPorEntidade(Entidade entidade, int cdTipoRegistro, char stTramiteRecebido, char stTramiteEspera){
		Query q = em.createQuery("SELECT count(p) FROM TblTramites t " +
				"JOIN t.idProcesso p " +
				"JOIN t.cdSetor s " +
				"WHERE s.idEntidade = :idEntidade " +
					"AND p.cdTipoRegistro = :cdTipoRegistro " +
					"AND t.stTramite = :stTramiteEspera OR t.stTramite = :stTramiteRecebido");
		q.setParameter("idEntidade", entidade);
		q.setParameter("cdTipoRegistro", cdTipoRegistro);
		q.setParameter("stTramiteEspera", stTramiteEspera);
		q.setParameter("stTramiteRecebido", stTramiteRecebido);
		try{
			return (Long) q.getSingleResult();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblProcessos> listaPorEntidade(Entidade entidade, int cdTipoRegistor, char stTramiteRecebido, char stTramiteEspera){
		Query q = em.createQuery("SELECT p FROM TblTramites t " +
				"JOIN t.idProcesso p " +
				"JOIN t.cdSetor s " +
				"WHERE s.idEntidade = :idEntidade " +
					"AND p.cdTipoRegistro = :cdTipoRegistro " +
					"AND t.stTramite = :stTramiteEspera OR t.stTramite = :stTramiteRecebido " +
				"ORDER BY p.dtAberturaProcesso DESC");
		q.setParameter("idEntidade", entidade);
		q.setParameter("cdTipoRegistro", cdTipoRegistor);
		q.setParameter("stTramiteEspera", stTramiteEspera);
		q.setParameter("stTramiteRecebido", stTramiteRecebido);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblProcessos> listaPorEntidade(Entidade entidade,int cdTipoRetistro, char stTramiteRecebido, char stTramiteEspera, int startAt, int maxPerPage){
		Query q = em.createQuery("SELECT p FROM TblTramites t " +
				"JOIN t.idProcesso p " +
				"JOIN t.cdSetor s " +
				"WHERE s.idEntidade = :idEntidade " +
					"AND p.cdTipoRegistro = :cdTipoRegistro " +
					"AND t.stTramite = :stTramiteEspera OR t.stTramite = :stTramiteRecebido");
		q.setParameter("idEntidade", entidade);
		q.setParameter("cdTipoRegistro", cdTipoRetistro);
		q.setParameter("stTramiteEspera", stTramiteEspera);
		q.setParameter("stTramiteRecebido", stTramiteRecebido);
		q.setFirstResult(startAt);
		q.setMaxResults(maxPerPage);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblProcessos> listaPorNumeroProcAnoEntidade(BigInteger id, short ano, Entidade entidade){
		Query q = em.createQuery("SELECT p FROM TblProcessos p WHERE p.idEntidade = :idEntidade  AND p.nrProcesso = :nrProcesso AND p.nrAno = :nrAno ORDER BY p.dtAberturaProcesso DESC");
		q.setParameter("idEntidade", entidade);
		q.setParameter("nrProcesso", id);
		q.setParameter("nrAno", ano);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblProcessos> listaPorNumeroProcAnoSetor(BigInteger nrprocesso, short anoproc,int cdTipoProcesso ,TblSetor setor){
		Query q = em.createQuery("SELECT p FROM TblTramites t JOIN t.idProcesso p WHERE " +
				" t.cdSetor = :cdSetor AND t.stTramite = :stTramite " +
				" AND p.nrProcesso = :nrProcesso AND p.nrAno = :nrAno " +
				" AND p.cdTipoRegistro = :cdTipoRegistro " +
				"ORDER BY p.dtAberturaProcesso DESC");
		q.setParameter("nrProcesso", nrprocesso);
		q.setParameter("nrAno", anoproc);
		q.setParameter("cdTipoRegistro", cdTipoProcesso);
		q.setParameter("cdSetor", setor);
		q.setParameter("stTramite", ListaApp.SITUACAOTRAMITERECEBIDO);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	public Long countProcessosPorSetor(TblSetor setor,int cdTipoRegistro){
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT count(p) FROM TblTramites t ");
		sql.append("JOIN t.idProcesso p WHERE t.cdSetor = :cdSetor AND t.stTramite = :stTramite ");
		sql.append("AND t.idProcesso.cdTipoRegistro = :cdTipoRegistro ");
		sql.append("AND t.idProcesso.stProcesso = :stProcesso ");
//		Query q = em.createQuery("SELECT count(p) FROM TblTramites t JOIN t.idProcesso p WHERE t.cdSetor = :cdSetor AND t.stTramite = :stTramite");
		Query q = em.createQuery(sql.toString());
		q.setParameter("cdSetor", setor);
		q.setParameter("cdTipoRegistro", cdTipoRegistro);
		q.setParameter("stTramite", ListaApp.SITUACAOTRAMITERECEBIDO);
		q.setParameter("stProcesso", ListaApp.SITUACAOPROCESSOABERTO);
		try{
			return (Long) q.getSingleResult();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	
	// Divida
	public List<TblProcessos> listaPorSetor(TblSetor setor){
		Query q = em.createQuery("SELECT p FROM TblTramites t JOIN t.idProcesso p WHERE t.cdSetor = :cdSetor AND t.stTramite = :stTramite ORDER BY p.dtAberturaProcesso DESC");
		q.setParameter("cdSetor", setor);
		q.setParameter("stTramite", ListaApp.SITUACAOTRAMITERECEBIDO);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblProcessos> listaPorSetor(TblSetor setor,int cdTipoRegistro ,int startAt, int maxPerPage){
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT p FROM TblTramites t ");
		sql.append("JOIN t.idProcesso p ");
		sql.append("WHERE t.cdSetor = :cdSetor AND t.stTramite = :stTramite ");
		sql.append("AND t.idProcesso.cdTipoRegistro = :cdTipoRegistro ");
		sql.append("AND t.idProcesso.stProcesso = :stProcesso ");
		sql.append("ORDER BY p.dtAberturaProcesso DESC");
		
		Query q = em.createQuery(sql.toString());
		q.setParameter("cdSetor", setor);
		q.setParameter("cdTipoRegistro", cdTipoRegistro);
		q.setParameter("stTramite", ListaApp.SITUACAOTRAMITERECEBIDO);
		q.setParameter("stProcesso", ListaApp.SITUACAOPROCESSOABERTO);
		q.setFirstResult(startAt);
		q.setMaxResults(maxPerPage);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	public TblProcessos salvarProcesso(TblProcessos processo){
		return em.merge(processo);
	}
	
	public Long countPorSetorSituacaoEmEspera(TblSetor setor){
		Query q = em.createQuery("SELECT count(p) FROM TblTramites t JOIN t.idProcesso p WHERE t.cdSetor = :cdSetor AND t.stTramite = :stTramite");
		q.setParameter("cdSetor", setor);
		q.setParameter("stTramite", ListaApp.SITUACAOTRAMITEESPERA);
		try{
			return (Long)q.getSingleResult();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblProcessos> listaPorSetorSituacaoEmEspera(TblSetor setor){
		Query q = em.createQuery("SELECT p FROM TblTramites t JOIN t.idProcesso p WHERE t.cdSetor = :cdSetor AND t.stTramite = :stTramite ORDER BY p.dtAberturaProcesso DESC");
		q.setParameter("cdSetor", setor);
		q.setParameter("stTramite", ListaApp.SITUACAOTRAMITEESPERA);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblProcessos> listaPorSetorSituacaoEmEspera(TblSetor setor, int startAt, int maxPerPage){
		Query q = em.createQuery("SELECT p FROM TblTramites t JOIN t.idProcesso p WHERE t.cdSetor = :cdSetor AND t.stTramite = :stTramite ORDER BY p.dtAberturaProcesso DESC");
		q.setParameter("cdSetor", setor);
		q.setParameter("stTramite", ListaApp.SITUACAOTRAMITEESPERA);
		q.setFirstResult(startAt);
		q.setMaxResults(maxPerPage);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	
	public List<TblProcessos> pesquisaProcesso(TblProcessos processo){
		String query = "SELECT p FROM TblProcessos p ";
		String joinCampos = "";
		String whereCampos = "WHERE ";
		String AND = "AND ";
		boolean flagAND = false;
		if(processo.getNrProcesso() != null){
			whereCampos += "p.nrProcesso = :nrProcesso ";
			flagAND = true;
		}
		if(processo.getNrAno() != 0){
			if(flagAND){ whereCampos += AND;}
			whereCampos += "p.nrAno = :nrAno ";
			flagAND = true;
		}
		if(processo.getIdInteressado().getNmInteressado() != null){
			joinCampos += "JOIN p.idInteressado i ";
			if(flagAND){ whereCampos += AND;}
			whereCampos += "i.nmInteressado LIKE :nmInteressado ";
			flagAND = true;
		}
		if(processo.getCdTipoprocesso() != null){
			if(flagAND){ whereCampos += AND;}
			whereCampos += "p.cdTipoprocesso = :cdTipoprocesso ";
			flagAND = true;
		}
		if(processo.getIdEntidade() != null){
			if(flagAND){ whereCampos += AND;}
			whereCampos += "p.idEntidade = :idEntidade ";
			flagAND = true;
		}
		if(processo.getStProcesso() != 'X'){
			if(flagAND){ whereCampos += AND;}
			whereCampos += "p.stProcesso = :stProcesso ";
			flagAND = true;
		}
		
		query += joinCampos + whereCampos;
		Query q = em.createQuery(query);
		
		if(processo.getNrProcesso() != null){ q.setParameter("nrProcesso", processo.getNrProcesso()); }
		if(processo.getNrAno() != 0){ q.setParameter("nrAno", processo.getNrAno()); }
		if(processo.getIdInteressado().getNmInteressado() != null){ q.setParameter("nmInteressado", "%"+ processo.getIdInteressado().getNmInteressado() + "%"); }
		if(processo.getCdTipoprocesso() != null){ q.setParameter("cdTipoprocesso", processo.getCdTipoprocesso()); }
		if(processo.getIdEntidade() != null){ q.setParameter("idEntidade", processo.getIdEntidade()); }
		if(processo.getStProcesso() != 'X'){ q.setParameter("stProcesso",processo.getStProcesso()); }
		
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public Integer getNumeroProcesso(Entidade entidade){
		Query q = em.createNativeQuery("DECLARE	@nrProcesso int EXEC sp_get_numero_processo @nrProcesso, @idEntidade = "+entidade.getIdEntidade().toString());
		return (Integer) q.getSingleResult();
	}
	
	public Long countListaProcessosEncerrados(Entidade entidade, char situacaoProcesso, char stTramite, int tiporegistro){
		Query q = em.createQuery("SELECT count(p) FROM TblTramites t " +
				"JOIN t.idProcesso p JOIN t.cdSetor s " +
				"WHERE s.idEntidade = :entidade " +
				"AND t.stTramite = :stTramite " +
				"AND p.stProcesso = :situacao " +
				"AND p.cdTipoRegistro = :tiporegistro");
		q.setParameter("entidade", entidade);
		q.setParameter("situacao", situacaoProcesso);
		q.setParameter("stTramite", stTramite);
		q.setParameter("tiporegistro", tiporegistro);
		try{
			return (Long) q.getSingleResult();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TblProcessos> listaProcessosEncerrados(Entidade entidade, char situacaoProcesso, char stTramite, int tiporegistro, int startAt, int maxPerPage){
		Query q = em.createQuery("SELECT p FROM TblTramites t " +
				"JOIN t.idProcesso p JOIN t.cdSetor s " +
				"WHERE s.idEntidade = :entidade " +
				"AND t.stTramite = :stTramite " +
				"AND p.stProcesso = :situacao "+
				"AND p.cdTipoRegistro = :tiporegistro");
		q.setParameter("entidade", entidade);
		q.setParameter("situacao", situacaoProcesso);
		q.setParameter("stTramite", stTramite);
		q.setParameter("tiporegistro", tiporegistro);
		q.setFirstResult(startAt);
		q.setMaxResults(maxPerPage);
		try{
			return q.getResultList();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public TblProcessos findPorNumAnoProcesso(TblProcessos proc){
		Query q = em.createQuery("SELECT p FROM TblProcessos p " +
				"WHERE p.nrProcesso = :numproc AND p.nrAno = :nrano");
		q.setParameter("numproc", proc.getNrProcesso());
		q.setParameter("nrano", proc.getNrAno());
		try{
			return (TblProcessos) q.getSingleResult();
		}catch(NoResultException nores){
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	
}
