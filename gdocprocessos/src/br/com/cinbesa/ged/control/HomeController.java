package br.com.cinbesa.ged.control;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.cinbesa.ged.intercept.Restrito;
import br.com.cinbesa.ged.intercept.SessaoUsuario;
import br.com.cinbesa.ged.util.ListaApp;

@Resource
public class HomeController {
	private final SessaoUsuario sessao;
	private Result result;
	public HomeController(SessaoUsuario sessao, Result result) {
		this.sessao = sessao;
		this.result = result;
	}
	
	@Restrito(nome = ListaApp.HOME)
	@Path("/home")
	public void home(){
		result.redirectTo(ProcessoController.class).processosetor(null);
	}
	
	public void sempermissao(){}
	
	public void logout(){
		sessao.sair();
		result.redirectTo(IndexController.class).index();
	}
}
