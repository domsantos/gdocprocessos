package br.com.cinbesa.ged.control;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.cinbesa.ged.intercept.Restrito;
import br.com.cinbesa.ged.intercept.SessaoUsuario;
import br.com.cinbesa.ged.util.ListaApp;

@Resource
public class MenuadminController {

	private final Result result;
	private final Validator validator;
	private SessaoUsuario sessaoUsuario;
	
	public MenuadminController(Result result, Validator validator, SessaoUsuario sessaoUsuario) {
		this.result = result;
		this.validator = validator;
		this.sessaoUsuario = sessaoUsuario;
	}

	@Restrito(nome=ListaApp.MENU_ADMIN)
	@Path("/menuadmin")
	public void menu(){
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		}
	}
}
