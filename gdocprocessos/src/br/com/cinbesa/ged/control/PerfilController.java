package br.com.cinbesa.ged.control;

import java.util.ArrayList;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.cinbesa.ged.dao.AplicacoesDAO;
import br.com.cinbesa.ged.dao.PerfilDAO;
import br.com.cinbesa.ged.entidade.TblAplicacoes;
import br.com.cinbesa.ged.entidade.TblPerfil;
import br.com.cinbesa.ged.intercept.Restrito;
import br.com.cinbesa.ged.intercept.SessaoUsuario;
import br.com.cinbesa.ged.log.control.LogPerfilAplicacoesController;
import br.com.cinbesa.ged.log.control.LogPerfilController;
import br.com.cinbesa.ged.util.ListaApp;
import br.com.cinbesa.ged.util.PaginacaoUtil;

@Resource
public class PerfilController {

	private final Result result;
	private final Validator validator;
	private final PerfilDAO perfilDao;
	private final AplicacoesDAO aplicacoesDAO;
	private LogPerfilController logPerfilController;
	private LogPerfilAplicacoesController logPerfilAplicacoesController;
	private SessaoUsuario sessaoUsuario;

	public PerfilController(Result result, Validator validator,
			PerfilDAO perfilDAO, AplicacoesDAO aplicacoesDAO,
			LogPerfilController logPerfilController,
			LogPerfilAplicacoesController logPerfilAplicacoesController,
			SessaoUsuario sessaoUsuario) {
		this.result = result;
		this.validator = validator;
		this.perfilDao = perfilDAO;
		this.aplicacoesDAO = aplicacoesDAO;
		this.logPerfilController = logPerfilController;
		this.logPerfilAplicacoesController = logPerfilAplicacoesController;
		this.sessaoUsuario = sessaoUsuario;

	}

	@Restrito(nome = ListaApp.LISTAPERFIL)
	@Path({ "/perfil", "/perfil/pag-{pag}" })
	public void lista(String pag) {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
				result.redirectTo(HomeController.class).sempermissao();
			} else {
				List<Integer> paginas = PaginacaoUtil.listaPaginas(perfilDao
						.count());
				int paginaSelecionada = PaginacaoUtil.numeroDaPagina(pag,
						paginas);

				if (paginas.size() != 0) {
					result.include("paginationList", PaginacaoUtil
							.listaPaginasView(paginas, paginaSelecionada));
					result.include("pagSelec", pag);
					result.include("first", PaginacaoUtil.FIRST);
					result.include("last", PaginacaoUtil.LAST);
					result.include("link", "/perfil/pag-");
				}
				result.include("tblPerfilList", perfilDao.listatodos(
						PaginacaoUtil.reginicial(paginaSelecionada),
						PaginacaoUtil.TAMTABELA, sessaoUsuario
								.getLotacaoUsuario().getCdSetor()
								.getIdEntidade()));
			}
		}
	}

	@Restrito(nome = ListaApp.ADDPERFIL)
	public List<TblAplicacoes> novo() {
		return aplicacoesDAO.listatodos();
	}

	@Restrito(nome = ListaApp.ADDPERFIL)
	@Post
	public void cadastrar(TblPerfil perfil, List<Integer> lista) {
		if (perfil.getDsPerfil() == null) {
			validator.add(new ValidationMessage("Informe o nome do perfil",
					"dsPerfil"));
		}
		if (lista == null) {
			validator.add(new ValidationMessage(
					"Selecione alguma aplica��o para o Perfil", "appList"));
		}
		if (perfilDao.findPorDescricao(perfil) != null) {
			validator.add(new ValidationMessage(
					"Perfil com este nome j� existe", "sameDsPerfil"));
		}
		validator.onErrorRedirectTo(PerfilController.class).novo();
		perfil.setDsPerfil(perfil.getDsPerfil().toUpperCase());
		perfil.setIdEntidade(sessaoUsuario.getLotacaoUsuario().getCdSetor()
				.getIdEntidade());
		EntityManager em = perfilDao.getEm();
		perfil = em.merge(perfil);
		logPerfilController.registrar(perfil, ListaApp.LOG_OPERACAO_INSERCAO);
		if (perfil.getTblAplicacoesList() == null) {
			List<TblAplicacoes> listaaux = new ArrayList<TblAplicacoes>();
			perfil.setTblAplicacoesList(listaaux);
		}
		for (int i = 0; i < lista.size(); i++) {
			TblAplicacoes app = aplicacoesDAO.find(lista.get(i));

			perfil.getTblAplicacoesList().add(app);
			app.getTblPerfilList().add(perfil);

			em.merge(perfil);
			em.merge(app);
			logPerfilAplicacoesController.registrar(perfil.getCdPerfil(),
					app.getCdAplicacao(), ListaApp.LOG_OPERACAO_INSERCAO);
		}

		result.redirectTo(PerfilController.class).lista(null);
	}

	@Restrito(nome = ListaApp.EDITPERFIL)
	@Path("/perfil/editar/{id}")
	public List<TblAplicacoes> editar(Integer id) {
		TblPerfil p = perfilDao.find(TblPerfil.class, id);
		result.include("p", p);
		return aplicacoesDAO.listatodos();
	}

	@Restrito(nome = ListaApp.EDITPERFIL)
	@Post
	public void salvar(TblPerfil perfil, List<Integer> lista) {
		if (perfil.getDsPerfil() == null) {
			validator.add(new ValidationMessage("Descri��o inv�lida",
					"dsPerfil"));
		}
		if (lista == null) {
			validator.add(new ValidationMessage("Selecione uma aplica��o",
					"lista"));
		}
		validator.onErrorForwardTo(PerfilController.class).editar(
				perfil.getCdPerfil());

		TblPerfil perfildb = perfilDao.find(TblPerfil.class,
				perfil.getCdPerfil());

		List<TblAplicacoes> removedoperfil = new ArrayList<TblAplicacoes>();
		perfildb.setDsPerfil(perfil.getDsPerfil().toUpperCase());
		for (TblAplicacoes app : perfildb.getTblAplicacoesList()) {
			if (!lista.contains(app.getCdAplicacao())) {
				removedoperfil.add(app);
			}
		}

		List<TblAplicacoes> addnoperfil = new ArrayList<TblAplicacoes>();
		boolean aux = false;
		for (Integer i : lista) {
			aux = false;
			for (TblAplicacoes ap : perfildb.getTblAplicacoesList()) {
				if (ap.getCdAplicacao() == i) {
					aux = true;
					break;
				}
			}
			if (!aux) {
				addnoperfil.add(aplicacoesDAO.find(i));
			}
		}
		EntityManager em = perfilDao.getEm();
		for (TblAplicacoes app : removedoperfil) {
			perfildb.getTblAplicacoesList().remove(app);
			app.getTblPerfilList().remove(perfildb);

			// em.merge(perfildb);
			em.merge(app);
			logPerfilAplicacoesController.registrar(perfildb.getCdPerfil(),
					app.getCdAplicacao(), ListaApp.LOG_OPERACAO_EXCLUSAO);
		}
		for (TblAplicacoes app : addnoperfil) {
			perfildb.getTblAplicacoesList().add(app);
			app.getTblPerfilList().add(perfildb);

			// em.merge(perfildb);
			em.merge(app);
			logPerfilAplicacoesController.registrar(perfildb.getCdPerfil(),
					app.getCdAplicacao(), ListaApp.LOG_OPERACAO_INSERCAO);
		}

		perfildb = em.merge(perfildb);
		logPerfilController
				.registrar(perfildb, ListaApp.LOG_OPERACAO_ALTERACAO);
		result.redirectTo(PerfilController.class).lista(null);
	}

}
