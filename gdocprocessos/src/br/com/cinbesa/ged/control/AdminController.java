package br.com.cinbesa.ged.control;

import static br.com.caelum.vraptor.view.Results.json;

import java.util.Date;
import java.util.List;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.cinbesa.ged.dao.LotacaoDAO;
import br.com.cinbesa.ged.dao.PerfilDAO;
import br.com.cinbesa.ged.dao.SetorDAO;
import br.com.cinbesa.ged.dao.UsuarioDAO;
import br.com.cinbesa.ged.entidade.TblLotacao;
import br.com.cinbesa.ged.entidade.TblPerfil;
import br.com.cinbesa.ged.entidade.TblSetor;
import br.com.cinbesa.ged.entidade.TblTipoprocesso;
import br.com.cinbesa.ged.entidade.TblUsuario;
import br.com.cinbesa.ged.intercept.Restrito;
import br.com.cinbesa.ged.intercept.SessaoUsuario;
import br.com.cinbesa.ged.util.ListaApp;
import br.com.cinbesa.ged.util.Util;

@Resource
public class AdminController {

	private final Result result;
	private final Validator validator;
	private UsuarioDAO usuarioDAO;
	private SetorDAO setorDAO;
	private PerfilDAO perfilDAO;
	private SessaoUsuario sessaoUsuario;
	private LotacaoDAO lotacaoDAO;

	public AdminController(Result result, Validator validator,
			UsuarioDAO usuarioDAO, SessaoUsuario sessaoUsuario,
			PerfilDAO perfilDAO, SetorDAO setorDAO, LotacaoDAO lotacaoDAO) {
		this.result = result;
		this.validator = validator;
		this.usuarioDAO = usuarioDAO;
		this.sessaoUsuario = sessaoUsuario;
		this.setorDAO = setorDAO;
		this.perfilDAO = perfilDAO;
		this.lotacaoDAO = lotacaoDAO;
	}

	public void novo() {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() != ListaApp.CD_NIVEL_ADMIN_SUPERADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			result.include(
					"tblSetorList",
					setorDAO.listaPorEntidade(sessaoUsuario.getLotacaoUsuario()
							.getCdSetor().getIdEntidade()));
			result.include(
					"tblPerfilList",
					perfilDAO.lista(sessaoUsuario.getLotacaoUsuario()
							.getCdSetor().getIdEntidade()));
		}
	}

	@Post
	public void cadastrar(TblUsuario usuario, String confSenha, Integer cdSetor) {
		if (usuario.getNrMatricula() == null) {
			validator.add(new ValidationMessage(
					"Informe o n�mero da Matr�cula", "Num. Matr�cula"));
		} else {
			if (usuarioDAO.buscaPorMatricula(Util.limpaString(usuario
					.getNrMatricula())) != null) {
				validator.add(new ValidationMessage(
						"Usu�rio j� posui cadastro no sistema",
						"Usu�rio cadastrado"));
			}
		}
		if (usuario.getVlSenha() == null) {
			validator.add(new ValidationMessage("Insira a senha do Usu�rio",
					"Senha"));
		} else {
			if (confSenha == null) {
				validator.add(new ValidationMessage(
						"Confirme a senha do Usu�rio", "Confirma��o Senha"));
			} else {
				if (!usuario.getVlSenha().equals(confSenha)) {
					validator.add(new ValidationMessage(
							"Senha informada n�o est� confirmada",
							"Senha e confirma��o n�o coincidem"));
				}
			}
		}
		if (usuario.getNmUsuario() == null) {
			validator.add(new ValidationMessage("Informe o nome do usu�rio",
					"Nome do Usu�rio"));
		}
		if (usuario.getCdPerfil().getCdPerfil() == 0) {
			validator
					.add(new ValidationMessage("Selecione um perfil", "Perfil"));
		}
		if (cdSetor == 0) {
			validator.add(new ValidationMessage(
					"Selecione um Setor de Lota��o", "Setor"));
		}

		validator.onErrorRedirectTo(this).novo();

		usuario.setNrMatricula(Util.limpaString(usuario.getNrMatricula()));
		usuario.setCdPerfil(perfilDAO.find(TblPerfil.class, usuario
				.getCdPerfil().getCdPerfil()));
		usuario.setNmUsuario(usuario.getNmUsuario().toUpperCase());
		usuario.setVlSenha(Util.encripta(usuario.getVlSenha()));
		usuario.setCdNivelAdministrador(ListaApp.CD_NIVEL_ADMIN_ENTIDADE);
		usuario = usuarioDAO.salvar(usuario);

		TblLotacao lotacao = new TblLotacao();
		lotacao.setCdSetor(setorDAO.find(TblSetor.class, cdSetor));
		lotacao.setIdUsuario(usuario);
		lotacao.setDtLotacao(new Date());
		lotacao.setStLotacao(ListaApp.LOTACAOATIVADA);

		lotacaoDAO.salvar(lotacao);

		result.redirectTo(HomeController.class).home();
	}

	@Get
	@Path("admin/verificamatricula.json")
	public void listaTiposProcessos(String matricula) {
		matricula = Util.limpaString(matricula);
		TblUsuario usuario = usuarioDAO.buscaPorMatricula(matricula);
		if (usuario == null) {
			result.nothing();
		} else {
			result.use(json())
					.withoutRoot()
					.from(usuario)
					.exclude("nrMatricula", "nmUsuario", "vlSenha",
							"cdNivelAdministrador", "cdPerfil",
							"tblTramitesList", "tblTramitesList1",
							"tblLotacaoList", "tblProcessosList").serialize();
		}
	}

	
	public void registraradmin() {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() != ListaApp.CD_NIVEL_ADMIN_SUPERADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			result.include("tblUsuarioList", usuarioDAO.listarPorEntidadeLotacaoAdmin(
					sessaoUsuario.getLotacaoUsuario().getCdSetor()
							.getIdEntidade(), ListaApp.LOTACAOATIVADA,
					ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN));
			result.include("adminEntidade", ListaApp.CD_NIVEL_ADMIN_ENTIDADE);
			result.include("adminMaster", ListaApp.CD_NIVEL_ADMIN_SUPERADMIN);
		}
	}

	@Post
	public void registrar(TblUsuario usuario, String statusAdmin) {
		if (usuario.getIdUsuario() == null) {
			validator.add(new ValidationMessage("Selecione o Usu�rio",
					"Usu�rio"));
		}
		if (statusAdmin == "") {
			validator.add(new ValidationMessage("Selecione o Usu�rio",
					"Usu�rio"));
		}
		validator.onErrorRedirectTo(this).registraradmin();

		TblUsuario user = usuarioDAO.find(TblUsuario.class,
				usuario.getIdUsuario());
		user.setCdNivelAdministrador(statusAdmin.charAt(0));
		usuarioDAO.salvar(user);

		result.redirectTo(HomeController.class).home();
	}

}
