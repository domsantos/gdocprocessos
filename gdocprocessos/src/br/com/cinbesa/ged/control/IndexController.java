/***
 * Copyright (c) 2009 Caelum - www.caelum.com.br/opensource
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.cinbesa.ged.control;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.cinbesa.ged.dao.UsuarioDAO;
import br.com.cinbesa.ged.entidade.TblLotacao;
import br.com.cinbesa.ged.entidade.TblUsuario;
import br.com.cinbesa.ged.intercept.SessaoUsuario;
import br.com.cinbesa.ged.util.ListaApp;
import br.com.cinbesa.ged.util.Util;

@Resource
public class IndexController {

	private final Result result;
	private final UsuarioDAO usuariodao;
	private Validator validator;
	private final SessaoUsuario sessao;

	public IndexController(UsuarioDAO usuariodao, Result result,
			Validator validator, SessaoUsuario sessao) {
		this.result = result;
		this.usuariodao = usuariodao;
		this.validator = validator;
		this.sessao = sessao;
	}

	@Path("")
	// @Path("/")
	public void index() {
		if (sessao.isLogado()) {
			result.redirectTo(MenuadminController.class).menu();
		}
	}

	@Post
	public void entrar(TblUsuario usuario) {
		if (usuario.getNrMatricula() == null) {
			validator.add(new ValidationMessage("Matr�cula", "nrMatricula"));
		}
		if (usuario.getVlSenha() == null) {
			validator.add(new ValidationMessage("Senha", "vlSenha"));
		}
		validator.onErrorUsePageOf(IndexController.class).index();

		usuario.setNrMatricula(Util.limpaString(usuario.getNrMatricula()));
		usuario.setVlSenha(Util.encripta(usuario.getVlSenha()));

		TblUsuario userBase = usuariodao.login(usuario);
		if (userBase != null) {
			if (!(userBase.getNrMatricula().equals(usuario.getNrMatricula()))
					|| !(userBase.getVlSenha().equals(usuario.getVlSenha()))) {
				validator.add(new ValidationMessage("Login/Senha inválidos",
						"LS"));
			}
		

		boolean aux = false;
		for (TblLotacao lotacao : userBase.getTblLotacaoList()) {
			if (lotacao.getStLotacao() == ListaApp.LOTACAOATIVADA) {
				aux = true;
			}
		}

		if (!aux) {
			validator.add(new ValidationMessage("Usuário não lotado",
					"user nao lotado"));
		}
		validator.onErrorUsePageOf(IndexController.class).index();

		sessao.login(userBase);

		result.redirectTo(MenuadminController.class).menu();
		}else{
			validator.add(new ValidationMessage("Login não encontrado", "LS"));
		}
		
		validator.onErrorUsePageOf(IndexController.class).index();
	}

	public void sessaoexpirada() {
	}

}
