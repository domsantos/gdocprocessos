package br.com.cinbesa.ged.control;

import static br.com.caelum.vraptor.view.Results.json;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.hibernate.ejb.criteria.path.ListAttributeJoin;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.interceptor.download.Download;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.cinbesa.ged.dao.DocsAnexosDAO;
import br.com.cinbesa.ged.dao.EntidadeDAO;
import br.com.cinbesa.ged.dao.InteressadoDAO;
import br.com.cinbesa.ged.dao.LocalArquivoDAO;
import br.com.cinbesa.ged.dao.ProcArquivadoDAO;
import br.com.cinbesa.ged.dao.ProcessoDAO;
import br.com.cinbesa.ged.dao.ProcessoJuntadoDAO;
import br.com.cinbesa.ged.dao.RegraDAO;
import br.com.cinbesa.ged.dao.RotaDAO;
import br.com.cinbesa.ged.dao.TipoProcessoDAO;
import br.com.cinbesa.ged.dao.TramiteDAO;
import br.com.cinbesa.ged.dto.DocAnexoDTO;
import br.com.cinbesa.ged.dto.RotaEfetivadaDTO;
import br.com.cinbesa.ged.entidade.Entidade;
import br.com.cinbesa.ged.entidade.TblDocumentosanexos;
import br.com.cinbesa.ged.entidade.TblEnderecoarquivamento;
import br.com.cinbesa.ged.entidade.TblInteressados;
import br.com.cinbesa.ged.entidade.TblProcessoArquivado;
import br.com.cinbesa.ged.entidade.TblProcessoArquivadoPK;
import br.com.cinbesa.ged.entidade.TblProcessos;
import br.com.cinbesa.ged.entidade.TblProcessosJuntados;
import br.com.cinbesa.ged.entidade.TblRegras;
import br.com.cinbesa.ged.entidade.TblRotas;
import br.com.cinbesa.ged.entidade.TblSetor;
import br.com.cinbesa.ged.entidade.TblTipoprocesso;
import br.com.cinbesa.ged.entidade.TblTramites;
import br.com.cinbesa.ged.intercept.Restrito;
import br.com.cinbesa.ged.intercept.SessaoUsuario;
import br.com.cinbesa.ged.util.JasperMaker;
import br.com.cinbesa.ged.util.ListaApp;
import br.com.cinbesa.ged.util.PaginacaoUtil;
import br.com.cinbesa.ged.util.Util;

@Resource
public class ProcessoController {
	private final Result result;
	private final Validator validator;
	private SessaoUsuario sessao;
	private ProcessoDAO processoDAO;
	private InteressadoDAO interessadoDAO;
	private TipoProcessoDAO tipoProcessoDAO;
	private TramiteDAO tramiteDAO;
	private EntidadeDAO entidadeDAO;
	private RotaDAO rotaDAO;
	private RegraDAO regraDAO;
	private ServletContext context;
	private JasperMaker jasperMaker;
	private DocsAnexosDAO docsAnexosDAO;
	private LocalArquivoDAO localArquivoDAO;
	private ProcArquivadoDAO procArquivadoDAO;
	private ProcessoJuntadoDAO processoJuntadoDAO;

	public ProcessoController(Result result, Validator validator,
			SessaoUsuario sessao, ProcessoDAO processoDAO,
			InteressadoDAO interessadoDAO, TipoProcessoDAO tipoProcessoDAO,
			TramiteDAO tramiteDAO, EntidadeDAO entidadeDAO, RotaDAO rotaDAO,
			RegraDAO regraDAO, DocsAnexosDAO docsAnexosDAO,
			LocalArquivoDAO localArquivoDAO, ProcArquivadoDAO procArquivadoDAO,
			ProcessoJuntadoDAO processoJuntadoDAO, ServletContext context,
			JasperMaker jasperMaker) {
		this.result = result;
		this.validator = validator;
		this.sessao = sessao;
		this.processoDAO = processoDAO;
		this.interessadoDAO = interessadoDAO;
		this.tipoProcessoDAO = tipoProcessoDAO;
		this.tramiteDAO = tramiteDAO;
		this.entidadeDAO = entidadeDAO;
		this.rotaDAO = rotaDAO;
		this.regraDAO = regraDAO;
		this.docsAnexosDAO = docsAnexosDAO;
		this.context = context;
		this.jasperMaker = jasperMaker;
		this.localArquivoDAO = localArquivoDAO;
		this.procArquivadoDAO = procArquivadoDAO;
		this.processoJuntadoDAO = processoJuntadoDAO;
	}

	@Restrito(nome = ListaApp.PROCPORENTIDADE)
	@Path({ "/processo/processosentidade",
			"/processo/processosentidade/pag-{pag}" })
	public List<TblProcessos> processosentidade(String pag) {
		List<Integer> paginas = PaginacaoUtil.listaPaginas(processoDAO
				.countPorEntidade(sessao.getLotacaoUsuario().getCdSetor()
						.getIdEntidade(), ListaApp.COD_TIPO_REGISTRO_PROCESSO,
						ListaApp.SITUACAOTRAMITERECEBIDO,
						ListaApp.SITUACAOTRAMITEESPERA));
		int paginaSelecionada = PaginacaoUtil.numeroDaPagina(pag, paginas);
		if (paginas.size() != 0) {
			result.include("paginationList",
					PaginacaoUtil.listaPaginasView(paginas, paginaSelecionada));
			result.include("pagSelec", pag);
			result.include("first", PaginacaoUtil.FIRST);
			result.include("last", PaginacaoUtil.LAST);
			result.include("link", "/processo/processosentidade/pag-");
		}
		return processoDAO.listaPorEntidade(sessao.getLotacaoUsuario()
				.getCdSetor().getIdEntidade(),
				ListaApp.COD_TIPO_REGISTRO_PROCESSO,
				ListaApp.SITUACAOTRAMITERECEBIDO,
				ListaApp.SITUACAOTRAMITEESPERA,
				PaginacaoUtil.reginicial(paginaSelecionada),
				PaginacaoUtil.TAMTABELA);
	}

	public void pesquisaprocessosentidade(String numprocesso, String nrano) {
		BigInteger processo = null;
		Short ano = null;
		try {
			processo = new BigInteger(numprocesso);
			ano = new Short(nrano);
		} catch (Exception e) {
			validator.add(new ValidationMessage("Informe Somente n�meros",
					"numProcesso"));
		}
		validator.onErrorRedirectTo(this).processosentidade(null);

		List<TblProcessos> lista = processoDAO.listaPorNumeroProcAnoEntidade(
				processo, ano, sessao.getLotacaoUsuario().getCdSetor()
						.getIdEntidade());
		result.include("tblProcessosList", lista);
		result.of(ProcessoController.class).processosentidade(null);
	}

	@Restrito(nome = ListaApp.PROCPORSETOR)
	@Path({ "/processo/processosetor", "/processo/processosetor/pag-{pag}" })
	public List<TblProcessos> processosetor(String pag) {
		List<Integer> paginas = PaginacaoUtil.listaPaginas(processoDAO
				.countProcessosPorSetor(
						sessao.getLotacaoUsuario().getCdSetor(), 0));
		int paginaSelecionada = PaginacaoUtil.numeroDaPagina(pag, paginas);
		if (paginas.size() != 0) {
			result.include("paginationList",
					PaginacaoUtil.listaPaginasView(paginas, paginaSelecionada));
			result.include("pagSelec", pag);
			result.include("first", PaginacaoUtil.FIRST);
			result.include("last", PaginacaoUtil.LAST);
			result.include("link", "/processo/processosetor/pag-");
		}
		return processoDAO.listaPorSetor(sessao.getLotacaoUsuario()
				.getCdSetor(), ListaApp.COD_TIPO_REGISTRO_PROCESSO,
				PaginacaoUtil.reginicial(paginaSelecionada),
				PaginacaoUtil.TAMTABELA);
	}

	public void pesquisaprocessosetor(String numprocesso, String nrano) {
		BigInteger processo = null;
		Short ano = null;
		try {
			processo = new BigInteger(numprocesso);
			ano = new Short(nrano);
		} catch (Exception e) {
			validator.add(new ValidationMessage("Informe Somente n�meros",
					"numProcesso"));
		}
		validator.onErrorRedirectTo(this).processosetor(null);

		List<TblProcessos> lista = processoDAO.listaPorNumeroProcAnoSetor(
				processo, ano, ListaApp.COD_TIPO_REGISTRO_PROCESSO, sessao
						.getLotacaoUsuario().getCdSetor());
		result.include("tblProcessosList", lista);
		result.of(ProcessoController.class).processosetor(null);
	}

	@Restrito(nome = ListaApp.NOVOPROC)
	@Path("/processo/novoprocesso/{id}")
	public TblInteressados novoprocesso(Integer id) {
		result.include(
				"tblTipoprocessoList",
				tipoProcessoDAO.listaPorEntidade(sessao.getLotacaoUsuario()
						.getCdSetor().getIdEntidade()));
		return interessadoDAO.find(TblInteressados.class, id);
	}

	@Restrito(nome = ListaApp.NOVOPROC)
	@Post
	public void cadastrarprocesso(TblProcessos tblProcessos) {
		if (tblProcessos.getCdTipoprocesso().getCdTipoprocesso() == null) {
			validator.add(new ValidationMessage("Selecione o Tipo do Processo",
					"tpproc"));
		} else {
			if (tblProcessos.getCdTipoprocesso().getCdTipoprocesso() == 0) {
				validator.add(new ValidationMessage(
						"Selecione o Tipo do Processo", "tpproc"));
			}
		}
		if (tblProcessos.getCdTipoprocesso().getCdTipoprocesso() == null) {
			validator.add(new ValidationMessage("Selecione o Tipo do Processo",
					"tpproc"));
		}
		if (tblProcessos.getDsProcesso() == null) {
			validator.add(new ValidationMessage(
					"Informe uma descri��o para o Processo", "dsproc"));
		}
		validator.onErrorRedirectTo(this).novoprocesso(
				tblProcessos.getIdInteressado().getIdInteressado());
		if (tblProcessos.getDsEndereco() != null) {
			tblProcessos.setDsEndereco(tblProcessos.getDsEndereco()
					.toUpperCase());
		}
		if (tblProcessos.getDsComplemento() != null) {
			tblProcessos.setDsComplemento(tblProcessos.getDsComplemento()
					.toUpperCase());
		}
		if (tblProcessos.getNrCep() != null) {
			tblProcessos.setNrCep(Util.limpaString(tblProcessos.getNrCep()));
		}
		if (tblProcessos.getDsBairro() != null) {
			tblProcessos.setDsBairro(tblProcessos.getDsBairro().toUpperCase());
		}
		if (tblProcessos.getNmCidade() != null) {
			tblProcessos.setNmCidade(tblProcessos.getNmCidade().toUpperCase());
		}
		if (tblProcessos.getDsUf() != null) {
			tblProcessos.setDsUf(tblProcessos.getDsUf().toUpperCase());
		}

		tblProcessos.setDsProcesso(tblProcessos.getDsProcesso().toUpperCase());
		tblProcessos.setCdTipoprocesso(tipoProcessoDAO.find(
				TblTipoprocesso.class, tblProcessos.getCdTipoprocesso()
						.getCdTipoprocesso()));
		tblProcessos.setIdInteressado(interessadoDAO.find(
				TblInteressados.class, tblProcessos.getIdInteressado()
						.getIdInteressado()));
		tblProcessos.setIdUsuario(sessao.getUsuario());
		tblProcessos.setIdEntidade(sessao.getLotacaoUsuario().getCdSetor()
				.getIdEntidade());
		tblProcessos.setNrProcesso(new BigInteger(
				processoDAO
						.getNumeroProcesso(
								sessao.getLotacaoUsuario().getCdSetor()
										.getIdEntidade()).toString()));
		tblProcessos.setDtAberturaProcesso(new java.util.Date());
		tblProcessos.setNrAno(new Short(new SimpleDateFormat("yyyy")
				.format(new java.util.Date())));
		tblProcessos.setStProcesso(ListaApp.SITUACAOPROCESSOABERTO);
		tblProcessos.setStProcessoweb(ListaApp.SITUACAOPROCESSOABERTO);

		tblProcessos = processoDAO.salvarProcesso(tblProcessos);

		TblTramites tramite = new TblTramites();
		tramite.setIdUsuarioTramite(sessao.getUsuario());
		tramite.setIdUsuarioRecebe(sessao.getUsuario());
		tramite.setCdSetor(sessao.getLotacaoUsuario().getCdSetor());
		tramite.setIdProcesso(tblProcessos);
		tramite.setDtTramite(new java.util.Date());
		tramite.setStTramite(ListaApp.SITUACAOTRAMITERECEBIDO);
		tramite.setDsDispacho(tblProcessos.getDsProcesso());
		tramite.setDtRecebimento(new java.util.Date());

		tramite = tramiteDAO.salvarTramite(tramite);
		ArrayList<TblTramites> listatramites = new ArrayList<TblTramites>();
		listatramites.add(tramite);
		tblProcessos.setTblTramitesList(listatramites);
		tblProcessos = processoDAO.salvarProcesso(tblProcessos);

		result.redirectTo(this).processoresumo(tblProcessos.getIdProcesso());
	}

	@Restrito(nome = ListaApp.PROCRESUMO)
	@Path("/processo/{id}/resumo")
	public void processoresumo(Long id) {
		if (id == null) {
			result.redirectTo(HomeController.class).home();
		} else {
			TblProcessos processo = processoDAO.find(TblProcessos.class, id);
			result.include("interessado", processo.getIdInteressado());
			result.include("processo", processo);
			result.include("tipoprocesso", processo.getCdTipoprocesso());
		}
	}

	@Restrito(nome = ListaApp.DETALHARPROCESSO)
	@Path("/processo/{id}/detalhe")
	public void processodetalhe(Long id) {
		TblProcessos proc = processoDAO.find(TblProcessos.class, id);
		List<TblTramites> tramites = tramiteDAO.listaTramitesPorProcesso(proc);
		List<TblRotas> rotas = rotaDAO.listaPorTipoProcesso(proc
				.getCdTipoprocesso());
		List<RotaEfetivadaDTO> lista = new ArrayList<RotaEfetivadaDTO>();
		List<TblProcessosJuntados> processospai = processoJuntadoDAO
				.listaProcessosPais(proc);
		List<TblProcessosJuntados> processosfilhos = processoJuntadoDAO
				.listaProcessosFilhos(proc);
		// comparando tramites com rotas
		if (rotas != null) {
			int ponteiro = 0;
			boolean saircomparacao = false;
			
			for (int indrota = 0; indrota < rotas.size(); indrota++) {

				if(!saircomparacao){
					saircomparacao = true;
					TblSetor setorrota = null;
					TblSetor setortramite = null;
					for (int indtram = ponteiro; indtram < tramites.size(); indtram++) {
							
							setorrota = rotas.get(indrota).getCdSetor();
							setortramite = tramites.get(indtram).getCdSetor();						
							if(setorrota == setortramite){
								TblTramites tram = tramites.get(indtram);
								if(!(tram.getStTramite() == ListaApp.SITUACAOTRAMITEESPERA)){
									saircomparacao = false;
									indtram++;
									ponteiro = indtram;
									lista.add(new RotaEfetivadaDTO(rotas.get(indrota),RotaEfetivadaDTO.PASSOU));
									break;
							}
						}				
					}
				}
				if(saircomparacao){
					lista.add(new RotaEfetivadaDTO(rotas.get(indrota),RotaEfetivadaDTO.NAOPASSOU));
				}			
				
			}
			
		}

		if (proc.getStProcesso() == ListaApp.SITUACAOPROCESSOABERTO) {
			result.include("tempoProcesso", Util.duracaoProcesso(
					proc.getDtAberturaProcesso(), new Date()));
		} else {
			result.include(
					"tempoProcesso",
					Util.duracaoProcesso(proc.getDtAberturaProcesso(),
							proc.getDtEncerramento()));
		}
		if (proc.getStProcesso() == ListaApp.SITUACAOPROCESSOARQUIVADO) {
			TblProcessoArquivado procarquivado = procArquivadoDAO
					.findPorProcesso(proc);

			TblEnderecoarquivamento end = procarquivado
					.getTblEnderecoarquivamento();
			List<TblEnderecoarquivamento> listaend = new ArrayList<TblEnderecoarquivamento>();
			listaend.add(0, end);
			while (end.getIdNivelsuperior() != ListaApp.LOCAL_ARQUIVO_NIVEL_RAIZ) {
				end = localArquivoDAO.find(TblEnderecoarquivamento.class,
						end.getIdNivelsuperior());
				listaend.add(0, end);
			}

			result.include("localArquivo", procarquivado);
			result.include("listalocalArquivo", listaend);

		}
		result.include("aberto", ListaApp.SITUACAOPROCESSOABERTO);
		result.include("fechado", ListaApp.SITUACAOPROCESSOARQUIVADO);
		result.include("cancelado", ListaApp.SITUACAOPROCESSOCANCELADO);
		result.include("encerrado", ListaApp.SITUACAOPROCESSOENCERRADO);
		result.include("processospai", processospai);
		result.include("processosfilho", processosfilhos);
		result.include("PASSOU", RotaEfetivadaDTO.PASSOU);
		result.include("NAOPASSOU", RotaEfetivadaDTO.NAOPASSOU);
		result.include("tblProcessos", proc);
		result.include("tblTramitesList", tramites);
		result.include("tblRotasList", lista);

	}

	@Restrito(nome = ListaApp.PESQUISARPROCESSO)
	public void pesquisaprocesso() {

	}

	@Get
	@Path("/processo/listaentidades.json")
	public void listaEntidades() {
		List<Entidade> lista = entidadeDAO.listatodos();
		result.use(json())
				.withoutRoot()
				.from(lista)
				.exclude("tblSetorList", "tblTipoprocessoList",
						"tblProcessosList").serialize();
	}

	@Get
	@Path("/processo/listatiposprocesso.json")
	public void listaTiposProcessos() {
		List<TblTipoprocesso> lista = tipoProcessoDAO.listaTipos(sessao
				.getLotacaoUsuario().getCdSetor().getIdEntidade());
		result.use(json())
				.withoutRoot()
				.from(lista)
				.exclude("dsTipoprocesso", "stConfirmacaoPresencia",
						"stAbrirInternet", "tblRegrasList", "idEntidade",
						"tblProcessosList").serialize();
	}

	@Restrito(nome = ListaApp.PESQUISARPROCESSO)
	@Post
	public void pesquisar(TblProcessos tblProcessos, boolean maiscampos) {
		if (!maiscampos) {
			if (tblProcessos.getNrProcesso() == null) {
				validator.add(new ValidationMessage(
						"Informe o N�mero do Processo", ""));
			}
			if (tblProcessos.getNrAno() == 0) {
				validator.add(new ValidationMessage(
						"Informe o Ano do Processo", ""));
			}
		}
		validator.onErrorRedirectTo(this).pesquisaprocesso();

		if (tblProcessos.getCdTipoprocesso().getCdTipoprocesso() != 0) {
			tblProcessos.setCdTipoprocesso(tipoProcessoDAO.find(
					TblTipoprocesso.class, tblProcessos.getCdTipoprocesso()
							.getCdTipoprocesso()));
		} else {
			tblProcessos.setCdTipoprocesso(null);
		}

		if (tblProcessos.getIdEntidade().getIdEntidade() != 0) {
			tblProcessos.setIdEntidade(entidadeDAO.find(Entidade.class,
					tblProcessos.getIdEntidade().getIdEntidade()));
		} else {
			tblProcessos.setIdEntidade(null);
		}

		result.include("tblProcessosList",
				processoDAO.pesquisaProcesso(tblProcessos));
		result.redirectTo(this).pesquisaprocesso();
	}

	@Get
	@Path("/processo/listaregras.json")
	public void listaSetores(String id) {
		List<TblRegras> lista = regraDAO.listartodos(tipoProcessoDAO.find(
				TblTipoprocesso.class, Integer.parseInt(id)));
		result.use(json()).withoutRoot().from(lista)
				.exclude("dsRegra", "cdTipoprocesso", "cdRegra").serialize();
	}

	@Restrito(nome = ListaApp.CAPA_DO_PROCESSO)
	@Path("processo/{id}/capaProcesso")
	public Download capaProcesso(Long id) {
		TblProcessos processo = processoDAO.find(TblProcessos.class, id);
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("nomeEntidade", processo.getIdEntidade()
				.getDsEntidadeExtenso());
		parametros
				.put("numeroProcesso", Util.zerosEsquerda(processo
						.getNrProcesso().toString(), 8, "0"));
		parametros.put("anoProcesso", processo.getNrAno());
		parametros.put("dataAbertura", new SimpleDateFormat("dd/MM/yyyy")
				.format(processo.getDtAberturaProcesso()));
		parametros.put("interessado", processo.getIdInteressado()
				.getNmInteressado());
		parametros.put("assunto", processo.getCdTipoprocesso()
				.getNmTipoprocesso());
		parametros.put("endereco", processo.getIdInteressado().getDsEndereco());
		parametros.put("bairro", processo.getIdInteressado().getDsBairro());
		parametros.put("cep",
				Util.formataCEP(processo.getIdInteressado().getNrCep()));
		parametros.put("complemento", processo.getIdInteressado()
				.getDsComplemento());
		parametros.put("observacao", processo.getDsProcesso());
		parametros.put("enderecoAdic", processo.getDsEndereco());
		parametros.put("bairroAdic", processo.getDsBairro());
		parametros.put("cepAdic", Util.formataCEP(processo.getNrCep()));
		parametros.put("complementoAdic", processo.getDsComplemento());

		try {
			File file = new File(context.getRealPath("img/brasao_350.jpg"));
			parametros.put("brasao", new FileInputStream(file));
		} catch (FileNotFoundException f) {
			f.printStackTrace();
			parametros.put("brasao", null);
		}
		return jasperMaker.makePdf("capaProcesso.jasper", null, "capa.pdf",
				false, parametros);
	}

	@Restrito(nome = ListaApp.ENCERRARPROCESSO)
	@Path("processo/{id}/encerrarprocesso")
	public TblProcessos encerrarprocesso(Long id) {
		return processoDAO.find(TblProcessos.class, id);
	}

	@Restrito(nome = ListaApp.ENCERRARPROCESSO)
	@Post
	public void salvarenceprocesso(TblProcessos tblProcessos) {
		if (tblProcessos.getDsEncerramento() == null) {
			validator.add(new ValidationMessage(
					"Insira a descri��o do encerramento.", "dsEncerremento"));
		}
		validator.onErrorUsePageOf(this).encerrarprocesso(
				tblProcessos.getIdProcesso());

		TblProcessos processo = processoDAO.find(TblProcessos.class,
				tblProcessos.getIdProcesso());
		processo.setDsEncerramento(tblProcessos.getDsEncerramento()
				.toUpperCase());
		processo.setDtEncerramento(new Date());
		processo.setStProcesso(ListaApp.SITUACAOPROCESSOENCERRADO);
		processo.setStProcessoweb(ListaApp.SITUACAOPROCESSOENCERRADO);

		processoDAO.update(processo);

		result.redirectTo(this).processodetalhe(processo.getIdProcesso());
	}

	@Path("processo/{id}/cartaoprocesso")
	public Download cartaoProcesso(Long id) {
		TblProcessos processo = processoDAO.find(TblProcessos.class, id);
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("SIGLAENTIDADE", processo.getIdEntidade()
				.getDsEntidade());
		parametros.put("NROPROCESSO",
				processo.getNrProcesso() + "/" + processo.getNrAno());
		parametros.put("DATAPROCESSO", new SimpleDateFormat("dd/MM/yyyy")
				.format(processo.getDtAberturaProcesso()));
		parametros.put("NOMEUSUARIO", sessao.getUsuario().getNmUsuario());
		parametros.put("ASSUNTO", processo.getCdTipoprocesso()
				.getNmTipoprocesso());
		String documentos = "";
		int cont = 0;
		List<DocAnexoDTO> docs = docsAnexosDAO.listaPorProcesso(processo);

		for (DocAnexoDTO d : docs) {
			cont += 1;
			documentos += cont + ") " + d.getNmDocumentoanexo() + "    ";
		}

		parametros.put("DOCSANEXADOS", documentos);
		try {
			File file = new File(context.getRealPath("img/brasao_350.jpg"));
			parametros.put("BRASAO", new FileInputStream(file));
		} catch (FileNotFoundException f) {
			f.printStackTrace();
			parametros.put("BRASAO", null);
		}
		return jasperMaker.makePdf("cartaoProcesso.jasper", null, "cartao.pdf",
				false, parametros);
	}

	@Restrito(nome = ListaApp.LISTA_PROCESSOS_ENCERRADOS)
	@Path({ "/processo/processoencerrado",
			"/processo/processoencerrado/pag-{pag}" })
	public List<TblProcessos> processoencerrado(String pag) {
		List<Integer> paginas = PaginacaoUtil.listaPaginas(processoDAO
				.countListaProcessosEncerrados(sessao.getLotacaoUsuario()
						.getCdSetor().getIdEntidade(),
						ListaApp.SITUACAOPROCESSOENCERRADO,
						ListaApp.SITUACAOTRAMITERECEBIDO,
						ListaApp.COD_TIPO_REGISTRO_PROCESSO));
		int paginaSelecionada = PaginacaoUtil.numeroDaPagina(pag, paginas);
		if (paginas.size() != 0) {
			result.include("paginationList",
					PaginacaoUtil.listaPaginasView(paginas, paginaSelecionada));
			result.include("pagSelec", pag);
			result.include("first", PaginacaoUtil.FIRST);
			result.include("last", PaginacaoUtil.LAST);
			result.include("link", "/processo/processoencerrado/pag-");
		}
		return processoDAO.listaProcessosEncerrados(sessao.getLotacaoUsuario()
				.getCdSetor().getIdEntidade(),
				ListaApp.SITUACAOPROCESSOENCERRADO,
				ListaApp.SITUACAOTRAMITERECEBIDO,
				ListaApp.COD_TIPO_REGISTRO_PROCESSO, PaginacaoUtil.reginicial(paginaSelecionada),
				PaginacaoUtil.TAMTABELA);
	}

	@Path("/processo/protocolardocumento")
	public void protocolardocumento() {
	}

	@Path("/processo/salvarprotocolo")
	public void salvarprotocolo(TblProcessos tblProcessos) {
		if (tblProcessos.getDsProcesso() == null) {
			validator.add(new ValidationMessage(
					"Informe uma descri��o para o Processo", "dsproc"));
		}

		tblProcessos.setDsProcesso(tblProcessos.getDsProcesso().toUpperCase());
		tblProcessos.setCdTipoprocesso(tipoProcessoDAO.find(
				TblTipoprocesso.class, tblProcessos.getCdTipoprocesso()
						.getCdTipoprocesso()));
		tblProcessos.setIdInteressado(interessadoDAO.find(
				TblInteressados.class, tblProcessos.getIdInteressado()
						.getIdInteressado()));
		tblProcessos.setIdUsuario(sessao.getUsuario());
		tblProcessos.setIdEntidade(sessao.getLotacaoUsuario().getCdSetor()
				.getIdEntidade());
		tblProcessos.setNrProcesso(new BigInteger(
				processoDAO
						.getNumeroProcesso(
								sessao.getLotacaoUsuario().getCdSetor()
										.getIdEntidade()).toString()));
		tblProcessos.setDtAberturaProcesso(new java.util.Date());
		tblProcessos.setNrAno(new Short(new SimpleDateFormat("yyyy")
				.format(new java.util.Date())));
		tblProcessos.setStProcesso(ListaApp.SITUACAOPROCESSOABERTO);
		tblProcessos.setStProcessoweb(ListaApp.SITUACAOPROCESSOABERTO);
		tblProcessos.setCdTipoRegistro(ListaApp.COD_TIPO_REGISTRO_PROTOCOLO);
		tblProcessos = processoDAO.salvarProcesso(tblProcessos);

		result.redirectTo(TramiteController.class).tramiteprotocolo(
				tblProcessos.getIdProcesso());
	}

	@Restrito(nome = ListaApp.ARQUIVAR_PROCESSO)
	@Path("/processo/arquivarprocesso/{id}")
	public void arquivarprocesso(Long id) {
		result.include("processo", processoDAO.find(TblProcessos.class, id));
		result.include(
				"localList",
				Util.sortLocal(localArquivoDAO.listatodos(sessao
						.getLotacaoUsuario().getCdSetor().getIdEntidade())));
	}

	@Restrito(nome = ListaApp.ARQUIVAR_PROCESSO)
	@Post
	public void arquivar(TblProcessoArquivadoPK local, String dsarq) {
		if (local.getIdEnderecoarquivamento() == 0) {
			validator.add(new ValidationMessage(
					"Informe o local de armazenamento", ""));
		}
		if (dsarq == null) {
			validator.add(new ValidationMessage(
					"Informe uma descri��o para o armazenamento", ""));
		}
		validator.onErrorRedirectTo(this).processoencerrado(null);
		TblProcessoArquivado procarquivado = new TblProcessoArquivado(local);
		procarquivado.setDtArquivamento(new Date());
		procarquivado.setDsArquivamento(dsarq.toUpperCase());
		TblProcessos processo = processoDAO.find(TblProcessos.class,
				local.getIdProcesso());
		processo.setStProcesso(ListaApp.SITUACAOPROCESSOARQUIVADO);
		processo.setStProcessoweb(ListaApp.SITUACAOPROCESSOARQUIVADO);
		processoDAO.update(processo);
		procArquivadoDAO.cadastrar(procarquivado);
		result.redirectTo(this).processoencerrado(null);
	}

}
