package br.com.cinbesa.ged.control;

import static br.com.caelum.vraptor.view.Results.json;

import java.util.ArrayList;
import java.util.List;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.cinbesa.ged.dao.DocsexigidosDAO;
import br.com.cinbesa.ged.dao.EntidadeDAO;
import br.com.cinbesa.ged.dao.RotaDAO;
import br.com.cinbesa.ged.dao.SetorDAO;
import br.com.cinbesa.ged.dao.TipoProcessoDAO;
import br.com.cinbesa.ged.dto.RotaDocumentosDTO;
import br.com.cinbesa.ged.entidade.Entidade;
import br.com.cinbesa.ged.entidade.TblDocumentosexigidos;
import br.com.cinbesa.ged.entidade.TblRotas;
import br.com.cinbesa.ged.entidade.TblSetor;
import br.com.cinbesa.ged.entidade.TblTipoprocesso;
import br.com.cinbesa.ged.intercept.Restrito;
import br.com.cinbesa.ged.intercept.SessaoUsuario;
import br.com.cinbesa.ged.util.ListaApp;
import br.com.cinbesa.ged.util.PaginacaoUtil;

@Resource
public class RotaController {

	private final Result result;
	private final Validator validator;
	private TipoProcessoDAO tipoProcessoDAO;
	private EntidadeDAO entidadeDAO;
	private SetorDAO setorDAO;
	private RotaDAO rotaDAO;
	private DocsexigidosDAO docsexigidosDAO;
	private SessaoUsuario sessaoUsuario;

	public RotaController(Result result, Validator validator,
			TipoProcessoDAO tipoProcessoDAO, EntidadeDAO entidadeDAO,
			SetorDAO setorDAO, RotaDAO rotaDAO,
			DocsexigidosDAO docsexigidosDAO, SessaoUsuario sessaoUsuario) {
		this.result = result;
		this.validator = validator;
		this.tipoProcessoDAO = tipoProcessoDAO;
		this.entidadeDAO = entidadeDAO;
		this.setorDAO = setorDAO;
		this.rotaDAO = rotaDAO;
		this.docsexigidosDAO = docsexigidosDAO;
		this.sessaoUsuario = sessaoUsuario;
	}

	@Restrito(nome = ListaApp.ADICIONAROTA)
	@Path({"rota/nova/{id}"})
	public void nova(Integer id) {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			
			TblTipoprocesso tipoProcesso = tipoProcessoDAO.find(TblTipoprocesso.class, id);
			Integer nrOrdem = rotaDAO.getMaxOrdem(tipoProcesso);
			nrOrdem++;
			result.include("nrOrdem",nrOrdem);
			result.include("tipoProcesso",tipoProcesso);
			result.include(
					"tblSetorList",
					setorDAO.listaPorEntidade(sessaoUsuario.getLotacaoUsuario()
							.getCdSetor().getIdEntidade()));
			result.include("prioridadealta", ListaApp.ROTAPRIORIDADEALTA);
			result.include("prioridadenormal", ListaApp.ROTAPRIORIDADENORMAL);
			result.include("prioridadebaixa", ListaApp.ROTAPRIORIDADEBAIXA);
			result.include("paralelasim", ListaApp.ROTAPARALELASIM);
			result.include("paralelanao", ListaApp.ROTAPARALELANAO);
			result.include("idTipoProcesso", id);
		}
	}

	@Restrito(nome = ListaApp.ADICIONAROTA)
	@Post
	public void cadastrarrota(TblRotas tblRotas) {
		if (tblRotas.getNmRota() == null) {
			validator.add(new ValidationMessage("Informe o nome da rota", ""));
		}
		if (tblRotas.getCdTipoprocesso().getCdTipoprocesso() == null) {
			validator.add(new ValidationMessage("Informe o Tipo do Processo",
					""));
		}
		if (tblRotas.getCdSetor().getCdSetor() == null) {
			validator.add(new ValidationMessage("Informe o Setor", ""));
		}
		if (tblRotas.getCdPrioridade() == 0) {
			validator.add(new ValidationMessage("Informe a Prioridade", ""));
		}
		if (tblRotas.getStRotaParalela() == 0) {
			validator.add(new ValidationMessage("Informe se a Rota � paralela",
					""));
		}
		if (tblRotas.getNrOrdemrota() == 0) {
			validator.add(new ValidationMessage(
					"Informe o n�m. da ordem da rota", ""));
		}
		tblRotas.setNmRota(tblRotas.getNmRota().toUpperCase());
		tblRotas.setCdTipoprocesso(tipoProcessoDAO.find(TblTipoprocesso.class,
				tblRotas.getCdTipoprocesso().getCdTipoprocesso()));
		tblRotas.setCdSetor(setorDAO.find(TblSetor.class, tblRotas.getCdSetor()
				.getCdSetor()));

		if (rotaDAO.getRotaPorTPProcOrdem(tblRotas.getCdTipoprocesso(),
				tblRotas.getNrOrdemrota()) != null) {
			validator
					.add(new ValidationMessage(
							"Sistema j� possui uma rota cadastrada para este Tipo de Processo e Ordem da Rota",
							""));
		}
		validator.onErrorRedirectTo(this).nova(tblRotas.getCdTipoprocesso().getCdTipoprocesso());

		rotaDAO.salvarRota(tblRotas);

		result.redirectTo(this).lista(tblRotas.getCdTipoprocesso().getCdTipoprocesso(),null);
	}

	@Restrito(nome = ListaApp.LISTROTA)
	@Path({ "/rota", "/rota/{id}","/rota/{id}/pag-{pag}" })
	public void lista(Integer id,String pag) {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			
			TblTipoprocesso tipoProcesso = tipoProcessoDAO.find(TblTipoprocesso.class,id);
			
			List<Integer> paginas = PaginacaoUtil.listaPaginas(rotaDAO
					.count(sessaoUsuario.getLotacaoUsuario().getCdSetor()
							.getIdEntidade(),tipoProcesso));
			int paginaSelecionada = PaginacaoUtil.numeroDaPagina(pag, paginas);
			
			if (paginas.size() != 0) {
				result.include("paginationList", PaginacaoUtil
						.listaPaginasView(paginas, paginaSelecionada));
				result.include("pagSelec", pag);
				result.include("first", PaginacaoUtil.FIRST);
				result.include("last", PaginacaoUtil.LAST);
				result.include("link", "/rota/"+tipoProcesso.getCdTipoprocesso()+"/pag-");
			}
			
			result.include("tipoProcesso",tipoProcesso);
			
			result.include("tblRotasList", rotaDAO.listaRotasPorTipoProcessoOrdenado(
					PaginacaoUtil.reginicial(paginaSelecionada),
					PaginacaoUtil.TAMTABELA, sessaoUsuario.getLotacaoUsuario().getCdSetor().getIdEntidade(),
					tipoProcesso
					));
		}
	}

	@Restrito(nome = ListaApp.EDITARROTA)
	@Path("/rota/editar/{id}")
	public void editar(Integer id) {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			TblRotas tblRotas = rotaDAO.find(TblRotas.class, id);
			result.include("tblRotas", tblRotas);
			result.include("setor", tblRotas.getCdSetor());
			result.include("tblSetorList", setorDAO.listaPorEntidade(tblRotas
					.getCdSetor().getIdEntidade()));
			result.include("tipoprocesso", tblRotas.getCdTipoprocesso());
			result.include("entidadeList", entidadeDAO
					.listaPorTipoProcesso(tblRotas.getCdTipoprocesso()
							.getCdTipoprocesso()));
			result.include(
					"tblTipoprocessoList",
					tipoProcessoDAO.listaTipos(sessaoUsuario
							.getLotacaoUsuario().getCdSetor().getIdEntidade()));
			result.include("prioridadealta", ListaApp.ROTAPRIORIDADEALTA);
			result.include("prioridadenormal", ListaApp.ROTAPRIORIDADENORMAL);
			result.include("prioridadebaixa", ListaApp.ROTAPRIORIDADEBAIXA);
			result.include("paralelasim", ListaApp.ROTAPARALELASIM);
			result.include("paralelanao", ListaApp.ROTAPARALELANAO);
		}
	}

	@Restrito(nome = ListaApp.EDITARROTA)
	@Post
	public void salvaredicao(TblRotas tblRotas) {
		if (tblRotas.getNmRota() == null) {
			validator.add(new ValidationMessage("Informe o nome da rota", ""));
		}
		if (tblRotas.getCdTipoprocesso().getCdTipoprocesso() == null) {
			validator.add(new ValidationMessage("Informe o Tipo do Processo",
					""));
		}
		if (tblRotas.getCdSetor().getCdSetor() == null) {
			validator.add(new ValidationMessage("Informe o Setor", ""));
		}
		if (tblRotas.getCdPrioridade() == 0) {
			validator.add(new ValidationMessage("Informe a Prioridade", ""));
		}
		if (tblRotas.getStRotaParalela() == 0) {
			validator.add(new ValidationMessage("Informe se a Rota � paralela",
					""));
		}
		if (tblRotas.getNrOrdemrota() == 0) {
			validator.add(new ValidationMessage(
					"Informe o n�m. da ordem da rota", ""));
		}
		validator.onErrorRedirectTo(this).editar(tblRotas.getCdRota());
		tblRotas.setNmRota(tblRotas.getNmRota().toUpperCase());
		tblRotas.setCdTipoprocesso(tipoProcessoDAO.find(TblTipoprocesso.class,
				tblRotas.getCdTipoprocesso().getCdTipoprocesso()));
		tblRotas.setCdSetor(setorDAO.find(TblSetor.class, tblRotas.getCdSetor()
				.getCdSetor()));

		rotaDAO.update(tblRotas);

		result.redirectTo(this).lista(tblRotas.getCdTipoprocesso().getCdTipoprocesso(), null);
	}

	@Path("/rota/{id}/regras")
	public void regras(Integer id) {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			TblRotas rotaAcess = rotaDAO.find(TblRotas.class, id);
			result.include("numrota", rotaAcess);
			List<TblDocumentosexigidos> listadocs = docsexigidosDAO.lista();
			ArrayList<RotaDocumentosDTO> listaview = new ArrayList<RotaDocumentosDTO>();
			for (TblDocumentosexigidos docs : listadocs) {
				boolean aux = false;
				for (TblRotas rota : docs.getTblRotasList()) {
					if (rotaAcess.equals(rota)) {
						aux = true;
						break;
					}
				}
				if (aux) {
					listaview.add(new RotaDocumentosDTO(docs
							.getCdDocumentoexigido().toString(), docs
							.getNmDocumentoexigido(), RotaDocumentosDTO.SIM));
				} else {
					listaview.add(new RotaDocumentosDTO(docs
							.getCdDocumentoexigido().toString(), docs
							.getNmDocumentoexigido(), RotaDocumentosDTO.NAO));
				}
			}
			result.include("lista", listaview);
			result.include("sim", RotaDocumentosDTO.SIM);
			result.include("nao", RotaDocumentosDTO.NAO);
		}
	}

	@Path("/rota/adicionadocumento/{cdRota}/{id}")
	public void adicionadocumento(Integer cdRota, Integer id) {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			TblRotas rota = rotaDAO.find(TblRotas.class, cdRota);
			TblDocumentosexigidos doc = docsexigidosDAO.find(
					TblDocumentosexigidos.class, id);
			doc.getTblRotasList().add(rota);
			docsexigidosDAO.update(doc);
			result.redirectTo(this).regras(cdRota);
		}
	}

	@Path("/rota/removedocumento/{cdRota}/{id}")
	public void removedocumento(Integer cdRota, Integer id) {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			TblRotas rota = rotaDAO.find(TblRotas.class, cdRota);
			TblDocumentosexigidos doc = docsexigidosDAO.find(
					TblDocumentosexigidos.class, id);
			doc.getTblRotasList().remove(rota);
			docsexigidosDAO.update(doc);
			result.redirectTo(this).regras(cdRota);
		}
	}
}
