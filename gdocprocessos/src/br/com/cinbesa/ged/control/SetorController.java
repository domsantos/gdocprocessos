package br.com.cinbesa.ged.control;

import java.util.List;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.cinbesa.ged.dao.EntidadeDAO;
import br.com.cinbesa.ged.dao.Parametros;
import br.com.cinbesa.ged.dao.SetorDAO;
import br.com.cinbesa.ged.entidade.Entidade;
import br.com.cinbesa.ged.entidade.TblEnderecoarquivamento;
import br.com.cinbesa.ged.entidade.TblSetor;
import br.com.cinbesa.ged.intercept.Restrito;
import br.com.cinbesa.ged.intercept.SessaoUsuario;
import br.com.cinbesa.ged.log.control.LogSetorController;
import br.com.cinbesa.ged.util.ListaApp;
import br.com.cinbesa.ged.util.PaginacaoUtil;

@Resource
public class SetorController {
	private final SetorDAO setorDAO;
	private final EntidadeDAO entidadeDAO;
	private final Result result;
	private final Validator validator;
	private final LogSetorController logSetorController;
	private SessaoUsuario sessaoUsuario;

	public SetorController(SetorDAO setorDAO, EntidadeDAO entidadeDAO,
			Result result, Validator validator,
			LogSetorController logSetorController, SessaoUsuario sessaoUsuario) {
		this.setorDAO = setorDAO;
		this.result = result;
		this.entidadeDAO = entidadeDAO;
		this.validator = validator;
		this.logSetorController = logSetorController;
		this.sessaoUsuario = sessaoUsuario;
	}

	@Restrito(nome = ListaApp.LISTASETORES)
	@Path({ "/setor","/setor/{id}", "/setor/{id}/pag-{pag}" })
	public void lista(Integer id, String pag) {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			
//			List<Integer> paginas = PaginacaoUtil.listaPaginas(setorDAO
//					.count(sessaoUsuario.getLotacaoUsuario().getCdSetor()
//							.getIdEntidade()));
			
			Entidade entidade = entidadeDAO.find(Entidade.class, id);
			
			List<Integer> paginas = PaginacaoUtil.listaPaginas(setorDAO
					.count(entidade));
			
			int paginaSelecionada = PaginacaoUtil.numeroDaPagina(pag, paginas);
			if (paginas.size() != 0) {
				result.include("paginationList", PaginacaoUtil
						.listaPaginasView(paginas, paginaSelecionada));
				result.include("pagSelec", pag);
				result.include("first", PaginacaoUtil.FIRST);
				result.include("last", PaginacaoUtil.LAST);
				result.include("link", "/setor/"+entidade.getIdEntidade()+"/pag-");
			}
			
//			result.include("tblSetorList", setorDAO.listatodos(
//					PaginacaoUtil.reginicial(paginaSelecionada),
//					PaginacaoUtil.TAMTABELA, sessaoUsuario.getLotacaoUsuario()
//							.getCdSetor().getIdEntidade()));
			
			result.include("entidade",entidade);
			result.include("tblSetorList", setorDAO.listatodos(
					PaginacaoUtil.reginicial(paginaSelecionada),
					PaginacaoUtil.TAMTABELA, entidade));
		}
	}

	@Restrito(nome = ListaApp.ADDSETOR)
	@Path({"/setor/novo/{id}"})
	public void novo(Integer id) {
		result.include("entidade",entidadeDAO.find(Entidade.class, id));
	}

	@Restrito(nome = ListaApp.ADDSETOR)
	@Post
	public void cadastrar(TblSetor tblSetor) {
		if (tblSetor.getNmSetor() == null) {
			validator.add(new ValidationMessage("Nome do Setor Inv�lido",
					"nmSetor"));
		}
//		tblSetor.setIdEntidade(sessaoUsuario.getLotacaoUsuario().getCdSetor()
//				.getIdEntidade());
		tblSetor = setorDAO.salvar(tblSetor);
		logSetorController.registrar(tblSetor, ListaApp.LOG_OPERACAO_INSERCAO);
		result.redirectTo(SetorController.class).lista(tblSetor.getIdEntidade().getIdEntidade(), null);
	}

	@Restrito(nome = ListaApp.EDITSETOR)
	@Path("/setor/editar/{id}")
	public void editar(Integer id) {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			TblSetor setor = setorDAO.find(TblSetor.class, id);
			result.include("entidade",setor.getIdEntidade());
			result.include("tblSetor", setor);
		}
	}

	@Restrito(nome = ListaApp.EDITSETOR)
	@Post
	public void salvaedicao(TblSetor tblSetor) {
		if (tblSetor.getNmSetor() == null) {
			validator.add(new ValidationMessage("Nome do Setor Inv�lido",
					"nmSetor"));
		}
		validator.onErrorRedirectTo(this).editar(
				tblSetor.getCdSetor());
		
		TblSetor setor = setorDAO.find(TblSetor.class, tblSetor.getCdSetor());
		setor.setNmSetor(tblSetor.getNmSetor().toUpperCase());
		if(tblSetor.getDsSetor() != null){
			setor.setDsSetor(tblSetor.getDsSetor().toUpperCase());
		}
		
		
			tblSetor = setorDAO.salvar(setor);
			logSetorController.registrar(tblSetor,
					ListaApp.LOG_OPERACAO_ALTERACAO);
	

	
		result.redirectTo(SetorController.class).lista(setor.getIdEntidade().getIdEntidade(), null);
	}

}
