package br.com.cinbesa.ged.control;

import java.util.List;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.cinbesa.ged.dao.EntidadeDAO;
import br.com.cinbesa.ged.dao.Parametros;
import br.com.cinbesa.ged.entidade.Entidade;
import br.com.cinbesa.ged.intercept.Restrito;
import br.com.cinbesa.ged.intercept.SessaoUsuario;
import br.com.cinbesa.ged.log.control.LogEntidadeController;
import br.com.cinbesa.ged.util.ListaApp;
import br.com.cinbesa.ged.util.PaginacaoUtil;

@Resource
public class EntidadeController {

	private EntidadeDAO dao;
	private final Result result;
	private Validator validator;
	private LogEntidadeController logEntidadeController;
	private SessaoUsuario sessaoUsuario;

	public EntidadeController(EntidadeDAO dao, Result result,
			Validator validator, LogEntidadeController logEntidadeController, 
			SessaoUsuario sessaoUsuario) {
		this.dao = dao;
		this.result = result;
		this.validator = validator;
		this.logEntidadeController = logEntidadeController;
		this.sessaoUsuario = sessaoUsuario;
	}

	@Restrito(nome= ListaApp.LISTAENTID)
	@Path({ "/entidade", "/entidade/pag-{pag}" })
	public List<Entidade> lista(String pag) {
		if(sessaoUsuario.getUsuario().getCdNivelAdministrador() !=  ListaApp.CD_NIVEL_ADMIN_SUPERADMIN){
			result.redirectTo(HomeController.class).sempermissao();
		}
		List<Integer> paginas = PaginacaoUtil.listaPaginas(dao.count());
		int paginaSelecionada = PaginacaoUtil.numeroDaPagina(pag, paginas);
		if (paginas.size() != 0) {
			result.include("paginationList",
					PaginacaoUtil.listaPaginasView(paginas, paginaSelecionada));
			result.include("pagSelec", pag);
			result.include("first", PaginacaoUtil.FIRST);
			result.include("last", PaginacaoUtil.LAST);
			result.include("link", "/entidade/pag-");
		}
		return dao.listatodos(PaginacaoUtil.reginicial(paginaSelecionada),
				PaginacaoUtil.TAMTABELA);
	}

	@Restrito(nome = ListaApp.ADDENTID)
	public void nova() {
		if(sessaoUsuario.getUsuario().getCdNivelAdministrador() !=  ListaApp.CD_NIVEL_ADMIN_SUPERADMIN){
			result.redirectTo(HomeController.class).sempermissao();
		}
	}

	@Restrito(nome = ListaApp.ADDENTID)
	@Post
	public void cadastrar(Entidade entidade) {
		if (entidade.getDsEntidade() == null) {
			validator.add(new ValidationMessage("descri��o inv�lida",
					"dsEntidade"));
		} else {
			Parametros<String> param = new Parametros<String>("dsEntidade",
					entidade.getDsEntidade());
			if (dao.findSingleResult("Entidade.findByDsEntidade", param) == null) {
				if(entidade.getCdEntidade() == null){
					validator.add(new ValidationMessage("Informe o C�digo da Entidade",
							"cdEntidade"));
				}else{
					if(entidade.getDsEntidadeExtenso() == null){
						validator.add(new ValidationMessage("Informe o nome da Entidade na Capa do Processo",
								"dsNomeExtenso"));
					}else{
						entidade.setDsEntidade(entidade.getDsEntidade().toUpperCase());
						entidade.setDsEntidadeExtenso(entidade.getDsEntidadeExtenso().toUpperCase());
						entidade = dao.salvar(entidade);
						logEntidadeController.registrar(entidade, ListaApp.LOG_OPERACAO_INSERCAO);
					}
				}
			} else {
				validator.add(new ValidationMessage("descri��o j� existe",
						"dsEntidade"));
			}
		}
		validator.onErrorUsePageOf(EntidadeController.class).nova();
		result.redirectTo(EntidadeController.class).lista(null);
	}

	@Restrito(nome= ListaApp.EDITENTID)
	@Path("/entidade/editar/{id}")
	public Entidade editar(Integer id) {
		if(sessaoUsuario.getUsuario().getCdNivelAdministrador() !=  ListaApp.CD_NIVEL_ADMIN_SUPERADMIN){
			result.redirectTo(HomeController.class).sempermissao();
		}
		Entidade entidade = dao.find(Entidade.class, id);
		return entidade;
	}

	@Restrito(nome= ListaApp.EDITENTID)
	@Post
	public void salvaredicao(Entidade entidade) {
		if (entidade.getDsEntidade() == null) {
			validator.add(new ValidationMessage("descri��o inv�lida",
					"dsEntidade"));
		}
		Parametros<String> param = new Parametros<String>("dsEntidade",
				entidade.getDsEntidade());
		if (dao.findSingleResult("Entidade.findByDsEntidade", param) == null) {
			if(entidade.getCdEntidade() == null){
				validator.add(new ValidationMessage("Informe o C�digo da Entidade",
						"cdEntidade"));
			}else{
				if(entidade.getDsEntidadeExtenso() == null){
					validator.add(new ValidationMessage("Informe o nome da Entidade na Capa do Processo",
							"dsNomeExtenso"));
				}else{
					entidade.setDsEntidade(entidade.getDsEntidade().toUpperCase());
					entidade.setDsEntidadeExtenso(entidade.getDsEntidadeExtenso().toUpperCase());
					entidade = dao.atualizar(entidade);
					logEntidadeController.registrar(entidade, ListaApp.LOG_OPERACAO_INSERCAO);
				}
			}
		} else {
			validator.add(new ValidationMessage("descri��o j� existe",
					"dsEntidade"));
		}
		validator.onErrorUsePageOf(EntidadeController.class).editar(
				entidade.getIdEntidade());
		result.redirectTo(EntidadeController.class).lista(null);
	}
}
