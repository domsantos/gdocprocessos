package br.com.cinbesa.ged.control;

import static br.com.caelum.vraptor.view.Results.json;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.interceptor.download.Download;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.cinbesa.ged.dao.DamDAO;
import br.com.cinbesa.ged.dao.ProcessoDAO;
import br.com.cinbesa.ged.dao.UsoatividadeDAO;
import br.com.cinbesa.ged.dao.ValordocumentoDAO;
import br.com.cinbesa.ged.dto.GuiaDTO;
import br.com.cinbesa.ged.dto.ParcelaGuiaDTO;

import br.com.cinbesa.ged.entidade.TblDam;
import br.com.cinbesa.ged.entidade.TblProcessos;

import br.com.cinbesa.ged.entidade.TblUfir;
import br.com.cinbesa.ged.entidade.TblUsoatividade;
import br.com.cinbesa.ged.entidade.TblValorDocumento;
import br.com.cinbesa.ged.intercept.Restrito;
import br.com.cinbesa.ged.intercept.SessaoUsuario;
import br.com.cinbesa.ged.util.GuiaUtil;
import br.com.cinbesa.ged.util.JasperMaker;
import br.com.cinbesa.ged.util.ListaApp;
import br.com.cinbesa.ged.util.NumeroGuia;


@Resource
public class GuiaController {
	private Result result;
	private final Validator validator;
	private UsoatividadeDAO usoatividadeDAO;
	private ProcessoDAO processoDAO;
	private SessaoUsuario sessaoUsuario;
	private ValordocumentoDAO valordocumentoDAO;
	private DamDAO damDAO;
	private NumeroGuia numeroGuia;
	private JasperMaker jasperMaker;
	private ServletContext context;
	
	public GuiaController(Validator validator, Result result, UsoatividadeDAO usoatividadeDAO, ProcessoDAO processoDAO, SessaoUsuario sessaoUsuario, ValordocumentoDAO valordocumentoDAO, DamDAO damDAO, JasperMaker jasperMaker, NumeroGuia numeroGuia, ServletContext context) {
		this.result = result;
		this.validator = validator;
		this.usoatividadeDAO = usoatividadeDAO;
		this.processoDAO = processoDAO;
		this.sessaoUsuario =sessaoUsuario;
		this.valordocumentoDAO = valordocumentoDAO;
		this.damDAO = damDAO;
		this.jasperMaker = jasperMaker;
		this.numeroGuia = numeroGuia;
		this.context = context;
	}
	
	@Restrito(nome = ListaApp.ADDDAM)
	@Path("/guia/novaguia/{id}")
	public void novaguia(Long id){
		result.include("processo", processoDAO.find(TblProcessos.class, id));
		result.include("diasVencimento", ListaApp.NUMDIASDATAVENCIMENTO);
		result.include("tblUsoatividadeList", usoatividadeDAO.listarPorEntidade(sessaoUsuario.getLotacaoUsuario().getCdSetor().getIdEntidade()));		
	}
	
	@Get
	@Path("/guia/ufir.json")
	public void getUfir(String id) {
			TblUsoatividade uso = usoatividadeDAO.find(TblUsoatividade.class, Integer.parseInt(id));
			TblUfir ufir = uso.getIdUfir();
			BigDecimal aux = uso.getNrQtdufir().multiply(ufir.getVlUfir()).setScale(2, BigDecimal.ROUND_CEILING);
			Object[] arr = new Object[3];
			arr[0] = ufir.getNrAno();
			arr[1] = ufir.getVlUfir().toString();
			arr[2] = aux;
		result.use(json()).withoutRoot().from(arr).serialize();
	}
	
	@Get
	@Path("/guia/calculoparcela.json")
	public void getUfir(int parcelas, String valor) {
		
			BigDecimal vl = new BigDecimal(valor.replace(",", "."));
			vl = vl.divide(new BigDecimal(parcelas), 2, BigDecimal.ROUND_CEILING);
		result.use(json()).withoutRoot().from(vl).serialize();
	}
	
	@Post
	@Restrito(nome = ListaApp.ADDDAM)
	public void gravarguia(TblValorDocumento tblValorDocumento, String valorDocumento, String usoatividade, Integer diasVencimento){
		if(usoatividade == null){
			validator.add(new ValidationMessage("Informe o Uso Atividade", "usoAtividade"));
		}
		if(valorDocumento == null){
			validator.add(new ValidationMessage("Informe o Valor do Documento", "valorDocumento"));
		}
		if(tblValorDocumento.getNrParcelas() == null){
			validator.add(new ValidationMessage("Informe o número de parcelas", "nrParcelas"));
		}
		if(tblValorDocumento.getDsPagamento() == null){
			validator.add(new ValidationMessage("Informe uma descrição para a DAM", "dsPagamento"));
		}
		validator.onErrorRedirectTo(this).novaguia(tblValorDocumento.getIdProcesso().getIdProcesso());
		
		tblValorDocumento.setIdProcesso(processoDAO.find(TblProcessos.class, tblValorDocumento.getIdProcesso().getIdProcesso()));
		TblUsoatividade usoativ = usoatividadeDAO.find(TblUsoatividade.class, Integer.parseInt(usoatividade));
		
		tblValorDocumento.setVlDocumento(new BigDecimal(valorDocumento.replace("," , ".")));
		tblValorDocumento.setDtLancamento(new java.util.Date());
		tblValorDocumento.setIdUfir(usoativ.getIdUfir());
		tblValorDocumento.setDsPagamento(tblValorDocumento.getDsPagamento().toUpperCase());
		
		tblValorDocumento = valordocumentoDAO.cadastrar(tblValorDocumento);
		
		String guia = numeroGuia.geraNumeroGuia(usoativ);
		tblValorDocumento.setNrGuia(guia);
		
		BigDecimal vl = tblValorDocumento.getVlDocumento();
		vl = vl.divide(new BigDecimal(tblValorDocumento.getNrParcelas()),2, BigDecimal.ROUND_CEILING);
		
		//valor sem ufir;
		BigDecimal vlsemufir = tblValorDocumento.getVlDocumento().divide(usoativ.getIdUfir().getVlUfir(), 2);
		//valor sem ufir da parcela;		
		BigDecimal parcsemufir = vlsemufir.divide(new BigDecimal(tblValorDocumento.getNrParcelas()), 2);
		
		for (int parc = 1; parc <= tblValorDocumento.getNrParcelas(); parc++) {
			Date dataVencimento = null;
			
			if(diasVencimento == null) {
				dataVencimento = GuiaUtil.geraDataVencimento(parc, ListaApp.NUMDIASDATAVENCIMENTO);
			} else {
				dataVencimento = GuiaUtil.geraDataVencimento(parc, diasVencimento.intValue());
			}
			
			String cod = GuiaUtil.gerarCodigoBarra(vl, dataVencimento, guia, parc);
			String linha = GuiaUtil.gerarLinhaDigitavel(cod);
			
			TblDam dam = new TblDam();
			
			dam.setIdProcesso(tblValorDocumento.getIdProcesso());
			dam.setCdUsoatividade(usoativ);
			dam.setCdValorDocumento(tblValorDocumento);
			dam.setNrParcela(parc);
			dam.setNrGuia(guia);
			dam.setVlDam(vl);
			dam.setDtVencimento(dataVencimento);
			dam.setDtEmissao(new Date());
			dam.setStSituacao(ListaApp.SITUACAO_DAM_EM_ABERTO);
			dam.setCdBarras(cod);
			dam.setCdLinhaDigitavel(linha);
			
			/*
			 * 	Vlr do Ufir na Tabela de DAM ser� o valor total calculado, divido pela Ufir na Tabela de Ufir 
			 * pelo registro de Uso Atividade selecionado.
			 * 	Assim, mesmo que o usu�rio altere o valor Total da Guia, ser� armazenado o valor para que no ano seguinte, 
			 * este possa emitir com o valor corrigido.
			 * 
			 */
			dam.setVlUfir(parcsemufir);
			
			dam = damDAO.cadastrarDAM(dam);	
			
		}

		result.redirectTo(this).geraguia(tblValorDocumento.getCdValorDocumento(), usoativ.getCdUsoatividade(), tblValorDocumento.getNrGuia());
		
	}
	
	public void geraguia(Integer documento, Integer atividade, String nrguia){
		result.include("documento", documento);
		result.include("atividade", atividade);
		result.include("nrguia", nrguia);
	}
	
	@Path("/guia/{documento}/{atividade}/imprimir")
	public Download imprimeguia(Integer documento, Integer atividade){
		TblValorDocumento tblValorDocumento = valordocumentoDAO.find(TblValorDocumento.class, documento);
		TblUsoatividade usoatividade = usoatividadeDAO.find(TblUsoatividade.class, atividade);
		TblProcessos processo = tblValorDocumento.getIdProcesso();
		List<TblDam> listaParcelasValorDocumento = damDAO.listaPorValorDocumento(tblValorDocumento);
		List<TblDam> listaParcelas = new ArrayList<TblDam>();
		for (TblDam item : listaParcelasValorDocumento) {
			if(GuiaUtil.VerificaDataVencGuia(item.getDtVencimento())){
				listaParcelas.add(item);
			}
		}
		
		GuiaDTO guiadto = new GuiaDTO();
		guiadto.getParametros().put(GuiaDTO.SIGLA_TRIBUTO, "DAM");
		guiadto.getParametros().put(GuiaDTO.INSCRICAO, GuiaUtil.INSCRICAO_DAM);
		guiadto.getParametros().put(GuiaDTO.SEQUENCIAL, GuiaUtil.INSCRICAO_DAM);
		guiadto.getParametros().put(GuiaDTO.NRO_DAM, tblValorDocumento.getNrGuia());
		guiadto.getParametros().put(GuiaDTO.NOME_RAZAO, processo.getIdInteressado().getNmInteressado());
		guiadto.getParametros().put(GuiaDTO.CPF_CNPJ, GuiaUtil.maskCpfCnpj(processo.getIdInteressado().getNrCpfCnpj()));
		guiadto.getParametros().put(GuiaDTO.ENDERECO, processo.getIdInteressado().getDsEndereco());
		guiadto.getParametros().put(GuiaDTO.BAIRRO_CIDADE_ESTADO, processo.getIdInteressado().getDsBairro()+" - "+processo.getIdInteressado().getNmCidade()+" - "+processo.getIdInteressado().getDsUf());
		guiadto.getParametros().put(GuiaDTO.USO_ATIVIDADE, usoatividade.getDsUsoatividade());
		guiadto.getParametros().put(GuiaDTO.NRO_PROCESSO, processo.getNrProcesso()+"/"+processo.getNrAno());
		guiadto.getParametros().put(GuiaDTO.NRO_PARCELAS, String.format("%02d", tblValorDocumento.getNrParcelas()));
		guiadto.getParametros().put(GuiaDTO.REFERENCIAS, tblValorDocumento.getDsPagamento());
		guiadto.getParametros().put(GuiaDTO.NOTA, usoatividade.getIdTributo().getNmTributo()+ "\nVALOR DO TRIBUTO: "+ usoatividade.getNrQtdufir().toString().replace(".", ","));
		
		//valor sem ufir;
		BigDecimal vlsemufir = tblValorDocumento.getVlDocumento().divide(usoatividade.getIdUfir().getVlUfir(), 2, BigDecimal.ROUND_CEILING);
		guiadto.getParametros().put(GuiaDTO.TRIBUTO_DEVIDO, vlsemufir.toString().replace(".", ","));
		guiadto.getParametros().put(GuiaDTO.EXERCICIOS,  new SimpleDateFormat("yyyy").format(tblValorDocumento.getDtLancamento()));
		try{
			File file = new File(context.getRealPath("img/brasao_40.jpg"));
			guiadto.getParametros().put(GuiaDTO.LOGO_MINI, new FileInputStream(file));
		}catch(FileNotFoundException f){
			f.printStackTrace();
			guiadto.getParametros().put(GuiaDTO.LOGO_MINI, null);
		}
		
		for (TblDam dam : listaParcelas) {
			guiadto.getParcelas().add(new ParcelaGuiaDTO(dam));
		}
		
		return jasperMaker.makePdf("boletosGenerics.jasper", guiadto.getParcelas(), "dam.pdf", false, guiadto.getParametros());
		
	}
	
	@Restrito(nome=ListaApp.LISTADAMSDOPROCESSO)
	@Path("/guia/{processo}/lista")
	public List<TblValorDocumento> listadocumentos(Long processo){
		TblProcessos proc = processoDAO.find(TblProcessos.class, processo);
		result.include("processo", proc);
		return valordocumentoDAO.listaDocumentos(proc);
	}
	
	@Path("/guia/{documento}/listarparcelas")
	public void detalhardocumento(Integer documento){
		TblValorDocumento vldoc = valordocumentoDAO.find(TblValorDocumento.class, documento);
		result.include("tblValorDocumento", vldoc);
		result.include("sim", ListaApp.SITUACAO_DAM_PAGA);
		result.include("nao", ListaApp.SITUACAO_DAM_EM_ABERTO);
		List<TblDam> damsNaoPagas = damDAO.listaParcelasPorVlDocSituacao(vldoc, ListaApp.SITUACAO_DAM_EM_ABERTO);
		if(damsNaoPagas.size() > 0){
			result.include("link", true);
		}
		result.include("tblDamList",damDAO.listaPorValorDocumento(vldoc));
		
	}
	
	@Path("/guia/{documento}/reimprimirguias")
	public Download reimprimirguias(Integer documento){
		TblValorDocumento valordocumento = valordocumentoDAO.find(TblValorDocumento.class, documento);
		List<TblDam> damsNaoPagas = damDAO.listaParcelasPorVlDocSituacao(valordocumento, ListaApp.SITUACAO_DAM_EM_ABERTO);
		
		for (int i = 0; i < damsNaoPagas.size(); i++) {
			if(damsNaoPagas.get(i).getDtVencimento().after(new Date())){
				Date dataVencimento  = GuiaUtil.geraDataVencimento(damsNaoPagas.get(i).getNrParcela(), ListaApp.NUMDIASDATAVENCIMENTO);
				String cod = GuiaUtil.gerarCodigoBarra(damsNaoPagas.get(i).getVlDam(), dataVencimento, damsNaoPagas.get(i).getNrGuia(), damsNaoPagas.get(i).getNrParcela());
				String linha = GuiaUtil.gerarLinhaDigitavel(cod);
				damsNaoPagas.get(i).setCdBarras(cod);
				damsNaoPagas.get(i).setCdLinhaDigitavel(linha);
				damsNaoPagas.set(i, damDAO.salvarDAM(damsNaoPagas.get(i)));
			}
		}
		TblUsoatividade usoatividade = damsNaoPagas.get(0).getCdUsoatividade();
		
		
		GuiaDTO guiadto = new GuiaDTO();
		guiadto.getParametros().put(GuiaDTO.SIGLA_TRIBUTO, "DAM");
		guiadto.getParametros().put(GuiaDTO.INSCRICAO, GuiaUtil.INSCRICAO_DAM);
		guiadto.getParametros().put(GuiaDTO.SEQUENCIAL, GuiaUtil.INSCRICAO_DAM);
		guiadto.getParametros().put(GuiaDTO.NRO_DAM, valordocumento.getNrGuia());
		guiadto.getParametros().put(GuiaDTO.NOME_RAZAO, valordocumento.getIdProcesso().getIdInteressado().getNmInteressado());
		guiadto.getParametros().put(GuiaDTO.CPF_CNPJ, GuiaUtil.maskCpfCnpj(valordocumento.getIdProcesso().getIdInteressado().getNrCpfCnpj()));
		guiadto.getParametros().put(GuiaDTO.ENDERECO, valordocumento.getIdProcesso().getIdInteressado().getDsEndereco());
		guiadto.getParametros().put(GuiaDTO.BAIRRO_CIDADE_ESTADO, valordocumento.getIdProcesso().getIdInteressado().getDsBairro()+" - "+valordocumento.getIdProcesso().getIdInteressado().getNmCidade()+" - "+valordocumento.getIdProcesso().getIdInteressado().getDsUf());
		guiadto.getParametros().put(GuiaDTO.USO_ATIVIDADE, usoatividade.getDsUsoatividade());
		guiadto.getParametros().put(GuiaDTO.NRO_PROCESSO, valordocumento.getIdProcesso().getNrProcesso()+"/"+valordocumento.getIdProcesso().getNrAno());
		guiadto.getParametros().put(GuiaDTO.NRO_PARCELAS, String.format("%02d", valordocumento.getNrParcelas()));
		guiadto.getParametros().put(GuiaDTO.REFERENCIAS, valordocumento.getDsPagamento());
		guiadto.getParametros().put(GuiaDTO.NOTA, usoatividade.getIdTributo().getNmTributo()+ "\nVALOR DO TRIBUTO: "+ usoatividade.getNrQtdufir().toString().replace(".", ","));
		
		//valor sem ufir;
		BigDecimal vlsemufir = valordocumento.getVlDocumento().divide(usoatividade.getIdUfir().getVlUfir(), 2, BigDecimal.ROUND_CEILING);
		guiadto.getParametros().put(GuiaDTO.TRIBUTO_DEVIDO, vlsemufir.toString().replace(".", ","));
		guiadto.getParametros().put(GuiaDTO.EXERCICIOS,  new SimpleDateFormat("yyyy").format(valordocumento.getDtLancamento()));
		try{
			File file = new File(context.getRealPath("img/brasao_40.jpg"));
			guiadto.getParametros().put(GuiaDTO.LOGO_MINI, new FileInputStream(file));
		}catch(FileNotFoundException f){
			f.printStackTrace();
			guiadto.getParametros().put(GuiaDTO.LOGO_MINI, null);
		}
		
		for (TblDam dam : damsNaoPagas) {
			guiadto.getParcelas().add(new ParcelaGuiaDTO(dam));
		}
		
		return jasperMaker.makePdf("boletosGenerics.jasper", guiadto.getParcelas(), "dam.pdf", false, guiadto.getParametros());
	}
	
	
}
