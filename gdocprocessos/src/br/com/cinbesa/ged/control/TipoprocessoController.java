package br.com.cinbesa.ged.control;

import java.util.List;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;

import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.cinbesa.ged.dao.EntidadeDAO;
import br.com.cinbesa.ged.dao.TipoProcessoDAO;
import br.com.cinbesa.ged.entidade.Entidade;

import br.com.cinbesa.ged.entidade.TblTipoprocesso;
import br.com.cinbesa.ged.intercept.Restrito;
import br.com.cinbesa.ged.intercept.SessaoUsuario;
import br.com.cinbesa.ged.log.control.LogTipoProcessoController;
import br.com.cinbesa.ged.util.ListaApp;
import br.com.cinbesa.ged.util.PaginacaoUtil;

@Resource
public class TipoprocessoController {

	private final TipoProcessoDAO tipoProcessoDAO;
	private final EntidadeDAO entidadeDAO;
	private final Result result;
	private final Validator validator;
	private final LogTipoProcessoController logTipoProcesso;
	private SessaoUsuario sessaoUsuario;

	public TipoprocessoController(TipoProcessoDAO tipoProcessoDAO,
			EntidadeDAO entidadeDAO, Result result, Validator validator,
			LogTipoProcessoController logTipoProcesso,
			SessaoUsuario sessaoUsuario) {
		this.tipoProcessoDAO = tipoProcessoDAO;
		this.result = result;
		this.entidadeDAO = entidadeDAO;
		this.validator = validator;
		this.logTipoProcesso = logTipoProcesso;
		this.sessaoUsuario = sessaoUsuario;
	}

	@Restrito(nome = ListaApp.LISTTPPROC)
	@Path({ "/tipoprocesso", "/tipoprocesso/pag-{pag}" })
	public void lista(String pag) {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			List<Integer> paginas = PaginacaoUtil.listaPaginas(tipoProcessoDAO
					.count(sessaoUsuario.getLotacaoUsuario().getCdSetor()
							.getIdEntidade()));
			int paginaSelecionada = PaginacaoUtil.numeroDaPagina(pag, paginas);
			if (paginas.size() != 0) {
				result.include("paginationList", PaginacaoUtil
						.listaPaginasView(paginas, paginaSelecionada));
				result.include("pagSelec", pag);
				result.include("first", PaginacaoUtil.FIRST);
				result.include("last", PaginacaoUtil.LAST);
				result.include("link", "/tipoprocesso/pag-");
			}
			result.include("tblTipoprocessoList", tipoProcessoDAO.listaTipos(
					PaginacaoUtil.reginicial(paginaSelecionada),
					PaginacaoUtil.TAMTABELA, sessaoUsuario.getLotacaoUsuario()
							.getCdSetor().getIdEntidade()));
		}
	}

	@Restrito(nome = ListaApp.ADDTPPROC)
	public void novo() {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		}
	}

	@Restrito(nome = ListaApp.ADDTPPROC)
	@Post
	public void cadastrar(TblTipoprocesso tblTipoprocesso) {
		if (tblTipoprocesso.getNmTipoprocesso() == null) {
			validator.add(new ValidationMessage("Informe o Tipo do Processo",
					"nmTipoprocesso"));
		}
		if (tblTipoprocesso.getStConfirmacaoPresencia() != 'S'
				&& tblTipoprocesso.getStConfirmacaoPresencia() != 'N') {
			validator.add(new ValidationMessage(
					"Selecione a confirmação Presencial", "stConfPresencial"));
		}
		if (tblTipoprocesso.getStAbrirInternet() != 'S'
				&& tblTipoprocesso.getStAbrirInternet() != 'N') {
			validator.add(new ValidationMessage(
					"Selecione a Abertura pela Internet", "stConfPresencial"));
		}
		validator.onErrorRedirectTo(this).novo();

		tblTipoprocesso.setNmTipoprocesso(tblTipoprocesso.getNmTipoprocesso()
				.toUpperCase());
		if (tblTipoprocesso.getDsTipoprocesso() != null) {
			tblTipoprocesso.setDsTipoprocesso(tblTipoprocesso
					.getDsTipoprocesso().toUpperCase());
		}
		tblTipoprocesso.setIdEntidade(sessaoUsuario.getLotacaoUsuario()
				.getCdSetor().getIdEntidade());
		tblTipoprocesso = tipoProcessoDAO.salvar(tblTipoprocesso);
		logTipoProcesso.registra(tblTipoprocesso,
				ListaApp.LOG_OPERACAO_INSERCAO);
		result.redirectTo(this).lista(null);
	}

	@Restrito(nome = ListaApp.EDITTPPROC)
	@Path("/tipoprocesso/editar/{id}")
	public void editar(Integer id) {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			result.include("tpativo", ListaApp.SITUACAOTIPOPROCATIVA);
			result.include("tpdesativo", ListaApp.SITUACAOTIPOPROCDESATIVA);
			result.include("tblTipoprocesso", tipoProcessoDAO.find(TblTipoprocesso.class, id));
		}
	}

	@Restrito(nome = ListaApp.EDITTPPROC)
	@Post
	public void salvaredicao(TblTipoprocesso tblTipoprocesso) {
		if (tblTipoprocesso.getNmTipoprocesso() == null) {
			validator.add(new ValidationMessage("Informe o Tipo do Processo",
					"nmTipoprocesso"));
		}
		if (tblTipoprocesso.getStConfirmacaoPresencia() != 'S'
				&& tblTipoprocesso.getStConfirmacaoPresencia() != 'N') {
			validator.add(new ValidationMessage(
					"Selecione a confirmação Presencial", "stConfPresencial"));
		}
		if (tblTipoprocesso.getStAbrirInternet() != 'S'
				&& tblTipoprocesso.getStAbrirInternet() != 'N') {
			validator.add(new ValidationMessage(
					"Selecione a Abertura pela Internet", "stConfPresencial"));
		}
		validator.onErrorRedirectTo(this).editar(
				tblTipoprocesso.getCdTipoprocesso());

		tblTipoprocesso.setNmTipoprocesso(tblTipoprocesso.getNmTipoprocesso()
				.toUpperCase());
		if (tblTipoprocesso.getDsTipoprocesso() != null) {
			tblTipoprocesso.setDsTipoprocesso(tblTipoprocesso
					.getDsTipoprocesso().toUpperCase());
		}
		tblTipoprocesso.setIdEntidade(sessaoUsuario.getLotacaoUsuario()
				.getCdSetor().getIdEntidade());
		tblTipoprocesso = tipoProcessoDAO.salvar(tblTipoprocesso);
		logTipoProcesso.registra(tblTipoprocesso,
				ListaApp.LOG_OPERACAO_INSERCAO);
		result.redirectTo(this).lista(null);

	}

}
