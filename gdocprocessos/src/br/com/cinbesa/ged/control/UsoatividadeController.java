package br.com.cinbesa.ged.control;

import java.math.BigDecimal;
import java.util.List;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;

import br.com.cinbesa.ged.dao.EntidadeDAO;
import br.com.cinbesa.ged.dao.TributoDAO;
import br.com.cinbesa.ged.dao.UfirDAO;
import br.com.cinbesa.ged.dao.UsoatividadeDAO;
import br.com.cinbesa.ged.entidade.TblUsoatividade;

import br.com.cinbesa.ged.intercept.Restrito;
import br.com.cinbesa.ged.intercept.SessaoUsuario;
import br.com.cinbesa.ged.log.control.LogUsoAtividadeController;
import br.com.cinbesa.ged.util.ListaApp;
import br.com.cinbesa.ged.util.PaginacaoUtil;

@Resource
public class UsoatividadeController {
	private final Result result;
	private final Validator validator;
	private UsoatividadeDAO usoatividadeDAO;
	private TributoDAO tributoDAO;
	private UfirDAO ufirDAO;
	private SessaoUsuario sessaoUsuario;
	private LogUsoAtividadeController logUsoAtividadeController;

	public UsoatividadeController(Result result, Validator validator,
			UsoatividadeDAO usoatividadeDAO, TributoDAO tributoDAO,
			SessaoUsuario sessaoUsuario, UfirDAO ufirDAO,
			LogUsoAtividadeController logUsoAtividadeController) {
		this.result = result;
		this.validator = validator;
		this.usoatividadeDAO = usoatividadeDAO;
		this.tributoDAO = tributoDAO;
		this.sessaoUsuario = sessaoUsuario;
		this.ufirDAO = ufirDAO;
		this.logUsoAtividadeController = logUsoAtividadeController;
	}

	@Restrito(nome = ListaApp.ADDUSOATIVIDADE)
	public void novo() {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			result.include("tblTributoList", tributoDAO.listaTodos());
			result.include("tblUfirList", ufirDAO.listartodos());
			result.include("sim", ListaApp.SITUACAOTRIBUTOEXCLUSIVOSIM);
			result.include("nao", ListaApp.SITUACAOTRIBUTOEXCLUSIVONAO);
		}
	}

	@Restrito(nome = ListaApp.ADDUSOATIVIDADE)
	@Post
	public void cadastrar(TblUsoatividade tblUsoatividade, String valorufir) {
		if (tblUsoatividade.getDsUsoatividade() == null) {
			validator.add(new ValidationMessage(
					"Informe a descri��o do Documento", "dsDocumento"));
		}
		if (valorufir == null) {
			validator.add(new ValidationMessage("Informe o valor do Uso",
					"vlUfir"));
		}
		if (tblUsoatividade.getIdTributo().getIdTributo() == 0) {
			validator
					.add(new ValidationMessage("Informe o Tributo", "tributo"));
		}

		if (tblUsoatividade.getIdEntidade().getIdEntidade() == 0) {
			validator.add(new ValidationMessage("Informe a Entidade",
					"entidade"));
		}
		if (tblUsoatividade.getIdUfir().getIdUfir() == 0) {
			validator.add(new ValidationMessage("Informe a UFIR", "ufir"));
		}
		if (tblUsoatividade.getCdDesmembramento() == null) {
			validator.add(new ValidationMessage(
					"Informe o c�d. de desmembramento", "cdDesmembramento"));
		}
		if (tblUsoatividade.getNrDocumentoContabil() == null) {
			validator.add(new ValidationMessage(
					"Informe o N�m. do Documento Cont�bil", "nrDocContabil"));
		}
		validator.onErrorUsePageOf(this).novo();

		if (tblUsoatividade.getCdDesmembramento() == " ") {
			tblUsoatividade.setCdDesmembramento("0");
		}

		valorufir = valorufir.replace(",", ".");
		BigDecimal big = new BigDecimal(valorufir);

		tblUsoatividade.setNrQtdufir(big);
		tblUsoatividade.setDsUsoatividade(tblUsoatividade.getDsUsoatividade()
				.toUpperCase());
		tblUsoatividade.setIdEntidade(sessaoUsuario.getLotacaoUsuario()
				.getCdSetor().getIdEntidade());
		tblUsoatividade = usoatividadeDAO.salvarUsoAtividade(tblUsoatividade);
		logUsoAtividadeController.registrar(tblUsoatividade,
				ListaApp.LOG_OPERACAO_INSERCAO);
		result.redirectTo(this).lista(null);
	}

	@Restrito(nome = ListaApp.LISTUSOATIVIDADE)
	@Path({ "/usoatividade", "/usoatividade/pag-{pag}" })
	public void lista(String pag) {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			List<Integer> paginas = PaginacaoUtil.listaPaginas(usoatividadeDAO
					.count(sessaoUsuario.getLotacaoUsuario().getCdSetor()
							.getIdEntidade()));
			int paginaSelecionada = PaginacaoUtil.numeroDaPagina(pag, paginas);
			if (paginas.size() != 0) {
				result.include("paginationList", PaginacaoUtil
						.listaPaginasView(paginas, paginaSelecionada));
				result.include("pagSelec", pag);
				result.include("first", PaginacaoUtil.FIRST);
				result.include("last", PaginacaoUtil.LAST);
				result.include("link", "/usoatividade/pag-");
			}
			result.include("tblUsoatividadeList", usoatividadeDAO.listartodos(
					PaginacaoUtil.reginicial(paginaSelecionada),
					PaginacaoUtil.TAMTABELA, sessaoUsuario.getLotacaoUsuario()
							.getCdSetor().getIdEntidade()));
		}
	}

	@Restrito(nome = ListaApp.EDITUSOATIVIDADE)
	@Path("/usoatividade/editar/{id}")
	public void editar(Integer id) {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			result.include("tblTributoList", tributoDAO.listaTodos());
			result.include("tblUfirList", ufirDAO.listartodos());
			result.include("sim", ListaApp.SITUACAOTRIBUTOEXCLUSIVOSIM);
			result.include("nao", ListaApp.SITUACAOTRIBUTOEXCLUSIVONAO);
			result.include("tblUsoatividade",
					usoatividadeDAO.find(TblUsoatividade.class, id));
		}
	}

	@Restrito(nome = ListaApp.EDITUSOATIVIDADE)
	@Post
	public void salvaredicao(TblUsoatividade tblUsoatividade, String valorufir) {
		if (tblUsoatividade.getDsUsoatividade() == null) {
			validator.add(new ValidationMessage(
					"Informe a descri��o do Documento", "dsDocumento"));
		}
		if (valorufir == null) {
			validator.add(new ValidationMessage("Informe o valor do Uso",
					"vlUfir"));
		}
		if (tblUsoatividade.getIdTributo().getIdTributo() == 0) {
			validator
					.add(new ValidationMessage("Informe o Tributo", "tributo"));
		}

		if (tblUsoatividade.getIdUfir().getIdUfir() == 0) {
			validator.add(new ValidationMessage("Informe a UFIR", "ufir"));
		}
		if (tblUsoatividade.getCdDesmembramento() == null) {
			validator.add(new ValidationMessage(
					"Informe o c�d. de desmembramento", "cdDesmembramento"));
		}
		if (tblUsoatividade.getNrDocumentoContabil() == null) {
			validator.add(new ValidationMessage(
					"Informe o N�m. do Documento Cont�bil", "nrDocContabil"));
		}
		validator.onErrorUsePageOf(this).editar(
				tblUsoatividade.getCdUsoatividade());

		valorufir = valorufir.replace(",", ".");
		BigDecimal big = new BigDecimal(valorufir);

		tblUsoatividade.setNrQtdufir(big);
		tblUsoatividade.setDsUsoatividade(tblUsoatividade.getDsUsoatividade()
				.toUpperCase());
		tblUsoatividade = usoatividadeDAO.salvarUsoAtividade(tblUsoatividade);
		logUsoAtividadeController.registrar(tblUsoatividade,
				ListaApp.LOG_OPERACAO_ALTERACAO);

		result.redirectTo(this).lista(null);
	}

}
