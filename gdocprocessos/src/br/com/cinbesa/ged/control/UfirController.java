package br.com.cinbesa.ged.control;

import java.math.BigDecimal;
import java.util.List;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.cinbesa.ged.dao.UfirDAO;
import br.com.cinbesa.ged.entidade.TblUfir;
import br.com.cinbesa.ged.intercept.Restrito;
import br.com.cinbesa.ged.intercept.SessaoUsuario;
import br.com.cinbesa.ged.log.control.LogUfirController;
import br.com.cinbesa.ged.util.ListaApp;
import br.com.cinbesa.ged.util.PaginacaoUtil;

@Resource
public class UfirController {

	private Result result;
	private Validator validator;
	private UfirDAO ufirDAO;
	private LogUfirController logUfirController;
	private SessaoUsuario sessaoUsuario;

	public UfirController(Result result, Validator validator, UfirDAO ufirDAO,
			LogUfirController logUfirController, SessaoUsuario sessaoUsuario) {
		this.result = result;
		this.validator = validator;
		this.ufirDAO = ufirDAO;
		this.logUfirController = logUfirController;
		this.sessaoUsuario = sessaoUsuario;
	}

	@Restrito(nome = ListaApp.ADDUFIR)
	public void novo() {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() != ListaApp.CD_NIVEL_ADMIN_SUPERADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		}
	}

	@Restrito(nome = ListaApp.ADDUFIR)
	@Post
	public void cadastrar(TblUfir tblUfir, String valorufir) {
		if (tblUfir.getNrAno() == 0) {
			validator.add(new ValidationMessage("Informe o Ano da Ufir", ""));
		}
		if (valorufir == null) {
			validator.add(new ValidationMessage("Informe o Valor da Ufir", ""));
		}
		validator.onErrorUsePageOf(this).novo();

		if (ufirDAO.ufirPorAno(tblUfir.getNrAno()) != null) {
			validator.add(new ValidationMessage(
					"Este Exerc�cio j� possui Ufir", ""));
		}
		validator.onErrorUsePageOf(this).novo();

		valorufir = valorufir.replace(",", ".");
		tblUfir.setVlUfir(new BigDecimal(valorufir));

		tblUfir = ufirDAO.salvar(tblUfir);
		logUfirController.registrar(tblUfir, ListaApp.LOG_OPERACAO_INSERCAO);
		result.redirectTo(this).lista(null);
	}

	@Restrito(nome = ListaApp.LISTUFIR)
	@Path({ "/ufir", "/ufir/pag-{pag}" })
	public void lista(String pag) {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			List<Integer> paginas = PaginacaoUtil.listaPaginas(ufirDAO.count());
			int paginaSelecionada = PaginacaoUtil.numeroDaPagina(pag, paginas);

			if (paginas.size() != 0) {
				result.include("paginationList", PaginacaoUtil
						.listaPaginasView(paginas, paginaSelecionada));
				result.include("pagSelec", pag);
				result.include("first", PaginacaoUtil.FIRST);
				result.include("last", PaginacaoUtil.LAST);
				result.include("link", "/ufir/pag-");
			}
			result.include("tblUfirList", ufirDAO.listartodos(
					PaginacaoUtil.reginicial(paginaSelecionada),
					PaginacaoUtil.TAMTABELA));
		}
	}

	@Restrito(nome = ListaApp.EDITUFIR)
	@Path("/ufir/editar/{id}")
	public void editar(Integer id) {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() != ListaApp.CD_NIVEL_ADMIN_SUPERADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			result.include("tblUfir", ufirDAO.find(TblUfir.class, id));
		}
	}

	@Restrito(nome = ListaApp.EDITUFIR)
	@Post
	public void salvar(TblUfir tblUfir, String valorufir) {
		if (tblUfir.getNrAno() == 0) {
			validator.add(new ValidationMessage("Informe o Ano da Ufir", ""));
		}
		if (valorufir == null) {
			validator.add(new ValidationMessage("Informe o Valor da Ufir", ""));
		}
		validator.onErrorUsePageOf(this).editar(tblUfir.getIdUfir());
		TblUfir aux = ufirDAO.ufirPorAno(tblUfir.getNrAno());
		if (aux != null) {
			if (aux.getIdUfir() != tblUfir.getIdUfir()) {
				validator.add(new ValidationMessage(
						"Este Exerc�cio j� possui Ufir", ""));
			}
		}
		validator.onErrorUsePageOf(this).editar(tblUfir.getIdUfir());

		valorufir = valorufir.replace(",", ".");
		tblUfir.setVlUfir(new BigDecimal(valorufir));

		tblUfir = ufirDAO.salvar(tblUfir);
		logUfirController.registrar(tblUfir, ListaApp.LOG_OPERACAO_ALTERACAO);
		result.redirectTo(this).lista(null);
	}
}
