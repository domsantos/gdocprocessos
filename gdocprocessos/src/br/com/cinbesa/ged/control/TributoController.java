package br.com.cinbesa.ged.control;

import java.util.List;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.cinbesa.ged.dao.TributoDAO;
import br.com.cinbesa.ged.entidade.TblTributo;
import br.com.cinbesa.ged.intercept.Restrito;
import br.com.cinbesa.ged.intercept.SessaoUsuario;
import br.com.cinbesa.ged.log.control.LogTributoController;
import br.com.cinbesa.ged.util.ListaApp;
import br.com.cinbesa.ged.util.PaginacaoUtil;

@Resource
public class TributoController {

	private Result result;
	private Validator validator;
	private TributoDAO tributoDAO;
	private LogTributoController logTributoController;
	private SessaoUsuario sessaoUsuario;
	
	public TributoController(Result result, Validator validator, 
			TributoDAO tributoDAO, LogTributoController logTributoController,
			SessaoUsuario sessaoUsuario) {
		this.result = result;
		this.validator = validator;
		this.tributoDAO = tributoDAO;
		this.logTributoController = logTributoController;
		this.sessaoUsuario = sessaoUsuario;
	}
	
	@Restrito(nome=ListaApp.ADDTRIBUTO)
	public void novo(){
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() != ListaApp.CD_NIVEL_ADMIN_SUPERADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		}
	}
	
	@Restrito(nome=ListaApp.ADDTRIBUTO)
	public void cadastrar(TblTributo tblTributo){
		if(tblTributo.getCdTributo() == null){
			validator.add(new ValidationMessage("Informe o C�digo do Tributo", "cdTributo"));
		}
		if(tblTributo.getNmTributo() == null){
			validator.add(new ValidationMessage("Informe o Nome do Tributo", "cdTributo"));
		}
		if(tblTributo.getDsTributo() == null){
			validator.add(new ValidationMessage("Informe a Descri��o do Tributo", "cdTributo"));
		}
		validator.onErrorUsePageOf(this).novo();
		
		tblTributo.setNmTributo(tblTributo.getNmTributo().toUpperCase());
		tblTributo.setDsTributo(tblTributo.getDsTributo().toUpperCase());
		tblTributo = tributoDAO.salvar(tblTributo);
		logTributoController.registrar(tblTributo, ListaApp.LOG_OPERACAO_INSERCAO);
		result.redirectTo(this).lista(null);
	}
	
	@Restrito(nome=ListaApp.LISTTRIBUTO)
	@Path({"/tributo","/tributo/pag-{pag}"})
	public void lista(String pag){
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
		List<Integer> paginas = PaginacaoUtil.listaPaginas(tributoDAO.count());
		int paginaSelecionada = PaginacaoUtil.numeroDaPagina(pag, paginas);
		
		if(paginas.size() != 0){
			result.include("paginationList", PaginacaoUtil.listaPaginasView(paginas, paginaSelecionada));
			result.include("pagSelec", pag);
			result.include("first", PaginacaoUtil.FIRST);
			result.include("last", PaginacaoUtil.LAST);
			result.include("link", "/perfil/pag-");
		}	
		result.include("tblTributoList", tributoDAO.listaTodos(PaginacaoUtil.reginicial(paginaSelecionada), PaginacaoUtil.TAMTABELA));
		}
	}
	
	
	@Restrito(nome=ListaApp.EDITTRIBUTO)
	@Path("/tributo/editar/{id}")
	public void editar(Integer id){
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() != ListaApp.CD_NIVEL_ADMIN_SUPERADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			result.include("tblTributo", tributoDAO.find(TblTributo.class, id));
		}
	}
	
	@Restrito(nome=ListaApp.EDITTRIBUTO)
	public void salvaredicao(TblTributo tblTributo){
		if(tblTributo.getCdTributo() == null){
			validator.add(new ValidationMessage("Informe o C�digo do Tributo", "cdTributo"));
		}
		if(tblTributo.getNmTributo() == null){
			validator.add(new ValidationMessage("Informe o Nome do Tributo", "cdTributo"));
		}
		if(tblTributo.getDsTributo() == null){
			validator.add(new ValidationMessage("Informe a Descri��o do Tributo", "cdTributo"));
		}
		validator.onErrorUsePageOf(this).editar(tblTributo.getIdTributo());
		
		tblTributo.setNmTributo(tblTributo.getNmTributo().toUpperCase());
		tblTributo.setDsTributo(tblTributo.getDsTributo().toUpperCase());
		
		tblTributo = tributoDAO.salvar(tblTributo);
		logTributoController.registrar(tblTributo, ListaApp.LOG_OPERACAO_ALTERACAO);
		result.redirectTo(this).lista(null);
	}
	
}
