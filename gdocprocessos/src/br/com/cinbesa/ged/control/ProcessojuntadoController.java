package br.com.cinbesa.ged.control;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.cinbesa.ged.dao.ProcessoDAO;
import br.com.cinbesa.ged.dao.ProcessoJuntadoDAO;
import br.com.cinbesa.ged.entidade.TblProcessos;
import br.com.cinbesa.ged.entidade.TblProcessosJuntados;
import br.com.cinbesa.ged.intercept.Restrito;
import br.com.cinbesa.ged.util.ListaApp;


@Resource
public class ProcessojuntadoController {

	
	private Result result;
	private Validator validator;
	private ProcessoDAO processoDAO;
	private ProcessoJuntadoDAO processoJuntadoDAO;
	
	public ProcessojuntadoController(Result result, 
			Validator validator,
			ProcessoDAO processoDAO,
			ProcessoJuntadoDAO processoJuntadoDAO){
		this.result = result;
		this.validator = validator;
		this.processoDAO = processoDAO;
		this.processoJuntadoDAO = processoJuntadoDAO;
	}
	
	@Restrito(nome=ListaApp.JUNTAR_PROCESSO)
	@Path("/processojuntado/juntarprocesso/{id}")
	public void juntarprocesso(Long id){
		result.include("processo", processoDAO.find(TblProcessos.class, id));
	}
	
	@Restrito(nome=ListaApp.JUNTAR_PROCESSO)
	@Post
	public void juntar(TblProcessos processo, Long idproc){
		if(processo.getNrProcesso() == null){
			validator.add(new ValidationMessage("Informe o n�mero do Processo a ser juntado", ""));
		}
		if(processo.getNrAno() == 0){
			validator.add(new ValidationMessage("Informe o ano do Processo a ser juntado", ""));
		}
		validator.onErrorRedirectTo(this).juntarprocesso(idproc);
		
		TblProcessos procpai = processoDAO.find(TblProcessos.class, idproc);
		TblProcessos procfilho = processoDAO.findPorNumAnoProcesso(processo);
		TblProcessosJuntados procjuntado = new TblProcessosJuntados();
		procjuntado.setIdProcessoPai(procpai);
		procjuntado.setIdProcessoFilho(procfilho);
		processoJuntadoDAO.cadastrar(procjuntado);
		result.redirectTo(ProcessoController.class).processoresumo(procpai.getIdProcesso());
	}
	
}
