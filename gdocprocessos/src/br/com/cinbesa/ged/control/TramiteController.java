package br.com.cinbesa.ged.control;

import java.util.ArrayList;
import java.util.List;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import static br.com.caelum.vraptor.view.Results.*;
import br.com.cinbesa.ged.dao.DocsexigidosDAO;
import br.com.cinbesa.ged.dao.EntidadeDAO;
import br.com.cinbesa.ged.dao.ProcessoDAO;
import br.com.cinbesa.ged.dao.RotaDAO;
import br.com.cinbesa.ged.dao.SetorDAO;
import br.com.cinbesa.ged.dao.TramiteDAO;

import br.com.cinbesa.ged.entidade.Entidade;
import br.com.cinbesa.ged.entidade.TblDocumentosexigidos;
import br.com.cinbesa.ged.entidade.TblProcessos;
import br.com.cinbesa.ged.entidade.TblRotas;
import br.com.cinbesa.ged.entidade.TblSetor;
import br.com.cinbesa.ged.entidade.TblTramites;
import br.com.cinbesa.ged.intercept.Restrito;
import br.com.cinbesa.ged.intercept.SessaoUsuario;
import br.com.cinbesa.ged.util.ListaApp;
import br.com.cinbesa.ged.util.PaginacaoUtil;
import br.com.cinbesa.ged.util.Util;

@Resource
public class TramiteController {

	private final Validator validator;
	private final Result result;
	private ProcessoDAO processoDAO;
	private EntidadeDAO entidadeDAO;
	private SetorDAO setorDAO;
	private SessaoUsuario sessao;
	private TramiteDAO tramiteDAO;
	private RotaDAO rotaDAO;
	private DocsexigidosDAO docsexigidosDAO;
	public TramiteController(Validator validator, Result result, ProcessoDAO processoDAO, EntidadeDAO entidadeDAO, SetorDAO setorDAO, SessaoUsuario sessao, TramiteDAO tramiteDAO, RotaDAO rotaDAO, DocsexigidosDAO docsexigidosDAO) {
		this.validator = validator;
		this.result = result;
		this.processoDAO = processoDAO;
		this.entidadeDAO = entidadeDAO;
		this.setorDAO = setorDAO;
		this.sessao = sessao;
		this.tramiteDAO = tramiteDAO;
		this.rotaDAO = rotaDAO;
		this.docsexigidosDAO = docsexigidosDAO;
	}
	
	@Restrito(nome=ListaApp.ADDTRAMITE)
	@Path("/tramite/novo/{idprocesso}")
	public void novotramite(Long idprocesso){
		TblProcessos processo = processoDAO.find(TblProcessos.class, idprocesso);
		List<TblTramites> lista = tramiteDAO.listaTramitesPorProcesso(processo);
		
		
		
		List<TblRotas> rotas = rotaDAO.listaPorTipoProcesso(processo.getCdTipoprocesso());
		TblRotas proximaRota = new TblRotas();
		ArrayList<TblDocumentosexigidos> listad = new ArrayList<TblDocumentosexigidos>();
		
		if(rotas != null) {
			proximaRota = Util.proximaRota(rotas, lista);
			
			List<TblDocumentosexigidos> listdocs = docsexigidosDAO.lista();
			for (TblDocumentosexigidos doc : listdocs) {
				if(doc.getTblRotasList().contains(proximaRota)){
					listad.add(doc);
				}
			}
		}
		
		
		
		
		
		
		result.include("tblRotas", proximaRota);
		result.include("listadocs", listad);
		result.include("tblProcessos", processo);
		result.include("entidadeList", entidadeDAO.listatodos());
	}
	
	@Get
	@Path("/tramite/listasetores.json")
	public void listaSetores(String id){
		List<TblSetor> lista = setorDAO.listaPorEntidade(entidadeDAO.find(Entidade.class, Integer.parseInt(id)));
		result.use(json()).withoutRoot().from(lista).exclude("dsSetor", "tblRotasList", "idEntidade", "tblTramitesList", "tblLotacaoList").serialize();
	}
	
	@Restrito(nome=ListaApp.ADDTRAMITE)
	@Post
	public void tramitar(TblTramites tblTramites, String tpTramite, TblRotas tblRotas){
		if(tpTramite == null){
			validator.add(new ValidationMessage("Selecione o Tipo de Tramite", "tpTramite"));
		}else{
			if(tpTramite.equals("1")){
				if(tblRotas.getCdRota() == null){
					validator.add(new ValidationMessage("Nenhuma rota cadastrada para este tr�mite", "cdRota"));
				}else{
					if(tblTramites.getDsDispacho() == null){
						validator.add(new ValidationMessage("Informe o Despacho do Tramite", "dsDispacho"));
					}
				}
			}else{
				if(tblTramites.getCdSetor().getCdSetor() == 0){
					validator.add(new ValidationMessage("Selecione o Setor de Destino", "cdSetor"));
				}
				if(tblTramites.getDsDispacho() == null){
					validator.add(new ValidationMessage("Informe o Despacho do Tramite", "dsDispacho"));
				}
			}
		}
		validator.onErrorRedirectTo(this).novotramite(tblTramites.getIdProcesso().getIdProcesso());
		
		
		TblProcessos processo = processoDAO.find(TblProcessos.class, tblTramites.getIdProcesso().getIdProcesso());
		TblSetor setor = sessao.getLotacaoUsuario().getCdSetor();
		TblTramites tramite = tramiteDAO.getUltimoTramite(processo, ListaApp.SITUACAOTRAMITERECEBIDO, setor);
		tramite.setStTramite(ListaApp.SITUACAOTRAMITETRAMITADO);
		tramiteDAO.update(tramite);
		
		tblTramites.setIdProcesso(processo);
		
		if(tpTramite.equals("1")){
			TblRotas rota = rotaDAO.find(TblRotas.class, tblRotas.getCdRota());
			tblTramites.setCdSetor(rota.getCdSetor());
		}else{
			tblTramites.setCdSetor(setorDAO.find(TblSetor.class, tblTramites.getCdSetor().getCdSetor()));
		}
		
		tblTramites.setIdUsuarioTramite(sessao.getUsuario());
		tblTramites.setDtTramite(new java.util.Date());
		tblTramites.setStTramite(ListaApp.SITUACAOTRAMITEESPERA);
		tblTramites.setDsDispacho(tblTramites.getDsDispacho().toUpperCase());
		tramiteDAO.salvarTramite(tblTramites);
		
		result.redirectTo(HomeController.class).home();
	}
	
	@Restrito(nome= ListaApp.RECEBERTRAMITE)
	@Path({"/tramite/recebertramite","/tramite/recebertramite/pag-{pag}"})
	public List<TblProcessos> recebertramite(String pag){
		List<Integer> paginas = PaginacaoUtil.listaPaginas(processoDAO.countPorSetorSituacaoEmEspera(sessao.getLotacaoUsuario().getCdSetor()));
		int paginaSelecionada = PaginacaoUtil.numeroDaPagina(pag, paginas);
		if(paginas.size() != 0){
			result.include("paginationList", PaginacaoUtil.listaPaginasView(paginas, paginaSelecionada));
			result.include("pagSelec", pag);
			result.include("first", PaginacaoUtil.FIRST);
			result.include("last", PaginacaoUtil.LAST);
			result.include("link", "/tramite/recebertramite/pag-");
		}
		return processoDAO.listaPorSetorSituacaoEmEspera(sessao.getLotacaoUsuario().getCdSetor(), PaginacaoUtil.reginicial(paginaSelecionada), PaginacaoUtil.TAMTABELA);
	}
	
	@Restrito(nome= ListaApp.RECEBERTRAMITE)
	@Path("/tramite/receberprocesso/{id}")
	public void receberprocesso(Long id){
		TblTramites tramite = tramiteDAO.getUltimoTramite(processoDAO.find(TblProcessos.class, id), ListaApp.SITUACAOTRAMITEESPERA, sessao.getLotacaoUsuario().getCdSetor());
		tramite.setStTramite(ListaApp.SITUACAOTRAMITERECEBIDO);
		tramite.setDtRecebimento(new java.util.Date());
		tramite.setIdUsuarioRecebe(sessao.getUsuario());
		tramiteDAO.update(tramite);
		result.include("mensagem", "Processo recebido");
		result.redirectTo(this).recebertramite(null);
	}
	
	@Restrito(nome=ListaApp.LISTA_TRAMITES_ESTORNO)
	@Path({"/tramite/listaestorno", "/tramite/listaestorno/pag-{pag}"})
	public List<TblProcessos> listaestorno(String pag){
		List<Integer> paginas = PaginacaoUtil.listaPaginas(tramiteDAO.countlistaProcessosAEstornar(ListaApp.SITUACAOTRAMITEESPERA, sessao.getLotacaoUsuario().getCdSetor()));
		int paginaSelecionada = PaginacaoUtil.numeroDaPagina(pag, paginas);
		if(paginas.size() != 0){
			result.include("paginationList", PaginacaoUtil.listaPaginasView(paginas, paginaSelecionada));
			result.include("pagSelec", pag);
			result.include("first", PaginacaoUtil.FIRST);
			result.include("last", PaginacaoUtil.LAST);
			result.include("link", "/tramite/listaestorno/pag-");
		}
		
		return tramiteDAO.listaProcessosAEstornar(ListaApp.SITUACAOTRAMITEESPERA, 
				sessao.getLotacaoUsuario().getCdSetor(), 
				PaginacaoUtil.reginicial(paginaSelecionada), 
				PaginacaoUtil.TAMTABELA);
	}
	
	@Restrito(nome=ListaApp.ESTORNO_TRAMITE)
	@Path("/tramite/estornartramite/{id}")
	public void estornartramite(Long id){
		TblProcessos proc = processoDAO.find(TblProcessos.class, id);
		tramiteDAO.deletarTramiteEspera(ListaApp.SITUACAOTRAMITEESPERA, proc);
		TblTramites tramite = tramiteDAO.estornarTramite(ListaApp.SITUACAOTRAMITETRAMITADO, proc, sessao.getLotacaoUsuario().getCdSetor());
		tramite.setStTramite(ListaApp.SITUACAOTRAMITERECEBIDO);
		tramiteDAO.salvarTramite(tramite);
		result.include("mensagem", "Tramite Estornado");
		result.redirectTo(this).listaestorno(null);
	}
	
	@Path("/tramite/tramiteprotocolo/{idprocesso}")
	public void tramiteprotocolo(Long idprocesso){
		TblProcessos processo = processoDAO.find(TblProcessos.class, idprocesso);
		
		result.include("tblProcessos", processo);
		result.include("entidadeList", entidadeDAO.listatodos());
	}
	
	@Path("/tramite/salvartramiteprotocolo/")
	public void salvarTramiteProtocolo(TblTramites tramite){
		System.out.println(tramite.toString());
	}
	
	
}
