package br.com.cinbesa.ged.control;

import static br.com.caelum.vraptor.view.Results.json;

import java.util.List;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.cinbesa.ged.dao.EntidadeDAO;
import br.com.cinbesa.ged.dao.LotacaoDAO;
import br.com.cinbesa.ged.dao.SetorDAO;
import br.com.cinbesa.ged.dao.UsuarioDAO;
import br.com.cinbesa.ged.entidade.Entidade;
import br.com.cinbesa.ged.entidade.TblLotacao;
import br.com.cinbesa.ged.entidade.TblSetor;
import br.com.cinbesa.ged.entidade.TblUsuario;
import br.com.cinbesa.ged.intercept.Restrito;
import br.com.cinbesa.ged.intercept.SessaoUsuario;
import br.com.cinbesa.ged.log.control.LogLotacaoController;
import br.com.cinbesa.ged.util.ListaApp;
import br.com.cinbesa.ged.util.PaginacaoUtil;

@Resource
public class LotacaoController {

	private final Result result;
	private final Validator validator;
	private final UsuarioDAO usuarioDAO;
	private final SetorDAO setorDAO;
	private final LotacaoDAO lotacaoDAO;
	private final LogLotacaoController logLotacaoController;
	private SessaoUsuario sessaoUsuario;
	private EntidadeDAO entidadeDAO;

	public LotacaoController(Result result, Validator validator,
			UsuarioDAO usuarioDAO, SetorDAO setorDAO, LotacaoDAO lotacaoDAO,
			LogLotacaoController logLotacaoController,
			SessaoUsuario sessaoUsuario, EntidadeDAO entidadeDAO) {
		this.result = result;
		this.validator = validator;
		this.usuarioDAO = usuarioDAO;
		this.setorDAO = setorDAO;
		this.lotacaoDAO = lotacaoDAO;
		this.logLotacaoController = logLotacaoController;
		this.sessaoUsuario = sessaoUsuario;
		this.entidadeDAO = entidadeDAO;
	}

	@Restrito(nome = ListaApp.LISTALOTACAO)
	@Path({ "/lotacao", "/lotacao/pag-{pag}" })
	public void lista(String pag) {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			result.include("ativada", ListaApp.LOTACAOATIVADA);
			result.include("desativada", ListaApp.LOTACAODESATIVADA);
			List<Integer> paginas = PaginacaoUtil.listaPaginas(lotacaoDAO
					.countListaTodos(sessaoUsuario.getLotacaoUsuario()
							.getCdSetor().getIdEntidade()));
			int paginaSelecionada = PaginacaoUtil.numeroDaPagina(pag, paginas);
			if (paginas.size() != 0) {
				result.include("paginationList", PaginacaoUtil
						.listaPaginasView(paginas, paginaSelecionada));
				result.include("pagSelec", pag);
				result.include("first", PaginacaoUtil.FIRST);
				result.include("last", PaginacaoUtil.LAST);
				result.include("link", "/lotacao/pag-");
			}
			result.include("tblLotacaoList", lotacaoDAO.listatodos(
					PaginacaoUtil.reginicial(paginaSelecionada),
					PaginacaoUtil.TAMTABELA, sessaoUsuario.getLotacaoUsuario()
							.getCdSetor().getIdEntidade()));
		}
	}

	@Restrito(nome = ListaApp.ADDLOTACAO)
	public void nova() {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			List<TblUsuario> listauser = usuarioDAO
					.listarPorEntidadeLotacaoAdmin(sessaoUsuario
							.getLotacaoUsuario().getCdSetor().getIdEntidade(),
							ListaApp.LOTACAOATIVADA,
							ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN);
			List<Entidade> entidades = entidadeDAO.listatodos();
			List<TblSetor> listasetor = setorDAO.listaPorEntidade(sessaoUsuario
					.getLotacaoUsuario().getCdSetor().getIdEntidade());
			result.include("listaUser", listauser);
			result.include("listaEntidade", entidades);
			result.include("listSetor", listasetor);
			result.include("entidadeSessao", sessaoUsuario.getLotacaoUsuario()
					.getCdSetor().getIdEntidade().getIdEntidade());
		}
	}

	@Restrito(nome = ListaApp.ADDLOTACAO)
	@Post
	public void cadastrar(TblLotacao lotacao) {
		if (lotacao.getIdUsuario().getIdUsuario() == null) {
			validator.add(new ValidationMessage("Selecione um Usu�rio",
					"idUsuario"));
		}
		if (lotacao.getCdSetor().getCdSetor() == 0) {
			validator
					.add(new ValidationMessage("Selecione um Setor", "cdSetor"));
		}

		TblUsuario user = usuarioDAO.find(TblUsuario.class, lotacao
				.getIdUsuario().getIdUsuario());
		TblSetor setor = setorDAO.find(TblSetor.class, lotacao.getCdSetor()
				.getCdSetor());

		if (lotacaoDAO.verificaLotacao(user, setor) != null) {
			validator.add(new ValidationMessage(
					"J� existe uma lotacao com este usu�rio e Setor",
					"cdSetor idUsuario"));
		}

		validator.onErrorUsePageOf(this).nova();

		TblLotacao lotacaoAtivada = lotacaoDAO.verificaLotacaoUsuario(user,
				ListaApp.LOTACAOATIVADA);
		if (lotacaoAtivada != null) {
			lotacaoAtivada.setStLotacao(ListaApp.LOTACAODESATIVADA);
			lotacaoDAO.update(lotacaoAtivada);
		}
		lotacao.setStLotacao(ListaApp.LOTACAOATIVADA);
		lotacao.setDtLotacao(new java.util.Date());
		lotacao.setCdSetor(setor);
		lotacao.setIdUsuario(user);
		lotacao = lotacaoDAO.salvar(lotacao);
		logLotacaoController.registrar(lotacao, ListaApp.LOG_OPERACAO_INSERCAO);
		result.redirectTo(LotacaoController.class).lista(null);
	}

	@Restrito(nome = ListaApp.EDITLOTACAO)
	@Path("/lotacao/editar/{id}")
	public void editar(Integer id) {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			TblLotacao lotacao = lotacaoDAO.find(TblLotacao.class, id);
			List<TblSetor> listasetor = setorDAO.listaPorEntidade(sessaoUsuario
					.getLotacaoUsuario().getCdSetor().getIdEntidade());
			List<Entidade> entidades = entidadeDAO.listatodos();
			List<TblUsuario> listauser = usuarioDAO.listartodos(sessaoUsuario
					.getLotacaoUsuario().getCdSetor().getIdEntidade(),
					ListaApp.LOTACAOATIVADA);
			result.include("listaSetor", listasetor);
			result.include("listaUser", listauser);
			result.include("lot", lotacao);
			result.include("ativada", ListaApp.LOTACAOATIVADA);
			result.include("desativada", ListaApp.LOTACAODESATIVADA);
			result.include("listaEntidade", entidades);
			result.include("entidadeSessao", sessaoUsuario.getLotacaoUsuario()
					.getCdSetor().getIdEntidade().getIdEntidade());
		}
	}

	@Restrito(nome = ListaApp.EDITLOTACAO)
	@Post
	public void salvaredicao(TblLotacao lotacao) {
		if (lotacao.getIdUsuario().getIdUsuario() == null) {
			validator.add(new ValidationMessage("Selecione um Usu�rio",
					"idUsuario"));
		}
		if (lotacao.getCdSetor().getCdSetor() == null) {
			validator
					.add(new ValidationMessage("Selecione um Setor", "cdSetor"));
		}
		TblSetor setor = setorDAO.find(TblSetor.class, lotacao.getCdSetor()
				.getCdSetor());
		TblUsuario usuario = usuarioDAO.find(TblUsuario.class, lotacao
				.getIdUsuario().getIdUsuario());
		if (lotacaoDAO.verificaLotacao(usuario, setor) != null) {
			validator.add(new ValidationMessage(
					"J� existe uma lotacao com este usu�rio e Setor",
					"cdSetor idUsuario"));
		}
		validator.onErrorRedirectTo(this).editar(lotacao.getCdLotacao());

		TblLotacao lot = lotacaoDAO.find(TblLotacao.class,
				lotacao.getCdLotacao());
		lot.setIdUsuario(usuario);
		lot.setCdSetor(setor);
		lotacaoDAO.update(lot);
		logLotacaoController.registrar(lot, ListaApp.LOG_OPERACAO_ALTERACAO);
		result.redirectTo(LotacaoController.class).lista(null);
	}

	@Restrito(nome = ListaApp.ATIVLOTACAO)
	@Path("/lotacao/ativar/{id}")
	public void ativar(Integer id) {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			TblLotacao lot = lotacaoDAO.find(TblLotacao.class, id);
			for (TblLotacao userlot : lot.getIdUsuario().getTblLotacaoList()) {
				if (userlot.getStLotacao() == ListaApp.LOTACAOATIVADA) {
					validator.add(new ValidationMessage(
							"Usu�rio j� possui lota��o ativada", "lotAtivada"));
				}
			}
			validator.onErrorRedirectTo(this).lista(null);
			lot.setStLotacao(ListaApp.LOTACAOATIVADA);
			lotacaoDAO.update(lot);
			logLotacaoController
					.registrar(lot, ListaApp.LOG_OPERACAO_ALTERACAO);
			result.redirectTo(this).lista(null);
		}
	}

	@Restrito(nome = ListaApp.DESATIVLOTACAO)
	@Path("/lotacao/desativar/{id}")
	public void desativar(Integer id) {
		if (sessaoUsuario.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			TblLotacao lot = lotacaoDAO.find(TblLotacao.class, id);
			lot.setStLotacao(ListaApp.LOTACAODESATIVADA);
			lotacaoDAO.update(lot);
			logLotacaoController
					.registrar(lot, ListaApp.LOG_OPERACAO_ALTERACAO);
			result.redirectTo(this).lista(null);
		}
	}

	@Path("/lotacao/pesquisalotacao")
	public void pesquisalotacao(String nomeUsuario) {
		if (nomeUsuario == null) {
			validator.add(new ValidationMessage("Informe o Nome do Usu�rio",
					"nmUsuario"));
		}
		validator.onErrorRedirectTo(this).lista(null);

		List<TblLotacao> lista = lotacaoDAO
				.pesquisaLotacaoPorNomeUsuario(nomeUsuario);
		result.include("ativada", ListaApp.LOTACAOATIVADA);
		result.include("desativada", ListaApp.LOTACAODESATIVADA);
		result.include("tblLotacaoList", lista);
		result.of(this).lista(null);

	}

	@Get
	@Path("/lotacao/listasetores.json")
	public void listaSetores(String id) {
		List<TblSetor> lista = setorDAO.listaPorEntidade(entidadeDAO.find(
				Entidade.class, Integer.parseInt(id)));
		result.use(json())
				.withoutRoot()
				.from(lista)
				.exclude("dsSetor", "tblRotasList", "idEntidade",
						"tblTramitesList", "tblLotacaoList").serialize();
	}

}
