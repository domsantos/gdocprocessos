package br.com.cinbesa.ged.control;

import java.util.List;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.cinbesa.ged.dao.InteressadoDAO;

import br.com.cinbesa.ged.entidade.TblInteressados;

import br.com.cinbesa.ged.intercept.Restrito;
import br.com.cinbesa.ged.log.control.LogInteressadoController;
import br.com.cinbesa.ged.util.ListaApp;
import br.com.cinbesa.ged.util.Util;

@Resource
public class InteressadoController {
	private final Result result;
	private final Validator validator;
	private InteressadoDAO interessadoDAO;
	private LogInteressadoController logInteressadoController;
	
	public InteressadoController(Result result, Validator validator, InteressadoDAO interessadoDAO, LogInteressadoController logInteressadoController ) {
		this.result = result;
		this.validator = validator;
		this.interessadoDAO = interessadoDAO;
		this.logInteressadoController =  logInteressadoController;
	}
	
	public void selecionainteressado(){
		
	}
	
	@Post
	public void pesquisar(TblInteressados tblInteressados){
		if(tblInteressados.getNmInteressado() == null && tblInteressados.getNrCpfCnpj() == null){
			validator.add(new ValidationMessage("Informe um m�todo de busca", "category method"));
		}
		if(tblInteressados.getNrCpfCnpj() != null){
			if(tblInteressados.getNrCpfCnpj().length() < 11){
				validator.add(new ValidationMessage("CPF Inv�lido", "cpf inv"));
			}else{
				if(tblInteressados.getNrCpfCnpj().length() > 11 && tblInteressados.getNrCpfCnpj().length() < 14){
					validator.add(new ValidationMessage("CNPJ Inv�lido", "cnpj inv"));
				}
			}
		}
		validator.onErrorRedirectTo(this).selecionainteressado();
		List<TblInteressados> lista = null;
		if(tblInteressados.getNrCpfCnpj() != null){
			lista = interessadoDAO.listaPorCPFCNPJ(Util.limpaString(tblInteressados.getNrCpfCnpj()));
		}
		if(tblInteressados.getNmInteressado() != null){
			lista = interessadoDAO.listaPorNome(tblInteressados);
		}
		
		result.include("listaInteressados", lista);
		result.redirectTo(this).selecionainteressado();
	}
	
	@Restrito(nome = ListaApp.NOVOINTERESSADO)
	public void novo(){
		
	}
	
	@Restrito(nome = ListaApp.NOVOINTERESSADO)
	@Post
	public void cadastrar(TblInteressados tblInteressados){
		if(tblInteressados.getNmInteressado() == null){
			validator.add(new ValidationMessage("Informe o nome do Interessado", "nmInteressado"));
		}
		
		if(tblInteressados.getDsEndereco() == null){
			validator.add(new ValidationMessage("Informe o endere�o do Interessado", "dsEndereco"));
		}
		boolean valid = false;
		if(tblInteressados.getNrCpfCnpj() == null){
			validator.add(new ValidationMessage("Informe o CPFCNPJ", "nrCpfCnpj"));
		}else{
			if(tblInteressados.getNrCpfCnpj() == ""){
				valid = false;
			}
			if(Util.limpaString(tblInteressados.getNrCpfCnpj()).length() == 11){
						valid = Util.isCPF(Util.limpaString(tblInteressados.getNrCpfCnpj()));
			}else{
				if(Util.limpaString(tblInteressados.getNrCpfCnpj()).length() == 14){
					valid = Util.isCNPJ(Util.limpaString(tblInteressados.getNrCpfCnpj()));
				}else{
					valid = false;
				}
			}
		}
		if(!valid){
			validator.add(new ValidationMessage("CPF/CNPJ Inv�lido", ""));
		}else{
			List<TblInteressados> lista = interessadoDAO.listaPorCPFCNPJ(Util.limpaString(tblInteressados.getNrCpfCnpj()));
			if(lista.size() != 0){
				validator.add(new ValidationMessage("CPF/CNPJ possui um cadastrado no sistema", ""));
			}
		}
		validator.onErrorUsePageOf(this).novo();
		tblInteressados.setNrCpfCnpj(Util.limpaString(tblInteressados.getNrCpfCnpj()));
		tblInteressados.setNmInteressado(tblInteressados.getNmInteressado().toUpperCase());
		tblInteressados.setDsEndereco(tblInteressados.getDsEndereco().toUpperCase());
		if(tblInteressados.getDsBairro() != null){
			tblInteressados.setDsBairro(tblInteressados.getDsBairro().toUpperCase());
		}
		if(tblInteressados.getDsComplemento() != null){
			tblInteressados.setDsComplemento(tblInteressados.getDsComplemento().toUpperCase());
		}
		if(tblInteressados.getNrCep() != null){
			tblInteressados.setNrCep(Util.limpaString(tblInteressados.getNrCep()));
		}
		if(tblInteressados.getNmCidade() != null){
			tblInteressados.setNmCidade(tblInteressados.getNmCidade().toUpperCase());
		}
		if(tblInteressados.getDsUf() != null){
			tblInteressados.setDsUf(tblInteressados.getDsUf().toUpperCase());
		}
		
		tblInteressados = interessadoDAO.salvar(tblInteressados);
		logInteressadoController.registrarLog(tblInteressados, ListaApp.LOG_OPERACAO_INSERCAO);

		result.redirectTo(ProcessoController.class).novoprocesso(tblInteressados.getIdInteressado());
	}
	
	@Restrito(nome= ListaApp.EDITINTERESSADO)
	@Path("/interessado/editar/{id}")
	public void editar(Integer id){
		TblInteressados tblInteressados = interessadoDAO.find(TblInteressados.class, id);
		if(tblInteressados.getNrCpfCnpj().equals("99999999999999")){
			result.redirectTo(this).selecionainteressado();
		}else{
			result.include("tblInteressados", tblInteressados);
		}
	}
	
	@Restrito(nome= ListaApp.EDITINTERESSADO)
	public void salvaredicao(TblInteressados tblInteressados){
		if(tblInteressados.getNmInteressado() == null){
			validator.add(new ValidationMessage("Informe o nome do Interessado", "nmInteressado"));
		}
		if(tblInteressados.getDsEndereco() == null){
			validator.add(new ValidationMessage("Informe o endere�o do Interessado", "dsEndereco"));
		}
		boolean valid = false;
		if(tblInteressados.getNrCpfCnpj() == null){
			validator.add(new ValidationMessage("Informe o CPFCNPJ", "nrCpfCnpj"));
		}else{
			if(tblInteressados.getNrCpfCnpj() == ""){
				valid = false;
			}
			if(Util.limpaString(tblInteressados.getNrCpfCnpj()).length() == 11){
						valid = Util.isCPF(Util.limpaString(tblInteressados.getNrCpfCnpj()));
			}else{
				if(Util.limpaString(tblInteressados.getNrCpfCnpj()).length() == 14){
					valid = Util.isCNPJ(Util.limpaString(tblInteressados.getNrCpfCnpj()));
				}else{
					valid = false;
				}
			}
		}
		if(!valid){
			validator.add(new ValidationMessage("CPF/CNPJ Inv�lido", ""));
		}else{
			List<TblInteressados> lista = interessadoDAO.listaPorCPFCNPJ(Util.limpaString(tblInteressados.getNrCpfCnpj()));
			if(lista.size() != 0){
				if(lista.get(0).getIdInteressado() != tblInteressados.getIdInteressado()){
					validator.add(new ValidationMessage("CPF/CNPJ j� cadastrado no sistema", ""));
				}
			}
		}
		validator.onErrorUsePageOf(this).editar(tblInteressados.getIdInteressado());
		
		tblInteressados.setNmInteressado(tblInteressados.getNmInteressado().toUpperCase());
		tblInteressados.setNrCpfCnpj(Util.limpaString(tblInteressados.getNrCpfCnpj()));
		tblInteressados.setDsEndereco(tblInteressados.getDsEndereco().toUpperCase());
		if(tblInteressados.getDsBairro() != null){
			tblInteressados.setDsBairro(tblInteressados.getDsBairro().toUpperCase());
		}
		if(tblInteressados.getDsComplemento() != null){
			tblInteressados.setDsComplemento(tblInteressados.getDsComplemento().toUpperCase());
		}
		if(tblInteressados.getNrCep() != null){
			tblInteressados.setNrCep(Util.limpaString(tblInteressados.getNrCep()));
		}
		if(tblInteressados.getNmCidade() != null){
			tblInteressados.setNmCidade(tblInteressados.getNmCidade().toUpperCase());
		}
		if(tblInteressados.getDsUf() != null){
			tblInteressados.setDsUf(tblInteressados.getDsUf().toUpperCase());
		}
		
		tblInteressados = interessadoDAO.salvar(tblInteressados);
		logInteressadoController.registrarLog(tblInteressados, ListaApp.LOG_OPERACAO_ALTERACAO);
		
		result.redirectTo(this).selecionainteressado();
	}
	
	@Restrito(nome=ListaApp.NOVO_PROCESSO_ANONIMO)
	public void processoanonimo(){
		String anonimo = "99999999999999";
		TblInteressados interessados = interessadoDAO.getInteressadoAnonimo(anonimo);
		result.redirectTo(ProcessoController.class).novoprocesso(interessados.getIdInteressado());
	}
	
}
