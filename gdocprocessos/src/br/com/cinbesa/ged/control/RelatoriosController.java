package br.com.cinbesa.ged.control;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.interceptor.download.Download;
import br.com.cinbesa.ged.dao.RelatoriosDAO;
import br.com.cinbesa.ged.dto.DocAnexoDTO;
import br.com.cinbesa.ged.dto.RelatorioProcessoAtrasoDTO;
import br.com.cinbesa.ged.dto.RelatorioProcessoVsSetorDTO;
import br.com.cinbesa.ged.entidade.TblProcessos;
import br.com.cinbesa.ged.intercept.Restrito;
import br.com.cinbesa.ged.intercept.SessaoUsuario;
import br.com.cinbesa.ged.util.JasperMaker;
import br.com.cinbesa.ged.util.ListaApp;

@Resource
public class RelatoriosController {
	private final Result result;
	private final Validator validator;
	private SessaoUsuario sessaoUsuario;
	private RelatoriosDAO relatoriosDAO;
	private ServletContext context;
	private JasperMaker jasperMaker;

	public RelatoriosController(Result result, Validator validatos,
			SessaoUsuario sessaoUsuario, RelatoriosDAO relatoriosDAO,
			ServletContext context, JasperMaker jasperMaker) {
		this.result = result;
		this.validator = validatos;
		this.sessaoUsuario = sessaoUsuario;
		this.relatoriosDAO = relatoriosDAO;
		this.context = context;
		this.jasperMaker = jasperMaker;
	}

	@Restrito(nome=ListaApp.MENU_RELATORIOS)
	public void menurelatorio() {
	}

	@Restrito(nome=ListaApp.RELATORIO_PROCESSOS_EM_ATRASO)
	public void processosatrasados() {
	}
	
	@Restrito(nome=ListaApp.RELATORIO_PROCESSOS_EM_ATRASO)
	public Download processosematraso(String datainicial, String datafinal) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date d1 = null;
		Date d2 = null;
		try {
			d1 = sdf.parse(datainicial);
			d2 = sdf.parse(datafinal);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Calendar c = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();
		c.clear();
		c2.clear();
		c.setTimeInMillis(d1.getTime());
		c2.setTimeInMillis(d2.getTime());
		c2.set(Calendar.HOUR_OF_DAY, 23);
		c2.set(Calendar.MINUTE, 59);
		Date dataInicial = new Date(c.getTimeInMillis());
		Date dataFinal = new Date(c2.getTimeInMillis());
		Date dataImpressao = new Date();
		List<RelatorioProcessoAtrasoDTO> lista = relatoriosDAO.listaProcessosAtrasados(dataInicial, dataFinal,
				sessaoUsuario.getLotacaoUsuario().getCdSetor().getIdEntidade(),
				ListaApp.SITUACAOPROCESSOABERTO);

		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("dataInicial", dataInicial);
		parametros.put("dataFinal", dataFinal);
		parametros.put("dataImpressao", dataImpressao);
		try {
			File file = new File(context.getRealPath("img/brasao_350.jpg"));
			parametros.put("brasao", new FileInputStream(file));
		} catch (FileNotFoundException f) {
			f.printStackTrace();
			parametros.put("brasao", null);
		}
		return jasperMaker.makePdf("processosAtrasados.jasper", lista,
				"processosAtrasados.pdf", true, parametros);

	}

	@Restrito(nome=ListaApp.RELATORIO_PROCESSOS_VS_SETOR)
	public void processovssetor(){
	}
	
	@Restrito(nome=ListaApp.RELATORIO_PROCESSOS_VS_SETOR)
	public Download relatorioprocessovssetor(String datainicial, String datafinal){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date d1 = null;
		Date d2 = null;
		try {
			d1 = sdf.parse(datainicial);
			d2 = sdf.parse(datafinal);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Calendar c = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();
		c.clear();
		c2.clear();
		c.setTimeInMillis(d1.getTime());
		c2.setTimeInMillis(d2.getTime());
		c2.set(Calendar.HOUR_OF_DAY, 23);
		c2.set(Calendar.MINUTE, 59);
		Date dataInicial = new Date(c.getTimeInMillis());
		Date dataFinal = new Date(c2.getTimeInMillis());
		Date dataImpressao = new Date();
		List<RelatorioProcessoVsSetorDTO> lista = relatoriosDAO.listaProcessosVsSetor(dataInicial, 
				dataFinal, 
				sessaoUsuario.getLotacaoUsuario().getCdSetor().getIdEntidade(), 
				ListaApp.SITUACAOPROCESSOABERTO, 
				ListaApp.SITUACAOPROCESSOENCERRADO, 
				ListaApp.SITUACAOTRAMITEFINALIZADO);
		
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("dataInicial", dataInicial);
		parametros.put("dataFinal", dataFinal);
		parametros.put("dataImpressao", dataImpressao);
		try {
			File file = new File(context.getRealPath("img/brasao_350.jpg"));
			File f = new File(context.getRealPath("img/gdoc-Cinbesa-PROCESSOS2.png"));
			parametros.put("brasao", new FileInputStream(file));
			parametros.put("logo", new FileInputStream(f));
		} catch (FileNotFoundException f) {
			f.printStackTrace();
			parametros.put("brasao", null);
			parametros.put("logo", null);
		}
				return jasperMaker.makePdf("processosVsSetor.jasper", lista,
						"processosVsSetor.pdf", true, parametros);
		
	}
	
}
