package br.com.cinbesa.ged.control;

import java.util.List;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.cinbesa.ged.dao.RegraDAO;
import br.com.cinbesa.ged.dao.TipoProcessoDAO;

import br.com.cinbesa.ged.entidade.TblRegras;
import br.com.cinbesa.ged.entidade.TblTipoprocesso;
import br.com.cinbesa.ged.intercept.Restrito;
import br.com.cinbesa.ged.intercept.SessaoUsuario;
import br.com.cinbesa.ged.util.ListaApp;
import br.com.cinbesa.ged.util.PaginacaoUtil;

@Resource
public class RegraController {
	
	private final Result result;
	private final Validator validator;
	private RegraDAO regraDAO;
	private TipoProcessoDAO tipoProcessoDAO;
	private SessaoUsuario sessao;
    public RegraController(Result result, Validator validator, RegraDAO regraDAO, TipoProcessoDAO tipoProcessoDAO, SessaoUsuario sessaoUsuario) {
		this.result = result;
		this.validator = validator;
		this.regraDAO = regraDAO;
		this.tipoProcessoDAO = tipoProcessoDAO;
		sessao = sessaoUsuario;
	}
    
    @Restrito(nome=ListaApp.LISTREGRAS)
    @Path({"/regra/{idTipoProcesso}","/regra/{idTipoProcesso}/pag-{pag}"})
    public List<TblRegras> lista(String pag, Integer idTipoProcesso){
    	TblTipoprocesso tpproc = tipoProcessoDAO.find(TblTipoprocesso.class, idTipoProcesso);
    	List<Integer> paginas = PaginacaoUtil.listaPaginas(regraDAO.count(tpproc));
		int paginaSelecionada = PaginacaoUtil.numeroDaPagina(pag, paginas);
		if(paginas.size() != 0){	
			result.include("paginationList", PaginacaoUtil.listaPaginasView(paginas, paginaSelecionada));
			result.include("pagSelec", pag);
			result.include("first", PaginacaoUtil.FIRST);
			result.include("last", PaginacaoUtil.LAST);
			result.include("link", "/regra/"+idTipoProcesso+"/pag-");
		}
		result.include("tipoprocesso", tpproc);
		return regraDAO.listartodos(PaginacaoUtil.reginicial(paginaSelecionada), PaginacaoUtil.TAMTABELA, tpproc);
    }

    @Restrito(nome=ListaApp.ADDREGRAS)
    @Path("/regra/{cdtipoprocesso}/novo")
    public TblTipoprocesso novo(Integer cdtipoprocesso){
    	return tipoProcessoDAO.find(TblTipoprocesso.class, cdtipoprocesso);
    }
    
    @Restrito(nome=ListaApp.ADDREGRAS)
    @Post
    public void cadastrar(TblRegras tblRegras){
    	if(tblRegras.getNmRegra() == null){
    		validator.add(new ValidationMessage("Informe o Nome da Regra", "Nome da Regra"));
    	}
    	if(tblRegras.getCdTipoprocesso().getCdTipoprocesso() == 0){
    		validator.add(new ValidationMessage("Selecione o Tipo de Processo da Regra", "Tipo de Processo"));
    	}
    	if(tblRegras.getDsRegra() == null){
    		validator.add(new ValidationMessage("Informe a descri��o da Regra", "Descri��o da Regra"));
    	}
    	validator.onErrorUsePageOf(this).novo(tblRegras.getCdTipoprocesso().getCdTipoprocesso());
    	tblRegras.setCdTipoprocesso(tipoProcessoDAO.find(TblTipoprocesso.class, tblRegras.getCdTipoprocesso().getCdTipoprocesso()));
    	tblRegras.setNmRegra(tblRegras.getNmRegra().toUpperCase());
    	tblRegras.setDsRegra(tblRegras.getDsRegra().toUpperCase());
    	regraDAO.cadastrar(tblRegras);
    	result.redirectTo(this).lista(null, tblRegras.getCdTipoprocesso().getCdTipoprocesso());
    }
    
    @Restrito(nome=ListaApp.EDITREGRAS)
    @Path("/regra/editar/{id}")
    public void editar(Integer id){
    	result.include("regra",regraDAO.find(TblRegras.class, id));
    }
    
    @Restrito(nome=ListaApp.EDITREGRAS)
    @Post
    public void salvar(TblRegras tblRegras){
    	if(tblRegras.getNmRegra() == null){
    		validator.add(new ValidationMessage("Informe o Nome da Regra", "Nome da Regra"));
    	}
    	if(tblRegras.getCdTipoprocesso().getCdTipoprocesso() == 0){
    		validator.add(new ValidationMessage("Selecione o Tipo de Processo da Regra", "Tipo de Processo"));
    	}
    	if(tblRegras.getDsRegra() == null){
    		validator.add(new ValidationMessage("Informe a descri��o da Regra", "Descri��o da Regra"));
    	}
    	validator.onErrorUsePageOf(this).editar(tblRegras.getCdRegra());
    	tblRegras.setCdTipoprocesso(tipoProcessoDAO.find(TblTipoprocesso.class, tblRegras.getCdTipoprocesso().getCdTipoprocesso()));
    	tblRegras.setNmRegra(tblRegras.getNmRegra().toUpperCase());
    	tblRegras.setDsRegra(tblRegras.getDsRegra().toUpperCase());
    	regraDAO.update(tblRegras);
    	result.redirectTo(this).lista(null, tblRegras.getCdTipoprocesso().getCdTipoprocesso());
    }
    
    @Path("/regra/excluir/{id}")
    public void excluir(Integer id){
    	TblRegras r = regraDAO.find(TblRegras.class, id);
    	regraDAO.delete(r);
    	result.redirectTo(this).lista(null, r.getCdTipoprocesso().getCdTipoprocesso());
    }
}
