package br.com.cinbesa.ged.control;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;

import com.sun.xml.internal.ws.developer.MemberSubmissionAddressing.Validation;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.interceptor.download.ByteArrayDownload;
import br.com.caelum.vraptor.interceptor.download.Download;
import br.com.caelum.vraptor.interceptor.multipart.UploadedFile;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.cinbesa.ged.dao.DocsAnexosDAO;
import br.com.cinbesa.ged.dao.EspecieDAO;
import br.com.cinbesa.ged.dao.ProcessoDAO;
import br.com.cinbesa.ged.dto.DocAnexoDTO;
import br.com.cinbesa.ged.entidade.TblDocumentosanexos;
import br.com.cinbesa.ged.entidade.TblProcessos;
import br.com.cinbesa.ged.intercept.Restrito;
import br.com.cinbesa.ged.log.control.LogDocumentosanexosController;
import br.com.cinbesa.ged.util.ListaApp;

@Resource
public class DocumentosanexosController {
	private final Result result;
	private final Validator validator;
	private ProcessoDAO processoDAO;
	private DocsAnexosDAO docsAnexosDAO;
	private LogDocumentosanexosController logDocumentosanexosController;
	private EspecieDAO especieDAO;

	public DocumentosanexosController(Result result, Validator validator,
			ProcessoDAO processoDAO, DocsAnexosDAO docsAnexosDAO,
			LogDocumentosanexosController logDocumentosanexosController,
			EspecieDAO especieDAO) {
		this.result = result;
		this.validator = validator;
		this.processoDAO = processoDAO;
		this.docsAnexosDAO = docsAnexosDAO;
		this.logDocumentosanexosController = logDocumentosanexosController;
		this.especieDAO = especieDAO;
	}

	@Restrito(nome = ListaApp.LISTDOCANEXO)
	@Path("/documentosanexos/{id}/documentos")
	public void lista(Long id) {
		TblProcessos processo = processoDAO.find(TblProcessos.class, id);
		result.include("idProcesso", processo.getIdProcesso());
		List<DocAnexoDTO> lista = docsAnexosDAO.listaPorProcesso(processo);
		result.include("docAnexoList", lista);
	}

	@Restrito(nome = ListaApp.ADDDOCANEXO)
	@Path("/documentosanexos/{id}/novoanexo")
	public TblProcessos novo(Long id) {
		result.include("especieList", especieDAO.listatodos());
		return processoDAO.find(TblProcessos.class, id);
	}

	@Restrito(nome=ListaApp.ADDDOCANEXO)
	@Post
	public void cadastrarnovoanexo(TblDocumentosanexos tblDocumentosanexos,
			int especie, UploadedFile arquivo) throws IOException {

		if (tblDocumentosanexos.getNmDocumentoanexo() == null) {
			validator.add(new ValidationMessage("Informe o nome do Anexo", ""));
		}

		if (arquivo != null) {
			InputStream stream = arquivo.getFile();
			String nomearq = arquivo.getFileName();
			String[] aux = nomearq.split("\\.");
			String tipoarq = aux[aux.length-1];
			if(tipoarq.equalsIgnoreCase("sh") ||
					tipoarq.equalsIgnoreCase("exe") ||
					tipoarq.equalsIgnoreCase("dat") ||
					tipoarq.equalsIgnoreCase("bat") ||
					tipoarq.equalsIgnoreCase("dll") ||
					tipoarq.equalsIgnoreCase("so") ||
					tipoarq.equalsIgnoreCase("com")){
				validator.add(new ValidationMessage("Formato do anexo inv�lido", ""));
			}
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			IOUtils.copy(stream, baos);
			tblDocumentosanexos.setBiArquivo(baos.toByteArray());
			tblDocumentosanexos.setDsPathArquivo(arquivo.getContentType());
		}

		validator.onErrorRedirectTo(this).novo(
				tblDocumentosanexos.getIdProcesso().getIdProcesso());
		tblDocumentosanexos.setIdEspecie(especie);
		tblDocumentosanexos.setNmDocumentoanexo(tblDocumentosanexos
				.getNmDocumentoanexo().toUpperCase());
		tblDocumentosanexos.setIdProcesso(processoDAO.find(TblProcessos.class,
				tblDocumentosanexos.getIdProcesso().getIdProcesso()));
		tblDocumentosanexos.setDtAnexo(new Date());

		tblDocumentosanexos = docsAnexosDAO.novo(tblDocumentosanexos);
		logDocumentosanexosController.registrar(tblDocumentosanexos,
				ListaApp.LOG_OPERACAO_INSERCAO);

		result.redirectTo(this).lista(
				tblDocumentosanexos.getIdProcesso().getIdProcesso());
	}

	@Restrito(nome = ListaApp.EDITDOCANEXO)
	@Path("/documentosanexos/{id}/editar")
	public void editar(Integer id) {
		TblDocumentosanexos doc = docsAnexosDAO.find(TblDocumentosanexos.class,
				id);
		if(doc.getBiArquivo() == null){
			result.include("existearquivo", "N");
		}else{
			result.include("existearquivo", "S");
		}
		result.include("especieList", especieDAO.listatodos());
		result.include("tblDocumentosanexos", doc);
		result.include("tblProcessos", processoDAO.find(TblProcessos.class, doc
				.getIdProcesso().getIdProcesso()));
	}

	@Restrito(nome = ListaApp.EDITDOCANEXO)
	@Post
	public void salvaredicao(TblDocumentosanexos tblDocumentosanexos, Integer especie, UploadedFile arquivo) throws IOException {
		if (tblDocumentosanexos.getNmDocumentoanexo() == null) {
			validator.add(new ValidationMessage("Informe o nome do Anexo", ""));
		}
		
		validator.onErrorRedirectTo(this).editar(
				tblDocumentosanexos.getCdDocumentoanexo());
		TblDocumentosanexos docs = docsAnexosDAO.find(
				TblDocumentosanexos.class,
				tblDocumentosanexos.getCdDocumentoanexo());
		if (arquivo != null) {
			InputStream stream = arquivo.getFile();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			IOUtils.copy(stream, baos);
			docs.setBiArquivo(baos.toByteArray());
			docs.setDsPathArquivo(arquivo.getContentType());
		}
		docs.setIdEspecie(especie);
		docs.setNmDocumentoanexo(tblDocumentosanexos.getNmDocumentoanexo()
				.toUpperCase());

		docs = docsAnexosDAO.salvarEdicao(docs);
		logDocumentosanexosController.registrar(docs,
				ListaApp.LOG_OPERACAO_ALTERACAO);

		result.redirectTo(this).lista(docs.getIdProcesso().getIdProcesso());
	}

	@Restrito(nome=ListaApp.DOWNLOAD_ANEXO)
	@Path("/documentosanexos/{cdDoc}/baixaranexo")
	public Download baixaranexo(Integer cdDoc){
		TblDocumentosanexos doc = docsAnexosDAO.find(TblDocumentosanexos.class, cdDoc);
		if(doc.getBiArquivo() == null){
			validator.add(new ValidationMessage("O Anexo n�o possui arquivo a ser baixado", ""));
		}
		validator.onErrorRedirectTo(this).lista(doc.getIdProcesso().getIdProcesso());
		ByteArrayDownload bd =  new ByteArrayDownload(doc.getBiArquivo(), doc.getDsPathArquivo(), ""+doc.getIdProcesso().getNrProcesso()+doc.getIdProcesso().getNrAno());
		return bd;
	}
	
}
