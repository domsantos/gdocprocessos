package br.com.cinbesa.ged.control;

import java.util.Date;
import java.util.List;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.cinbesa.ged.dao.LotacaoDAO;
import br.com.cinbesa.ged.dao.PerfilDAO;
import br.com.cinbesa.ged.dao.SetorDAO;
import br.com.cinbesa.ged.dao.UsuarioDAO;

import br.com.cinbesa.ged.entidade.TblLotacao;
import br.com.cinbesa.ged.entidade.TblPerfil;
import br.com.cinbesa.ged.entidade.TblSetor;
import br.com.cinbesa.ged.entidade.TblUsuario;
import br.com.cinbesa.ged.intercept.Restrito;
import br.com.cinbesa.ged.intercept.SessaoUsuario;
import br.com.cinbesa.ged.log.control.LogLotacaoController;
import br.com.cinbesa.ged.log.control.LogUsuarioController;
import br.com.cinbesa.ged.util.ListaApp;
import br.com.cinbesa.ged.util.PaginacaoUtil;
import br.com.cinbesa.ged.util.Util;

@Resource
public class UsuarioController {

	private final Result result;
	private final UsuarioDAO usuariodao;
	private final PerfilDAO perfilDao;
	private final SessaoUsuario sessao;
	private Validator validator;
	private LogUsuarioController logUsuarioController;
	private SetorDAO setorDAO;
	private LotacaoDAO lotacaoDAO;
	private LogLotacaoController logLotacaoController;

	public UsuarioController(UsuarioDAO usuariodao, Result result,
			Validator validator, PerfilDAO perfDao, SessaoUsuario sessao,
			LogUsuarioController logUsuarioController, SetorDAO setorDAO,
			LotacaoDAO lotacaoDAO, LogLotacaoController logLotacaoController) {
		this.result = result;
		this.usuariodao = usuariodao;
		this.validator = validator;
		this.perfilDao = perfDao;
		this.sessao = sessao;
		this.logUsuarioController = logUsuarioController;
		this.setorDAO = setorDAO;
		this.lotacaoDAO = lotacaoDAO;
		this.logLotacaoController = logLotacaoController;
	}

	@Restrito(nome = ListaApp.LISTUSER)
	@Path({ "/usuario", "/usuario/pag-{pag}" })
	public void lista(String pag) {
		if (sessao.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			List<Integer> paginas = PaginacaoUtil.listaPaginas(usuariodao
					.count(sessao.getLotacaoUsuario().getCdSetor()
							.getIdEntidade(), ListaApp.LOTACAOATIVADA));
			int paginaSelecionada = PaginacaoUtil.numeroDaPagina(pag, paginas);
			if (paginas.size() != 0) {
				result.include("paginationList", PaginacaoUtil
						.listaPaginasView(paginas, paginaSelecionada));
				result.include("pagSelec", pag);
				result.include("first", PaginacaoUtil.FIRST);
				result.include("last", PaginacaoUtil.LAST);
				result.include("link", "/usuario/pag-");
			}
			result.include("tblUsuarioList", usuariodao.listartodos(
					PaginacaoUtil.reginicial(paginaSelecionada),
					PaginacaoUtil.TAMTABELA, sessao.getLotacaoUsuario()
							.getCdSetor().getIdEntidade(),
					ListaApp.LOTACAOATIVADA));
		}
	}

	@Restrito(nome = ListaApp.ADDUSER)
	public void novo() {
		if (sessao.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
			result.include(
					"tblPerfilList",
					perfilDao.listatodos(sessao.getLotacaoUsuario()
							.getCdSetor().getIdEntidade()));
			result.include(
					"tblSetorList",
					setorDAO.listaPorEntidade(sessao.getLotacaoUsuario()
							.getCdSetor().getIdEntidade()));
		}
	}

	@Restrito(nome = ListaApp.ADDUSER)
	@Post
	public void cadastrarusuario(TblUsuario usuario, String confSenha,
			Integer cdSetor) {

		if (usuario.getNrMatricula() == null) {
			validator.add(new ValidationMessage("Nro. matr�cula n�o informado",
					"nrMatricula"));
		}
		if (usuario.getNmUsuario() == null) {
			validator.add(new ValidationMessage(
					"nome do usu�rio n�o informado", "nmUsuario"));
		}
		if (usuario.getVlSenha() == null) {
			validator.add(new ValidationMessage("senha n�o informada",
					"vlSenha"));
		} else {
			if (!usuario.getVlSenha().equals(confSenha)) {
				validator.add(new ValidationMessage("confirme a senha",
						"vlSenha"));
			}
		}
		if (usuario.getCdPerfil().getCdPerfil() == null) {
			validator.add(new ValidationMessage("Informe o Perfil do usu�rio",
					"cdPerfil"));
		}
		usuario.setNmUsuario(usuario.getNmUsuario().toUpperCase());
		usuario.setNrMatricula(Util.limpaString(usuario.getNrMatricula()));

		if (usuariodao.buscaPorMatricula(usuario.getNrMatricula()) != null) {
			validator.add(new ValidationMessage(
					"Matr�cula j� existente no sistema", "nrMatricula"));
		}
		if (cdSetor == 0) {
			validator.add(new ValidationMessage(
					"Selecione o Setor que o usu�rio ser� lotado",
					"Setor de Lota��o"));
		}

		validator.onErrorUsePageOf(UsuarioController.class).novo();

		usuario.setVlSenha(Util.encripta(usuario.getVlSenha()));
		TblPerfil perfil = perfilDao.find(TblPerfil.class, usuario
				.getCdPerfil().getCdPerfil());
		usuario.setCdPerfil(perfil);
		usuario.setCdNivelAdministrador(ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN);
		usuario = usuariodao.salvar(usuario);
		logUsuarioController.registrar(usuario, ListaApp.LOG_OPERACAO_INSERCAO);

		TblSetor setor = setorDAO.find(TblSetor.class, cdSetor);
		TblLotacao lotacao = new TblLotacao();
		lotacao.setIdUsuario(usuario);
		lotacao.setCdSetor(setor);
		lotacao.setDtLotacao(new Date());
		lotacao.setStLotacao(ListaApp.LOTACAOATIVADA);

		lotacao = lotacaoDAO.salvar(lotacao);
		logLotacaoController.registrar(lotacao, ListaApp.LOG_OPERACAO_INSERCAO);

		result.redirectTo(UsuarioController.class).lista("0");
	}

	@Restrito(nome = ListaApp.EDITUSER)
	@Path("/usuario/editar/{id}")
	public void editar(Integer id) {
		if (sessao.getUsuario().getCdNivelAdministrador() == ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
			result.redirectTo(HomeController.class).sempermissao();
		} else {
		TblUsuario tblUsuario = usuariodao.find(TblUsuario.class, id);
		if (tblUsuario == null) {
			validator.add(new ValidationMessage("usuario N�o encontrado",
					"usuario"));
		}
		validator.onErrorUsePageOf(UsuarioController.class).lista("0");
		List<TblPerfil> tblPerfilList = perfilDao.listatodos(sessao
				.getLotacaoUsuario().getCdSetor().getIdEntidade());
		result.include("tblPerfilList", tblPerfilList);
		result.include("tblUsuario", tblUsuario);
		}
	}

	@Restrito(nome = ListaApp.EDITUSER)
	@Post
	public void salvarusuario(TblUsuario usuario) {
		if (usuario.getNrMatricula() == null) {
			validator.add(new ValidationMessage("Nro. matr�cula n�o informado",
					"nrMatricula"));
		}
		if (usuario.getNmUsuario() == null) {
			validator.add(new ValidationMessage(
					"nome do usu�rio n�o informado", "nmUsuario"));
		}

		if (usuario.getCdPerfil().getCdPerfil() == null) {
			validator.add(new ValidationMessage("Informe o Perfil do usu�rio",
					"cdPerfil"));
		}
		validator.onErrorRedirectTo(UsuarioController.class).editar(
				usuario.getIdUsuario());

		usuario.setNrMatricula(Util.limpaString(usuario.getNrMatricula()));

		TblUsuario user = usuariodao
				.buscaPorMatricula(usuario.getNrMatricula());
		if (user.getIdUsuario() != usuario.getIdUsuario()) {
			validator.onErrorRedirectTo(UsuarioController.class).editar(
					usuario.getIdUsuario());
		}
		validator.onErrorRedirectTo(UsuarioController.class).editar(
				usuario.getIdUsuario());
		usuario.setNmUsuario(usuario.getNmUsuario().toUpperCase());
		usuario.setVlSenha(user.getVlSenha());
		TblPerfil perfil = perfilDao.find(TblPerfil.class, usuario
				.getCdPerfil().getCdPerfil());
		usuario.setCdPerfil(perfil);
		usuario.setCdNivelAdministrador(ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN);

		usuario = usuariodao.salvar(usuario);
		logUsuarioController
				.registrar(usuario, ListaApp.LOG_OPERACAO_ALTERACAO);

		result.redirectTo(UsuarioController.class).lista("0");

	}

	@Restrito(nome = ListaApp.REDEFSENHAUSER)
	public TblUsuario redefinirsenha() {
		return sessao.getUsuario();
	}

	@Restrito(nome = ListaApp.REDEFSENHAUSER)
	@Post
	public void salvarnovasenha(TblUsuario usuario, String novaSenha,
			String confNovaSenha) {
		if (usuario.getVlSenha() == null) {
			validator.add(new ValidationMessage("Informe sua antiga senha",
					"vlSenha"));
		}
		if (novaSenha == null) {
			validator.add(new ValidationMessage("Informe sua nova senha",
					"novaSenha"));
		}
		if (confNovaSenha == null) {
			validator.add(new ValidationMessage("Confirme sua nova senha",
					"confNovaSenha"));
		}

		String senhaAnt = Util.encripta(usuario.getVlSenha());
		TblUsuario user = usuariodao.find(TblUsuario.class,
				usuario.getIdUsuario());

		if (!user.getVlSenha().equals(senhaAnt)) {
			validator.add(new ValidationMessage("Senha Anterior incorreta",
					"vlSenha"));
		}

		if (!novaSenha.equals(confNovaSenha)) {
			validator.add(new ValidationMessage("Nova Senha n�o confirmada",
					"confNovaSenha"));
		}

		validator.onErrorRedirectTo(this).redefinirsenha();

		user.setVlSenha(Util.encripta(novaSenha));

		user = usuariodao.salvar(user);
		logUsuarioController.registrar(user, ListaApp.LOG_OPERACAO_INSERCAO);

		result.redirectTo(this).infonovasenha();
	}

	public void infonovasenha() {
	}
}
