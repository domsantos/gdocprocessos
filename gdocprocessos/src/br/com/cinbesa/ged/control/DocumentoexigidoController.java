package br.com.cinbesa.ged.control;
//testeeeee
import java.util.List;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.cinbesa.ged.dao.DocsexigidosDAO;
import br.com.cinbesa.ged.entidade.TblDocumentosexigidos;
import br.com.cinbesa.ged.intercept.Restrito;
import br.com.cinbesa.ged.util.ListaApp;
import br.com.cinbesa.ged.util.PaginacaoUtil;

@Resource
public class DocumentoexigidoController {

	private Result result;
	private Validator validator;
	private DocsexigidosDAO docsexigidosDAO;

	public DocumentoexigidoController(Result result, Validator validator,
			DocsexigidosDAO docsexigidosDAO) {
		this.result = result;
		this.validator = validator;
		this.docsexigidosDAO = docsexigidosDAO;
	}

	@Restrito(nome = ListaApp.LISTDOCSEXIGIDOS)
	@Path({ "/docsexigidos", "/docsexigidos/pag-{pag}" })
	public List<TblDocumentosexigidos> lista(String pag) {
		List<Integer> paginas = PaginacaoUtil.listaPaginas(docsexigidosDAO
				.count());
		int paginaSelecionada = PaginacaoUtil.numeroDaPagina(pag, paginas);

		if (paginas.size() != 0) {
			result.include("paginationList", PaginacaoUtil.listaPaginasView(paginas, paginaSelecionada));
			result.include("pagSelec", pag);
			result.include("first", PaginacaoUtil.FIRST);
			result.include("last", PaginacaoUtil.LAST);
			result.include("link", "/docsexigidos/pag-");
		}
		return docsexigidosDAO.lista(
				PaginacaoUtil.reginicial(paginaSelecionada),
				PaginacaoUtil.TAMTABELA);
	}

	@Restrito(nome = ListaApp.ADDDOCSEXIGIDOS)
	@Path("/docsexigidos/novo")
	public void novo() {

	}

	@Restrito(nome = ListaApp.ADDDOCSEXIGIDOS)
	public void cadastrar(TblDocumentosexigidos tblDocumentosexigidos) {
		if (tblDocumentosexigidos.getNmDocumentoexigido() == null) {
			validator.add(new ValidationMessage("Informe o nome do Documento",
					"Nome do Documento"));
		}
		validator.onErrorUsePageOf(this).novo();
		tblDocumentosexigidos.setNmDocumentoexigido(tblDocumentosexigidos
				.getNmDocumentoexigido().toUpperCase());
		if (tblDocumentosexigidos.getDsDocumentoexigido() != null) {
			tblDocumentosexigidos.setDsDocumentoexigido(tblDocumentosexigidos
					.getDsDocumentoexigido().toUpperCase());
		}
		docsexigidosDAO.cadastrar(tblDocumentosexigidos);
		result.redirectTo(this).lista(null);
	}

	@Restrito(nome = ListaApp.EDITDOCSEXIGIDOS)
	@Path("/docsexigidos/editar/{id}")
	public TblDocumentosexigidos editar(Integer id) {
		return docsexigidosDAO.find(TblDocumentosexigidos.class, id);
	}

	@Restrito(nome = ListaApp.EDITDOCSEXIGIDOS)
	public void salvar(TblDocumentosexigidos tblDocumentosexigidos) {
		if (tblDocumentosexigidos.getNmDocumentoexigido() == null) {
			validator.add(new ValidationMessage("Informe o nome do Documento",
					"Nome do Documento"));
		}
		validator.onErrorUsePageOf(this).novo();
		tblDocumentosexigidos.setNmDocumentoexigido(tblDocumentosexigidos
				.getNmDocumentoexigido().toUpperCase());
		if (tblDocumentosexigidos.getDsDocumentoexigido() != null) {
			tblDocumentosexigidos.setDsDocumentoexigido(tblDocumentosexigidos
					.getDsDocumentoexigido().toUpperCase());
		}
		docsexigidosDAO.update(tblDocumentosexigidos);
		result.redirectTo(this).lista(null);
	}
}
