package br.com.cinbesa.ged.control;

import java.util.List;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.cinbesa.ged.dao.LocalArquivoDAO;
import br.com.cinbesa.ged.entidade.TblEnderecoarquivamento;
import br.com.cinbesa.ged.intercept.Restrito;
import br.com.cinbesa.ged.intercept.SessaoUsuario;
import br.com.cinbesa.ged.util.ListaApp;
import br.com.cinbesa.ged.util.Util;

@Resource
public class LocalarquivoController {

	private Result result;
	private Validator validator;
	private LocalArquivoDAO localArquivoDAO;
	private SessaoUsuario sessaoUsuario;

	public LocalarquivoController(Result result, Validator validator,
			LocalArquivoDAO localArquivoDAO, SessaoUsuario sessaoUsuario) {
		this.result = result;
		this.validator = validator;
		this.localArquivoDAO = localArquivoDAO;
		this.sessaoUsuario = sessaoUsuario;
	}

	@Restrito(nome=ListaApp.LOCAL_ARQUIVAMENTO_CAD)
	public void novo() {
		result.include(
				"localList",
				Util.sortLocal(localArquivoDAO.listatodos(sessaoUsuario
						.getLotacaoUsuario().getCdSetor().getIdEntidade())));
		result.include("localRaiz", ListaApp.LOCAL_ARQUIVO_NIVEL_RAIZ);
	}

	@Restrito(nome=ListaApp.LOCAL_ARQUIVAMENTO_CAD)
	@Post
	public void cadastrar(TblEnderecoarquivamento local) {
		if (local.getIdNivelsuperior() == null) {
			validator.add(new ValidationMessage("Selecione o n�vel do Local",
					""));
		}
		if (local.getDsEnderecoarquivamento() == null) {
			validator.add(new ValidationMessage("Informe o nome do Local", ""));
		}
		validator.onErrorRedirectTo(this).novo();
		local.setDsEnderecoarquivamento(local.getDsEnderecoarquivamento()
				.toUpperCase());
		local.setIdEntidade(sessaoUsuario.getLotacaoUsuario().getCdSetor()
				.getIdEntidade());
		localArquivoDAO.salvar(local);

		result.redirectTo(this).lista();
	}

	@Restrito(nome=ListaApp.LOCAL_ARQUIVAMENTO_LISTA)
	@Path("/localarquivo")
	public void lista() {
		result.include(
				"localList",
				Util.sortLocal(localArquivoDAO.listatodos(sessaoUsuario
						.getLotacaoUsuario().getCdSetor().getIdEntidade())));
	}

	@Restrito(nome=ListaApp.LOCAL_ARQUIVAMENTO_EDITAR)
	@Path("/localarquivo/editar/{id}")
	public void editar(Integer id) {
		result.include(
				"localList",
				Util.sortLocal(localArquivoDAO.listatodos(sessaoUsuario
						.getLotacaoUsuario().getCdSetor().getIdEntidade())));
		result.include("localRaiz", ListaApp.LOCAL_ARQUIVO_NIVEL_RAIZ);
		result.include("reg",
				localArquivoDAO.find(TblEnderecoarquivamento.class, id));
	}

	@Restrito(nome=ListaApp.LOCAL_ARQUIVAMENTO_EDITAR)
	public void salvaredicao(TblEnderecoarquivamento local) {
		if (local.getIdNivelsuperior() == null) {
			validator.add(new ValidationMessage("Selecione o n�vel do Local",
					""));
		}
		if (local.getDsEnderecoarquivamento() == null) {
			validator.add(new ValidationMessage("Informe o nome do Local", ""));
		}
		TblEnderecoarquivamento localbanco = localArquivoDAO.find(
				TblEnderecoarquivamento.class,
				local.getIdEnderecoarquivamento());
		if (local.getIdNivelsuperior() != localbanco.getIdNivelsuperior()) {
			if (localbanco.getTblProcessoArquivadoList().size() != 0) {
				validator
						.add(new ValidationMessage(
								"Existe Processos Atrelados a este n�vel do local",
								""));
			} else {
				if (localArquivoDAO.listaLocaisFilhos(localbanco).size() != 0) {
					validator
							.add(new ValidationMessage(
									"Existem locais registrados abaixo deste n�vel",
									""));
				}
			}
		}
		validator.onErrorRedirectTo(this).editar(
				local.getIdEnderecoarquivamento());
		localbanco.setIdNivelsuperior(local.getIdNivelsuperior());
		localbanco.setDsEnderecoarquivamento(local.getDsEnderecoarquivamento()
				.toUpperCase());
		localArquivoDAO.update(localbanco);
		result.redirectTo(this).lista();
	}

	@Restrito(nome=ListaApp.LOCAL_ARQUIVAMENTO_EXCLUIR)
	@Path("/localarquivo/excluir/{id}")
	public void excluir(Integer id) {
		TblEnderecoarquivamento localbanco = localArquivoDAO.find(TblEnderecoarquivamento.class, id);
		if (localbanco.getTblProcessoArquivadoList().size() != 0) {
			validator.add(new ValidationMessage( "Existe Processos Atrelados a este n�vel do local", ""));
		} else {
			if (localArquivoDAO.listaLocaisFilhos(localbanco).size() != 0) {
				validator.add(new ValidationMessage( "Existem locais registrados abaixo deste n�vel", ""));
			}
		}
		validator.onErrorRedirectTo(this).lista();
		localArquivoDAO.excluir(localbanco);
		result.redirectTo(this).lista();
	}

}
