package br.com.cinbesa.ged.converter;

import java.util.ResourceBundle;

import br.com.caelum.vraptor.Converter;
import br.com.cinbesa.ged.dao.PerfilDAO;
import br.com.cinbesa.ged.entidade.TblPerfil;

//@Convert(TblPerfil.class)
public class PerfilConverter implements Converter<TblPerfil>{

	private final PerfilDAO perfildao;
	public PerfilConverter(PerfilDAO perfil) {
		perfildao = perfil;
	}
	
	public TblPerfil convert(String value, Class<? extends TblPerfil> arg1, ResourceBundle arg2) {
		if(value == null || value.equals("")){
			return null;
		}else{
			try{
				TblPerfil perfil = perfildao.find(TblPerfil.class, Integer.parseInt(value));
				return perfil;
			}catch(Exception e){
				e.printStackTrace();
				return null;
			}
		}
		
	}

	
}
