/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_enderecoarquivamento")
public class TblEnderecoarquivamento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_enderecoarquivamento")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer idEnderecoarquivamento;
    @Column(name = "ds_enderecoarquivamento")
    private String dsEnderecoarquivamento;
    @Column(name = "id_nivelsuperior")
    private Integer idNivelsuperior;
    @JoinColumn(name = "id_entidade", referencedColumnName = "id_entidade")
    @ManyToOne(optional = false)
    private Entidade idEntidade;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tblEnderecoarquivamento")
    private List<TblProcessoArquivado> tblProcessoArquivadoList;

    public TblEnderecoarquivamento() {
    }

    public TblEnderecoarquivamento(Integer idEnderecoarquivamento) {
        this.idEnderecoarquivamento = idEnderecoarquivamento;
    }

    public Integer getIdEnderecoarquivamento() {
        return idEnderecoarquivamento;
    }

    public void setIdEnderecoarquivamento(Integer idEnderecoarquivamento) {
        this.idEnderecoarquivamento = idEnderecoarquivamento;
    }

    public String getDsEnderecoarquivamento() {
        return dsEnderecoarquivamento;
    }

    public void setDsEnderecoarquivamento(String dsEnderecoarquivamento) {
        this.dsEnderecoarquivamento = dsEnderecoarquivamento;
    }

    public Integer getIdNivelsuperior() {
        return idNivelsuperior;
    }

    public void setIdNivelsuperior(Integer idNivelsuperior) {
        this.idNivelsuperior = idNivelsuperior;
    }

    public Entidade getIdEntidade() {
        return idEntidade;
    }

    public void setIdEntidade(Entidade idEntidade) {
        this.idEntidade = idEntidade;
    }

    public List<TblProcessoArquivado> getTblProcessoArquivadoList() {
        return tblProcessoArquivadoList;
    }

    public void setTblProcessoArquivadoList(List<TblProcessoArquivado> tblProcessoArquivadoList) {
        this.tblProcessoArquivadoList = tblProcessoArquivadoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnderecoarquivamento != null ? idEnderecoarquivamento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblEnderecoarquivamento)) {
            return false;
        }
        TblEnderecoarquivamento other = (TblEnderecoarquivamento) object;
        if ((this.idEnderecoarquivamento == null && other.idEnderecoarquivamento != null) || (this.idEnderecoarquivamento != null && !this.idEnderecoarquivamento.equals(other.idEnderecoarquivamento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.cinbesa.ged.entidade.TblEnderecoarquivamento[ idEnderecoarquivamento=" + idEnderecoarquivamento + " ]";
    }
    
}
