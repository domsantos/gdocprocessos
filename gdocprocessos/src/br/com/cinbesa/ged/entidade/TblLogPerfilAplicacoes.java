/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_log_perfil_aplicacoes")
@NamedQueries({
    @NamedQuery(name = "TblLogPerfilAplicacoes.findAll", query = "SELECT t FROM TblLogPerfilAplicacoes t"),
    @NamedQuery(name = "TblLogPerfilAplicacoes.findByIdLogPerfilAplicacoes", query = "SELECT t FROM TblLogPerfilAplicacoes t WHERE t.idLogPerfilAplicacoes = :idLogPerfilAplicacoes"),
    @NamedQuery(name = "TblLogPerfilAplicacoes.findByCdAplicacao", query = "SELECT t FROM TblLogPerfilAplicacoes t WHERE t.cdAplicacao = :cdAplicacao"),
    @NamedQuery(name = "TblLogPerfilAplicacoes.findByCdPerfil", query = "SELECT t FROM TblLogPerfilAplicacoes t WHERE t.cdPerfil = :cdPerfil"),
    @NamedQuery(name = "TblLogPerfilAplicacoes.findByCdOperacao", query = "SELECT t FROM TblLogPerfilAplicacoes t WHERE t.cdOperacao = :cdOperacao"),
    @NamedQuery(name = "TblLogPerfilAplicacoes.findByDtOperacao", query = "SELECT t FROM TblLogPerfilAplicacoes t WHERE t.dtOperacao = :dtOperacao"),
    @NamedQuery(name = "TblLogPerfilAplicacoes.findByIdUsuarioOperacao", query = "SELECT t FROM TblLogPerfilAplicacoes t WHERE t.idUsuarioOperacao = :idUsuarioOperacao")})
public class TblLogPerfilAplicacoes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_log_perfil_aplicacoes")
    private Integer idLogPerfilAplicacoes;
    @Column(name = "cd_aplicacao")
    private Integer cdAplicacao;
    @Column(name = "cd_perfil")
    private Integer cdPerfil;
    @Basic(optional = false)
    @Column(name = "cd_operacao")
    private char cdOperacao;
    @Basic(optional = false)
    @Column(name = "dt_operacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtOperacao;
    @Column(name = "id_usuario_operacao")
    private Integer idUsuarioOperacao;

    public TblLogPerfilAplicacoes() {
    }

    public TblLogPerfilAplicacoes(Integer idLogPerfilAplicacoes) {
        this.idLogPerfilAplicacoes = idLogPerfilAplicacoes;
    }

    public TblLogPerfilAplicacoes(Integer idLogPerfilAplicacoes, char cdOperacao, Date dtOperacao) {
        this.idLogPerfilAplicacoes = idLogPerfilAplicacoes;
        this.cdOperacao = cdOperacao;
        this.dtOperacao = dtOperacao;
    }

    public Integer getIdLogPerfilAplicacoes() {
        return idLogPerfilAplicacoes;
    }

    public void setIdLogPerfilAplicacoes(Integer idLogPerfilAplicacoes) {
        this.idLogPerfilAplicacoes = idLogPerfilAplicacoes;
    }

    public Integer getCdAplicacao() {
        return cdAplicacao;
    }

    public void setCdAplicacao(Integer cdAplicacao) {
        this.cdAplicacao = cdAplicacao;
    }

    public Integer getCdPerfil() {
        return cdPerfil;
    }

    public void setCdPerfil(Integer cdPerfil) {
        this.cdPerfil = cdPerfil;
    }

    public char getCdOperacao() {
        return cdOperacao;
    }

    public void setCdOperacao(char cdOperacao) {
        this.cdOperacao = cdOperacao;
    }

    public Date getDtOperacao() {
        return dtOperacao;
    }

    public void setDtOperacao(Date dtOperacao) {
        this.dtOperacao = dtOperacao;
    }

    public Integer getIdUsuarioOperacao() {
        return idUsuarioOperacao;
    }

    public void setIdUsuarioOperacao(Integer idUsuarioOperacao) {
        this.idUsuarioOperacao = idUsuarioOperacao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLogPerfilAplicacoes != null ? idLogPerfilAplicacoes.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblLogPerfilAplicacoes)) {
            return false;
        }
        TblLogPerfilAplicacoes other = (TblLogPerfilAplicacoes) object;
        if ((this.idLogPerfilAplicacoes == null && other.idLogPerfilAplicacoes != null) || (this.idLogPerfilAplicacoes != null && !this.idLogPerfilAplicacoes.equals(other.idLogPerfilAplicacoes))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.cinbesa.ged.entidade.TblLogPerfilAplicacoes[ idLogPerfilAplicacoes=" + idLogPerfilAplicacoes + " ]";
    }
    
}
