/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_receitas")
@NamedQueries({
    @NamedQuery(name = "TblReceitas.findAll", query = "SELECT t FROM TblReceitas t"),
    @NamedQuery(name = "TblReceitas.findByIdReceita", query = "SELECT t FROM TblReceitas t WHERE t.idReceita = :idReceita"),
    @NamedQuery(name = "TblReceitas.findByNrGuia", query = "SELECT t FROM TblReceitas t WHERE t.nrGuia = :nrGuia"),
    @NamedQuery(name = "TblReceitas.findByVlPago", query = "SELECT t FROM TblReceitas t WHERE t.vlPago = :vlPago"),
    @NamedQuery(name = "TblReceitas.findByDtPagamento", query = "SELECT t FROM TblReceitas t WHERE t.dtPagamento = :dtPagamento")})
public class TblReceitas implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_receita")
    private Integer idReceita;
    @Basic(optional = false)
    @Column(name = "nr_guia")
    private String nrGuia;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "vl_pago")
    private BigDecimal vlPago;
    @Basic(optional = false)
    @Column(name = "dt_pagamento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtPagamento;

    public TblReceitas() {
    }

    public TblReceitas(Integer idReceita) {
        this.idReceita = idReceita;
    }

    public TblReceitas(Integer idReceita, String nrGuia, Date dtPagamento) {
        this.idReceita = idReceita;
        this.nrGuia = nrGuia;
        this.dtPagamento = dtPagamento;
    }

    public Integer getIdReceita() {
        return idReceita;
    }

    public void setIdReceita(Integer idReceita) {
        this.idReceita = idReceita;
    }

    public String getNrGuia() {
        return nrGuia;
    }

    public void setNrGuia(String nrGuia) {
        this.nrGuia = nrGuia;
    }

    public BigDecimal getVlPago() {
        return vlPago;
    }

    public void setVlPago(BigDecimal vlPago) {
        this.vlPago = vlPago;
    }

    public Date getDtPagamento() {
        return dtPagamento;
    }

    public void setDtPagamento(Date dtPagamento) {
        this.dtPagamento = dtPagamento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idReceita != null ? idReceita.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblReceitas)) {
            return false;
        }
        TblReceitas other = (TblReceitas) object;
        if ((this.idReceita == null && other.idReceita != null) || (this.idReceita != null && !this.idReceita.equals(other.idReceita))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.entidades.TblReceitas[ idReceita=" + idReceita + " ]";
    }
    
}
