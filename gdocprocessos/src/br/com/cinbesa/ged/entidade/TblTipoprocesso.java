/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "tbl_tipoprocesso")
public class TblTipoprocesso implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cd_tipoprocesso")
    private Integer cdTipoprocesso;
    @Basic(optional = false)
    @Column(name = "nm_tipoprocesso")
    private String nmTipoprocesso;
    @Column(name = "ds_tipoprocesso")
    private String dsTipoprocesso;
    @Basic(optional = false)
    @Column(name = "st_confirmacao_presencia")
    private char stConfirmacaoPresencia;
    @Basic(optional = false)
    @Column(name = "st_abrir_internet")
    private char stAbrirInternet;
    @Column(name = "id_classe")
    private Integer idClasse;
    @Column(name = "id_subclasse")
    private Integer idSubclasse;
    @Column(name = "id_grupo")
    private Integer idGrupo;
    @Column(name = "id_subgrupo")
    private Integer idSubgrupo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cdTipoprocesso")
    private List<TblRegras> tblRegrasList;
    @JoinColumn(name = "id_entidade", referencedColumnName = "id_entidade")
    @ManyToOne
    private Entidade idEntidade;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cdTipoprocesso")
    private List<TblProcessos> tblProcessosList;

    public TblTipoprocesso() {
    }

    public TblTipoprocesso(Integer cdTipoprocesso) {
        this.cdTipoprocesso = cdTipoprocesso;
    }

    public TblTipoprocesso(Integer cdTipoprocesso, String nmTipoprocesso, char stConfirmacaoPresencia, char stAbrirInternet) {
        this.cdTipoprocesso = cdTipoprocesso;
        this.nmTipoprocesso = nmTipoprocesso;
        this.stConfirmacaoPresencia = stConfirmacaoPresencia;
        this.stAbrirInternet = stAbrirInternet;
    }

    public Integer getCdTipoprocesso() {
        return cdTipoprocesso;
    }

    public void setCdTipoprocesso(Integer cdTipoprocesso) {
        this.cdTipoprocesso = cdTipoprocesso;
    }

    public String getNmTipoprocesso() {
        return nmTipoprocesso;
    }

    public void setNmTipoprocesso(String nmTipoprocesso) {
        this.nmTipoprocesso = nmTipoprocesso;
    }

    public String getDsTipoprocesso() {
        return dsTipoprocesso;
    }

    public void setDsTipoprocesso(String dsTipoprocesso) {
        this.dsTipoprocesso = dsTipoprocesso;
    }

    public char getStConfirmacaoPresencia() {
        return stConfirmacaoPresencia;
    }

    public void setStConfirmacaoPresencia(char stConfirmacaoPresencia) {
        this.stConfirmacaoPresencia = stConfirmacaoPresencia;
    }

    public char getStAbrirInternet() {
        return stAbrirInternet;
    }

    public void setStAbrirInternet(char stAbrirInternet) {
        this.stAbrirInternet = stAbrirInternet;
    }

    public Entidade getIdEntidade() {
        return idEntidade;
    }

    public void setIdEntidade(Entidade idEntidade) {
        this.idEntidade = idEntidade;
    }
    
    public Integer getIdClasse() {
        return idClasse;
    }

    public void setIdClasse(Integer idClasse) {
        this.idClasse = idClasse;
    }

    public Integer getIdSubclasse() {
        return idSubclasse;
    }

    public void setIdSubclasse(Integer idSubclasse) {
        this.idSubclasse = idSubclasse;
    }

    public Integer getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(Integer idGrupo) {
        this.idGrupo = idGrupo;
    }

    public Integer getIdSubgrupo() {
        return idSubgrupo;
    }

    public void setIdSubgrupo(Integer idSubgrupo) {
        this.idSubgrupo = idSubgrupo;
    }
    
    public List<TblRegras> getTblRegrasList() {
        return tblRegrasList;
    }

    public void setTblRegrasList(List<TblRegras> tblRegrasList) {
        this.tblRegrasList = tblRegrasList;
    }

    public List<TblProcessos> getTblProcessosList() {
        return tblProcessosList;
    }

    public void setTblProcessosList(List<TblProcessos> tblProcessosList) {
        this.tblProcessosList = tblProcessosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cdTipoprocesso != null ? cdTipoprocesso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblTipoprocesso)) {
            return false;
        }
        TblTipoprocesso other = (TblTipoprocesso) object;
        if ((this.cdTipoprocesso == null && other.cdTipoprocesso != null) || (this.cdTipoprocesso != null && !this.cdTipoprocesso.equals(other.cdTipoprocesso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.entidades.TblTipoprocesso[ cdTipoprocesso=" + cdTipoprocesso + " ]";
    }
    
}
