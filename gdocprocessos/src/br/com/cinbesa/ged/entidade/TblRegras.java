/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_regras")
@NamedQueries({
    @NamedQuery(name = "TblRegras.findAll", query = "SELECT t FROM TblRegras t"),
    @NamedQuery(name = "TblRegras.findByCdRegra", query = "SELECT t FROM TblRegras t WHERE t.cdRegra = :cdRegra"),
    @NamedQuery(name = "TblRegras.findByNmRegra", query = "SELECT t FROM TblRegras t WHERE t.nmRegra = :nmRegra")})
public class TblRegras implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cd_regra")
    private Integer cdRegra;
    @Basic(optional = false)
    @Column(name = "nm_regra")
    private String nmRegra;
    @Basic(optional = false)
    @Lob
    @Column(name = "ds_regra")
    private String dsRegra;
    @JoinColumn(name = "cd_tipoprocesso", referencedColumnName = "cd_tipoprocesso")
    @ManyToOne(optional = false)
    private TblTipoprocesso cdTipoprocesso;

    public TblRegras() {
    }

    public TblRegras(Integer cdRegra) {
        this.cdRegra = cdRegra;
    }

    public TblRegras(Integer cdRegra, String nmRegra, String dsRegra) {
        this.cdRegra = cdRegra;
        this.nmRegra = nmRegra;
        this.dsRegra = dsRegra;
    }

    public Integer getCdRegra() {
        return cdRegra;
    }

    public void setCdRegra(Integer cdRegra) {
        this.cdRegra = cdRegra;
    }

    public String getNmRegra() {
        return nmRegra;
    }

    public void setNmRegra(String nmRegra) {
        this.nmRegra = nmRegra;
    }

    public String getDsRegra() {
        return dsRegra;
    }

    public void setDsRegra(String dsRegra) {
        this.dsRegra = dsRegra;
    }

    public TblTipoprocesso getCdTipoprocesso() {
        return cdTipoprocesso;
    }

    public void setCdTipoprocesso(TblTipoprocesso cdTipoprocesso) {
        this.cdTipoprocesso = cdTipoprocesso;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cdRegra != null ? cdRegra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblRegras)) {
            return false;
        }
        TblRegras other = (TblRegras) object;
        if ((this.cdRegra == null && other.cdRegra != null) || (this.cdRegra != null && !this.cdRegra.equals(other.cdRegra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.entidades.TblRegras[ cdRegra=" + cdRegra + " ]";
    }
    
}
