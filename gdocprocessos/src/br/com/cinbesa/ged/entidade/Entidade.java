/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "entidade")
@NamedQueries({
    @NamedQuery(name = "Entidade.findAll", query = "SELECT e FROM Entidade e"),
    @NamedQuery(name = "Entidade.findByIdEntidade", query = "SELECT e FROM Entidade e WHERE e.idEntidade = :idEntidade"),
    @NamedQuery(name = "Entidade.findByDsEntidade", query = "SELECT e FROM Entidade e WHERE e.dsEntidade = :dsEntidade")})
public class Entidade implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_entidade")
    private Integer idEntidade;
    @Basic(optional = false)
    @Column(name = "ds_entidade")
    private String dsEntidade;
    @Column(name = "cd_entidade")
    private String cdEntidade;
    @Column(name = "ds_entidade_extenso")
    private String dsEntidadeExtenso;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEntidade")
    private List<TblSetor> tblSetorList;
    @OneToMany(mappedBy = "idEntidade")
    private List<TblTipoprocesso> tblTipoprocessoList;
    @OneToMany(mappedBy = "idEntidade")
    private List<TblProcessos> tblProcessosList;

    public Entidade() {
    }

    public Entidade(Integer idEntidade) {
        this.idEntidade = idEntidade;
    }

    public Entidade(Integer idEntidade, String dsEntidade) {
        this.idEntidade = idEntidade;
        this.dsEntidade = dsEntidade;
    }

    public Integer getIdEntidade() {
        return idEntidade;
    }

    public void setIdEntidade(Integer idEntidade) {
        this.idEntidade = idEntidade;
    }

    public String getDsEntidade() {
        return dsEntidade;
    }

    public void setDsEntidade(String dsEntidade) {
        this.dsEntidade = dsEntidade;
    }

    public String getCdEntidade() {
		return cdEntidade;
	}
    
    public void setCdEntidade(String cdEntidade) {
		this.cdEntidade = cdEntidade;
	}
    
    public String getDsEntidadeExtenso() {
		return dsEntidadeExtenso;
	}
    
    public void setDsEntidadeExtenso(String dsEntidadeExtenso) {
		this.dsEntidadeExtenso = dsEntidadeExtenso;
	}
    
    public List<TblSetor> getTblSetorList() {
        return tblSetorList;
    }

    public void setTblSetorList(List<TblSetor> tblSetorList) {
        this.tblSetorList = tblSetorList;
    }

    public List<TblProcessos> getTblProcessosList() {
        return tblProcessosList;
    }

    public void setTblProcessosList(List<TblProcessos> tblProcessosList) {
        this.tblProcessosList = tblProcessosList;
    }

    public List<TblTipoprocesso> getTblTipoprocessoList() {
        return tblTipoprocessoList;
    }

    public void setTblTipoprocessoList(List<TblTipoprocesso> tblTipoprocessoList) {
        this.tblTipoprocessoList = tblTipoprocessoList;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEntidade != null ? idEntidade.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof Entidade)) {
            return false;
        }
        Entidade other = (Entidade) object;
        if ((this.idEntidade == null && other.idEntidade != null) || (this.idEntidade != null && !this.idEntidade.equals(other.idEntidade))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.entidades.Entidade[ idEntidade=" + idEntidade + " ]";
    }
    
}
