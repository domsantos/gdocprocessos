/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_usoatividade")
@NamedQueries({
    @NamedQuery(name = "TblUsoatividade.findAll", query = "SELECT t FROM TblUsoatividade t"),
    @NamedQuery(name = "TblUsoatividade.findByCdUsoatividade", query = "SELECT t FROM TblUsoatividade t WHERE t.cdUsoatividade = :cdUsoatividade"),
    @NamedQuery(name = "TblUsoatividade.findByCdDesmembramento", query = "SELECT t FROM TblUsoatividade t WHERE t.cdDesmembramento = :cdDesmembramento"),
    @NamedQuery(name = "TblUsoatividade.findByNrDocumentoContabil", query = "SELECT t FROM TblUsoatividade t WHERE t.nrDocumentoContabil = :nrDocumentoContabil"),
    @NamedQuery(name = "TblUsoatividade.findByDsUsoatividade", query = "SELECT t FROM TblUsoatividade t WHERE t.dsUsoatividade = :dsUsoatividade"),
    @NamedQuery(name = "TblUsoatividade.findByNrQtdufir", query = "SELECT t FROM TblUsoatividade t WHERE t.nrQtdufir = :nrQtdufir"),
    @NamedQuery(name = "TblUsoatividade.findByNrAnoExercicio", query = "SELECT t FROM TblUsoatividade t WHERE t.nrAnoExercicio = :nrAnoExercicio"),
    @NamedQuery(name = "TblUsoatividade.findByNrSequencia", query = "SELECT t FROM TblUsoatividade t WHERE t.nrSequencia = :nrSequencia"),
    @NamedQuery(name = "TblUsoatividade.findByStTributoExclusivo", query = "SELECT t FROM TblUsoatividade t WHERE t.stTributoExclusivo = :stTributoExclusivo")})
public class TblUsoatividade implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cd_usoatividade")
    private Integer cdUsoatividade;
    @Basic(optional = false)
    @Column(name = "cd_desmembramento")
    private String cdDesmembramento;
    @Basic(optional = false)
    @Column(name = "nr_documento_contabil")
    private String nrDocumentoContabil;
    @Basic(optional = false)
    @Column(name = "ds_usoatividade")
    private String dsUsoatividade;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "nr_qtdufir")
    private BigDecimal nrQtdufir;
    @Column(name = "nr_ano_exercicio")
    private Short nrAnoExercicio;
    @Column(name = "nr_sequencia")
    private Integer nrSequencia;
    @Column(name = "st_tributo_exclusivo")
    private Character stTributoExclusivo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cdUsoatividade")
    private List<TblDam> tblDamList;
    @JoinColumn(name = "id_ufir", referencedColumnName = "id_ufir")
    @ManyToOne(optional = false)
    private TblUfir idUfir;
    @JoinColumn(name = "id_tributo", referencedColumnName = "id_tributo")
    @ManyToOne(optional = false)
    private TblTributo idTributo;
    @JoinColumn(name = "id_entidade", referencedColumnName = "id_entidade")
    @ManyToOne(optional = false)
    private Entidade idEntidade;

    public TblUsoatividade() {
    }

    public TblUsoatividade(Integer cdUsoatividade) {
        this.cdUsoatividade = cdUsoatividade;
    }

    public TblUsoatividade(Integer cdUsoatividade, String cdDesmembramento, String nrDocumentoContabil, String dsUsoatividade, BigDecimal nrQtdufir) {
        this.cdUsoatividade = cdUsoatividade;
        this.cdDesmembramento = cdDesmembramento;
        this.nrDocumentoContabil = nrDocumentoContabil;
        this.dsUsoatividade = dsUsoatividade;
        this.nrQtdufir = nrQtdufir;
    }

    public Integer getCdUsoatividade() {
        return cdUsoatividade;
    }

    public void setCdUsoatividade(Integer cdUsoatividade) {
        this.cdUsoatividade = cdUsoatividade;
    }

    public String getCdDesmembramento() {
        return cdDesmembramento;
    }

    public void setCdDesmembramento(String cdDesmembramento) {
        this.cdDesmembramento = cdDesmembramento;
    }

    public String getNrDocumentoContabil() {
        return nrDocumentoContabil;
    }

    public void setNrDocumentoContabil(String nrDocumentoContabil) {
        this.nrDocumentoContabil = nrDocumentoContabil;
    }

    public String getDsUsoatividade() {
        return dsUsoatividade;
    }

    public void setDsUsoatividade(String dsUsoatividade) {
        this.dsUsoatividade = dsUsoatividade;
    }

    public BigDecimal getNrQtdufir() {
        return nrQtdufir;
    }

    public void setNrQtdufir(BigDecimal nrQtdufir) {
        this.nrQtdufir = nrQtdufir;
    }

    public Short getNrAnoExercicio() {
        return nrAnoExercicio;
    }

    public void setNrAnoExercicio(Short nrAnoExercicio) {
        this.nrAnoExercicio = nrAnoExercicio;
    }

    public Integer getNrSequencia() {
        return nrSequencia;
    }

    public void setNrSequencia(Integer nrSequencia) {
        this.nrSequencia = nrSequencia;
    }

    public Character getStTributoExclusivo() {
        return stTributoExclusivo;
    }

    public void setStTributoExclusivo(Character stTributoExclusivo) {
        this.stTributoExclusivo = stTributoExclusivo;
    }

    public List<TblDam> getTblDamList() {
        return tblDamList;
    }

    public void setTblDamList(List<TblDam> tblDamList) {
        this.tblDamList = tblDamList;
    }

    public TblUfir getIdUfir() {
        return idUfir;
    }

    public void setIdUfir(TblUfir idUfir) {
        this.idUfir = idUfir;
    }

    public TblTributo getIdTributo() {
        return idTributo;
    }

    public void setIdTributo(TblTributo idTributo) {
        this.idTributo = idTributo;
    }

    public Entidade getIdEntidade() {
        return idEntidade;
    }

    public void setIdEntidade(Entidade idEntidade) {
        this.idEntidade = idEntidade;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cdUsoatividade != null ? cdUsoatividade.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblUsoatividade)) {
            return false;
        }
        TblUsoatividade other = (TblUsoatividade) object;
        if ((this.cdUsoatividade == null && other.cdUsoatividade != null) || (this.cdUsoatividade != null && !this.cdUsoatividade.equals(other.cdUsoatividade))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ged.cinbesa.entidade.TblUsoatividade[ cdUsoatividade=" + cdUsoatividade + " ]";
    }
    
}
