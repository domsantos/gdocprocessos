/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_valor_documento")
@NamedQueries({
    @NamedQuery(name = "TblValorDocumento.findAll", query = "SELECT t FROM TblValorDocumento t"),
    @NamedQuery(name = "TblValorDocumento.findByCdValorDocumento", query = "SELECT t FROM TblValorDocumento t WHERE t.cdValorDocumento = :cdValorDocumento"),
    @NamedQuery(name = "TblValorDocumento.findByVlDocumento", query = "SELECT t FROM TblValorDocumento t WHERE t.vlDocumento = :vlDocumento"),
    @NamedQuery(name = "TblValorDocumento.findByDtLancamento", query = "SELECT t FROM TblValorDocumento t WHERE t.dtLancamento = :dtLancamento"),
    @NamedQuery(name = "TblValorDocumento.findByNrParcelas", query = "SELECT t FROM TblValorDocumento t WHERE t.nrParcelas = :nrParcelas")})
public class TblValorDocumento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "cd_valor_documento")
    private Integer cdValorDocumento;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "vl_documento")
    private BigDecimal vlDocumento;
    @Column(name = "dt_lancamento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtLancamento;
    @Column(name = "nr_parcelas")
    private Integer nrParcelas;
    @Column(name = "ds_pagamento")
    private String dsPagamento;
    @Column(name = "nr_guia")
    private String nrGuia;
    @JoinColumn(name = "id_ufir", referencedColumnName = "id_ufir")
    @ManyToOne(optional = false)
    private TblUfir idUfir;
    @JoinColumn(name = "id_processo", referencedColumnName = "id_processo")
    @ManyToOne(optional = false)
    private TblProcessos idProcesso;

    public TblValorDocumento() {
    }

    public TblValorDocumento(Integer cdValorDocumento) {
        this.cdValorDocumento = cdValorDocumento;
    }

    public Integer getCdValorDocumento() {
        return cdValorDocumento;
    }

    public void setCdValorDocumento(Integer cdValorDocumento) {
        this.cdValorDocumento = cdValorDocumento;
    }

    public BigDecimal getVlDocumento() {
        return vlDocumento;
    }

    public void setVlDocumento(BigDecimal vlDocumento) {
        this.vlDocumento = vlDocumento;
    }

    public Date getDtLancamento() {
        return dtLancamento;
    }

    public void setDtLancamento(Date dtLancamento) {
        this.dtLancamento = dtLancamento;
    }

    public Integer getNrParcelas() {
        return nrParcelas;
    }

    public void setNrParcelas(Integer nrParcelas) {
        this.nrParcelas = nrParcelas;
    }

    public TblUfir getIdUfir() {
        return idUfir;
    }

    public void setIdUfir(TblUfir idUfir) {
        this.idUfir = idUfir;
    }

    public TblProcessos getIdProcesso() {
        return idProcesso;
    }

    public void setIdProcesso(TblProcessos idProcesso) {
        this.idProcesso = idProcesso;
    }

    public String getDsPagamento() {
		return dsPagamento;
	}
    
    public void setDsPagamento(String dsPagamento) {
		this.dsPagamento = dsPagamento;
	}
    
    public String getNrGuia() {
		return nrGuia;
	}
    
    public void setNrGuia(String nrGuia) {
		this.nrGuia = nrGuia;
	}
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cdValorDocumento != null ? cdValorDocumento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblValorDocumento)) {
            return false;
        }
        TblValorDocumento other = (TblValorDocumento) object;
        if ((this.cdValorDocumento == null && other.cdValorDocumento != null) || (this.cdValorDocumento != null && !this.cdValorDocumento.equals(other.cdValorDocumento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ged.cinbesa.entidade.TblValorDocumento[ cdValorDocumento=" + cdValorDocumento + " ]";
    }
    
}
