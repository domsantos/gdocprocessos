/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_setor")
@NamedQueries({
    @NamedQuery(name = "TblSetor.findAll", query = "SELECT t FROM TblSetor t"),
    @NamedQuery(name = "TblSetor.findByCdSetor", query = "SELECT t FROM TblSetor t WHERE t.cdSetor = :cdSetor"),
    @NamedQuery(name = "TblSetor.findByNmSetor", query = "SELECT t FROM TblSetor t WHERE t.nmSetor = :nmSetor"),
    @NamedQuery(name = "TblSetor.findByDsSetor", query = "SELECT t FROM TblSetor t WHERE t.dsSetor = :dsSetor")})
public class TblSetor implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cd_setor")
    private Integer cdSetor;
    @Basic(optional = false)
    @Column(name = "nm_setor")
    private String nmSetor;
    @Column(name = "ds_setor")
    private String dsSetor;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cdSetor")
    private List<TblRotas> tblRotasList;
    @JoinColumn(name = "id_entidade", referencedColumnName = "id_entidade")
    @ManyToOne(optional = false)
    private Entidade idEntidade;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cdSetor")
    private List<TblTramites> tblTramitesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cdSetor")
    private List<TblLotacao> tblLotacaoList;

    public TblSetor() {
    }

    public TblSetor(Integer cdSetor) {
        this.cdSetor = cdSetor;
    }

    public TblSetor(Integer cdSetor, String nmSetor) {
        this.cdSetor = cdSetor;
        this.nmSetor = nmSetor;
    }

    public Integer getCdSetor() {
        return cdSetor;
    }

    public void setCdSetor(Integer cdSetor) {
        this.cdSetor = cdSetor;
    }

    public String getNmSetor() {
        return nmSetor;
    }

    public void setNmSetor(String nmSetor) {
        this.nmSetor = nmSetor;
    }

    public String getDsSetor() {
        return dsSetor;
    }

    public void setDsSetor(String dsSetor) {
        this.dsSetor = dsSetor;
    }

    public List<TblRotas> getTblRotasList() {
        return tblRotasList;
    }

    public void setTblRotasList(List<TblRotas> tblRotasList) {
        this.tblRotasList = tblRotasList;
    }

    public Entidade getIdEntidade() {
        return idEntidade;
    }

    public void setIdEntidade(Entidade idEntidade) {
        this.idEntidade = idEntidade;
    }

    public List<TblTramites> getTblTramitesList() {
        return tblTramitesList;
    }

    public void setTblTramitesList(List<TblTramites> tblTramitesList) {
        this.tblTramitesList = tblTramitesList;
    }

    public List<TblLotacao> getTblLotacaoList() {
        return tblLotacaoList;
    }

    public void setTblLotacaoList(List<TblLotacao> tblLotacaoList) {
        this.tblLotacaoList = tblLotacaoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cdSetor != null ? cdSetor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblSetor)) {
            return false;
        }
        TblSetor other = (TblSetor) object;
        if ((this.cdSetor == null && other.cdSetor != null) || (this.cdSetor != null && !this.cdSetor.equals(other.cdSetor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ged.cinbesa.entidade.TblSetor[ cdSetor=" + cdSetor + " ]";
    }
    
}
