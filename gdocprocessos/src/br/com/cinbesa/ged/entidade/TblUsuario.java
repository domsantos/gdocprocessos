/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_usuario")
@NamedQueries({
    @NamedQuery(name = "TblUsuario.findAll", query = "SELECT t FROM TblUsuario t"),
    @NamedQuery(name = "TblUsuario.findByIdUsuario", query = "SELECT t FROM TblUsuario t WHERE t.idUsuario = :idUsuario"),
    @NamedQuery(name = "TblUsuario.findByNrMatricula", query = "SELECT t FROM TblUsuario t WHERE t.nrMatricula = :nrMatricula"),
    @NamedQuery(name = "TblUsuario.findByNmUsuario", query = "SELECT t FROM TblUsuario t WHERE t.nmUsuario = :nmUsuario"),
    @NamedQuery(name = "TblUsuario.findByVlSenha", query = "SELECT t FROM TblUsuario t WHERE t.vlSenha = :vlSenha")})
public class TblUsuario implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_usuario")
    private Integer idUsuario;
    @Basic(optional = false)
    @Column(name = "nr_matricula")
    private String nrMatricula;
    @Basic(optional = false)
    @Column(name = "nm_usuario")
    private String nmUsuario;
    @Basic(optional = false)
    @Column(name = "vl_senha")
    private String vlSenha;
    @Basic(optional = false)
    @Column(name = "cd_nivel_administrador")
    private char cdNivelAdministrador; 
    @JoinColumn(name = "cd_perfil", referencedColumnName = "cd_perfil")
    @ManyToOne(optional = false)
    private TblPerfil cdPerfil;
    @OneToMany(mappedBy = "idUsuarioRecebe")
    private List<TblTramites> tblTramitesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUsuarioTramite")
    private List<TblTramites> tblTramitesList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUsuario")
    private List<TblLotacao> tblLotacaoList;
    @OneToMany(mappedBy = "idUsuario")
    private List<TblProcessos> tblProcessosList;

    
    public TblUsuario() {
    }

    public TblUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public TblUsuario(Integer idUsuario, String nrMatricula, String nmUsuario, String vlSenha) {
        this.idUsuario = idUsuario;
        this.nrMatricula = nrMatricula;
        this.nmUsuario = nmUsuario;
        this.vlSenha = vlSenha;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNrMatricula() {
        return nrMatricula;
    }

    public String getNrMatriculaMASK() {
    	return nrMatricula.substring(0, 7)+"-"+nrMatricula.substring(7, 10); 
    }
    
    public void setNrMatricula(String nrMatricula) {
        this.nrMatricula = nrMatricula;
    }

    public String getNmUsuario() {
        return nmUsuario;
    }

    public void setNmUsuario(String nmUsuario) {
        this.nmUsuario = nmUsuario;
    }

    public String getVlSenha() {
        return vlSenha;
    }

    public void setVlSenha(String vlSenha) {
        this.vlSenha = vlSenha;
    }

    public TblPerfil getCdPerfil() {
        return cdPerfil;
    }

    public void setCdPerfil(TblPerfil cdPerfil) {
        this.cdPerfil = cdPerfil;
    }

    public Character getCdNivelAdministrador() {
		return cdNivelAdministrador;
	}
    
    public void setCdNivelAdministrador(char cdNivelAdministrador) {
		this.cdNivelAdministrador = cdNivelAdministrador;
	}
    
    public List<TblTramites> getTblTramitesList() {
        return tblTramitesList;
    }

    public void setTblTramitesList(List<TblTramites> tblTramitesList) {
        this.tblTramitesList = tblTramitesList;
    }

    public List<TblTramites> getTblTramitesList1() {
        return tblTramitesList1;
    }

    public void setTblTramitesList1(List<TblTramites> tblTramitesList1) {
        this.tblTramitesList1 = tblTramitesList1;
    }

    public List<TblLotacao> getTblLotacaoList() {
        return tblLotacaoList;
    }

    public void setTblLotacaoList(List<TblLotacao> tblLotacaoList) {
        this.tblLotacaoList = tblLotacaoList;
    }

    public List<TblProcessos> getTblProcessosList() {
        return tblProcessosList;
    }

    public void setTblProcessosList(List<TblProcessos> tblProcessosList) {
        this.tblProcessosList = tblProcessosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuario != null ? idUsuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblUsuario)) {
            return false;
        }
        TblUsuario other = (TblUsuario) object;
        if ((this.idUsuario == null && other.idUsuario != null) || (this.idUsuario != null && !this.idUsuario.equals(other.idUsuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.cinbesa.ged.entidade.TblUsuario[ idUsuario=" + idUsuario + " ]";
    }
    
}
