/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_log_tributo")
@NamedQueries({
    @NamedQuery(name = "TblLogTributo.findAll", query = "SELECT t FROM TblLogTributo t"),
    @NamedQuery(name = "TblLogTributo.findByIdLogTributo", query = "SELECT t FROM TblLogTributo t WHERE t.idLogTributo = :idLogTributo"),
    @NamedQuery(name = "TblLogTributo.findByCdTributo", query = "SELECT t FROM TblLogTributo t WHERE t.cdTributo = :cdTributo"),
    @NamedQuery(name = "TblLogTributo.findByNmTributo", query = "SELECT t FROM TblLogTributo t WHERE t.nmTributo = :nmTributo"),
    @NamedQuery(name = "TblLogTributo.findByDsTributo", query = "SELECT t FROM TblLogTributo t WHERE t.dsTributo = :dsTributo"),
    @NamedQuery(name = "TblLogTributo.findByCdOperacao", query = "SELECT t FROM TblLogTributo t WHERE t.cdOperacao = :cdOperacao"),
    @NamedQuery(name = "TblLogTributo.findByDtOperacao", query = "SELECT t FROM TblLogTributo t WHERE t.dtOperacao = :dtOperacao"),
    @NamedQuery(name = "TblLogTributo.findByIdUsuarioOperacao", query = "SELECT t FROM TblLogTributo t WHERE t.idUsuarioOperacao = :idUsuarioOperacao")})
public class TblLogTributo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_log_tributo")
    private Integer idLogTributo;
    @Column(name = "cd_tributo")
    private String cdTributo;
    @Column(name = "nm_tributo")
    private String nmTributo;
    @Column(name = "ds_tributo")
    private String dsTributo;
    @Column(name = "cd_operacao")
    private Character cdOperacao;
    @Column(name = "dt_operacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtOperacao;
    @Column(name = "id_usuario_operacao")
    private Integer idUsuarioOperacao;
    @JoinColumn(name = "id_tributo", referencedColumnName = "id_tributo")
    @ManyToOne(optional = false)
    private TblTributo idTributo;

    public TblLogTributo() {
    }

    public TblLogTributo(Integer idLogTributo) {
        this.idLogTributo = idLogTributo;
    }

    public Integer getIdLogTributo() {
        return idLogTributo;
    }

    public void setIdLogTributo(Integer idLogTributo) {
        this.idLogTributo = idLogTributo;
    }

    public String getCdTributo() {
        return cdTributo;
    }

    public void setCdTributo(String cdTributo) {
        this.cdTributo = cdTributo;
    }

    public String getNmTributo() {
        return nmTributo;
    }

    public void setNmTributo(String nmTributo) {
        this.nmTributo = nmTributo;
    }

    public String getDsTributo() {
        return dsTributo;
    }

    public void setDsTributo(String dsTributo) {
        this.dsTributo = dsTributo;
    }

    public Character getCdOperacao() {
        return cdOperacao;
    }

    public void setCdOperacao(Character cdOperacao) {
        this.cdOperacao = cdOperacao;
    }

    public Date getDtOperacao() {
        return dtOperacao;
    }

    public void setDtOperacao(Date dtOperacao) {
        this.dtOperacao = dtOperacao;
    }

    public Integer getIdUsuarioOperacao() {
        return idUsuarioOperacao;
    }

    public void setIdUsuarioOperacao(Integer idUsuarioOperacao) {
        this.idUsuarioOperacao = idUsuarioOperacao;
    }

    public TblTributo getIdTributo() {
        return idTributo;
    }

    public void setIdTributo(TblTributo idTributo) {
        this.idTributo = idTributo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLogTributo != null ? idLogTributo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblLogTributo)) {
            return false;
        }
        TblLogTributo other = (TblLogTributo) object;
        if ((this.idLogTributo == null && other.idLogTributo != null) || (this.idLogTributo != null && !this.idLogTributo.equals(other.idLogTributo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ged.cinbesa.entidade.TblLogTributo[ idLogTributo=" + idLogTributo + " ]";
    }
    
}
