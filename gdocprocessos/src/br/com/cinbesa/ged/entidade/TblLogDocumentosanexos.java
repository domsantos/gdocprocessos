/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_log_documentosanexos")
@NamedQueries({
    @NamedQuery(name = "TblLogDocumentosanexos.findAll", query = "SELECT t FROM TblLogDocumentosanexos t"),
    @NamedQuery(name = "TblLogDocumentosanexos.findByIdLogDocumentosanexos", query = "SELECT t FROM TblLogDocumentosanexos t WHERE t.idLogDocumentosanexos = :idLogDocumentosanexos"),
    @NamedQuery(name = "TblLogDocumentosanexos.findByDtAnexo", query = "SELECT t FROM TblLogDocumentosanexos t WHERE t.dtAnexo = :dtAnexo"),
    @NamedQuery(name = "TblLogDocumentosanexos.findByNmDocumentoanexo", query = "SELECT t FROM TblLogDocumentosanexos t WHERE t.nmDocumentoanexo = :nmDocumentoanexo"),
    @NamedQuery(name = "TblLogDocumentosanexos.findByDtOperacao", query = "SELECT t FROM TblLogDocumentosanexos t WHERE t.dtOperacao = :dtOperacao"),
    @NamedQuery(name = "TblLogDocumentosanexos.findByCdOperacao", query = "SELECT t FROM TblLogDocumentosanexos t WHERE t.cdOperacao = :cdOperacao"),
    @NamedQuery(name = "TblLogDocumentosanexos.findByIdUsuarioOperacao", query = "SELECT t FROM TblLogDocumentosanexos t WHERE t.idUsuarioOperacao = :idUsuarioOperacao")})
public class TblLogDocumentosanexos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_log_documentosanexos")
    private Integer idLogDocumentosanexos;
    @Column(name = "dt_anexo")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtAnexo;
    @Lob
    @Column(name = "ds_path_arquivo")
    private String dsPathArquivo;
    @Column(name = "nm_documentoanexo")
    private String nmDocumentoanexo;
    @Column(name = "dt_operacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtOperacao;
    @Column(name = "cd_operacao")
    private Character cdOperacao;
    @Column(name = "id_usuario_operacao")
    private Integer idUsuarioOperacao;
    @JoinColumn(name = "cd_documentoanexo", referencedColumnName = "cd_documentoanexo")
    @ManyToOne(optional = false)
    private TblDocumentosanexos cdDocumentoanexo;

    public TblLogDocumentosanexos() {
    }

    public TblLogDocumentosanexos(Integer idLogDocumentosanexos) {
        this.idLogDocumentosanexos = idLogDocumentosanexos;
    }

    public Integer getIdLogDocumentosanexos() {
        return idLogDocumentosanexos;
    }

    public void setIdLogDocumentosanexos(Integer idLogDocumentosanexos) {
        this.idLogDocumentosanexos = idLogDocumentosanexos;
    }

    public Date getDtAnexo() {
        return dtAnexo;
    }

    public void setDtAnexo(Date dtAnexo) {
        this.dtAnexo = dtAnexo;
    }

    public String getDsPathArquivo() {
        return dsPathArquivo;
    }

    public void setDsPathArquivo(String dsPathArquivo) {
        this.dsPathArquivo = dsPathArquivo;
    }

    public String getNmDocumentoanexo() {
        return nmDocumentoanexo;
    }

    public void setNmDocumentoanexo(String nmDocumentoanexo) {
        this.nmDocumentoanexo = nmDocumentoanexo;
    }


    public Date getDtOperacao() {
        return dtOperacao;
    }

    public void setDtOperacao(Date dtOperacao) {
        this.dtOperacao = dtOperacao;
    }

    public Character getCdOperacao() {
        return cdOperacao;
    }

    public void setCdOperacao(Character cdOperacao) {
        this.cdOperacao = cdOperacao;
    }

    public Integer getIdUsuarioOperacao() {
        return idUsuarioOperacao;
    }

    public void setIdUsuarioOperacao(Integer idUsuarioOperacao) {
        this.idUsuarioOperacao = idUsuarioOperacao;
    }

    public TblDocumentosanexos getCdDocumentoanexo() {
        return cdDocumentoanexo;
    }

    public void setCdDocumentoanexo(TblDocumentosanexos cdDocumentoanexo) {
        this.cdDocumentoanexo = cdDocumentoanexo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLogDocumentosanexos != null ? idLogDocumentosanexos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblLogDocumentosanexos)) {
            return false;
        }
        TblLogDocumentosanexos other = (TblLogDocumentosanexos) object;
        if ((this.idLogDocumentosanexos == null && other.idLogDocumentosanexos != null) || (this.idLogDocumentosanexos != null && !this.idLogDocumentosanexos.equals(other.idLogDocumentosanexos))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ged.cinbesa.entidade.TblLogDocumentosanexos[ idLogDocumentosanexos=" + idLogDocumentosanexos + " ]";
    }
    
}
