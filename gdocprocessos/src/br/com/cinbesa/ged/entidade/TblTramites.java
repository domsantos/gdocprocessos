/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_tramites")
@NamedQueries({
    @NamedQuery(name = "TblTramites.findAll", query = "SELECT t FROM TblTramites t"),
    @NamedQuery(name = "TblTramites.findByIdTramite", query = "SELECT t FROM TblTramites t WHERE t.idTramite = :idTramite"),
    @NamedQuery(name = "TblTramites.findByDtTramite", query = "SELECT t FROM TblTramites t WHERE t.dtTramite = :dtTramite"),
    @NamedQuery(name = "TblTramites.findByStTramite", query = "SELECT t FROM TblTramites t WHERE t.stTramite = :stTramite"),
    @NamedQuery(name = "TblTramites.findByDtRecebimento", query = "SELECT t FROM TblTramites t WHERE t.dtRecebimento = :dtRecebimento")})
public class TblTramites implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_tramite")
    private Long idTramite;
    @Basic(optional = false)
    @Column(name = "dt_tramite")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtTramite;
    @Basic(optional = false)
    @Column(name = "st_tramite")
    private char stTramite;
    @Basic(optional = false)
    @Lob
    @Column(name = "ds_dispacho")
    private String dsDispacho;
    @Column(name = "dt_recebimento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtRecebimento;
    @JoinColumn(name = "id_usuario_recebe", referencedColumnName = "id_usuario")
    @ManyToOne
    private TblUsuario idUsuarioRecebe;
    @JoinColumn(name = "id_usuario_tramite", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false)
    private TblUsuario idUsuarioTramite;
    @JoinColumn(name = "cd_setor", referencedColumnName = "cd_setor")
    @ManyToOne(optional = false)
    private TblSetor cdSetor;
    @JoinColumn(name = "id_processo", referencedColumnName = "id_processo")
    @ManyToOne(optional = false)
    private TblProcessos idProcesso;

    public TblTramites() {
    }

    public TblTramites(Long idTramite) {
        this.idTramite = idTramite;
    }

    public TblTramites(Long idTramite, Date dtTramite, char stTramite, String dsDispacho) {
        this.idTramite = idTramite;
        this.dtTramite = dtTramite;
        this.stTramite = stTramite;
        this.dsDispacho = dsDispacho;
    }

    public Long getIdTramite() {
        return idTramite;
    }

    public void setIdTramite(Long idTramite) {
        this.idTramite = idTramite;
    }

    public Date getDtTramite() {
        return dtTramite;
    }

    public void setDtTramite(Date dtTramite) {
        this.dtTramite = dtTramite;
    }

    public char getStTramite() {
        return stTramite;
    }

    public void setStTramite(char stTramite) {
        this.stTramite = stTramite;
    }

    public String getDsDispacho() {
        return dsDispacho;
    }

    public void setDsDispacho(String dsDispacho) {
        this.dsDispacho = dsDispacho;
    }

    public Date getDtRecebimento() {
        return dtRecebimento;
    }

    public void setDtRecebimento(Date dtRecebimento) {
        this.dtRecebimento = dtRecebimento;
    }

    public TblUsuario getIdUsuarioRecebe() {
        return idUsuarioRecebe;
    }

    public void setIdUsuarioRecebe(TblUsuario idUsuarioRecebe) {
        this.idUsuarioRecebe = idUsuarioRecebe;
    }

    public TblUsuario getIdUsuarioTramite() {
        return idUsuarioTramite;
    }

    public void setIdUsuarioTramite(TblUsuario idUsuarioTramite) {
        this.idUsuarioTramite = idUsuarioTramite;
    }

    public TblSetor getCdSetor() {
        return cdSetor;
    }

    public void setCdSetor(TblSetor cdSetor) {
        this.cdSetor = cdSetor;
    }

    public TblProcessos getIdProcesso() {
        return idProcesso;
    }

    public void setIdProcesso(TblProcessos idProcesso) {
        this.idProcesso = idProcesso;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTramite != null ? idTramite.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
      
        if (!(object instanceof TblTramites)) {
            return false;
        }
        TblTramites other = (TblTramites) object;
        if ((this.idTramite == null && other.idTramite != null) || (this.idTramite != null && !this.idTramite.equals(other.idTramite))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.entidades.TblTramites[ idTramite=" + idTramite + " ]";
    }
    
}
