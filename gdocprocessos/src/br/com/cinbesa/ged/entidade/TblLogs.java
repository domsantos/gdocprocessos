/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_logs")
@NamedQueries({
    @NamedQuery(name = "TblLogs.findAll", query = "SELECT t FROM TblLogs t"),
    @NamedQuery(name = "TblLogs.findById", query = "SELECT t FROM TblLogs t WHERE t.id = :id"),
    @NamedQuery(name = "TblLogs.findByDsTabela", query = "SELECT t FROM TblLogs t WHERE t.dsTabela = :dsTabela"),
    @NamedQuery(name = "TblLogs.findByDtLog", query = "SELECT t FROM TblLogs t WHERE t.dtLog = :dtLog"),
    @NamedQuery(name = "TblLogs.findByCdOp", query = "SELECT t FROM TblLogs t WHERE t.cdOp = :cdOp"),
    @NamedQuery(name = "TblLogs.findByDsUsuario", query = "SELECT t FROM TblLogs t WHERE t.dsUsuario = :dsUsuario")})
public class TblLogs implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "ds_tabela")
    private String dsTabela;
    @Lob
    @Column(name = "ds_conteudo")
    private String dsConteudo;
    @Column(name = "dt_log")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtLog;
    @Column(name = "cd_op")
    private Character cdOp;
    @Column(name = "ds_usuario")
    private String dsUsuario;

    public TblLogs() {
    }

    public TblLogs(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDsTabela() {
        return dsTabela;
    }

    public void setDsTabela(String dsTabela) {
        this.dsTabela = dsTabela;
    }

    public String getDsConteudo() {
        return dsConteudo;
    }

    public void setDsConteudo(String dsConteudo) {
        this.dsConteudo = dsConteudo;
    }

    public Date getDtLog() {
        return dtLog;
    }

    public void setDtLog(Date dtLog) {
        this.dtLog = dtLog;
    }

    public Character getCdOp() {
        return cdOp;
    }

    public void setCdOp(Character cdOp) {
        this.cdOp = cdOp;
    }

    public String getDsUsuario() {
        return dsUsuario;
    }

    public void setDsUsuario(String dsUsuario) {
        this.dsUsuario = dsUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblLogs)) {
            return false;
        }
        TblLogs other = (TblLogs) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.entidades.TblLogs[ id=" + id + " ]";
    }
    
}
