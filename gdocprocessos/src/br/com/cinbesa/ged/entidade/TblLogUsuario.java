/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_log_usuario")
@NamedQueries({
    
    @NamedQuery(name = "TblLogUsuario.findByNrMatricula", query = "SELECT t FROM TblLogUsuario t WHERE t.nrMatricula = :nrMatricula"),
    @NamedQuery(name = "TblLogUsuario.findByCdPerfil", query = "SELECT t FROM TblLogUsuario t WHERE t.cdPerfil = :cdPerfil"),
    @NamedQuery(name = "TblLogUsuario.findByNmUsuario", query = "SELECT t FROM TblLogUsuario t WHERE t.nmUsuario = :nmUsuario"),
    @NamedQuery(name = "TblLogUsuario.findByVlSenha", query = "SELECT t FROM TblLogUsuario t WHERE t.vlSenha = :vlSenha"),
    @NamedQuery(name = "TblLogUsuario.findByIdUsuarioOperacao", query = "SELECT t FROM TblLogUsuario t WHERE t.idUsuarioOperacao = :idUsuarioOperacao")})
public class TblLogUsuario implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_log_usuario")
    private Integer idLogUsuario;
    @Column(name = "nr_matricula")
    private String nrMatricula;
    @Column(name = "cd_perfil")
    private Integer cdPerfil;
    @Column(name = "nm_usuario")
    private String nmUsuario;
    @Column(name = "vl_senha")
    private String vlSenha;
    @Column(name = "id_usuario_operacao")
    private Integer idUsuarioOperacao;
    @Column(name = "cd_operacao")
    private Character cdOperacao;
    @Column(name = "dt_operacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtOperacao;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false)
    private TblUsuario idUsuario;

    public TblLogUsuario() {
    }

    public TblLogUsuario(Integer tblLogUsuario) {
        this.idLogUsuario = tblLogUsuario;
    }

    public Integer getTblLogUsuario() {
        return idLogUsuario;
    }

    public void setTblLogUsuario(Integer tblLogUsuario) {
        this.idLogUsuario = tblLogUsuario;
    }

    public String getNrMatricula() {
        return nrMatricula;
    }

    public void setNrMatricula(String nrMatricula) {
        this.nrMatricula = nrMatricula;
    }

    public Integer getCdPerfil() {
        return cdPerfil;
    }

    public void setCdPerfil(Integer cdPerfil) {
        this.cdPerfil = cdPerfil;
    }

    public String getNmUsuario() {
        return nmUsuario;
    }

    public void setNmUsuario(String nmUsuario) {
        this.nmUsuario = nmUsuario;
    }

    public String getVlSenha() {
        return vlSenha;
    }

    public void setVlSenha(String vlSenha) {
        this.vlSenha = vlSenha;
    }

    public Integer getIdUsuarioOperacao() {
        return idUsuarioOperacao;
    }

    public void setIdUsuarioOperacao(Integer idUsuarioOperacao) {
        this.idUsuarioOperacao = idUsuarioOperacao;
    }

    public TblUsuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(TblUsuario idUsuario) {
        this.idUsuario = idUsuario;
    }
    public Character getCdOperacao() {
		return cdOperacao;
	}
    public void setCdOperacao(Character cdOperacao) {
		this.cdOperacao = cdOperacao;
	}
    public Date getDtOperacao() {
		return dtOperacao;
	}
    public void setDtOperacao(Date dtOperacao) {
		this.dtOperacao = dtOperacao;
	}

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLogUsuario != null ? idLogUsuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblLogUsuario)) {
            return false;
        }
        TblLogUsuario other = (TblLogUsuario) object;
        if ((this.idLogUsuario == null && other.idLogUsuario != null) || (this.idLogUsuario != null && !this.idLogUsuario.equals(other.idLogUsuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ged.cinbesa.entidade.TblLogUsuario[ tblLogUsuario=" + idLogUsuario + " ]";
    }
    
}
