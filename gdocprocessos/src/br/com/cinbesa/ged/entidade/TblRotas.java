/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_rotas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblRotas.findAll", query = "SELECT t FROM TblRotas t"),
    @NamedQuery(name = "TblRotas.findByCdRota", query = "SELECT t FROM TblRotas "
    		+ "t WHERE t.cdRota = :cdRota"),
    @NamedQuery(name = "TblRotas.findByCdPapel", query = "SELECT t FROM TblRotas t WHERE t.cdPapel = :cdPapel"),
    @NamedQuery(name = "TblRotas.findByCdPrioridade", query = "SELECT t FROM TblRotas t WHERE t.cdPrioridade = :cdPrioridade"),
    @NamedQuery(name = "TblRotas.findByNmRota", query = "SELECT t FROM TblRotas t WHERE t.nmRota = :nmRota"),
    @NamedQuery(name = "TblRotas.findByStRotaParalela", query = "SELECT t FROM TblRotas t WHERE t.stRotaParalela = :stRotaParalela"),
    @NamedQuery(name = "TblRotas.findByNrOrdemrota", query = "SELECT t FROM TblRotas t WHERE t.nrOrdemrota = :nrOrdemrota"),
    @NamedQuery(name = "TblRotas.findByNrPrazoRota", query = "SELECT t FROM TblRotas t WHERE t.nrPrazoRota = :nrPrazoRota")//,
    //@NamedQuery(name = "TblRotas.findByCdTipodocumento", query = "SELECT t FROM TblRotas t WHERE t.cdTipodocumento = :cdTipodocumento")
    })
public class TblRotas implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cd_rota")
    private Integer cdRota;
    @Basic(optional = false)
    @Column(name = "cd_papel")
    private int cdPapel;
    @Basic(optional = false)
    @Column(name = "cd_prioridade")
    private short cdPrioridade;
    @Basic(optional = false)
    @Column(name = "nm_rota")
    private String nmRota;
    @Basic(optional = false)
    @Column(name = "st_rota_paralela")
    private short stRotaParalela;
    @Basic(optional = false)
    @Column(name = "nr_ordemrota")
    private short nrOrdemrota;
    @Column(name = "nr_prazo_rota")
    private Integer nrPrazoRota;
    //@Column(name = "cd_tipodocumento")
    //private Integer cdTipodocumento;
    @JoinColumn(name = "cd_tipoprocesso", referencedColumnName = "cd_tipoprocesso")
    @ManyToOne(optional = false)
    private TblTipoprocesso cdTipoprocesso;
    @JoinColumn(name = "cd_setor", referencedColumnName = "cd_setor")
    @ManyToOne(optional = false)
    private TblSetor cdSetor;

    public TblRotas() {
    }

    public TblRotas(Integer cdRota) {
        this.cdRota = cdRota;
    }

    public TblRotas(Integer cdRota, int cdPapel, short cdPrioridade, String nmRota, short stRotaParalela, short nrOrdemrota) {
        this.cdRota = cdRota;
        this.cdPapel = cdPapel;
        this.cdPrioridade = cdPrioridade;
        this.nmRota = nmRota;
        this.stRotaParalela = stRotaParalela;
        this.nrOrdemrota = nrOrdemrota;
    }

    public Integer getCdRota() {
        return cdRota;
    }

    public void setCdRota(Integer cdRota) {
        this.cdRota = cdRota;
    }

    public int getCdPapel() {
        return cdPapel;
    }

    public void setCdPapel(int cdPapel) {
        this.cdPapel = cdPapel;
    }

    public short getCdPrioridade() {
        return cdPrioridade;
    }

    public void setCdPrioridade(short cdPrioridade) {
        this.cdPrioridade = cdPrioridade;
    }

    public String getNmRota() {
        return nmRota;
    }

    public void setNmRota(String nmRota) {
        this.nmRota = nmRota;
    }

    public short getStRotaParalela() {
        return stRotaParalela;
    }

    public void setStRotaParalela(short stRotaParalela) {
        this.stRotaParalela = stRotaParalela;
    }

    public short getNrOrdemrota() {
        return nrOrdemrota;
    }

    public void setNrOrdemrota(short nrOrdemrota) {
        this.nrOrdemrota = nrOrdemrota;
    }

    public Integer getNrPrazoRota() {
        return nrPrazoRota;
    }

    public void setNrPrazoRota(Integer nrPrazoRota) {
        this.nrPrazoRota = nrPrazoRota;
    }

    /*public Integer getCdTipodocumento() {
        return cdTipodocumento;
    }

    public void setCdTipodocumento(Integer cdTipodocumento) {
        this.cdTipodocumento = cdTipodocumento;
    }
*/
    public TblTipoprocesso getCdTipoprocesso() {
        return cdTipoprocesso;
    }

    public void setCdTipoprocesso(TblTipoprocesso cdTipoprocesso) {
        this.cdTipoprocesso = cdTipoprocesso;
    }

    public TblSetor getCdSetor() {
        return cdSetor;
    }

    public void setCdSetor(TblSetor cdSetor) {
        this.cdSetor = cdSetor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cdRota != null ? cdRota.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblRotas)) {
            return false;
        }
        TblRotas other = (TblRotas) object;
        if ((this.cdRota == null && other.cdRota != null) || (this.cdRota != null && !this.cdRota.equals(other.cdRota))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ged.cinbesa.entidade.TblRotas[ cdRota=" + cdRota + " ]";
    }
    
}
