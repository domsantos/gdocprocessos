/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_documentosanexos")
@NamedQueries({
    @NamedQuery(name = "TblDocumentosanexos.findAll", query = "SELECT t FROM TblDocumentosanexos t"),
    @NamedQuery(name = "TblDocumentosanexos.findByCdDocumentoanexo", query = "SELECT t FROM TblDocumentosanexos t WHERE t.cdDocumentoanexo = :cdDocumentoanexo"),
    @NamedQuery(name = "TblDocumentosanexos.findByDtAnexo", query = "SELECT t FROM TblDocumentosanexos t WHERE t.dtAnexo = :dtAnexo"),
    @NamedQuery(name = "TblDocumentosanexos.findByNmDocumentoanexo", query = "SELECT t FROM TblDocumentosanexos t WHERE t.nmDocumentoanexo = :nmDocumentoanexo")})
public class TblDocumentosanexos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cd_documentoanexo")
    private Integer cdDocumentoanexo;
    @Basic(optional = false)
    @Column(name = "dt_anexo")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtAnexo;
    @Column(name = "ds_path_arquivo")
    private String dsPathArquivo;
    @Column(name = "nm_documentoanexo")
    private String nmDocumentoanexo;
    @Column(name = "id_especie")
    private Integer idEspecie;
    @Basic(fetch = FetchType.LAZY)
    @Lob
    @Column(name = "bi_arquivo")
    private byte[] biArquivo;
    @JoinColumn(name = "id_processo", referencedColumnName = "id_processo")
    @ManyToOne(optional = false)
    private TblProcessos idProcesso;

    public TblDocumentosanexos() {
    }

    public TblDocumentosanexos(Integer cdDocumentoanexo) {
        this.cdDocumentoanexo = cdDocumentoanexo;
    }

    public TblDocumentosanexos(Integer cdDocumentoanexo, Date dtAnexo) {
        this.cdDocumentoanexo = cdDocumentoanexo;
        this.dtAnexo = dtAnexo;
    }

    public Integer getCdDocumentoanexo() {
        return cdDocumentoanexo;
    }

    public void setCdDocumentoanexo(Integer cdDocumentoanexo) {
        this.cdDocumentoanexo = cdDocumentoanexo;
    }

    public Date getDtAnexo() {
        return dtAnexo;
    }

    public void setDtAnexo(Date dtAnexo) {
        this.dtAnexo = dtAnexo;
    }

    public String getDsPathArquivo() {
        return dsPathArquivo;
    }

    public void setDsPathArquivo(String dsPathArquivo) {
        this.dsPathArquivo = dsPathArquivo;
    }

    public String getNmDocumentoanexo() {
        return nmDocumentoanexo;
    }

    public void setNmDocumentoanexo(String nmDocumentoanexo) {
        this.nmDocumentoanexo = nmDocumentoanexo;
    }

    public TblProcessos getIdProcesso() {
        return idProcesso;
    }

    public void setIdProcesso(TblProcessos idProcesso) {
        this.idProcesso = idProcesso;
    }
    
    public Integer getIdEspecie() {
        return idEspecie;
    }

    public void setIdEspecie(Integer idEspecie) {
        this.idEspecie = idEspecie;
    }
    
    public byte[] getBiArquivo() {
        return biArquivo;
    }

    public void setBiArquivo(byte[] biArquivo) {
        this.biArquivo = biArquivo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cdDocumentoanexo != null ? cdDocumentoanexo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblDocumentosanexos)) {
            return false;
        }
        TblDocumentosanexos other = (TblDocumentosanexos) object;
        if ((this.cdDocumentoanexo == null && other.cdDocumentoanexo != null) || (this.cdDocumentoanexo != null && !this.cdDocumentoanexo.equals(other.cdDocumentoanexo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.entidades.TblDocumentosanexos[ cdDocumentoanexo=" + cdDocumentoanexo + " ]";
    }
    
}
