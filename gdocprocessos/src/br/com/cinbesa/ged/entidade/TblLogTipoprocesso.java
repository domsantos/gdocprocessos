/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_log_tipoprocesso")
@NamedQueries({
    @NamedQuery(name = "TblLogTipoprocesso.findAll", query = "SELECT t FROM TblLogTipoprocesso t"),
    @NamedQuery(name = "TblLogTipoprocesso.findByIdLogTipoprocesso", query = "SELECT t FROM TblLogTipoprocesso t WHERE t.idLogTipoprocesso = :idLogTipoprocesso"),
    @NamedQuery(name = "TblLogTipoprocesso.findByIdEntidade", query = "SELECT t FROM TblLogTipoprocesso t WHERE t.idEntidade = :idEntidade"),
    @NamedQuery(name = "TblLogTipoprocesso.findByNmTipoprocesso", query = "SELECT t FROM TblLogTipoprocesso t WHERE t.nmTipoprocesso = :nmTipoprocesso"),
    @NamedQuery(name = "TblLogTipoprocesso.findByDsTipopocesso", query = "SELECT t FROM TblLogTipoprocesso t WHERE t.dsTipopocesso = :dsTipopocesso"),
    @NamedQuery(name = "TblLogTipoprocesso.findByStConfirmacaoPresencia", query = "SELECT t FROM TblLogTipoprocesso t WHERE t.stConfirmacaoPresencia = :stConfirmacaoPresencia"),
    @NamedQuery(name = "TblLogTipoprocesso.findByStAbrirInternet", query = "SELECT t FROM TblLogTipoprocesso t WHERE t.stAbrirInternet = :stAbrirInternet"),
    @NamedQuery(name = "TblLogTipoprocesso.findByCdOperacao", query = "SELECT t FROM TblLogTipoprocesso t WHERE t.cdOperacao = :cdOperacao"),
    @NamedQuery(name = "TblLogTipoprocesso.findByDtOperacao", query = "SELECT t FROM TblLogTipoprocesso t WHERE t.dtOperacao = :dtOperacao"),
    @NamedQuery(name = "TblLogTipoprocesso.findByIdUsuarioOperacao", query = "SELECT t FROM TblLogTipoprocesso t WHERE t.idUsuarioOperacao = :idUsuarioOperacao")})
public class TblLogTipoprocesso implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_log_tipoprocesso")
    private Integer idLogTipoprocesso;
    @Column(name = "id_entidade")
    private Integer idEntidade;
    @Column(name = "nm_tipoprocesso")
    private String nmTipoprocesso;
    @Column(name = "ds_tipopocesso")
    private String dsTipopocesso;
    @Column(name = "st_confirmacao_presencia")
    private Character stConfirmacaoPresencia;
    @Column(name = "st_abrir_internet")
    private Character stAbrirInternet;
    @Column(name = "cd_operacao")
    private Character cdOperacao;
    @Column(name = "dt_operacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtOperacao;
    @Column(name = "id_usuario_operacao")
    private int idUsuarioOperacao;
    @JoinColumn(name = "cd_tipoprocesso", referencedColumnName = "cd_tipoprocesso")
    @ManyToOne(optional = false)
    private TblTipoprocesso cdTipoprocesso;

    public TblLogTipoprocesso() {
    }

    public TblLogTipoprocesso(Integer idLogTipoprocesso) {
        this.idLogTipoprocesso = idLogTipoprocesso;
    }

    public Integer getIdLogTipoprocesso() {
        return idLogTipoprocesso;
    }

    public void setIdLogTipoprocesso(Integer idLogTipoprocesso) {
        this.idLogTipoprocesso = idLogTipoprocesso;
    }

    public Integer getIdEntidade() {
        return idEntidade;
    }

    public void setIdEntidade(Integer idEntidade) {
        this.idEntidade = idEntidade;
    }

    public String getNmTipoprocesso() {
        return nmTipoprocesso;
    }

    public void setNmTipoprocesso(String nmTipoprocesso) {
        this.nmTipoprocesso = nmTipoprocesso;
    }

    public String getDsTipopocesso() {
        return dsTipopocesso;
    }

    public void setDsTipopocesso(String dsTipopocesso) {
        this.dsTipopocesso = dsTipopocesso;
    }

    public Character getStConfirmacaoPresencia() {
        return stConfirmacaoPresencia;
    }

    public void setStConfirmacaoPresencia(Character stConfirmacaoPresencia) {
        this.stConfirmacaoPresencia = stConfirmacaoPresencia;
    }

    public Character getStAbrirInternet() {
        return stAbrirInternet;
    }

    public void setStAbrirInternet(Character stAbrirInternet) {
        this.stAbrirInternet = stAbrirInternet;
    }

    public Character getCdOperacao() {
        return cdOperacao;
    }

    public void setCdOperacao(Character cdOperacao) {
        this.cdOperacao = cdOperacao;
    }

    public Date getDtOperacao() {
        return dtOperacao;
    }

    public void setDtOperacao(Date dtOperacao) {
        this.dtOperacao = dtOperacao;
    }

    public int getIdUsuarioOperacao() {
        return idUsuarioOperacao;
    }

    public void setIdUsuarioOperacao(int idUsuarioOperacao) {
        this.idUsuarioOperacao = idUsuarioOperacao;
    }

    public TblTipoprocesso getCdTipoprocesso() {
        return cdTipoprocesso;
    }

    public void setCdTipoprocesso(TblTipoprocesso cdTipoprocesso) {
        this.cdTipoprocesso = cdTipoprocesso;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLogTipoprocesso != null ? idLogTipoprocesso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblLogTipoprocesso)) {
            return false;
        }
        TblLogTipoprocesso other = (TblLogTipoprocesso) object;
        if ((this.idLogTipoprocesso == null && other.idLogTipoprocesso != null) || (this.idLogTipoprocesso != null && !this.idLogTipoprocesso.equals(other.idLogTipoprocesso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ged.cinbesa.entidade.TblLogTipoprocesso[ idLogTipoprocesso=" + idLogTipoprocesso + " ]";
    }
    
}
