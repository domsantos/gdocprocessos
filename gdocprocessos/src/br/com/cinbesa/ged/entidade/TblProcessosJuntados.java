/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_processos_juntados")
@NamedQueries({
    @NamedQuery(name = "TblProcessosJuntados.findAll", query = "SELECT t FROM TblProcessosJuntados t"),
    @NamedQuery(name = "TblProcessosJuntados.findByIdProcessosAjuntados", query = "SELECT t FROM TblProcessosJuntados t WHERE t.idProcessosJuntados = :idProcessosJuntados")})
public class TblProcessosJuntados implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_processos_juntados")
    private Long idProcessosJuntados;
    @JoinColumn(name = "id_processo_filho", referencedColumnName = "id_processo")
    @ManyToOne(optional = false)
    private TblProcessos idProcessoFilho;
    @JoinColumn(name = "id_processo_pai", referencedColumnName = "id_processo")
    @ManyToOne(optional = false)
    private TblProcessos idProcessoPai;

    public TblProcessosJuntados() {
    }

    public TblProcessosJuntados(Long idProcessosAjuntados) {
        this.idProcessosJuntados = idProcessosAjuntados;
    }

    public Long getIdProcessosAjuntados() {
        return idProcessosJuntados;
    }

    public void setIdProcessosAjuntados(Long idProcessosAjuntados) {
        this.idProcessosJuntados = idProcessosAjuntados;
    }

    public TblProcessos getIdProcessoFilho() {
        return idProcessoFilho;
    }

    public void setIdProcessoFilho(TblProcessos idProcessoFilho) {
        this.idProcessoFilho = idProcessoFilho;
    }

    public TblProcessos getIdProcessoPai() {
        return idProcessoPai;
    }

    public void setIdProcessoPai(TblProcessos idProcessoPai) {
        this.idProcessoPai = idProcessoPai;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProcessosJuntados != null ? idProcessosJuntados.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblProcessosJuntados)) {
            return false;
        }
        TblProcessosJuntados other = (TblProcessosJuntados) object;
        if ((this.idProcessosJuntados == null && other.idProcessosJuntados != null) || (this.idProcessosJuntados != null && !this.idProcessosJuntados.equals(other.idProcessosJuntados))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.cinbesa.ged.entidade.TblProcessosAjuntados[ idProcessosAjuntados=" + idProcessosJuntados + " ]";
    }
    
}
