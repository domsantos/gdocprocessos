/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_log_lotacao")
@NamedQueries({
    @NamedQuery(name = "TblLogLotacao.findAll", query = "SELECT t FROM TblLogLotacao t"),
    @NamedQuery(name = "TblLogLotacao.findByIdLogLotacao", query = "SELECT t FROM TblLogLotacao t WHERE t.idLogLotacao = :idLogLotacao"),
    @NamedQuery(name = "TblLogLotacao.findByIdUsuario", query = "SELECT t FROM TblLogLotacao t WHERE t.idUsuario = :idUsuario"),
    @NamedQuery(name = "TblLogLotacao.findByStLotacao", query = "SELECT t FROM TblLogLotacao t WHERE t.stLotacao = :stLotacao"),
    @NamedQuery(name = "TblLogLotacao.findByDtLotacao", query = "SELECT t FROM TblLogLotacao t WHERE t.dtLotacao = :dtLotacao"),
    @NamedQuery(name = "TblLogLotacao.findByCdOperacao", query = "SELECT t FROM TblLogLotacao t WHERE t.cdOperacao = :cdOperacao"),
    @NamedQuery(name = "TblLogLotacao.findByDtOperacao", query = "SELECT t FROM TblLogLotacao t WHERE t.dtOperacao = :dtOperacao"),
    @NamedQuery(name = "TblLogLotacao.findByIdUsuarioOperacao", query = "SELECT t FROM TblLogLotacao t WHERE t.idUsuarioOperacao = :idUsuarioOperacao")})
public class TblLogLotacao implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_log_lotacao")
    private Integer idLogLotacao;
    @Column(name = "id_usuario")
    private Integer idUsuario;
    @Column(name = "st_lotacao")
    private Character stLotacao;
    @Column(name = "dt_lotacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtLotacao;
    @Column(name = "cd_operacao")
    private Character cdOperacao;
    @Column(name = "dt_operacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtOperacao;
    @Column(name = "id_usuario_operacao")
    private Integer idUsuarioOperacao;
    @JoinColumn(name = "cd_lotacao", referencedColumnName = "cd_lotacao")
    @ManyToOne(optional = false)
    private TblLotacao cdLotacao;

    public TblLogLotacao() {
    }

    public TblLogLotacao(Integer idLogLotacao) {
        this.idLogLotacao = idLogLotacao;
    }

    public Integer getIdLogLotacao() {
        return idLogLotacao;
    }

    public void setIdLogLotacao(Integer idLogLotacao) {
        this.idLogLotacao = idLogLotacao;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Character getStLotacao() {
        return stLotacao;
    }

    public void setStLotacao(Character stLotacao) {
        this.stLotacao = stLotacao;
    }

    public Date getDtLotacao() {
        return dtLotacao;
    }

    public void setDtLotacao(Date dtLotacao) {
        this.dtLotacao = dtLotacao;
    }

    public Character getCdOperacao() {
        return cdOperacao;
    }

    public void setCdOperacao(Character cdOperacao) {
        this.cdOperacao = cdOperacao;
    }

    public Date getDtOperacao() {
        return dtOperacao;
    }

    public void setDtOperacao(Date dtOperacao) {
        this.dtOperacao = dtOperacao;
    }

    public Integer getIdUsuarioOperacao() {
        return idUsuarioOperacao;
    }

    public void setIdUsuarioOperacao(Integer idUsuarioOperacao) {
        this.idUsuarioOperacao = idUsuarioOperacao;
    }

    public TblLotacao getCdLotacao() {
        return cdLotacao;
    }

    public void setCdLotacao(TblLotacao cdLotacao) {
        this.cdLotacao = cdLotacao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLogLotacao != null ? idLogLotacao.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblLogLotacao)) {
            return false;
        }
        TblLogLotacao other = (TblLogLotacao) object;
        if ((this.idLogLotacao == null && other.idLogLotacao != null) || (this.idLogLotacao != null && !this.idLogLotacao.equals(other.idLogLotacao))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ged.cinbesa.entidade.TblLogLotacao[ idLogLotacao=" + idLogLotacao + " ]";
    }
    
}
