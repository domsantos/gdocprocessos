/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_processo_arquivado")
@NamedQueries({
    @NamedQuery(name = "TblProcessoArquivado.findAll", query = "SELECT t FROM TblProcessoArquivado t"),
    @NamedQuery(name = "TblProcessoArquivado.findByIdProcesso", query = "SELECT t FROM TblProcessoArquivado t WHERE t.tblProcessoArquivadoPK.idProcesso = :idProcesso"),
    @NamedQuery(name = "TblProcessoArquivado.findByIdEnderecoarquivamento", query = "SELECT t FROM TblProcessoArquivado t WHERE t.tblProcessoArquivadoPK.idEnderecoarquivamento = :idEnderecoarquivamento"),
    @NamedQuery(name = "TblProcessoArquivado.findByDtArquivamento", query = "SELECT t FROM TblProcessoArquivado t WHERE t.dtArquivamento = :dtArquivamento"),
    @NamedQuery(name = "TblProcessoArquivado.findByDsArquivamento", query = "SELECT t FROM TblProcessoArquivado t WHERE t.dsArquivamento = :dsArquivamento")})
public class TblProcessoArquivado implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TblProcessoArquivadoPK tblProcessoArquivadoPK;
    @Column(name = "dt_arquivamento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtArquivamento;
    @Basic(optional = false)
    @Column(name = "ds_arquivamento")
    private String dsArquivamento;
    @JoinColumn(name = "id_processo", referencedColumnName = "id_processo", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TblProcessos tblProcessos;
    @JoinColumn(name = "id_enderecoarquivamento", referencedColumnName = "id_enderecoarquivamento", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TblEnderecoarquivamento tblEnderecoarquivamento;

    public TblProcessoArquivado() {
    }

    public TblProcessoArquivado(TblProcessoArquivadoPK tblProcessoArquivadoPK) {
        this.tblProcessoArquivadoPK = tblProcessoArquivadoPK;
    }

    public TblProcessoArquivado(TblProcessoArquivadoPK tblProcessoArquivadoPK, String dsArquivamento) {
        this.tblProcessoArquivadoPK = tblProcessoArquivadoPK;
        this.dsArquivamento = dsArquivamento;
    }

    public TblProcessoArquivado(long idProcesso, int idEnderecoarquivamento) {
        this.tblProcessoArquivadoPK = new TblProcessoArquivadoPK(idProcesso, idEnderecoarquivamento);
    }

    public TblProcessoArquivadoPK getTblProcessoArquivadoPK() {
        return tblProcessoArquivadoPK;
    }

    public void setTblProcessoArquivadoPK(TblProcessoArquivadoPK tblProcessoArquivadoPK) {
        this.tblProcessoArquivadoPK = tblProcessoArquivadoPK;
    }

    public Date getDtArquivamento() {
        return dtArquivamento;
    }

    public void setDtArquivamento(Date dtArquivamento) {
        this.dtArquivamento = dtArquivamento;
    }

    public String getDsArquivamento() {
        return dsArquivamento;
    }

    public void setDsArquivamento(String dsArquivamento) {
        this.dsArquivamento = dsArquivamento;
    }

    public TblProcessos getTblProcessos() {
        return tblProcessos;
    }

    public void setTblProcessos(TblProcessos tblProcessos) {
        this.tblProcessos = tblProcessos;
    }

    public TblEnderecoarquivamento getTblEnderecoarquivamento() {
        return tblEnderecoarquivamento;
    }

    public void setTblEnderecoarquivamento(TblEnderecoarquivamento tblEnderecoarquivamento) {
        this.tblEnderecoarquivamento = tblEnderecoarquivamento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tblProcessoArquivadoPK != null ? tblProcessoArquivadoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblProcessoArquivado)) {
            return false;
        }
        TblProcessoArquivado other = (TblProcessoArquivado) object;
        if ((this.tblProcessoArquivadoPK == null && other.tblProcessoArquivadoPK != null) || (this.tblProcessoArquivadoPK != null && !this.tblProcessoArquivadoPK.equals(other.tblProcessoArquivadoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.cinbesa.ged.entidade.TblProcessoArquivado[ tblProcessoArquivadoPK=" + tblProcessoArquivadoPK + " ]";
    }
    
}
