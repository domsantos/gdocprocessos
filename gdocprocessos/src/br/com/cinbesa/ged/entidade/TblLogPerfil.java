/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_log_perfil")
@NamedQueries({
    @NamedQuery(name = "TblLogPerfil.findAll", query = "SELECT t FROM TblLogPerfil t"),
    @NamedQuery(name = "TblLogPerfil.findByIdLogPerfil", query = "SELECT t FROM TblLogPerfil t WHERE t.idLogPerfil = :idLogPerfil"),
    @NamedQuery(name = "TblLogPerfil.findByCdOperacao", query = "SELECT t FROM TblLogPerfil t WHERE t.cdOperacao = :cdOperacao"),
    @NamedQuery(name = "TblLogPerfil.findByDsPerfil", query = "SELECT t FROM TblLogPerfil t WHERE t.dsPerfil = :dsPerfil"),
    @NamedQuery(name = "TblLogPerfil.findByIdUsuarioOperacao", query = "SELECT t FROM TblLogPerfil t WHERE t.idUsuarioOperacao = :idUsuarioOperacao")})
public class TblLogPerfil implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_log_perfil")
    private Integer idLogPerfil;
    @Basic(optional = false)
    @Column(name = "cd_operacao")
    private char cdOperacao;
    @Basic(optional = false)
    @Column(name = "ds_perfil")
    private String dsPerfil;
    @Column(name = "id_usuario_operacao")
    private Integer idUsuarioOperacao;
    @Column(name = "dt_operacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtOperacao;
    @JoinColumn(name = "cd_perfil", referencedColumnName = "cd_perfil")
    @ManyToOne(optional = false)
    private TblPerfil cdPerfil;

    public TblLogPerfil() {
    }

    public TblLogPerfil(Integer idLogPerfil) {
        this.idLogPerfil = idLogPerfil;
    }

    public TblLogPerfil(Integer idLogPerfil, char cdOperacao, String dsPerfil) {
        this.idLogPerfil = idLogPerfil;
        this.cdOperacao = cdOperacao;
        this.dsPerfil = dsPerfil;
    }

    public Date getDtOperacao() {
		return dtOperacao;
	}
    
    public void setDtOperacao(Date dtOperacao) {
		this.dtOperacao = dtOperacao;
	}
    
    public Integer getIdLogPerfil() {
        return idLogPerfil;
    }

    public void setIdLogPerfil(Integer idLogPerfil) {
        this.idLogPerfil = idLogPerfil;
    }

    public char getCdOperacao() {
        return cdOperacao;
    }

    public void setCdOperacao(char cdOperacao) {
        this.cdOperacao = cdOperacao;
    }

    public String getDsPerfil() {
        return dsPerfil;
    }

    public void setDsPerfil(String dsPerfil) {
        this.dsPerfil = dsPerfil;
    }

    public Integer getIdUsuarioOperacao() {
        return idUsuarioOperacao;
    }

    public void setIdUsuarioOperacao(Integer idUsuarioOperacao) {
        this.idUsuarioOperacao = idUsuarioOperacao;
    }

    public TblPerfil getCdPerfil() {
        return cdPerfil;
    }

    public void setCdPerfil(TblPerfil cdPerfil) {
        this.cdPerfil = cdPerfil;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLogPerfil != null ? idLogPerfil.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblLogPerfil)) {
            return false;
        }
        TblLogPerfil other = (TblLogPerfil) object;
        if ((this.idLogPerfil == null && other.idLogPerfil != null) || (this.idLogPerfil != null && !this.idLogPerfil.equals(other.idLogPerfil))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ged.cinbesa.entidade.TblLogPerfil[ idLogPerfil=" + idLogPerfil + " ]";
    }
    
}
