/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_log_usoatividade")
@NamedQueries({
    @NamedQuery(name = "TblLogUsoatividade.findAll", query = "SELECT t FROM TblLogUsoatividade t"),
    @NamedQuery(name = "TblLogUsoatividade.findByIdLogUsoatividade", query = "SELECT t FROM TblLogUsoatividade t WHERE t.idLogUsoatividade = :idLogUsoatividade"),
    @NamedQuery(name = "TblLogUsoatividade.findByIdTributo", query = "SELECT t FROM TblLogUsoatividade t WHERE t.idTributo = :idTributo"),
    @NamedQuery(name = "TblLogUsoatividade.findByIdUfir", query = "SELECT t FROM TblLogUsoatividade t WHERE t.idUfir = :idUfir"),
    @NamedQuery(name = "TblLogUsoatividade.findByIdEntidade", query = "SELECT t FROM TblLogUsoatividade t WHERE t.idEntidade = :idEntidade"),
    @NamedQuery(name = "TblLogUsoatividade.findByCdDesmembramento", query = "SELECT t FROM TblLogUsoatividade t WHERE t.cdDesmembramento = :cdDesmembramento"),
    @NamedQuery(name = "TblLogUsoatividade.findByNrDocumentoContabil", query = "SELECT t FROM TblLogUsoatividade t WHERE t.nrDocumentoContabil = :nrDocumentoContabil"),
    @NamedQuery(name = "TblLogUsoatividade.findByDsUsoatividade", query = "SELECT t FROM TblLogUsoatividade t WHERE t.dsUsoatividade = :dsUsoatividade"),
    @NamedQuery(name = "TblLogUsoatividade.findByNrQtdufir", query = "SELECT t FROM TblLogUsoatividade t WHERE t.nrQtdufir = :nrQtdufir"),
    @NamedQuery(name = "TblLogUsoatividade.findByNrAnoExercicio", query = "SELECT t FROM TblLogUsoatividade t WHERE t.nrAnoExercicio = :nrAnoExercicio"),
    @NamedQuery(name = "TblLogUsoatividade.findByNrSequencia", query = "SELECT t FROM TblLogUsoatividade t WHERE t.nrSequencia = :nrSequencia"),
    @NamedQuery(name = "TblLogUsoatividade.findByStTributoExclusivo", query = "SELECT t FROM TblLogUsoatividade t WHERE t.stTributoExclusivo = :stTributoExclusivo")})
public class TblLogUsoatividade implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_log_usoatividade")
    private Integer idLogUsoatividade;
    @Column(name = "id_tributo")
    private Integer idTributo;
    @Column(name = "id_ufir")
    private Integer idUfir;
    @Column(name = "id_entidade")
    private Integer idEntidade;
    @Column(name = "cd_desmembramento")
    private String cdDesmembramento;
    @Column(name = "nr_documento_contabil")
    private String nrDocumentoContabil;
    @Column(name = "ds_usoatividade")
    private String dsUsoatividade;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "nr_qtdufir")
    private BigDecimal nrQtdufir;
    @Column(name = "nr_ano_exercicio")
    private Short nrAnoExercicio;
    @Column(name = "nr_sequencia")
    private Integer nrSequencia;
    @Column(name = "st_tributo_exclusivo")
    private Character stTributoExclusivo;
    @Column(name = "cd_operacao")
    private Character cdOperacao;
    @Column(name = "dt_operacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtOperacao;
    @Column(name = "id_usuario_operacao")
    private Integer idUsuarioOperacao;
    @JoinColumn(name = "cd_usoatividade", referencedColumnName = "cd_usoatividade")
    @ManyToOne(optional = false)
    private TblUsoatividade cdUsoatividade;

    public TblLogUsoatividade() {
    }

    public TblLogUsoatividade(Integer idLogUsoatividade) {
        this.idLogUsoatividade = idLogUsoatividade;
    }

    public Integer getIdLogUsoatividade() {
        return idLogUsoatividade;
    }

    public void setIdLogUsoatividade(Integer idLogUsoatividade) {
        this.idLogUsoatividade = idLogUsoatividade;
    }

    public Integer getIdTributo() {
        return idTributo;
    }

    public void setIdTributo(Integer idTributo) {
        this.idTributo = idTributo;
    }

    public Integer getIdUfir() {
        return idUfir;
    }

    public void setIdUfir(Integer idUfir) {
        this.idUfir = idUfir;
    }

    public Integer getIdEntidade() {
        return idEntidade;
    }

    public void setIdEntidade(Integer idEntidade) {
        this.idEntidade = idEntidade;
    }

    public String getCdDesmembramento() {
        return cdDesmembramento;
    }

    public void setCdDesmembramento(String cdDesmembramento) {
        this.cdDesmembramento = cdDesmembramento;
    }

    public String getNrDocumentoContabil() {
        return nrDocumentoContabil;
    }

    public void setNrDocumentoContabil(String nrDocumentoContabil) {
        this.nrDocumentoContabil = nrDocumentoContabil;
    }

    public String getDsUsoatividade() {
        return dsUsoatividade;
    }

    public void setDsUsoatividade(String dsUsoatividade) {
        this.dsUsoatividade = dsUsoatividade;
    }

    public BigDecimal getNrQtdufir() {
        return nrQtdufir;
    }

    public void setNrQtdufir(BigDecimal nrQtdufir) {
        this.nrQtdufir = nrQtdufir;
    }

    public Short getNrAnoExercicio() {
        return nrAnoExercicio;
    }

    public void setNrAnoExercicio(Short nrAnoExercicio) {
        this.nrAnoExercicio = nrAnoExercicio;
    }

    public Integer getNrSequencia() {
        return nrSequencia;
    }

    public void setNrSequencia(Integer nrSequencia) {
        this.nrSequencia = nrSequencia;
    }

    public Character getStTributoExclusivo() {
        return stTributoExclusivo;
    }

    public void setStTributoExclusivo(Character stTributoExclusivo) {
        this.stTributoExclusivo = stTributoExclusivo;
    }

    public TblUsoatividade getCdUsoatividade() {
        return cdUsoatividade;
    }

    public void setCdUsoatividade(TblUsoatividade cdUsoatividade) {
        this.cdUsoatividade = cdUsoatividade;
    }
    
    public Character getCdOperacao() {
		return cdOperacao;
	}
    public void setCdOperacao(Character cdOperacao) {
		this.cdOperacao = cdOperacao;
	}
    public Date getDtOperacao() {
		return dtOperacao;
	}
    public void setDtOperacao(Date dtOperacao) {
		this.dtOperacao = dtOperacao;
	}
    public Integer getIdUsuarioOperacao() {
		return idUsuarioOperacao;
	}
    public void setIdUsuarioOperacao(Integer idUsuarioOperacao) {
		this.idUsuarioOperacao = idUsuarioOperacao;
	}

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLogUsoatividade != null ? idLogUsoatividade.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblLogUsoatividade)) {
            return false;
        }
        TblLogUsoatividade other = (TblLogUsoatividade) object;
        if ((this.idLogUsoatividade == null && other.idLogUsoatividade != null) || (this.idLogUsoatividade != null && !this.idLogUsoatividade.equals(other.idLogUsoatividade))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ged.cinbesa.entidade.TblLogUsoatividade[ idLogUsoatividade=" + idLogUsoatividade + " ]";
    }
    
}
