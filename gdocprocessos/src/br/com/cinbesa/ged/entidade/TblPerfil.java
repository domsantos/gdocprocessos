/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_perfil")
@NamedQueries({
    @NamedQuery(name = "TblPerfil.findAll", query = "SELECT t FROM TblPerfil t"),
    @NamedQuery(name = "TblPerfil.findByCdPerfil", query = "SELECT t FROM TblPerfil t WHERE t.cdPerfil = :cdPerfil"),
    @NamedQuery(name = "TblPerfil.findByDsPerfil", query = "SELECT t FROM TblPerfil t WHERE t.dsPerfil = :dsPerfil")})
public class TblPerfil implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cd_perfil")
    private Integer cdPerfil;
    @Basic(optional = false)
    @Column(name = "ds_perfil")
    private String dsPerfil;
    @JoinColumn(name = "id_entidade", referencedColumnName = "id_entidade")
    @ManyToOne
    private Entidade idEntidade;
    @ManyToMany(mappedBy = "tblPerfilList", fetch=FetchType.EAGER)
    private List<TblAplicacoes> tblAplicacoesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cdPerfil")
    private List<TblUsuario> tblUsuarioList;

    public TblPerfil() {
    }

    public TblPerfil(Integer cdPerfil) {
        this.cdPerfil = cdPerfil;
    }

    public TblPerfil(Integer cdPerfil, String dsPerfil) {
        this.cdPerfil = cdPerfil;
        this.dsPerfil = dsPerfil;
    }

    public Integer getCdPerfil() {
        return cdPerfil;
    }

    public void setCdPerfil(Integer cdPerfil) {
        this.cdPerfil = cdPerfil;
    }

    public String getDsPerfil() {
        return dsPerfil;
    }

    public void setDsPerfil(String dsPerfil) {
        this.dsPerfil = dsPerfil;
    }

    public Entidade getIdEntidade() {
		return idEntidade;
	}
    
    public void setIdEntidade(Entidade idEntidade) {
		this.idEntidade = idEntidade;
	}
    
    public List<TblAplicacoes> getTblAplicacoesList() {
        return tblAplicacoesList;
    }

    public void setTblAplicacoesList(List<TblAplicacoes> tblAplicacoesList) {
        this.tblAplicacoesList = tblAplicacoesList;
    }

    public List<TblUsuario> getTblUsuarioList() {
        return tblUsuarioList;
    }

    public void setTblUsuarioList(List<TblUsuario> tblUsuarioList) {
        this.tblUsuarioList = tblUsuarioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cdPerfil != null ? cdPerfil.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblPerfil)) {
            return false;
        }
        TblPerfil other = (TblPerfil) object;
        if ((this.cdPerfil == null && other.cdPerfil != null) || (this.cdPerfil != null && !this.cdPerfil.equals(other.cdPerfil))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.entidades.TblPerfil[ cdPerfil=" + cdPerfil + " ]";
    }
    
}
