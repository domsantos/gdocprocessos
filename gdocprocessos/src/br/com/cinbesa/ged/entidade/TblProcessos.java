/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "tbl_processos")
@NamedQueries({
    @NamedQuery(name = "TblProcessos.findAll", query = "SELECT t FROM TblProcessos t"),
    @NamedQuery(name = "TblProcessos.findByIdProcesso", query = "SELECT t FROM TblProcessos t WHERE t.idProcesso = :idProcesso"),
    @NamedQuery(name = "TblProcessos.findByNrAno", query = "SELECT t FROM TblProcessos t WHERE t.nrAno = :nrAno"),
    @NamedQuery(name = "TblProcessos.findByDtAberturaProcesso", query = "SELECT t FROM TblProcessos t WHERE t.dtAberturaProcesso = :dtAberturaProcesso"),
    @NamedQuery(name = "TblProcessos.findByStProcesso", query = "SELECT t FROM TblProcessos t WHERE t.stProcesso = :stProcesso"),
    @NamedQuery(name = "TblProcessos.findByStProcessoweb", query = "SELECT t FROM TblProcessos t WHERE t.stProcessoweb = :stProcessoweb"),
    @NamedQuery(name = "TblProcessos.findByDtEncerramento", query = "SELECT t FROM TblProcessos t WHERE t.dtEncerramento = :dtEncerramento"),
    @NamedQuery(name = "TblProcessos.findByDsEndereco", query = "SELECT t FROM TblProcessos t WHERE t.dsEndereco = :dsEndereco"),
    @NamedQuery(name = "TblProcessos.findByNrEndereco", query = "SELECT t FROM TblProcessos t WHERE t.nrEndereco = :nrEndereco"),
    @NamedQuery(name = "TblProcessos.findByDsComplemento", query = "SELECT t FROM TblProcessos t WHERE t.dsComplemento = :dsComplemento"),
    @NamedQuery(name = "TblProcessos.findByNrCep", query = "SELECT t FROM TblProcessos t WHERE t.nrCep = :nrCep"),
    @NamedQuery(name = "TblProcessos.findByDsBairro", query = "SELECT t FROM TblProcessos t WHERE t.dsBairro = :dsBairro"),
    @NamedQuery(name = "TblProcessos.findByNmCidade", query = "SELECT t FROM TblProcessos t WHERE t.nmCidade = :nmCidade"),
    @NamedQuery(name = "TblProcessos.findByDsUf", query = "SELECT t FROM TblProcessos t WHERE t.dsUf = :dsUf")})
public class TblProcessos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_processo")
    private Long idProcesso;
    @Column(name = "nr_processo")
    private BigInteger nrProcesso;
    @Basic(optional = false)
    @Column(name = "nr_ano")
    private short nrAno;
    @Basic(optional = false)
    @Column(name = "dt_abertura_processo")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtAberturaProcesso;
    @Basic(optional = false)
    @Column(name = "st_processo")
    private char stProcesso;
    @Basic(optional = false)
    @Column(name = "st_processoweb")
    private char stProcessoweb;
    @Lob
    @Column(name = "ds_processo")
    private String dsProcesso;
    @Lob
    @Column(name = "ds_encerramento")
    private String dsEncerramento;
    @Column(name = "dt_encerramento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtEncerramento;
    @Column(name = "ds_endereco")
    private String dsEndereco;
    @Column(name = "nr_endereco")
    private String nrEndereco;
    @Column(name = "ds_complemento")
    private String dsComplemento;
    @Column(name = "nr_cep")
    private String nrCep;
    @Column(name = "ds_bairro")
    private String dsBairro;
    @Column(name = "nm_cidade")
    private String nmCidade;
    @Column(name = "ds_uf")
    private String dsUf;
    @Column(name = "cd_tiporegistro",columnDefinition = "default 0")
    private int cdTipoRegistro;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProcesso")
    private List<TblDocumentosanexos> tblDocumentosanexosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProcesso")
    private List<TblValorDocumento> tblValorDocumentoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProcesso")
    private List<TblDam> tblDamList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProcesso")
    private List<TblTramites> tblTramitesList;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne
    private TblUsuario idUsuario;
    @JoinColumn(name = "cd_tipoprocesso", referencedColumnName = "cd_tipoprocesso")
    @ManyToOne(optional = false)
    private TblTipoprocesso cdTipoprocesso;
    @JoinColumn(name = "id_interessado", referencedColumnName = "id_interessado")
    @ManyToOne(optional = false)
    private TblInteressados idInteressado;
    @JoinColumn(name = "id_entidade", referencedColumnName = "id_entidade")
    @ManyToOne
    private Entidade idEntidade;

    public TblProcessos() {
    }

    public TblProcessos(Long idProcesso) {
        this.idProcesso = idProcesso;
    }

    public TblProcessos(Long idProcesso, short nrAno, Date dtAberturaProcesso, char stProcesso, char stProcessoweb) {
        this.idProcesso = idProcesso;
        this.nrAno = nrAno;
        this.dtAberturaProcesso = dtAberturaProcesso;
        this.stProcesso = stProcesso;
        this.stProcessoweb = stProcessoweb;
    }

    public Long getIdProcesso() {
        return idProcesso;
    }

    public void setIdProcesso(Long idProcesso) {
        this.idProcesso = idProcesso;
    }

    public short getNrAno() {
        return nrAno;
    }

    public void setNrAno(short nrAno) {
        this.nrAno = nrAno;
    }

    public Date getDtAberturaProcesso() {
        return dtAberturaProcesso;
    }

    public void setDtAberturaProcesso(Date dtAberturaProcesso) {
        this.dtAberturaProcesso = dtAberturaProcesso;
    }

    public char getStProcesso() {
        return stProcesso;
    }

    public void setStProcesso(char stProcesso) {
        this.stProcesso = stProcesso;
    }

    public char getStProcessoweb() {
        return stProcessoweb;
    }

    public void setStProcessoweb(char stProcessoweb) {
        this.stProcessoweb = stProcessoweb;
    }

    public String getDsProcesso() {
        return dsProcesso;
    }

    public void setDsProcesso(String dsProcesso) {
        this.dsProcesso = dsProcesso;
    }

    public String getDsEncerramento() {
        return dsEncerramento;
    }

    public void setDsEncerramento(String dsEncerramento) {
        this.dsEncerramento = dsEncerramento;
    }

    public Date getDtEncerramento() {
        return dtEncerramento;
    }

    public void setDtEncerramento(Date dtEncerramento) {
        this.dtEncerramento = dtEncerramento;
    }

    public String getDsEndereco() {
        return dsEndereco;
    }

    public void setDsEndereco(String dsEndereco) {
        this.dsEndereco = dsEndereco;
    }

    public String getNrEndereco() {
        return nrEndereco;
    }

    public void setNrEndereco(String nrEndereco) {
        this.nrEndereco = nrEndereco;
    }

    public String getDsComplemento() {
        return dsComplemento;
    }

    public void setDsComplemento(String dsComplemento) {
        this.dsComplemento = dsComplemento;
    }

    public String getNrCep() {
        return nrCep;
    }

    public void setNrCep(String nrCep) {
        this.nrCep = nrCep;
    }

    public String getDsBairro() {
        return dsBairro;
    }

    public void setDsBairro(String dsBairro) {
        this.dsBairro = dsBairro;
    }

    public String getNmCidade() {
        return nmCidade;
    }

    public void setNmCidade(String nmCidade) {
        this.nmCidade = nmCidade;
    }

    public String getDsUf() {
        return dsUf;
    }

    public void setDsUf(String dsUf) {
        this.dsUf = dsUf;
    }

    public List<TblDocumentosanexos> getTblDocumentosanexosList() {
        return tblDocumentosanexosList;
    }

    public void setTblDocumentosanexosList(List<TblDocumentosanexos> tblDocumentosanexosList) {
        this.tblDocumentosanexosList = tblDocumentosanexosList;
    }

    public List<TblValorDocumento> getTblValorDocumentoList() {
        return tblValorDocumentoList;
    }

    public void setTblValorDocumentoList(List<TblValorDocumento> tblValorDocumentoList) {
        this.tblValorDocumentoList = tblValorDocumentoList;
    }

    public List<TblDam> getTblDamList() {
        return tblDamList;
    }

    public void setTblDamList(List<TblDam> tblDamList) {
        this.tblDamList = tblDamList;
    }

    public List<TblTramites> getTblTramitesList() {
        return tblTramitesList;
    }

    public void setTblTramitesList(List<TblTramites> tblTramitesList) {
        this.tblTramitesList = tblTramitesList;
    }

    public TblUsuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(TblUsuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public TblTipoprocesso getCdTipoprocesso() {
        return cdTipoprocesso;
    }

    public void setCdTipoprocesso(TblTipoprocesso cdTipoprocesso) {
        this.cdTipoprocesso = cdTipoprocesso;
    }

    public TblInteressados getIdInteressado() {
        return idInteressado;
    }

    public void setIdInteressado(TblInteressados idInteressado) {
        this.idInteressado = idInteressado;
    }

    public Entidade getIdEntidade() {
        return idEntidade;
    }

    public void setIdEntidade(Entidade idEntidade) {
        this.idEntidade = idEntidade;
    }
    
    public BigInteger getNrProcesso() {
		return nrProcesso;
	}
    
    public void setNrProcesso(BigInteger nrProcesso) {
		this.nrProcesso = nrProcesso;
	}

    public int getCdTipoRegistro() {
		return cdTipoRegistro;
	}

	public void setCdTipoRegistro(int cdTipoRegistro) {
		this.cdTipoRegistro = cdTipoRegistro;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (idProcesso != null ? idProcesso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblProcessos)) {
            return false;
        }
        TblProcessos other = (TblProcessos) object;
        if ((this.idProcesso == null && other.idProcesso != null) || (this.idProcesso != null && !this.idProcesso.equals(other.idProcesso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ged.cinbesa.entidade.TblProcessos[ idProcesso=" + idProcesso + " ]";
    }
    
}
