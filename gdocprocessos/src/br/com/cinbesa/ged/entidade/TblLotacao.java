/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_lotacao")
@NamedQueries({
    @NamedQuery(name = "TblLotacao.findAll", query = "SELECT t FROM TblLotacao t"),
    @NamedQuery(name = "TblLotacao.findByCdLotacao", query = "SELECT t FROM TblLotacao t WHERE t.cdLotacao = :cdLotacao"),
    @NamedQuery(name = "TblLotacao.findByStLotacao", query = "SELECT t FROM TblLotacao t WHERE t.stLotacao = :stLotacao"),
    @NamedQuery(name = "TblLotacao.findByDtLotacao", query = "SELECT t FROM TblLotacao t WHERE t.dtLotacao = :dtLotacao")})
public class TblLotacao implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cd_lotacao")
    private Integer cdLotacao;
    @Basic(optional = false)
    @Column(name = "st_lotacao")
    private char stLotacao;
    @Column(name = "dt_lotacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtLotacao;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false)
    private TblUsuario idUsuario;
    @JoinColumn(name = "cd_setor", referencedColumnName = "cd_setor")
    @ManyToOne(optional = false)
    private TblSetor cdSetor;

    public TblLotacao() {
    }

    public TblLotacao(Integer cdLotacao) {
        this.cdLotacao = cdLotacao;
    }

    public TblLotacao(Integer cdLotacao, char stLotacao) {
        this.cdLotacao = cdLotacao;
        this.stLotacao = stLotacao;
    }

    public Integer getCdLotacao() {
        return cdLotacao;
    }

    public void setCdLotacao(Integer cdLotacao) {
        this.cdLotacao = cdLotacao;
    }

    public char getStLotacao() {
        return stLotacao;
    }

    public void setStLotacao(char stLotacao) {
        this.stLotacao = stLotacao;
    }

    public Date getDtLotacao() {
        return dtLotacao;
    }

    public void setDtLotacao(Date dtLotacao) {
        this.dtLotacao = dtLotacao;
    }

    public TblUsuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(TblUsuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public TblSetor getCdSetor() {
        return cdSetor;
    }

    public void setCdSetor(TblSetor cdSetor) {
        this.cdSetor = cdSetor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cdLotacao != null ? cdLotacao.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblLotacao)) {
            return false;
        }
        TblLotacao other = (TblLotacao) object;
        if ((this.cdLotacao == null && other.cdLotacao != null) || (this.cdLotacao != null && !this.cdLotacao.equals(other.cdLotacao))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ged.cinbesa.entidade.TblLotacao[ cdLotacao=" + cdLotacao + " ]";
    }
    
}
