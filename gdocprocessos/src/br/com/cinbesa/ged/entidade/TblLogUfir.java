/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_log_ufir")
@NamedQueries({
    @NamedQuery(name = "TblLogUfir.findAll", query = "SELECT t FROM TblLogUfir t"),
    @NamedQuery(name = "TblLogUfir.findByIdLogUfir", query = "SELECT t FROM TblLogUfir t WHERE t.idLogUfir = :idLogUfir"),
    @NamedQuery(name = "TblLogUfir.findByNrAno", query = "SELECT t FROM TblLogUfir t WHERE t.nrAno = :nrAno"),
    @NamedQuery(name = "TblLogUfir.findByVlUfir", query = "SELECT t FROM TblLogUfir t WHERE t.vlUfir = :vlUfir"),
    @NamedQuery(name = "TblLogUfir.findByCdOperacao", query = "SELECT t FROM TblLogUfir t WHERE t.cdOperacao = :cdOperacao"),
    @NamedQuery(name = "TblLogUfir.findByDtOperacao", query = "SELECT t FROM TblLogUfir t WHERE t.dtOperacao = :dtOperacao"),
    @NamedQuery(name = "TblLogUfir.findByIdUsuarioOperacao", query = "SELECT t FROM TblLogUfir t WHERE t.idUsuarioOperacao = :idUsuarioOperacao")})
public class TblLogUfir implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_log_ufir")
    private Integer idLogUfir;
    @Column(name = "nr_ano")
    private Short nrAno;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "vl_ufir")
    private BigDecimal vlUfir;
    @Column(name = "cd_operacao")
    private Character cdOperacao;
    @Column(name = "dt_operacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtOperacao;
    @Column(name = "id_usuario_operacao")
    private Integer idUsuarioOperacao;
    @JoinColumn(name = "id_ufir", referencedColumnName = "id_ufir")
    @ManyToOne(optional = false)
    private TblUfir idUfir;

    public TblLogUfir() {
    }

    public TblLogUfir(Integer idLogUfir) {
        this.idLogUfir = idLogUfir;
    }

    public Integer getIdLogUfir() {
        return idLogUfir;
    }

    public void setIdLogUfir(Integer idLogUfir) {
        this.idLogUfir = idLogUfir;
    }

    public Short getNrAno() {
        return nrAno;
    }

    public void setNrAno(Short nrAno) {
        this.nrAno = nrAno;
    }

    public BigDecimal getVlUfir() {
        return vlUfir;
    }

    public void setVlUfir(BigDecimal vlUfir) {
        this.vlUfir = vlUfir;
    }

    public Character getCdOperacao() {
        return cdOperacao;
    }

    public void setCdOperacao(Character cdOperacao) {
        this.cdOperacao = cdOperacao;
    }

    public Date getDtOperacao() {
        return dtOperacao;
    }

    public void setDtOperacao(Date dtOperacao) {
        this.dtOperacao = dtOperacao;
    }

    public Integer getIdUsuarioOperacao() {
        return idUsuarioOperacao;
    }

    public void setIdUsuarioOperacao(Integer idUsuarioOperacao) {
        this.idUsuarioOperacao = idUsuarioOperacao;
    }

    public TblUfir getIdUfir() {
        return idUfir;
    }

    public void setIdUfir(TblUfir idUfir) {
        this.idUfir = idUfir;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLogUfir != null ? idLogUfir.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblLogUfir)) {
            return false;
        }
        TblLogUfir other = (TblLogUfir) object;
        if ((this.idLogUfir == null && other.idLogUfir != null) || (this.idLogUfir != null && !this.idLogUfir.equals(other.idLogUfir))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ged.cinbesa.entidade.TblLogUfir[ idLogUfir=" + idLogUfir + " ]";
    }
    
}
