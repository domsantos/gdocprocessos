/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_ufir")
@NamedQueries({
    @NamedQuery(name = "TblUfir.findAll", query = "SELECT t FROM TblUfir t"),
    @NamedQuery(name = "TblUfir.findByIdUfir", query = "SELECT t FROM TblUfir t WHERE t.idUfir = :idUfir"),
    @NamedQuery(name = "TblUfir.findByNrAno", query = "SELECT t FROM TblUfir t WHERE t.nrAno = :nrAno"),
    @NamedQuery(name = "TblUfir.findByVlUfir", query = "SELECT t FROM TblUfir t WHERE t.vlUfir = :vlUfir")})
public class TblUfir implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_ufir")
    private Integer idUfir;
    @Basic(optional = false)
    @Column(name = "nr_ano")
    private short nrAno;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "vl_ufir")
    private BigDecimal vlUfir;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUfir")
    private List<TblValorDocumento> tblValorDocumentoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUfir")
    private List<TblUsoatividade> tblUsoatividadeList;

    public TblUfir() {
    }

    public TblUfir(Integer idUfir) {
        this.idUfir = idUfir;
    }

    public TblUfir(Integer idUfir, short nrAno) {
        this.idUfir = idUfir;
        this.nrAno = nrAno;
    }

    public Integer getIdUfir() {
        return idUfir;
    }

    public void setIdUfir(Integer idUfir) {
        this.idUfir = idUfir;
    }

    public short getNrAno() {
        return nrAno;
    }

    public void setNrAno(short nrAno) {
        this.nrAno = nrAno;
    }

    public BigDecimal getVlUfir() {
        return vlUfir;
    }

    public void setVlUfir(BigDecimal vlUfir) {
        this.vlUfir = vlUfir;
    }

    public List<TblValorDocumento> getTblValorDocumentoList() {
        return tblValorDocumentoList;
    }

    public void setTblValorDocumentoList(List<TblValorDocumento> tblValorDocumentoList) {
        this.tblValorDocumentoList = tblValorDocumentoList;
    }

    public List<TblUsoatividade> getTblUsoatividadeList() {
        return tblUsoatividadeList;
    }

    public void setTblUsoatividadeList(List<TblUsoatividade> tblUsoatividadeList) {
        this.tblUsoatividadeList = tblUsoatividadeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUfir != null ? idUfir.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblUfir)) {
            return false;
        }
        TblUfir other = (TblUfir) object;
        if ((this.idUfir == null && other.idUfir != null) || (this.idUfir != null && !this.idUfir.equals(other.idUfir))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ged.cinbesa.entidade.TblUfir[ idUfir=" + idUfir + " ]";
    }
    
}
