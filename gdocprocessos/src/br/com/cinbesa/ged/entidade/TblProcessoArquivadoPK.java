/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author fernandosantos
 */
@Embeddable
public class TblProcessoArquivadoPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "id_processo")
    private long idProcesso;
    @Basic(optional = false)
    @Column(name = "id_enderecoarquivamento")
    private int idEnderecoarquivamento;

    public TblProcessoArquivadoPK() {
    }

    public TblProcessoArquivadoPK(long idProcesso, int idEnderecoarquivamento) {
        this.idProcesso = idProcesso;
        this.idEnderecoarquivamento = idEnderecoarquivamento;
    }

    public long getIdProcesso() {
        return idProcesso;
    }

    public void setIdProcesso(long idProcesso) {
        this.idProcesso = idProcesso;
    }

    public int getIdEnderecoarquivamento() {
        return idEnderecoarquivamento;
    }

    public void setIdEnderecoarquivamento(int idEnderecoarquivamento) {
        this.idEnderecoarquivamento = idEnderecoarquivamento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idProcesso;
        hash += (int) idEnderecoarquivamento;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblProcessoArquivadoPK)) {
            return false;
        }
        TblProcessoArquivadoPK other = (TblProcessoArquivadoPK) object;
        if (this.idProcesso != other.idProcesso) {
            return false;
        }
        if (this.idEnderecoarquivamento != other.idEnderecoarquivamento) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.cinbesa.ged.entidade.TblProcessoArquivadoPK[ idProcesso=" + idProcesso + ", idEnderecoarquivamento=" + idEnderecoarquivamento + " ]";
    }
    
}
