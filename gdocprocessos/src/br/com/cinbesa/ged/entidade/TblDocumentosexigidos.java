/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_documentosexigidos")
@NamedQueries({
    @NamedQuery(name = "TblDocumentosexigidos.findAll", query = "SELECT t FROM TblDocumentosexigidos t"),
    @NamedQuery(name = "TblDocumentosexigidos.findByCdDocumentoexigido", query = "SELECT t FROM TblDocumentosexigidos t WHERE t.cdDocumentoexigido = :cdDocumentoexigido"),
    @NamedQuery(name = "TblDocumentosexigidos.findByNmDocumentoexigido", query = "SELECT t FROM TblDocumentosexigidos t WHERE t.nmDocumentoexigido = :nmDocumentoexigido"),
    @NamedQuery(name = "TblDocumentosexigidos.findByDsDocumentoexigido", query = "SELECT t FROM TblDocumentosexigidos t WHERE t.dsDocumentoexigido = :dsDocumentoexigido")})
public class TblDocumentosexigidos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cd_documentoexigido")
    private Integer cdDocumentoexigido;
    @Basic(optional = false)
    @Column(name = "nm_documentoexigido")
    private String nmDocumentoexigido;
    @Column(name = "ds_documentoexigido")
    private String dsDocumentoexigido;
    @JoinTable(name = "tbl_documentoexigido_rota", joinColumns = {
        @JoinColumn(name = "cd_documentoexigido", referencedColumnName = "cd_documentoexigido")}, inverseJoinColumns = {
        @JoinColumn(name = "cd_rota", referencedColumnName = "cd_rota")})
    @ManyToMany
    private List<TblRotas> tblRotasList;

    public TblDocumentosexigidos() {
    }

    public TblDocumentosexigidos(Integer cdDocumentoexigido) {
        this.cdDocumentoexigido = cdDocumentoexigido;
    }

    public TblDocumentosexigidos(Integer cdDocumentoexigido, String nmDocumentoexigido) {
        this.cdDocumentoexigido = cdDocumentoexigido;
        this.nmDocumentoexigido = nmDocumentoexigido;
    }

    public Integer getCdDocumentoexigido() {
        return cdDocumentoexigido;
    }

    public void setCdDocumentoexigido(Integer cdDocumentoexigido) {
        this.cdDocumentoexigido = cdDocumentoexigido;
    }

    public String getNmDocumentoexigido() {
        return nmDocumentoexigido;
    }

    public void setNmDocumentoexigido(String nmDocumentoexigido) {
        this.nmDocumentoexigido = nmDocumentoexigido;
    }

    public String getDsDocumentoexigido() {
        return dsDocumentoexigido;
    }

    public void setDsDocumentoexigido(String dsDocumentoexigido) {
        this.dsDocumentoexigido = dsDocumentoexigido;
    }

    public List<TblRotas> getTblRotasList() {
        return tblRotasList;
    }

    public void setTblRotasList(List<TblRotas> tblRotasList) {
        this.tblRotasList = tblRotasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cdDocumentoexigido != null ? cdDocumentoexigido.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblDocumentosexigidos)) {
            return false;
        }
        TblDocumentosexigidos other = (TblDocumentosexigidos) object;
        if ((this.cdDocumentoexigido == null && other.cdDocumentoexigido != null) || (this.cdDocumentoexigido != null && !this.cdDocumentoexigido.equals(other.cdDocumentoexigido))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.entidades.TblDocumentosexigidos[ cdDocumentoexigido=" + cdDocumentoexigido + " ]";
    }
    
}
