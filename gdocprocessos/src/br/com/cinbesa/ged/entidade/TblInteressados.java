/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_interessados")
@NamedQueries({
    @NamedQuery(name = "TblInteressados.findAll", query = "SELECT t FROM TblInteressados t"),
    @NamedQuery(name = "TblInteressados.findByIdInteressado", query = "SELECT t FROM TblInteressados t WHERE t.idInteressado = :idInteressado"),
    @NamedQuery(name = "TblInteressados.findByNrCpfCnpj", query = "SELECT t FROM TblInteressados t WHERE t.nrCpfCnpj = :nrCpfCnpj"),
    @NamedQuery(name = "TblInteressados.findByNmInteressado", query = "SELECT t FROM TblInteressados t WHERE t.nmInteressado LIKE :nmInteressado"),
    @NamedQuery(name = "TblInteressados.findByNrMatricula", query = "SELECT t FROM TblInteressados t WHERE t.nrMatricula = :nrMatricula"),
    @NamedQuery(name = "TblInteressados.findByDsEndereco", query = "SELECT t FROM TblInteressados t WHERE t.dsEndereco = :dsEndereco"),
    @NamedQuery(name = "TblInteressados.findByDsEmail", query = "SELECT t FROM TblInteressados t WHERE t.dsEmail = :dsEmail")})
public class TblInteressados implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_interessado")
    private Integer idInteressado;
    @Basic(optional = false)
    @Column(name = "nr_cpf_cnpj")
    private String nrCpfCnpj;
    @Basic(optional = false)
    @Column(name = "nm_interessado")
    private String nmInteressado;
    @Column(name = "nr_matricula")
    private String nrMatricula;
    @Basic(optional = false)
    @Column(name = "ds_endereco")
    private String dsEndereco;
    @Column(name = "ds_email")
    private String dsEmail;
    @Column(name = "vl_senha")
    private String vlSenha;
    @Column(name = "nr_endereco")
    private String nrEndereco;
    @Column(name = "ds_complemento")
    private String dsComplemento;
    @Column(name = "nr_cep")
    private String nrCep;
    @Column(name = "ds_bairro")
    private String dsBairro;
    @Column(name = "nm_cidade")
    private String nmCidade;
    @Column(name = "ds_uf")
    private String dsUf;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idInteressado")
    private List<TblProcessos> tblProcessosList;

    public TblInteressados() {
    }

    public TblInteressados(Integer idInteressado) {
        this.idInteressado = idInteressado;
    }

    public TblInteressados(Integer idInteressado, String nrCpfCnpj, String nmInteressado, String dsEndereco, String vlSenha) {
        this.idInteressado = idInteressado;
        this.nrCpfCnpj = nrCpfCnpj;
        this.nmInteressado = nmInteressado;
        this.dsEndereco = dsEndereco;
        this.vlSenha = vlSenha;
    }

    public Integer getIdInteressado() {
        return idInteressado;
    }

    public void setIdInteressado(Integer idInteressado) {
        this.idInteressado = idInteressado;
    }

    public String getNrCpfCnpj() {
        return nrCpfCnpj;
    }

    public void setNrCpfCnpj(String nrCpfCnpj) {
        this.nrCpfCnpj = nrCpfCnpj;
    }

    public String getNmInteressado() {
        return nmInteressado;
    }

    public void setNmInteressado(String nmInteressado) {
        this.nmInteressado = nmInteressado;
    }

    public String getNrMatricula() {
        return nrMatricula;
    }

    public void setNrMatricula(String nrMatricula) {
        this.nrMatricula = nrMatricula;
    }

    public String getDsEndereco() {
        return dsEndereco;
    }

    public void setDsEndereco(String dsEndereco) {
        this.dsEndereco = dsEndereco;
    }

    public String getDsEmail() {
        return dsEmail;
    }

    public void setDsEmail(String dsEmail) {
        this.dsEmail = dsEmail;
    }

    public String getVlSenha() {
        return vlSenha;
    }

    public void setVlSenha(String vlSenha) {
        this.vlSenha = vlSenha;
    }

    public String getNrEndereco() {
        return nrEndereco;
    }

    public void setNrEndereco(String nrEndereco) {
        this.nrEndereco = nrEndereco;
    }

    public String getDsComplemento() {
        return dsComplemento;
    }

    public void setDsComplemento(String dsComplemento) {
        this.dsComplemento = dsComplemento;
    }

    public String getNrCep() {
        return nrCep;
    }

    public void setNrCep(String nrCep) {
        this.nrCep = nrCep;
    }

    public String getDsBairro() {
        return dsBairro;
    }

    public void setDsBairro(String dsBairro) {
        this.dsBairro = dsBairro;
    }

    public String getNmCidade() {
        return nmCidade;
    }

    public void setNmCidade(String nmCidade) {
        this.nmCidade = nmCidade;
    }

    public String getDsUf() {
        return dsUf;
    }

    public void setDsUf(String dsUf) {
        this.dsUf = dsUf;
    }

    public List<TblProcessos> getTblProcessosList() {
        return tblProcessosList;
    }

    public void setTblProcessosList(List<TblProcessos> tblProcessosList) {
        this.tblProcessosList = tblProcessosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idInteressado != null ? idInteressado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblInteressados)) {
            return false;
        }
        TblInteressados other = (TblInteressados) object;
        if ((this.idInteressado == null && other.idInteressado != null) || (this.idInteressado != null && !this.idInteressado.equals(other.idInteressado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.entidades.TblInteressados[ idInteressado=" + idInteressado + " ]";
    }
    
}
