/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_ipcae")
@NamedQueries({
    @NamedQuery(name = "TblIpcae.findAll", query = "SELECT t FROM TblIpcae t"),
    @NamedQuery(name = "TblIpcae.findByIdIpcae", query = "SELECT t FROM TblIpcae t WHERE t.idIpcae = :idIpcae"),
    @NamedQuery(name = "TblIpcae.findByNrAnoIpcae", query = "SELECT t FROM TblIpcae t WHERE t.nrAnoIpcae = :nrAnoIpcae"),
    @NamedQuery(name = "TblIpcae.findByVlIpcae", query = "SELECT t FROM TblIpcae t WHERE t.vlIpcae = :vlIpcae")})
public class TblIpcae implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_ipcae")
    private Integer idIpcae;
    @Basic(optional = false)
    @Column(name = "nr_ano_ipcae")
    private short nrAnoIpcae;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "vl_ipcae")
    private BigDecimal vlIpcae;

    public TblIpcae() {
    }

    public TblIpcae(Integer idIpcae) {
        this.idIpcae = idIpcae;
    }

    public TblIpcae(Integer idIpcae, short nrAnoIpcae) {
        this.idIpcae = idIpcae;
        this.nrAnoIpcae = nrAnoIpcae;
    }

    public Integer getIdIpcae() {
        return idIpcae;
    }

    public void setIdIpcae(Integer idIpcae) {
        this.idIpcae = idIpcae;
    }

    public short getNrAnoIpcae() {
        return nrAnoIpcae;
    }

    public void setNrAnoIpcae(short nrAnoIpcae) {
        this.nrAnoIpcae = nrAnoIpcae;
    }

    public BigDecimal getVlIpcae() {
        return vlIpcae;
    }

    public void setVlIpcae(BigDecimal vlIpcae) {
        this.vlIpcae = vlIpcae;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idIpcae != null ? idIpcae.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblIpcae)) {
            return false;
        }
        TblIpcae other = (TblIpcae) object;
        if ((this.idIpcae == null && other.idIpcae != null) || (this.idIpcae != null && !this.idIpcae.equals(other.idIpcae))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ged.cinbesa.entidade.TblIpcae[ idIpcae=" + idIpcae + " ]";
    }
    
}
