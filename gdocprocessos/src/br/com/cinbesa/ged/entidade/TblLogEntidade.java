/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_log_entidade")
@NamedQueries({
    @NamedQuery(name = "TblLogEntidade.findAll", query = "SELECT t FROM TblLogEntidade t"),
    @NamedQuery(name = "TblLogEntidade.findByIdLogEntidade", query = "SELECT t FROM TblLogEntidade t WHERE t.idLogEntidade = :idLogEntidade"),
    @NamedQuery(name = "TblLogEntidade.findByDsEntidade", query = "SELECT t FROM TblLogEntidade t WHERE t.dsEntidade = :dsEntidade"),
    @NamedQuery(name = "TblLogEntidade.findByCdEntidade", query = "SELECT t FROM TblLogEntidade t WHERE t.cdEntidade = :cdEntidade"),
    @NamedQuery(name = "TblLogEntidade.findByDsEntidadeExtenso", query = "SELECT t FROM TblLogEntidade t WHERE t.dsEntidadeExtenso = :dsEntidadeExtenso"),
    @NamedQuery(name = "TblLogEntidade.findByDtOperacao", query = "SELECT t FROM TblLogEntidade t WHERE t.dtOperacao = :dtOperacao"),
    @NamedQuery(name = "TblLogEntidade.findByCdOperacao", query = "SELECT t FROM TblLogEntidade t WHERE t.cdOperacao = :cdOperacao"),
    @NamedQuery(name = "TblLogEntidade.findByIdUsuarioOperacao", query = "SELECT t FROM TblLogEntidade t WHERE t.idUsuarioOperacao = :idUsuarioOperacao")})
public class TblLogEntidade implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_log_entidade")
    private Integer idLogEntidade;
    @Column(name = "ds_entidade")
    private String dsEntidade;
    @Column(name = "cd_entidade")
    private String cdEntidade;
    @Column(name = "ds_entidade_extenso")
    private String dsEntidadeExtenso;
    @Column(name = "dt_operacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtOperacao;
    @Column(name = "cd_operacao")
    private Character cdOperacao;
    @Column(name = "id_usuario_operacao")
    private Integer idUsuarioOperacao;
    @JoinColumn(name = "id_entidade", referencedColumnName = "id_entidade")
    @ManyToOne(optional = false)
    private Entidade idEntidade;

    public TblLogEntidade() {
    }

    public TblLogEntidade(Integer idLogEntidade) {
        this.idLogEntidade = idLogEntidade;
    }

    public Integer getIdLogEntidade() {
        return idLogEntidade;
    }

    public void setIdLogEntidade(Integer idLogEntidade) {
        this.idLogEntidade = idLogEntidade;
    }

    public String getDsEntidade() {
        return dsEntidade;
    }

    public void setDsEntidade(String dsEntidade) {
        this.dsEntidade = dsEntidade;
    }

    public String getCdEntidade() {
        return cdEntidade;
    }

    public void setCdEntidade(String cdEntidade) {
        this.cdEntidade = cdEntidade;
    }

    public String getDsEntidadeExtenso() {
        return dsEntidadeExtenso;
    }

    public void setDsEntidadeExtenso(String dsEntidadeExtenso) {
        this.dsEntidadeExtenso = dsEntidadeExtenso;
    }

    public Date getDtOperacao() {
        return dtOperacao;
    }

    public void setDtOperacao(Date dtOperacao) {
        this.dtOperacao = dtOperacao;
    }

    public Character getCdOperacao() {
        return cdOperacao;
    }

    public void setCdOperacao(Character cdOperacao) {
        this.cdOperacao = cdOperacao;
    }

    public Integer getIdUsuarioOperacao() {
        return idUsuarioOperacao;
    }

    public void setIdUsuarioOperacao(Integer idUsuarioOperacao) {
        this.idUsuarioOperacao = idUsuarioOperacao;
    }

    public Entidade getIdEntidade() {
        return idEntidade;
    }

    public void setIdEntidade(Entidade idEntidade) {
        this.idEntidade = idEntidade;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLogEntidade != null ? idLogEntidade.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblLogEntidade)) {
            return false;
        }
        TblLogEntidade other = (TblLogEntidade) object;
        if ((this.idLogEntidade == null && other.idLogEntidade != null) || (this.idLogEntidade != null && !this.idLogEntidade.equals(other.idLogEntidade))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ged.cinbesa.entidade.TblLogEntidade[ idLogEntidade=" + idLogEntidade + " ]";
    }
    
}
