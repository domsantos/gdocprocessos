/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_log_ipcae")
@NamedQueries({
    @NamedQuery(name = "TblLogIpcae.findAll", query = "SELECT t FROM TblLogIpcae t"),
    @NamedQuery(name = "TblLogIpcae.findByIdLogIpcae", query = "SELECT t FROM TblLogIpcae t WHERE t.idLogIpcae = :idLogIpcae"),
    @NamedQuery(name = "TblLogIpcae.findByNrAnoIpcae", query = "SELECT t FROM TblLogIpcae t WHERE t.nrAnoIpcae = :nrAnoIpcae"),
    @NamedQuery(name = "TblLogIpcae.findByVlIpcae", query = "SELECT t FROM TblLogIpcae t WHERE t.vlIpcae = :vlIpcae"),
    @NamedQuery(name = "TblLogIpcae.findByDtOperacao", query = "SELECT t FROM TblLogIpcae t WHERE t.dtOperacao = :dtOperacao"),
    @NamedQuery(name = "TblLogIpcae.findByCdOperacao", query = "SELECT t FROM TblLogIpcae t WHERE t.cdOperacao = :cdOperacao"),
    @NamedQuery(name = "TblLogIpcae.findByIdUsuarioOperacao", query = "SELECT t FROM TblLogIpcae t WHERE t.idUsuarioOperacao = :idUsuarioOperacao")})
public class TblLogIpcae implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_log_ipcae")
    private Integer idLogIpcae;
    @Column(name = "nr_ano_ipcae")
    private Short nrAnoIpcae;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "vl_ipcae")
    private BigDecimal vlIpcae;
    @Column(name = "dt_operacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtOperacao;
    @Column(name = "cd_operacao")
    private Character cdOperacao;
    @Column(name = "id_usuario_operacao")
    private int idUsuarioOperacao;
    @JoinColumn(name = "id_ipcae", referencedColumnName = "id_ipcae")
    @ManyToOne(optional = false)
    private TblIpcae idIpcae;

    public TblLogIpcae() {
    }

    public TblLogIpcae(Integer idLogIpcae) {
        this.idLogIpcae = idLogIpcae;
    }

    public Integer getIdLogIpcae() {
        return idLogIpcae;
    }

    public void setIdLogIpcae(Integer idLogIpcae) {
        this.idLogIpcae = idLogIpcae;
    }

    public Short getNrAnoIpcae() {
        return nrAnoIpcae;
    }

    public void setNrAnoIpcae(Short nrAnoIpcae) {
        this.nrAnoIpcae = nrAnoIpcae;
    }

    public BigDecimal getVlIpcae() {
        return vlIpcae;
    }

    public void setVlIpcae(BigDecimal vlIpcae) {
        this.vlIpcae = vlIpcae;
    }

    public Date getDtOperacao() {
        return dtOperacao;
    }

    public void setDtOperacao(Date dtOperacao) {
        this.dtOperacao = dtOperacao;
    }

    public Character getCdOperacao() {
        return cdOperacao;
    }

    public void setCdOperacao(Character cdOperacao) {
        this.cdOperacao = cdOperacao;
    }

    public int getIdUsuarioOperacao() {
        return idUsuarioOperacao;
    }

    public void setIdUsuarioOperacao(int idUsuarioOperacao) {
        this.idUsuarioOperacao = idUsuarioOperacao;
    }

    public TblIpcae getIdIpcae() {
        return idIpcae;
    }

    public void setIdIpcae(TblIpcae idIpcae) {
        this.idIpcae = idIpcae;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLogIpcae != null ? idLogIpcae.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblLogIpcae)) {
            return false;
        }
        TblLogIpcae other = (TblLogIpcae) object;
        if ((this.idLogIpcae == null && other.idLogIpcae != null) || (this.idLogIpcae != null && !this.idLogIpcae.equals(other.idLogIpcae))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ged.cinbesa.entidade.TblLogIpcae[ idLogIpcae=" + idLogIpcae + " ]";
    }
    
}
