/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_log_setor")
@NamedQueries({
    @NamedQuery(name = "TblLogSetor.findAll", query = "SELECT t FROM TblLogSetor t"),
    @NamedQuery(name = "TblLogSetor.findByIdLogSetor", query = "SELECT t FROM TblLogSetor t WHERE t.idLogSetor = :idLogSetor"),
    @NamedQuery(name = "TblLogSetor.findByNmSetor", query = "SELECT t FROM TblLogSetor t WHERE t.nmSetor = :nmSetor"),
    @NamedQuery(name = "TblLogSetor.findByDsSetor", query = "SELECT t FROM TblLogSetor t WHERE t.dsSetor = :dsSetor"),
    @NamedQuery(name = "TblLogSetor.findByIdEntidade", query = "SELECT t FROM TblLogSetor t WHERE t.idEntidade = :idEntidade"),
    @NamedQuery(name = "TblLogSetor.findByDtOperacao", query = "SELECT t FROM TblLogSetor t WHERE t.dtOperacao = :dtOperacao"),
    @NamedQuery(name = "TblLogSetor.findByCdOperacao", query = "SELECT t FROM TblLogSetor t WHERE t.cdOperacao = :cdOperacao"),
    @NamedQuery(name = "TblLogSetor.findByIdUsuarioOperacao", query = "SELECT t FROM TblLogSetor t WHERE t.idUsuarioOperacao = :idUsuarioOperacao")})
public class TblLogSetor implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_log_setor")
    private Integer idLogSetor;
    @Column(name = "nm_setor")
    private String nmSetor;
    @Column(name = "ds_setor")
    private String dsSetor;
    @Column(name = "id_entidade")
    private Integer idEntidade;
    @Column(name = "dt_operacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtOperacao;
    @Column(name = "cd_operacao")
    private Character cdOperacao;
    @Column(name = "id_usuario_operacao")
    private Integer idUsuarioOperacao;
    @JoinColumn(name = "cd_setor", referencedColumnName = "cd_setor")
    @ManyToOne(optional = false)
    private TblSetor cdSetor;

    public TblLogSetor() {
    }

    public TblLogSetor(Integer idLogSetor) {
        this.idLogSetor = idLogSetor;
    }

    public Integer getIdLogSetor() {
        return idLogSetor;
    }

    public void setIdLogSetor(Integer idLogSetor) {
        this.idLogSetor = idLogSetor;
    }

    public String getNmSetor() {
        return nmSetor;
    }

    public void setNmSetor(String nmSetor) {
        this.nmSetor = nmSetor;
    }

    public String getDsSetor() {
        return dsSetor;
    }

    public void setDsSetor(String dsSetor) {
        this.dsSetor = dsSetor;
    }

    public Integer getIdEntidade() {
        return idEntidade;
    }

    public void setIdEntidade(Integer idEntidade) {
        this.idEntidade = idEntidade;
    }

    public Date getDtOperacao() {
        return dtOperacao;
    }

    public void setDtOperacao(Date dtOperacao) {
        this.dtOperacao = dtOperacao;
    }

    public Character getCdOperacao() {
        return cdOperacao;
    }

    public void setCdOperacao(Character cdOperacao) {
        this.cdOperacao = cdOperacao;
    }

    public Integer getIdUsuarioOperacao() {
        return idUsuarioOperacao;
    }

    public void setIdUsuarioOperacao(Integer idUsuarioOperacao) {
        this.idUsuarioOperacao = idUsuarioOperacao;
    }

    public TblSetor getCdSetor() {
        return cdSetor;
    }

    public void setCdSetor(TblSetor cdSetor) {
        this.cdSetor = cdSetor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLogSetor != null ? idLogSetor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblLogSetor)) {
            return false;
        }
        TblLogSetor other = (TblLogSetor) object;
        if ((this.idLogSetor == null && other.idLogSetor != null) || (this.idLogSetor != null && !this.idLogSetor.equals(other.idLogSetor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ged.cinbesa.entidade.TblLogSetor[ idLogSetor=" + idLogSetor + " ]";
    }
    
}
