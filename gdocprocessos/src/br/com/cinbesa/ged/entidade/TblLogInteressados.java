/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_log_interessados")
@NamedQueries({
    @NamedQuery(name = "TblLogInteressados.findAll", query = "SELECT t FROM TblLogInteressados t"),
    @NamedQuery(name = "TblLogInteressados.findByIdLogInteressados", query = "SELECT t FROM TblLogInteressados t WHERE t.idLogInteressados = :idLogInteressados"),
    @NamedQuery(name = "TblLogInteressados.findByNrCpfCnpj", query = "SELECT t FROM TblLogInteressados t WHERE t.nrCpfCnpj = :nrCpfCnpj"),
    @NamedQuery(name = "TblLogInteressados.findByNmInteressado", query = "SELECT t FROM TblLogInteressados t WHERE t.nmInteressado = :nmInteressado"),
    @NamedQuery(name = "TblLogInteressados.findByNrMatricula", query = "SELECT t FROM TblLogInteressados t WHERE t.nrMatricula = :nrMatricula"),
    @NamedQuery(name = "TblLogInteressados.findByDsEndereco", query = "SELECT t FROM TblLogInteressados t WHERE t.dsEndereco = :dsEndereco"),
    @NamedQuery(name = "TblLogInteressados.findByDsEmail", query = "SELECT t FROM TblLogInteressados t WHERE t.dsEmail = :dsEmail"),
    @NamedQuery(name = "TblLogInteressados.findByVlSenha", query = "SELECT t FROM TblLogInteressados t WHERE t.vlSenha = :vlSenha"),
    @NamedQuery(name = "TblLogInteressados.findByNrEndereco", query = "SELECT t FROM TblLogInteressados t WHERE t.nrEndereco = :nrEndereco"),
    @NamedQuery(name = "TblLogInteressados.findByDsComplemento", query = "SELECT t FROM TblLogInteressados t WHERE t.dsComplemento = :dsComplemento"),
    @NamedQuery(name = "TblLogInteressados.findByNrCep", query = "SELECT t FROM TblLogInteressados t WHERE t.nrCep = :nrCep"),
    @NamedQuery(name = "TblLogInteressados.findByDsBairro", query = "SELECT t FROM TblLogInteressados t WHERE t.dsBairro = :dsBairro"),
    @NamedQuery(name = "TblLogInteressados.findByNmCidade", query = "SELECT t FROM TblLogInteressados t WHERE t.nmCidade = :nmCidade"),
    @NamedQuery(name = "TblLogInteressados.findByDsUf", query = "SELECT t FROM TblLogInteressados t WHERE t.dsUf = :dsUf"),
    @NamedQuery(name = "TblLogInteressados.findByCdOperacao", query = "SELECT t FROM TblLogInteressados t WHERE t.cdOperacao = :cdOperacao"),
    @NamedQuery(name = "TblLogInteressados.findByDtOperacao", query = "SELECT t FROM TblLogInteressados t WHERE t.dtOperacao = :dtOperacao"),
    @NamedQuery(name = "TblLogInteressados.findByIdUsuarioOperacao", query = "SELECT t FROM TblLogInteressados t WHERE t.idUsuarioOperacao = :idUsuarioOperacao")})
public class TblLogInteressados implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_log_interessados")
    private Integer idLogInteressados;
    @Basic(optional = false)
    @Column(name = "nr_cpf_cnpj")
    private String nrCpfCnpj;
    @Basic(optional = false)
    @Column(name = "nm_interessado")
    private String nmInteressado;
    @Column(name = "nr_matricula")
    private String nrMatricula;
    @Basic(optional = false)
    @Column(name = "ds_endereco")
    private String dsEndereco;
    @Column(name = "ds_email")
    private String dsEmail;
    @Column(name = "vl_senha")
    private String vlSenha;
    @Column(name = "nr_endereco")
    private String nrEndereco;
    @Column(name = "ds_complemento")
    private String dsComplemento;
    @Column(name = "nr_cep")
    private String nrCep;
    @Column(name = "ds_bairro")
    private String dsBairro;
    @Column(name = "nm_cidade")
    private String nmCidade;
    @Column(name = "ds_uf")
    private String dsUf;
    @Column(name = "cd_operacao")
    private Character cdOperacao;
    @Column(name = "dt_operacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtOperacao;
    @Column(name = "id_usuario_operacao")
    private Integer idUsuarioOperacao;
    @JoinColumn(name = "id_interessado", referencedColumnName = "id_interessado")
    @ManyToOne(optional = false)
    private TblInteressados idInteressado;

    public TblLogInteressados() {
    }

    public TblLogInteressados(Integer idLogInteressados) {
        this.idLogInteressados = idLogInteressados;
    }

    public TblLogInteressados(Integer idLogInteressados, String nrCpfCnpj, String nmInteressado, String dsEndereco) {
        this.idLogInteressados = idLogInteressados;
        this.nrCpfCnpj = nrCpfCnpj;
        this.nmInteressado = nmInteressado;
        this.dsEndereco = dsEndereco;
    }

    public Integer getIdLogInteressados() {
        return idLogInteressados;
    }

    public void setIdLogInteressados(Integer idLogInteressados) {
        this.idLogInteressados = idLogInteressados;
    }

    public String getNrCpfCnpj() {
        return nrCpfCnpj;
    }

    public void setNrCpfCnpj(String nrCpfCnpj) {
        this.nrCpfCnpj = nrCpfCnpj;
    }

    public String getNmInteressado() {
        return nmInteressado;
    }

    public void setNmInteressado(String nmInteressado) {
        this.nmInteressado = nmInteressado;
    }

    public String getNrMatricula() {
        return nrMatricula;
    }

    public void setNrMatricula(String nrMatricula) {
        this.nrMatricula = nrMatricula;
    }

    public String getDsEndereco() {
        return dsEndereco;
    }

    public void setDsEndereco(String dsEndereco) {
        this.dsEndereco = dsEndereco;
    }

    public String getDsEmail() {
        return dsEmail;
    }

    public void setDsEmail(String dsEmail) {
        this.dsEmail = dsEmail;
    }

    public String getVlSenha() {
        return vlSenha;
    }

    public void setVlSenha(String vlSenha) {
        this.vlSenha = vlSenha;
    }

    public String getNrEndereco() {
        return nrEndereco;
    }

    public void setNrEndereco(String nrEndereco) {
        this.nrEndereco = nrEndereco;
    }

    public String getDsComplemento() {
        return dsComplemento;
    }

    public void setDsComplemento(String dsComplemento) {
        this.dsComplemento = dsComplemento;
    }

    public String getNrCep() {
        return nrCep;
    }

    public void setNrCep(String nrCep) {
        this.nrCep = nrCep;
    }

    public String getDsBairro() {
        return dsBairro;
    }

    public void setDsBairro(String dsBairro) {
        this.dsBairro = dsBairro;
    }

    public String getNmCidade() {
        return nmCidade;
    }

    public void setNmCidade(String nmCidade) {
        this.nmCidade = nmCidade;
    }

    public String getDsUf() {
        return dsUf;
    }

    public void setDsUf(String dsUf) {
        this.dsUf = dsUf;
    }

    public Character getCdOperacao() {
        return cdOperacao;
    }

    public void setCdOperacao(Character cdOperacao) {
        this.cdOperacao = cdOperacao;
    }

    public Date getDtOperacao() {
        return dtOperacao;
    }

    public void setDtOperacao(Date dtOperacao) {
        this.dtOperacao = dtOperacao;
    }

    public Integer getIdUsuarioOperacao() {
        return idUsuarioOperacao;
    }

    public void setIdUsuarioOperacao(Integer idUsuarioOperacao) {
        this.idUsuarioOperacao = idUsuarioOperacao;
    }

    public TblInteressados getIdInteressado() {
        return idInteressado;
    }

    public void setIdInteressado(TblInteressados idInteressado) {
        this.idInteressado = idInteressado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLogInteressados != null ? idLogInteressados.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblLogInteressados)) {
            return false;
        }
        TblLogInteressados other = (TblLogInteressados) object;
        if ((this.idLogInteressados == null && other.idLogInteressados != null) || (this.idLogInteressados != null && !this.idLogInteressados.equals(other.idLogInteressados))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ged.cinbesa.entidade.TblLogInteressados[ idLogInteressados=" + idLogInteressados + " ]";
    }
    
}
