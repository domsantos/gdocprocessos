/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_aplicacoes")
@NamedQueries({
    @NamedQuery(name = "TblAplicacoes.findAll", query = "SELECT t FROM TblAplicacoes t"),
    @NamedQuery(name = "TblAplicacoes.findByCdAplicacao", query = "SELECT t FROM TblAplicacoes t WHERE t.cdAplicacao = :cdAplicacao"),
    @NamedQuery(name = "TblAplicacoes.findByDsAplicacao", query = "SELECT t FROM TblAplicacoes t WHERE t.dsAplicacao = :dsAplicacao")})
public class TblAplicacoes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "cd_aplicacao")
    private Integer cdAplicacao;
    @Basic(optional = false)
    @Column(name = "ds_aplicacao")
    private String dsAplicacao;
    @JoinTable(name = "tbl_perfil_aplicacoes", joinColumns = {
        @JoinColumn(name = "cd_aplicacao", referencedColumnName = "cd_aplicacao")}, inverseJoinColumns = {
        @JoinColumn(name = "cd_perfil", referencedColumnName = "cd_perfil")})
    @ManyToMany
    private List<TblPerfil> tblPerfilList;

    public TblAplicacoes() {
    }

    public TblAplicacoes(Integer cdAplicacao) {
        this.cdAplicacao = cdAplicacao;
    }

    public TblAplicacoes(Integer cdAplicacao, String dsAplicacao) {
        this.cdAplicacao = cdAplicacao;
        this.dsAplicacao = dsAplicacao;
    }

    public Integer getCdAplicacao() {
        return cdAplicacao;
    }

    public void setCdAplicacao(Integer cdAplicacao) {
        this.cdAplicacao = cdAplicacao;
    }

    public String getDsAplicacao() {
        return dsAplicacao;
    }

    public void setDsAplicacao(String dsAplicacao) {
        this.dsAplicacao = dsAplicacao;
    }

    public List<TblPerfil> getTblPerfilList() {
        return tblPerfilList;
    }

    public void setTblPerfilList(List<TblPerfil> tblPerfilList) {
        this.tblPerfilList = tblPerfilList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cdAplicacao != null ? cdAplicacao.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblAplicacoes)) {
            return false;
        }
        TblAplicacoes other = (TblAplicacoes) object;
        if ((this.cdAplicacao == null && other.cdAplicacao != null) || (this.cdAplicacao != null && !this.cdAplicacao.equals(other.cdAplicacao))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.entidades.TblAplicacoes[ cdAplicacao=" + cdAplicacao + " ]";
    }
    
}
