/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_tributo")
@NamedQueries({
    @NamedQuery(name = "TblTributo.findAll", query = "SELECT t FROM TblTributo t"),
    @NamedQuery(name = "TblTributo.findByIdTributo", query = "SELECT t FROM TblTributo t WHERE t.idTributo = :idTributo"),
    @NamedQuery(name = "TblTributo.findByCdTributo", query = "SELECT t FROM TblTributo t WHERE t.cdTributo = :cdTributo"),
    @NamedQuery(name = "TblTributo.findByNmTributo", query = "SELECT t FROM TblTributo t WHERE t.nmTributo = :nmTributo"),
    @NamedQuery(name = "TblTributo.findByDsTributo", query = "SELECT t FROM TblTributo t WHERE t.dsTributo = :dsTributo")})
public class TblTributo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_tributo")
    private Integer idTributo;
    @Basic(optional = false)
    @Column(name = "cd_tributo")
    private String cdTributo;
    @Basic(optional = false)
    @Column(name = "nm_tributo")
    private String nmTributo;
    @Basic(optional = false)
    @Column(name = "ds_tributo")
    private String dsTributo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTributo")
    private List<TblUsoatividade> tblUsoatividadeList;

    public TblTributo() {
    }

    public TblTributo(Integer idTributo) {
        this.idTributo = idTributo;
    }

    public TblTributo(Integer idTributo, String cdTributo, String nmTributo, String dsTributo) {
        this.idTributo = idTributo;
        this.cdTributo = cdTributo;
        this.nmTributo = nmTributo;
        this.dsTributo = dsTributo;
    }

    public Integer getIdTributo() {
        return idTributo;
    }

    public void setIdTributo(Integer idTributo) {
        this.idTributo = idTributo;
    }

    public String getCdTributo() {
        return cdTributo;
    }

    public void setCdTributo(String cdTributo) {
        this.cdTributo = cdTributo;
    }

    public String getNmTributo() {
        return nmTributo;
    }

    public void setNmTributo(String nmTributo) {
        this.nmTributo = nmTributo;
    }

    public String getDsTributo() {
        return dsTributo;
    }

    public void setDsTributo(String dsTributo) {
        this.dsTributo = dsTributo;
    }

    public List<TblUsoatividade> getTblUsoatividadeList() {
        return tblUsoatividadeList;
    }

    public void setTblUsoatividadeList(List<TblUsoatividade> tblUsoatividadeList) {
        this.tblUsoatividadeList = tblUsoatividadeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTributo != null ? idTributo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblTributo)) {
            return false;
        }
        TblTributo other = (TblTributo) object;
        if ((this.idTributo == null && other.idTributo != null) || (this.idTributo != null && !this.idTributo.equals(other.idTributo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ged.cinbesa.entidade.TblTributo[ idTributo=" + idTributo + " ]";
    }
    
}
