/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cinbesa.ged.entidade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author fernandosantos
 */
@Entity
@Table(name = "tbl_dam")
public class TblDam implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_dam")
    private Integer idDam;
    @Basic(optional = false)
    @Column(name = "nr_parcela")
    private int nrParcela;
    @Column(name = "nr_guia")
    private String nrGuia;
    @Column(name = "nr_ordem")
    private Integer nrOrdem;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "vl_dam")
    private BigDecimal vlDam;
    @Column(name = "dt_vencimento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtVencimento;
    @Column(name = "dt_emissao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtEmissao;
    @Column(name = "st_situacao")
    private char stSituacao;
    @Basic(optional = false)
    @Column(name = "cd_barras")
    private String cdBarras;
    @Column(name = "cd_linha_digitavel")
    private String cdLinhaDigitavel;
    @Column(name = "dt_pagamento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtPagamento;
    @Column(name = "vl_ufir")
    private BigDecimal vlUfir;
    @JoinColumn(name = "cd_valor_documento", referencedColumnName = "cd_valor_documento")
    @ManyToOne(optional = false)
    private TblValorDocumento cdValorDocumento;
    @JoinColumn(name = "cd_usoatividade", referencedColumnName = "cd_usoatividade")
    @ManyToOne(optional = false)
    private TblUsoatividade cdUsoatividade;
    @JoinColumn(name = "id_processo", referencedColumnName = "id_processo")
    @ManyToOne(optional = false)
    private TblProcessos idProcesso;

    public TblDam() {
    }

    public TblDam(Integer idDam) {
        this.idDam = idDam;
    }

    public TblDam(Integer idDam, int nrParcela, Date dtTramite) {
        this.idDam = idDam;
        this.nrParcela = nrParcela;
    
    }

    public Integer getIdDam() {
        return idDam;
    }

    public void setIdDam(Integer idDam) {
        this.idDam = idDam;
    }


    public int getNrParcela() {
        return nrParcela;
    }

    public void setNrParcela(int nrParcela) {
        this.nrParcela = nrParcela;
    }

    public String getNrGuia() {
        return nrGuia;
    }

    public void setNrGuia(String nrGuia) {
        this.nrGuia = nrGuia;
    }

    public Integer getNrOrdem() {
        return nrOrdem;
    }

    public void setNrOrdem(Integer nrOrdem) {
        this.nrOrdem = nrOrdem;
    }

    public BigDecimal getVlDam() {
        return vlDam;
    }

    public void setVlDam(BigDecimal vlDam) {
        this.vlDam = vlDam;
    }

    public Date getDtVencimento() {
        return dtVencimento;
    }

    public void setDtVencimento(Date dtVencimento) {
        this.dtVencimento = dtVencimento;
    }

    public Date getDtEmissao() {
        return dtEmissao;
    }

    public void setDtEmissao(Date dtEmissao) {
        this.dtEmissao = dtEmissao;
    }

    public char getStSituacao() {
        return stSituacao;
    }

    public void setStSituacao(char stSituacao) {
        this.stSituacao = stSituacao;
    }

    public String getCdBarras() {
        return cdBarras;
    }

    public void setCdBarras(String cdBarras) {
        this.cdBarras = cdBarras;
    }

    public String getCdLinhaDigitavel() {
        return cdLinhaDigitavel;
    }

    public void setCdLinhaDigitavel(String cdLinhaDigitavel) {
        this.cdLinhaDigitavel = cdLinhaDigitavel;
    }

    public Date getDtPagamento() {
        return dtPagamento;
    }

    public void setDtPagamento(Date dtPagamento) {
        this.dtPagamento = dtPagamento;
    }

    public BigDecimal getVlUfir() {
        return vlUfir;
    }

    public void setVlUfir(BigDecimal vlUfir) {
        this.vlUfir = vlUfir;
    }

    public TblUsoatividade getCdUsoatividade() {
        return cdUsoatividade;
    }

    public void setCdUsoatividade(TblUsoatividade cdUsoatividade) {
        this.cdUsoatividade = cdUsoatividade;
    }

    public TblProcessos getIdProcesso() {
        return idProcesso;
    }

    public void setIdProcesso(TblProcessos idProcesso) {
        this.idProcesso = idProcesso;
    }
    
    public TblValorDocumento getCdValorDocumento() {
        return cdValorDocumento;
    }

    public void setCdValorDocumento(TblValorDocumento cdValorDocumento) {
        this.cdValorDocumento = cdValorDocumento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDam != null ? idDam.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof TblDam)) {
            return false;
        }
        TblDam other = (TblDam) object;
        if ((this.idDam == null && other.idDam != null) || (this.idDam != null && !this.idDam.equals(other.idDam))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.ged.cinbesa.entidade.TblDam[ idDam=" + idDam + " ]";
    }
    
}