package br.com.cinbesa.ged.util;

import java.math.BigDecimal;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;



public class GuiaUtil {

	public static String INSCRICAO_DAM = "888888";
	public static String COD_BANCO = "816";
	public static String COD_CONTRATO_PMB = "0511";

	public static Date geraDataVencimento(int nrparcela, int diasparavencer) {
		int parcelareal = nrparcela - 1;
		Calendar dataSis = Calendar.getInstance();
		Calendar data = Calendar.getInstance();
		data.set(dataSis.get(Calendar.YEAR), dataSis.get(Calendar.MONTH)
				+ parcelareal, dataSis.get(Calendar.DAY_OF_MONTH)
				+ diasparavencer);
		return new Date(data.getTimeInMillis());
	}

	public static boolean VerificaDataVencGuia(Date dataVenc) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		if (Calendar.getInstance().get(Calendar.YEAR) != Integer.parseInt(sdf
				.format(dataVenc))) {
			return false;
		} else {
			return true;
		}

	}

	public static String maskCpfCnpj(String cpfcnpj) {
		cpfcnpj = cpfcnpj.replace(" ", "");
		if (cpfcnpj.length() == 11) {
			String cpf = cpfcnpj.substring(0, 3) + "."
					+ cpfcnpj.substring(3, 6) + "." + cpfcnpj.substring(6, 9)
					+ "-" + cpfcnpj.substring(9, 11);
			return cpf;
		} else {
			String cnpj = cpfcnpj.substring(0, 2) + "."
					+ cpfcnpj.substring(2, 5) + "." + cpfcnpj.substring(5, 8)
					+ "/" + cpfcnpj.substring(8, 12) + "-"
					+ cpfcnpj.substring(12, 14);
			return cnpj;
		}
	}

	public static String gerarCodigoBarra(BigDecimal valorParcela,
			Date vencimento, String numGuia, int parcela) {
		
		String parte1 = GuiaUtil.COD_BANCO;
		
		String valor = Util.zerosEsquerda(
				valorParcela.toString().replace(".", ""), 11, "0");
		
		String datavencimento = new SimpleDateFormat("yyyyMMdd")
				.format(vencimento);
		
		String numguia = Util.zerosEsquerda(numGuia, 9, "0");
		
		String numparcela = String.format("%02d", parcela);
		
		String parte2 = valor + GuiaUtil.COD_CONTRATO_PMB + datavencimento
				+ numguia + numparcela + GuiaUtil.INSCRICAO_DAM;
		
//		String codBarras = parte1 + String.valueOf(GuiaUtil.calculoMod10(parte1))
//				+ parte2;
		
		String modPartes = GuiaUtil.mod10(parte1+parte2);
		String codBarras = parte1+modPartes+parte2;
		
		return codBarras;
	}

	public static String gerarLinhaDigitavel(String codigoDeBarra) {
		String parte1 = codigoDeBarra.substring(0, 11);
		parte1 += "-" + GuiaUtil.mod10(parte1);
		
		
		
		
		String parte2 = codigoDeBarra.substring(11, 22);
		
		parte2 += "-" + GuiaUtil.mod10(parte2);
		String parte3 = codigoDeBarra.substring(22, 33);
		
		parte3 += "-" + GuiaUtil.mod10(parte3);
		String parte4 = codigoDeBarra.substring(33, 44);
		
		parte4 += "-" + GuiaUtil.mod10(parte4);

		return parte1 + " " + parte2 + " " + parte3 + " " + parte4;
	}

	
	public static String calculoMod10(String sequencia){
		
		int valor = 0;
		int elem = 0;
		int base = 2;
		int modBase = 0;
		for (int x = 0; x < sequencia.length();x++ ) {
	
			elem = Integer.parseInt(String.valueOf(sequencia.charAt(x)));
			
			valor += (elem*base);
			
			base = (base == 2) ? 1 : 2;
			
		}
		
		modBase = 10 - (valor%10);
		
		if(modBase == 10) modBase = 0;
		
		return String.valueOf(modBase);
		
	}
	
	public static String mod10(String seq) {
		int peso = 2;
		int soma = 0;
		char[] toCharArray = seq.toCharArray();
		List<String> l = new ArrayList();
		int w;
		for (int i = toCharArray.length - 1; i >= 0; i--) {
			w = Integer.parseInt(String.valueOf(toCharArray[i]));
			if (i % 2 == 0) {
				int aux = w * peso;
				l = getPartes(String.valueOf(aux), l);
			} else {
				l = getPartes(String.valueOf(w), l);
			}
		}

		for (String valor : l) {
			soma += Integer.parseInt(valor);
		}

		int dac = 0;
		int calcDac = soma % 10;
		if (calcDac > 0) {
			dac = 10 - calcDac;
		}
		return String.valueOf(dac);
	}
	 /*     */
	 /*     */   public static List<String> getPartes(String s, List<String> l)
	 /*     */   {
	 /* 503 */     List aux = new ArrayList(l);
	 /* 504 */     if (s.length() > 1) {
	 /* 505 */       char[] toCharArray = s.toCharArray();
	 /* 506 */       for (int i = 0; i < toCharArray.length; i++)
	 /* 507 */         aux.add(String.valueOf(toCharArray[i]));
	 /*     */     }
	 /*     */     else {
	 /* 510 */       aux.add(s);
	 /*     */     }
	 /* 512 */     return aux;
	 /*     */   } 

	public static int getMod11(String num) {

		// vari�veis de instancia
		int soma = 0;
		int resto = 0;
		int dv = 0;
		String[] numeros = new String[num.length() + 1];
		int multiplicador = 2;

		for (int i = num.length(); i > 0; i--) {
			// Multiplica da direita pra esquerda, incrementando o multiplicador
			// de 2 a 9
			// Caso o multiplicador seja maior que 9 o mesmo recome�a em 2
			if (multiplicador > 9) {
				// pega cada numero isoladamente
				multiplicador = 2;
				numeros[i] = String.valueOf(Integer.valueOf(num.substring(
						i - 1, i)) * multiplicador);
				multiplicador++;
			} else {
				numeros[i] = String.valueOf(Integer.valueOf(num.substring(
						i - 1, i)) * multiplicador);
				multiplicador++;
			}
		}

		// Realiza a soma de todos os elementos do array e calcula o digito
		// verificador
		// na base 11 de acordo com a regra.
		for (int i = numeros.length; i > 0; i--) {
			if (numeros[i - 1] != null) {
				soma += Integer.valueOf(numeros[i - 1]);
			}
		}
		resto = soma % 11;
		dv = 11 - resto;

		// retorna o digito verificador
		return dv;
	}
}
