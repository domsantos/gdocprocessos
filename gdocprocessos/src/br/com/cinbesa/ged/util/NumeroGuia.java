package br.com.cinbesa.ged.util;

import br.com.caelum.vraptor.ioc.Component;

import br.com.cinbesa.ged.dao.UsoatividadeDAO;
import br.com.cinbesa.ged.entidade.TblUsoatividade;

@Component
public class NumeroGuia {

	private UsoatividadeDAO usoatividadeDAO;
	
	public NumeroGuia(UsoatividadeDAO usoatividadeDAO) {
		this.usoatividadeDAO = usoatividadeDAO;
	}
	
	
	public String geraNumeroGuia(TblUsoatividade usoatividade){
		int sequencial = atualizaSequencial(usoatividade);
		String guia ="";
		if(usoatividade.getStTributoExclusivo().equals(ListaApp.SITUACAOTRIBUTOEXCLUSIVOSIM)){
			if(usoatividade.getCdDesmembramento().equals("0")){
				String codTributo = usoatividade.getIdTributo().getCdTributo();
				guia = codTributo;
				guia += Util.zerosEsquerda(String.valueOf(sequencial), 7, "0");
			}else{
				String codTributo = usoatividade.getIdTributo().getCdTributo();
				guia = codTributo;
				
				String _codigoDesmembramento = null;
				if(usoatividade.getCdDesmembramento().length() > 1) {
					_codigoDesmembramento = usoatividade.getCdDesmembramento();
				} else {
					_codigoDesmembramento = Util.zerosEsquerda(usoatividade.getCdDesmembramento(), 2, "0");
				}
				
				guia += _codigoDesmembramento + Util.zerosEsquerda(String.valueOf(sequencial), 5, "0");
			}
		}else{
			String codTributo = usoatividade.getIdTributo().getCdTributo();
			guia = codTributo;
			guia += usoatividade.getIdEntidade().getCdEntidade() + Util.zerosEsquerda(String.valueOf(sequencial), 5, "0");
		}
		return guia;
	}
	
	public Integer atualizaSequencial(TblUsoatividade usoatividade){
		TblUsoatividade uso = usoatividadeDAO.find(TblUsoatividade.class, usoatividade.getCdUsoatividade());
		if(uso.getNrSequencia() == null){
			uso.setNrSequencia(1);
		}else{
			uso.setNrSequencia(uso.getNrSequencia()+1);
		}
		uso = usoatividadeDAO.atualizarUsoAtividade(uso);
		return uso.getNrSequencia();
	}
	
}
