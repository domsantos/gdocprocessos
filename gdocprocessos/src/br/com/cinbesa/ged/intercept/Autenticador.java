package br.com.cinbesa.ged.intercept;

import java.lang.annotation.Annotation;

import br.com.caelum.vraptor.InterceptionException;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.resource.ResourceMethod;

import br.com.cinbesa.ged.control.HomeController;
import br.com.cinbesa.ged.control.IndexController;
import br.com.cinbesa.ged.util.ListaApp;
import br.com.cinbesa.ged.util.Util;

@Intercepts
public class Autenticador implements Interceptor {

	private final SessaoUsuario sessao;
	private final Result result;
	

	public Autenticador(SessaoUsuario sessao, Result result) {
		this.sessao = sessao;
		this.result = result;
	}

	public boolean accepts(ResourceMethod arg0) {
		return arg0.containsAnnotation(Restrito.class);
	}

	public void intercept(InterceptorStack stack, ResourceMethod method,
			Object resourceInstance) throws InterceptionException {
		if (this.sessao == null) {
			result.redirectTo(IndexController.class).sessaoexpirada();
		}
		if (!this.sessao.isLogado()) {
			result.redirectTo(IndexController.class).sessaoexpirada();
		} else {
			Annotation a = method.getMethod().getAnnotation(Restrito.class);
			Restrito r = (Restrito) a;
			String aplicacao = r.nome();
			if (sessao.getUsuario().getCdNivelAdministrador() != ListaApp.CD_NIVEL_ADMIN_SEM_ADMIN) {
				stack.next(method, resourceInstance);
			} else {
				if (Util.verificaPerfil(sessao.getUsuario().getCdPerfil()
						.getTblAplicacoesList(), aplicacao)) {
					stack.next(method, resourceInstance);
				} else {
					result.redirectTo(HomeController.class).sempermissao();
				}
			}
		}
	}
}
