package br.com.cinbesa.ged.intercept;



import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.SessionScoped;

import br.com.cinbesa.ged.entidade.TblLotacao;

import br.com.cinbesa.ged.entidade.TblUsuario;
import br.com.cinbesa.ged.util.ListaApp;



@Component
@SessionScoped
public class SessaoUsuario {

	private TblUsuario user;
	private TblLotacao lotacaoUser;
	
	public void login(TblUsuario usuario){
		user = usuario;
		for (TblLotacao lotacao : user.getTblLotacaoList()) {
			if(lotacao.getStLotacao() == ListaApp.LOTACAOATIVADA){
				lotacaoUser = lotacao;
			}
		}
	}
	
	public TblUsuario getUsuario(){
		return user;
	}
	
	public TblLotacao getLotacaoUsuario(){
		return lotacaoUser;
	}
	
	public boolean isLogado(){
		if(user == null){
			return false;
		}else{
			return true;
		}
	}
	
	public void sair(){
		user = null;
		lotacaoUser = null;
	}
	
	public TblUsuario getUser() {
		return user;
	}
	
	public TblLotacao getLotacaoUser() {
		return lotacaoUser;
	}
	
}
