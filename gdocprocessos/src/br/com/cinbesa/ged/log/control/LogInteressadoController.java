package br.com.cinbesa.ged.log.control;

import java.util.Date;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.dao.LogInteressadoDAO;
import br.com.cinbesa.ged.entidade.TblInteressados;
import br.com.cinbesa.ged.entidade.TblLogInteressados;
import br.com.cinbesa.ged.intercept.SessaoUsuario;

@Component
public class LogInteressadoController {
	
	private LogInteressadoDAO logInteressadoDAO;
	private SessaoUsuario sessaoUsuario;
	
	
	public LogInteressadoController(LogInteressadoDAO logInteressadoDAO, SessaoUsuario sessaoUsuario) {
		this.logInteressadoDAO = logInteressadoDAO;
		this.sessaoUsuario = sessaoUsuario;
		
	}
	
	public void registrarLog(TblInteressados interessado, char cdOperacao){
		TblLogInteressados tblLogInteressados = new TblLogInteressados();
		tblLogInteressados.setIdInteressado(interessado);
		tblLogInteressados.setNrCpfCnpj(interessado.getNrCpfCnpj());
		tblLogInteressados.setNmInteressado(interessado.getNmInteressado());
		tblLogInteressados.setNrMatricula(interessado.getNrMatricula());
		tblLogInteressados.setDsEndereco(interessado.getDsEndereco());
		tblLogInteressados.setDsEmail(interessado.getDsEmail());
		tblLogInteressados.setVlSenha(interessado.getVlSenha());
		tblLogInteressados.setNrEndereco(interessado.getNrEndereco());
		tblLogInteressados.setDsComplemento(interessado.getDsComplemento());
		tblLogInteressados.setNrCep(interessado.getNrCep());
		tblLogInteressados.setDsBairro(interessado.getDsBairro());
		tblLogInteressados.setNmCidade(interessado.getNmCidade());
		tblLogInteressados.setDsUf(interessado.getDsUf());
		tblLogInteressados.setCdOperacao(cdOperacao);
		tblLogInteressados.setDtOperacao(new Date());
		tblLogInteressados.setIdUsuarioOperacao(sessaoUsuario.getUsuario().getIdUsuario());
		
		logInteressadoDAO.registrar(tblLogInteressados);
		
	
	}
	
	
	
}
