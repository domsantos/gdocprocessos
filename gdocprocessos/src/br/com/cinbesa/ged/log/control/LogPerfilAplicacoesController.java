package br.com.cinbesa.ged.log.control;

import java.util.Date;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.dao.LogPerfilAplicacoesDAO;
import br.com.cinbesa.ged.entidade.TblLogPerfilAplicacoes;
import br.com.cinbesa.ged.intercept.SessaoUsuario;

@Component
public class LogPerfilAplicacoesController {

	private LogPerfilAplicacoesDAO logPerfilAplicacoesDAO;
	private SessaoUsuario sessaoUsuario;
	
	public LogPerfilAplicacoesController(LogPerfilAplicacoesDAO logPerfilAplicacoesDAO, SessaoUsuario sessaoUsuario) {
		this.logPerfilAplicacoesDAO = logPerfilAplicacoesDAO;
		this.sessaoUsuario = sessaoUsuario;
	}
	
	public void registrar(int cdPerfil, int cdAplicacao, char cdOperacao){
		TblLogPerfilAplicacoes tblLogPerfilAplicacoes = new TblLogPerfilAplicacoes();
		tblLogPerfilAplicacoes.setIdUsuarioOperacao(sessaoUsuario.getUsuario().getIdUsuario());
		tblLogPerfilAplicacoes.setCdOperacao(cdOperacao);
		tblLogPerfilAplicacoes.setDtOperacao(new Date());
		tblLogPerfilAplicacoes.setCdPerfil(cdPerfil);
		tblLogPerfilAplicacoes.setCdAplicacao(cdAplicacao);
		
		logPerfilAplicacoesDAO.registrar(tblLogPerfilAplicacoes);
	}
	
}
