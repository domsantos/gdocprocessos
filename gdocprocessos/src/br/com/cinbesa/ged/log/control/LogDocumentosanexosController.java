package br.com.cinbesa.ged.log.control;

import java.util.Date;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.dao.LogDocumentosanexosDAO;
import br.com.cinbesa.ged.entidade.TblDocumentosanexos;
import br.com.cinbesa.ged.entidade.TblLogDocumentosanexos;
import br.com.cinbesa.ged.intercept.SessaoUsuario;


@Component
public class LogDocumentosanexosController {

	private LogDocumentosanexosDAO logDocumentosanexosDAO;
	private SessaoUsuario sessaoUsuario;
	
	public LogDocumentosanexosController(LogDocumentosanexosDAO logDocumentosanexosDAO, SessaoUsuario sessaoUsuario) {
		this.logDocumentosanexosDAO = logDocumentosanexosDAO;
		this.sessaoUsuario = sessaoUsuario;
	}
	
	public void registrar(TblDocumentosanexos tblDocumentosanexos, char cdOperacao){
		TblLogDocumentosanexos logDocumentosanexos = new TblLogDocumentosanexos();
		
		logDocumentosanexos.setCdDocumentoanexo(tblDocumentosanexos);
		logDocumentosanexos.setDtAnexo(tblDocumentosanexos.getDtAnexo());
		logDocumentosanexos.setDsPathArquivo(tblDocumentosanexos.getDsPathArquivo());
		logDocumentosanexos.setNmDocumentoanexo(tblDocumentosanexos.getNmDocumentoanexo());
		logDocumentosanexos.setDtOperacao(new Date());
		logDocumentosanexos.setCdOperacao(cdOperacao);
		logDocumentosanexos.setIdUsuarioOperacao(sessaoUsuario.getUsuario().getIdUsuario());
		
		logDocumentosanexosDAO.registrar(logDocumentosanexos);
	}
}
