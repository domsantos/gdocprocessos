package br.com.cinbesa.ged.log.control;

import java.util.Date;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.dao.LogTipoprocessoDAO;
import br.com.cinbesa.ged.entidade.TblLogTipoprocesso;
import br.com.cinbesa.ged.entidade.TblTipoprocesso;
import br.com.cinbesa.ged.intercept.SessaoUsuario;

@Component
public class LogTipoProcessoController {

	private LogTipoprocessoDAO logTipoprocessoDAO;
	private SessaoUsuario sessaoUsuario;
	
	public LogTipoProcessoController(LogTipoprocessoDAO logTipoprocessoDAO, SessaoUsuario sessaoUsuario) {
		this.logTipoprocessoDAO = logTipoprocessoDAO;
		this.sessaoUsuario = sessaoUsuario;
	}
	
	public void registra(TblTipoprocesso tipoprocesso, char cdOperacao){
		TblLogTipoprocesso logTipoprocesso = new TblLogTipoprocesso();
		logTipoprocesso.setCdTipoprocesso(tipoprocesso);
		logTipoprocesso.setCdOperacao(cdOperacao);
		logTipoprocesso.setDsTipopocesso(tipoprocesso.getDsTipoprocesso());
		logTipoprocesso.setDtOperacao(new Date());
		logTipoprocesso.setIdEntidade(tipoprocesso.getIdEntidade().getIdEntidade());
		logTipoprocesso.setIdUsuarioOperacao(sessaoUsuario.getUsuario().getIdUsuario());
		logTipoprocesso.setNmTipoprocesso(tipoprocesso.getNmTipoprocesso());
		logTipoprocesso.setStAbrirInternet(tipoprocesso.getStAbrirInternet());
		logTipoprocesso.setStConfirmacaoPresencia(tipoprocesso.getStConfirmacaoPresencia());
		
		logTipoprocessoDAO.registrar(logTipoprocesso);
	}
}
