package br.com.cinbesa.ged.log.control;

import java.util.Date;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.dao.LogPerfilDAO;
import br.com.cinbesa.ged.entidade.TblLogPerfil;
import br.com.cinbesa.ged.entidade.TblPerfil;
import br.com.cinbesa.ged.intercept.SessaoUsuario;

@Component
public class LogPerfilController {

	private LogPerfilDAO logPerfilDAO;
	private SessaoUsuario sessaoUsuario;
	
	public LogPerfilController(LogPerfilDAO logPerfilDAO, SessaoUsuario sessaoUsuario) {
		this.logPerfilDAO = logPerfilDAO;
		this.sessaoUsuario = sessaoUsuario;
	}
	
	public void registrar(TblPerfil perfil, char cdOperacao){
		TblLogPerfil logPerfil = new TblLogPerfil();
		logPerfil.setCdPerfil(perfil);
		logPerfil.setDsPerfil(perfil.getDsPerfil());
		logPerfil.setCdOperacao(cdOperacao);
		logPerfil.setDtOperacao(new Date());
		logPerfil.setIdUsuarioOperacao(sessaoUsuario.getUsuario().getIdUsuario());
		
		logPerfilDAO.registrar(logPerfil);
	}
	
}
