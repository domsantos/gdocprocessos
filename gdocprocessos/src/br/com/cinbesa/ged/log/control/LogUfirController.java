package br.com.cinbesa.ged.log.control;

import java.util.Date;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.dao.LogUfirDAO;
import br.com.cinbesa.ged.entidade.TblLogUfir;
import br.com.cinbesa.ged.entidade.TblUfir;
import br.com.cinbesa.ged.intercept.SessaoUsuario;

@Component
public class LogUfirController {

	private LogUfirDAO logUfirDAO;
	private SessaoUsuario sessaoUsuario;
	
	public LogUfirController(LogUfirDAO logUfirDAO, SessaoUsuario sessaoUsuario) {
		this.logUfirDAO = logUfirDAO;
		this.sessaoUsuario = sessaoUsuario;
	}
	
	public void registrar(TblUfir ufir, char cdOperacao){
		TblLogUfir logUfir = new TblLogUfir();
		logUfir.setCdOperacao(cdOperacao);
		logUfir.setDtOperacao(new Date());
		logUfir.setIdUfir(ufir);
		logUfir.setIdUsuarioOperacao(sessaoUsuario.getUsuario().getIdUsuario());
		logUfir.setNrAno(ufir.getNrAno());
		logUfir.setVlUfir(ufir.getVlUfir());
		
		logUfirDAO.registrar(logUfir);
	}
	
}
