package br.com.cinbesa.ged.log.control;

import java.util.Date;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.dao.LogSetorDAO;
import br.com.cinbesa.ged.entidade.TblLogSetor;
import br.com.cinbesa.ged.entidade.TblSetor;
import br.com.cinbesa.ged.intercept.SessaoUsuario;


@Component
public class LogSetorController {

	private LogSetorDAO logSetorDAO;
	private SessaoUsuario sessaoUsuario;
	
	public LogSetorController(LogSetorDAO logSetorDAO, SessaoUsuario sessaoUsuario) {
		this.logSetorDAO = logSetorDAO;
		this.sessaoUsuario = sessaoUsuario;
	}
	
	
	public void registrar(TblSetor setor, char cdOperacao){
		TblLogSetor tblLogSetor = new TblLogSetor();
		tblLogSetor.setCdOperacao(cdOperacao);
		tblLogSetor.setCdSetor(setor);
		tblLogSetor.setDsSetor(setor.getDsSetor());
		tblLogSetor.setDtOperacao(new Date());
		tblLogSetor.setNmSetor(setor.getNmSetor());
		tblLogSetor.setIdEntidade(setor.getIdEntidade().getIdEntidade());
		tblLogSetor.setIdUsuarioOperacao(sessaoUsuario.getUsuario().getIdUsuario());
		
		logSetorDAO.registro(tblLogSetor);
	}
}
