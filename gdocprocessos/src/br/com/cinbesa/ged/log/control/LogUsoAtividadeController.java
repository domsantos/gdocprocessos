package br.com.cinbesa.ged.log.control;

import java.util.Date;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.dao.LogUsoAtividadeDAO;
import br.com.cinbesa.ged.entidade.TblLogUsoatividade;
import br.com.cinbesa.ged.entidade.TblUsoatividade;
import br.com.cinbesa.ged.intercept.SessaoUsuario;

@Component
public class LogUsoAtividadeController {

	private LogUsoAtividadeDAO logUsoAtividadeDAO;
	private SessaoUsuario sessaoUsuario;
	
	public LogUsoAtividadeController(LogUsoAtividadeDAO logUsoAtividadeDAO, SessaoUsuario sessaoUsuario) {
		this.logUsoAtividadeDAO = logUsoAtividadeDAO;
		this.sessaoUsuario = sessaoUsuario;
	}
	
	public void registrar(TblUsoatividade usoatividade, char cdOperacao){
		TblLogUsoatividade log = new TblLogUsoatividade();
		log.setCdDesmembramento(usoatividade.getCdDesmembramento());
		log.setCdUsoatividade(usoatividade);
		log.setDsUsoatividade(usoatividade.getDsUsoatividade());
		log.setIdEntidade(usoatividade.getIdEntidade().getIdEntidade());
		log.setIdTributo(usoatividade.getIdTributo().getIdTributo());
		log.setIdUfir(usoatividade.getIdUfir().getIdUfir());
		log.setNrAnoExercicio(usoatividade.getNrAnoExercicio());
		log.setNrDocumentoContabil(usoatividade.getNrDocumentoContabil());
		log.setNrQtdufir(usoatividade.getNrQtdufir());
		log.setNrSequencia(usoatividade.getNrSequencia());
		log.setStTributoExclusivo(usoatividade.getStTributoExclusivo());
		log.setCdOperacao(cdOperacao);
		log.setDtOperacao(new Date());
		log.setIdUsuarioOperacao(sessaoUsuario.getUsuario().getIdUsuario());
		
		logUsoAtividadeDAO.registrar(log);
	}
	
	
}
