package br.com.cinbesa.ged.log.control;

import java.util.Date;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.dao.LogEntidadeDAO;
import br.com.cinbesa.ged.entidade.Entidade;
import br.com.cinbesa.ged.entidade.TblLogEntidade;
import br.com.cinbesa.ged.intercept.SessaoUsuario;


@Component
public class LogEntidadeController {

	private LogEntidadeDAO logEntidadeDAO;
	private SessaoUsuario sessaoUsuario;
	
	public LogEntidadeController(LogEntidadeDAO logEntidadeDAO, SessaoUsuario sessaoUsuario) {
		this.logEntidadeDAO = logEntidadeDAO;
		this.sessaoUsuario = sessaoUsuario;
	}
	
	public void registrar(Entidade entidade, char cdOperacao){
		TblLogEntidade logEntidade = new TblLogEntidade();
		logEntidade.setIdEntidade(entidade);
		logEntidade.setCdEntidade(entidade.getCdEntidade());
		logEntidade.setDsEntidade(entidade.getDsEntidade());
		logEntidade.setDsEntidadeExtenso(entidade.getDsEntidadeExtenso());
		logEntidade.setCdOperacao(cdOperacao);
		logEntidade.setDtOperacao(new Date());
		logEntidade.setIdUsuarioOperacao(sessaoUsuario.getUsuario().getIdUsuario());
		
		logEntidadeDAO.registrar(logEntidade);
		
	}
	
}
