package br.com.cinbesa.ged.log.control;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.dao.LogIpcaeDAO;
import br.com.cinbesa.ged.entidade.TblIpcae;
import br.com.cinbesa.ged.entidade.TblLogIpcae;
import br.com.cinbesa.ged.intercept.SessaoUsuario;


@Component
public class LogIpcaeController {

	private LogIpcaeDAO logIpcaeDAO;
	private SessaoUsuario sessaoUsuario;
	
	public LogIpcaeController(LogIpcaeDAO logIpcaeDAO, SessaoUsuario sessaoUsuario){
		this.logIpcaeDAO = logIpcaeDAO;
		this.sessaoUsuario = sessaoUsuario;
	}
	
	public void registrar(TblIpcae ipcae, char cdOperacao){
		TblLogIpcae logIpcae = new TblLogIpcae();
		logIpcae.setIdIpcae(ipcae);
		logIpcae.setNrAnoIpcae(ipcae.getNrAnoIpcae());
		logIpcae.setVlIpcae(ipcae.getVlIpcae());
		logIpcae.setCdOperacao(cdOperacao);
		logIpcae.setIdUsuarioOperacao(sessaoUsuario.getUsuario().getIdUsuario());
		
		logIpcaeDAO.registrar(logIpcae);
		
	}
	
}
