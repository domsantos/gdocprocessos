package br.com.cinbesa.ged.log.control;

import java.util.Date;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.dao.LogLotacaoDAO;
import br.com.cinbesa.ged.entidade.TblLogLotacao;
import br.com.cinbesa.ged.entidade.TblLotacao;
import br.com.cinbesa.ged.intercept.SessaoUsuario;

@Component
public class LogLotacaoController {

	private LogLotacaoDAO lotacaoDAO;
	private SessaoUsuario sessaoUsuario;
	
	public LogLotacaoController(LogLotacaoDAO lotacaoDAO, SessaoUsuario sessaoUsuario) {
		this.lotacaoDAO = lotacaoDAO;
		this.sessaoUsuario = sessaoUsuario;
	}
	
	public void registrar(TblLotacao lotacao, char cdOperacao){
		TblLogLotacao logLotacao = new TblLogLotacao();
		logLotacao.setCdLotacao(lotacao);
		logLotacao.setIdUsuario(lotacao.getIdUsuario().getIdUsuario());
		logLotacao.setStLotacao(lotacao.getStLotacao());
		logLotacao.setDtLotacao(lotacao.getDtLotacao());
		logLotacao.setCdOperacao(cdOperacao);
		logLotacao.setDtOperacao(new Date());
		logLotacao.setIdUsuarioOperacao(sessaoUsuario.getUsuario().getIdUsuario());
		lotacaoDAO.registrar(logLotacao);
	}
	
}
