package br.com.cinbesa.ged.log.control;

import java.util.Date;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.dao.LogTributoDAO;
import br.com.cinbesa.ged.entidade.TblLogTributo;
import br.com.cinbesa.ged.entidade.TblTributo;
import br.com.cinbesa.ged.intercept.SessaoUsuario;

@Component
public class LogTributoController {

	private LogTributoDAO logTributoDAO;
	private SessaoUsuario sessaoUsuario;
	
	public LogTributoController(LogTributoDAO logTributoDAO, SessaoUsuario sessaoUsuario) {
		this.logTributoDAO = logTributoDAO;
		this.sessaoUsuario = sessaoUsuario;
	}
	
	public void registrar(TblTributo tributo, char cdOperacao){
		TblLogTributo log = new TblLogTributo();
		log.setCdOperacao(cdOperacao);
		log.setCdTributo(tributo.getCdTributo());
		log.setDsTributo(tributo.getDsTributo());
		log.setDtOperacao(new Date());
		log.setIdTributo(tributo);
		log.setIdUsuarioOperacao(sessaoUsuario.getUsuario().getIdUsuario());
		log.setNmTributo(tributo.getNmTributo());
		
		logTributoDAO.registrar(log);
		
	}
	
}
