package br.com.cinbesa.ged.log.control;

import java.util.Date;

import br.com.caelum.vraptor.ioc.Component;
import br.com.cinbesa.ged.dao.LogUsuarioDAO;
import br.com.cinbesa.ged.entidade.TblLogUsuario;
import br.com.cinbesa.ged.entidade.TblUsuario;
import br.com.cinbesa.ged.intercept.SessaoUsuario;

@Component
public class LogUsuarioController {

	private LogUsuarioDAO logUsuarioDAO;
	private SessaoUsuario sessaoUsuario;
	
	public LogUsuarioController(LogUsuarioDAO logUsuarioDAO, SessaoUsuario sessaoUsuario) {
		this.logUsuarioDAO = logUsuarioDAO;
		this.sessaoUsuario = sessaoUsuario;
	}
	
	public void registrar(TblUsuario usuario, char cdOperacao){
		TblLogUsuario log = new TblLogUsuario();
		log.setCdPerfil(usuario.getIdUsuario());
		log.setIdUsuario(usuario);
		log.setIdUsuarioOperacao(sessaoUsuario.getUsuario().getIdUsuario());
		log.setNmUsuario(usuario.getNmUsuario());
		log.setNrMatricula(usuario.getNrMatricula());
		log.setVlSenha(usuario.getVlSenha());
		log.setCdOperacao(cdOperacao);
		log.setDtOperacao(new Date());
		
		logUsuarioDAO.registrar(log);
		
	}
}
