package br.com.cinbesa.ged.dto;

public class RotaDocumentosDTO {

	private String id;
	private String nome;
	private String situacao;
	
	public static final String SIM = "S";
	public static final String NAO = "N";
	
	
	public RotaDocumentosDTO(String id, String nome, String situacao) {
		this.id = id;
		this.nome = nome;
		this.situacao = situacao;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	
	public String getId() {
		return id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public String getSituacao() {
		return situacao;
	}
	
}
