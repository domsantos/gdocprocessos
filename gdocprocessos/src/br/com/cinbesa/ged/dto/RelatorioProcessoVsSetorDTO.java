package br.com.cinbesa.ged.dto;

import java.math.BigInteger;
import java.util.Date;

public class RelatorioProcessoVsSetorDTO {

	
	private int nrProcesso;
	private short nrAno;
	private Date dtAberturaProcesso;
	private String nmSetor;
	private String nmTipoProcesso;
	
	public RelatorioProcessoVsSetorDTO(BigInteger nrProcesso,
			short nrAno, 
			Date dtAberturaProcesso,
			String nmSetor,
			String nmTipoProcesso){
		this.nrProcesso = nrProcesso.intValue();
		this.nrAno = nrAno;
		this.dtAberturaProcesso = dtAberturaProcesso;
		this.nmSetor = nmSetor;
		this.nmTipoProcesso = nmTipoProcesso;
	}

	public int getNrProcesso() {
		return nrProcesso;
	}

	public void setNrProcesso(BigInteger nrProcesso) {
		this.nrProcesso = nrProcesso.intValue();
	}

	public short getNrAno() {
		return nrAno;
	}

	public void setNrAno(short nrAno) {
		this.nrAno = nrAno;
	}

	public Date getDtAberturaProcesso() {
		return dtAberturaProcesso;
	}

	public void setDtAberturaProcesso(Date dtAberturaProcesso) {
		this.dtAberturaProcesso = dtAberturaProcesso;
	}

	public String getNmSetor() {
		return nmSetor;
	}

	public void setNmSetor(String nmSetor) {
		this.nmSetor = nmSetor;
	}

	public String getNmTipoProcesso() {
		return nmTipoProcesso;
	}

	public void setNmTipoProcesso(String nmTipoProcesso) {
		this.nmTipoProcesso = nmTipoProcesso;
	}
	
	
	
}
