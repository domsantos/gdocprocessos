package br.com.cinbesa.ged.dto;

import java.util.Date;

public class DocAnexoDTO {

	private int cdCodAnexo;
	private Date dtAnexo;
	private String nmDocumentoanexo;
	
	
	
	public DocAnexoDTO(int cdCodAnexo, Date dtAnexo, String nmDocAnexo){
		this.cdCodAnexo = cdCodAnexo;
		this.dtAnexo = dtAnexo;
		this.nmDocumentoanexo = nmDocAnexo;
	}



	public int getCdCodAnexo() {
		return cdCodAnexo;
	}



	public void setCdCodAnexo(int cdCodAnexo) {
		this.cdCodAnexo = cdCodAnexo;
	}



	public Date getDtAnexo() {
		return dtAnexo;
	}



	public void setDtAnexo(Date dtAnexo) {
		this.dtAnexo = dtAnexo;
	}



	public String getNmDocumentoanexo() {
		return nmDocumentoanexo;
	}



	public void setNmDocumentoanexo(String nmDocumentoanexo) {
		this.nmDocumentoanexo = nmDocumentoanexo;
	}
	
	
	
}
