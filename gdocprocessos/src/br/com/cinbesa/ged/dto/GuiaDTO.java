package br.com.cinbesa.ged.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GuiaDTO {

	public static final String GUIA = "guia";
	public static final String USUARIO = "usuario";
	public static final String TRANSACAO = "transacao";
	public static final String SERVIDOR = "servidor";
	public static final String SIGLA_TRIBUTO = "SIGLA-TRIBUTO";
	public static final String INSCRICAO = "INSCRICAO";
	public static final String NRO_DAM = "NRO-DAM";
	public static final String NOME_RAZAO = "NOME-RAZAO";
	public static final String CPF_CNPJ = "CPF-CNPJ";
	public static final String ENDERECO = "ENDERECO";
	public static final String BAIRRO_CIDADE_ESTADO = "BAIRRO-CIDADE-ESTADO";
	public static final String USO_ATIVIDADE = "USO-ATIVIDADE";
	public static final String NRO_PROCESSO = "NRO-PROCESSO";
	public static final String NRO_PARCELAS = "NRO-PARCELAS";
	public static final String RECEITA_DIVIDA_ATIVA_DESC = "RECEITA-DIVIDA-ATIVA-DESC";
	public static final String VALOR_RECEITA_DIVIDA_ATIVA = "VALOR-RECEITA-DIVIDA-ATIVA";
	public static final String JUROS_DIVIDA_ATIVA_DESC = "JUROS-DIVIDA-ATIVA-DESC";
	public static final String VALOR_JUROS_DIVIDA_ATIVA = "VALOR-JUROS-DIVIDA-ATIVA";
	public static final String MULTA_DIVIDA_ATIVA_DESC = "MULTA-DIVIDA-ATIVA-DESC";
	public static final String VALOR_MULTA_DIVIDA_ATIVA = "VALOR-MULTA-DIVIDA-ATIVA";
	public static final String REMISSAO_LEI = "REMISSAO-LEI-7935-98-DESC";
	public static final String VALOR_REMISSAO = "VALOR-REMISSAO";
	public static final String VALOR_PAGAMENTOS_EFETUADOS = "VALOR-PAGAMENTOS-EFETUADOS";
	public static final String TRIBUTO_DEVIDO = "TRIBUTO-DEVIDO";
	public static final String SEQUENCIAL = "SEQUENCIAL";
	public static final String VALOR_HONORARIOS_DESC = "VALOR-HONORARIOS-DESC";
	public static final String VALOR_HONORARIOS = "VALOR-HONORARIOS";
	public static final String REFERENCIAS = "REFERENCIAS";
	public static final String NOTA = "NOTA";
	public static final String EXERCICIOS = "EXERCICIOS";
	public static final String LOGO_MINI = "LOGO-MINI";

	private Map<String, Object> parametros;

	private List<ParcelaGuiaDTO> parcelas;

	public GuiaDTO() {
		parametros = new HashMap<String, Object>();
		parcelas = new ArrayList<ParcelaGuiaDTO>();
	}

	public Map<String, Object> getParametros() {
		return parametros;
	}

	public List<ParcelaGuiaDTO> getParcelas() {
		return parcelas;
	}
	
	public void setParcelas(List<ParcelaGuiaDTO> parcelas) {
		this.parcelas = parcelas;
	}
}
