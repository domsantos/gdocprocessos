package br.com.cinbesa.ged.dto;

public class LocalArquivoDTO {

	private int id;
	private int idPai;
	private String desc;
	private String ordem;
	private int nivel;
	
	public LocalArquivoDTO(int id, int idPai, String desc) {
		this.id = id;
		this.idPai = idPai;
		this.desc = desc;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdPai() {
		return idPai;
	}

	public void setIdPai(int idPai) {
		this.idPai = idPai;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getOrdem() {
		return ordem;
	}

	public void setOrdem(String ordem) {
		this.ordem = ordem;
	}

	public int getNivel() {
		return nivel;
	}

	public void setNivel(int nivel) {
		this.nivel = nivel;
	}
	
	
}
