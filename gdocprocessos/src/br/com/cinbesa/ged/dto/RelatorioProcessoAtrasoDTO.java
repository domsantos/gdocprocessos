package br.com.cinbesa.ged.dto;

import java.math.BigInteger;
import java.util.Date;

public class RelatorioProcessoAtrasoDTO {

	private Long idProcesso;
	private int nrProcesso;
	private short nrAno;
	private Date dtTramite;
	private String nmSetor;
	private Long diasAtraso;

	public RelatorioProcessoAtrasoDTO(){}
	
	public RelatorioProcessoAtrasoDTO(
			Long idProcesso,
			BigInteger nrProcesso,
			short nrAno,
			Date dtTramite,
			String nmSetor,
			Long diasAtraso) {
		this.idProcesso = idProcesso;
		this.nrProcesso = nrProcesso.intValue();
		this.nrAno = nrAno;
		this.dtTramite = dtTramite;
		this.nmSetor = nmSetor;
		this.diasAtraso = diasAtraso;
	}
	
	public Long getIdProcesso() {
		return idProcesso;
	}

	public void setIdProcesso(Long idProcesso) {
		this.idProcesso = idProcesso;
	}

	public int getNrProcesso() {
		return nrProcesso;
	}

	public void setNrProcesso(BigInteger nrProcesso) {
		this.nrProcesso = nrProcesso.intValue();
	}

	public short getNrAno() {
		return nrAno;
	}

	public void setNrAno(short nrAno) {
		this.nrAno = nrAno;
	}

	public Date getDtTramite() {
		return dtTramite;
	}

	public void setDtTramite(Date dtTramite) {
		this.dtTramite = dtTramite;
	}

	public String getNmSetor() {
		return nmSetor;
	}

	public void setNmSetor(String nmSetor) {
		this.nmSetor = nmSetor;
	}

	public Long getDiasAtraso() {
		return diasAtraso;
	}

	public void setDiasAtraso(Long diasAtraso) {
		this.diasAtraso = diasAtraso;
	}
	
	
	
}
