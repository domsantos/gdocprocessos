package br.com.cinbesa.ged.dto;

import br.com.cinbesa.ged.entidade.TblRotas;

public class RotaEfetivadaDTO {

	public static String PASSOU = "PASSOU";
	public static String NAOPASSOU = "N�O PASSOU";
	
	private TblRotas rota;
	private String status;
	
	public RotaEfetivadaDTO(TblRotas rota, String status) {
		this.rota = rota;
		this.status = status;
	}
	
	public void setRota(TblRotas rota) {
		this.rota = rota;
	}
	
	public TblRotas getRota() {
		return rota;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getStatus() {
		return status;
	}
	
}
