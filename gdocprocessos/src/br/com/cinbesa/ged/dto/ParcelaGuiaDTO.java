package br.com.cinbesa.ged.dto;

import java.text.SimpleDateFormat;

import br.com.cinbesa.ged.entidade.TblDam;

public class ParcelaGuiaDTO {

	
	private TblDam parcela;

	private String PARCELA;
	private String APAGAR;
	private String DATAVENC;
	private String LINHADIGITAVEL;
	private String CODIGOBARRAS;
	
	public ParcelaGuiaDTO(TblDam dam) {
		parcela = dam;
	}
	
	public String getPARCELA() {
		PARCELA = String.format("%02d", parcela.getNrParcela());
//		PARCELA = String.valueOf();
		return PARCELA;
	}
	
	
	public String getAPAGAR() {
		APAGAR = parcela.getVlDam().toString().replace(".", ",");
		return APAGAR;
	}
	
	public String getDATAVENC() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		DATAVENC = sdf.format(parcela.getDtVencimento());
		return DATAVENC;
	}
	
	public String getLINHADIGITAVEL() {
		LINHADIGITAVEL = parcela.getCdLinhaDigitavel();
		return LINHADIGITAVEL;
	}
	
	public String getCODIGOBARRAS() {
		CODIGOBARRAS = parcela.getCdBarras();
		return CODIGOBARRAS;
	}
}
